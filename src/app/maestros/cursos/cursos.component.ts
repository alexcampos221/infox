import { Component, OnInit, ElementRef,AfterViewInit, ViewChild, OnDestroy} from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormControl, FormControlName, FormGroup, Validators } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { DatePipe } from '@angular/common';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { Utils } from 'src/app/shared/utils';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatriculaService } from 'src/app/shared/services/registros/matricula.service';
import { CursoFormComponent } from '../dialogs/curso-form/curso-form.component';
import { Subscription } from 'rxjs';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-cursos',
  templateUrl: './cursos.component.html',
  styleUrls: ['./cursos.component.sass'],
  providers: [DatePipe],
})
export class CursosComponent implements OnInit,OnDestroy {

  programas_totales : any[] = [];
  arreglo;
  dataSource_cursos_totales : MatTableDataSource<any>;
  programas_filtrados : any[] = [];

  //atributos para la tabla material
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;

  displayedColumns = [
    'codigo',
    'programa',
    'curso',
    'acciones',
  ];

  programas_procesados : any [] = [];

  filtro_control : FormControl;
  filtro$ : Subscription;

  constructor(
              private fb: FormBuilder,
              private adpatadorFecha: DateAdapter<Date>,
              private pipeFecha : DatePipe,
              public dialog: MatDialog,
              private snackBar: MatSnackBar,
              private ruteador: Router,
              private servicios:MatriculaService,
              private spinner:NgxSpinnerService,
              private loading:LoadingService
  ) { }
  ngOnDestroy(): void {
    if(this.filtro$){
      this.filtro$.unsubscribe();
    }
  }

  ngOnInit(): void {
    this.dataSource_cursos_totales = new MatTableDataSource();
    this.traerProgramas();
    this.filtro_control = new FormControl(false);
    this.pendienteFiltro();
  }

  private pendienteFiltro(){
    this.filtro$ = this.filtro_control.valueChanges.subscribe(resp=>{
      console.log(resp);
      if(resp){
        this.quitarFiltro();
      } 
      else{
        this.filtrarInformacion();
      }
    });
  }

  private traerProgramas(){

    this.servicios.listaProgramaCursos().subscribe((resp:any) =>{

      this.programas_totales = resp.result; 
      this.procesarProgramas();
      this.dataSource_cursos_totales.data = this.programas_procesados; 
      console.log("cursos ",resp);
    },(error)=>{
      //this.eventosServicio.hideLoading();
      console.info("error cursos: ",error);
    });
  }

  ngAfterViewInit(): void {
    this.paginator._intl.itemsPerPageLabel = 'Registros Por Página';
    this.dataSource_cursos_totales.paginator = this.paginator;
    this.dataSource_cursos_totales.sort = this.sort;
  }

  applyFilter(filterValue:string){
    this.dataSource_cursos_totales.filter = filterValue.trim().toLowerCase();
  }

  private procesarProgramas(){
    let arreglo_salida :any[] = [];
    //console.log(this.programas_cursos_lista); 
    this.programas_totales.forEach(elemento=>{
      elemento.pro_cursos.forEach(curso => {
        let objeto = {
          pro_id : elemento.pro_id,
          cur_id : curso.cur_id,
          nombre : curso.cur_nombre,
          tipo :'C',
          programa_nombre : elemento.pro_nombre
        }
        arreglo_salida.push(objeto);
      });
    });
    this.programas_procesados = arreglo_salida;
    //console.log(this.programas_procesados);
  }

  nuevoCurso(){
    const dialogRef = this.dialog.open(CursoFormComponent, {
      data: {
        nombre: "agregar",
        curso :  null,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
        if( result==undefined){
          console.log("No se recibe nada");
        }
        else if(result.estado=='EXITO'){
          //this.traerUsuarios();
          console.log("Se ha creado el curso");
        }
        else{
          console.log("ha ocurrido un error");
        }
    });
  }

  editarCurso(row){
    const dialogRef = this.dialog.open(CursoFormComponent, {
      data: {
        nombre: "editar",
        curso : row
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
        if( result==undefined){
          console.log("No se recibe nada");
        }
        else if(result.estado=='EXITO'){
          //this.traerUsuarios();
          console.log("Se ha actualizado el curso");
        }
        else{
          console.log("ha ocurrido un error");
        }
    });
  }

  private quitarFiltro(){
    this.programas_filtrados = this.programas_procesados;
  }

  private filtrarInformacion(){
    this.programas_totales.forEach(elemento =>{
      if(elemento.activo == 'Y'){
        this.programas_filtrados.push(elemento);
      }
    });
  }

  chageStateValue(data){
    if(data==='N') return 'Y';
    else if(data==='Y') return 'N';
    else throw 'error';  
  }

  public chageState(row){
    console.log(row);
    var body = {
      nuevoEstado: this.chageStateValue(row.ban_isactivo),
      // usuarioID: this.usuario.usu_id,
      bancoID:row.ban_id,
      nombre:row.ban_nombre,
    }
    console.log(body)
    this.spinner.show();
    this.loading.loading$.next({opacity:0.5});
    this.servicios.status(body).subscribe((resp:any)=>{
      console.log(resp)
      this.spinner.hide();
    },(error)=>{
      console.log(error);
      this.spinner.hide();
    });
  }


  sortData(sort:Sort){
    const data = this.programas_procesados.slice();
    console.log(this.programas_procesados)
    let arreglo_temporal :any []= [];
    if (!sort.active || sort.direction === '') {
      arreglo_temporal = data;
      this.dataSource_cursos_totales.data = arreglo_temporal;
      return;
    }

    arreglo_temporal = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'codigo': return this.compare(a.pro_id||'', b.pro_id||'', isAsc);
        case 'programa': return this.compare(a.programa_nombre||'', b.programa_nombre||'', isAsc);
        case 'curso': return this.compare(a.nombre||'', b.nombre||'', isAsc);
        default: return 0;
      }
    });
    this.dataSource_cursos_totales.data = arreglo_temporal;
  }
  private  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
}