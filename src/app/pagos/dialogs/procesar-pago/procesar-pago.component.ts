import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { DatePipe } from '@angular/common';
import { UniversidadService } from 'src/app/shared/services/backend/universidad.service';
import { ProgramaService } from 'src/app/shared/services/backend/programa.service';
import { PromocionService } from 'src/app/shared/services/backend/promocion.service';
import { BancoService } from 'src/app/shared/services/backend/banco.service';

@Component({
  selector: 'app-procesar-pago',
  templateUrl: './procesar-pago.component.html',
  styleUrls: ['./procesar-pago.component.sass']
})
export class ProcesarPagoComponent implements OnInit {

  procesarForm : FormGroup;
  informacion : any;

  constructor(private fb : FormBuilder,
              private dialogRef: MatDialogRef<ProcesarPagoComponent>,
              private programaServicio : ProgramaService,
              @Inject(MAT_DIALOG_DATA) public data: any,
              ) { }

  ngOnInit(): void {
    this.crearFormulario();
  }

  private crearFormulario(){
    this.procesarForm = this.fb.group({
      usuario : ['',[Validators.required]],
      clave : ['',[Validators.required]],
      correo : ['',[Validators.required]],
      nombres : ['',[Validators.required]],
      programa : ['',[Validators.required]]
    });
  }

  closeDialog(){
    this.dialogRef.close();
  }

  enviarFormulario(){
    console.log(this.procesarForm.value);
  }
}
