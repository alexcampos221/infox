import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfogramaComponent } from './infograma.component';

describe('InfogramaComponent', () => {
  let component: InfogramaComponent;
  let fixture: ComponentFixture<InfogramaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InfogramaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InfogramaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
