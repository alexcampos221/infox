import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

import * as Global from '../../globals';
import { Utils } from '../../utils';

@Injectable({
  providedIn: 'root'
})
export class PromocionService {

  constructor(public http: HttpClient) { }

  guardarRegistroPromoNew(body){
    const requestOptions = {
      headers: new HttpHeaders({
        //'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_base_infox+ 'guardarRegistroPromoNew',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
}
