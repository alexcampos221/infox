import { Component, OnInit, ElementRef,AfterViewInit, ViewChild, OnDestroy} from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormControl, FormControlName, FormGroup, Validators } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { DatePipe } from '@angular/common';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { Utils } from 'src/app/shared/utils';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ProgramaFormComponent } from '../dialogs/programa-form/programa-form.component';
import { ProgramaService } from 'src/app/shared/services/backend/programa.service';
import { Subscription } from 'rxjs';
import { AlertaService } from 'src/app/shared/services/general/alerta.service';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ThemeService } from 'ng2-charts';

@Component({
  selector: 'app-programas',
  templateUrl: './programas.component.html',
  styleUrls: ['./programas.component.sass'],
  providers: [DatePipe]
})
export class ProgramasComponent implements OnInit, OnDestroy {

  programas_totales : any[] = [];
  programas_filtrados : any[] = [];
  arreglo;
  dataSource_programas_totales : MatTableDataSource<any>;

  //atributos para la tabla material
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;

  displayedColumns = [
    // 'codigo',
    'expandir',
    'programa',
    'descripcion',
    'duracion',
    'tipo',
    'estado',
    'acciones',
  ];

  programas_procesados : any [] = [];

  filtro_control : FormControl;
  filtro$ : Subscription;


  constructor(
              private fb: FormBuilder,
              private adpatadorFecha: DateAdapter<Date>,
              private pipeFecha : DatePipe,
              public dialog: MatDialog,
              private snackBar: MatSnackBar,
              private ruteador: Router,
              private programaServicio: ProgramaService,
              private alertaServicio:AlertaService,
              private loadingService: LoadingService,
              private spinner: NgxSpinnerService, 
  ) { }
  ngOnDestroy(): void {
    if(this.filtro$){
      console.log("unsubscribe");
      this.filtro$.unsubscribe();
    }
  }

  ngOnInit(): void {
  
    this.dataSource_programas_totales = new MatTableDataSource();
    this.filtro_control = new FormControl(false);
    this.traerProgramas();
    this.pendienteCambios();
  }

  private pendienteCambios(){
    this.filtro$ = this.filtro_control.valueChanges.subscribe(resp=>{
      if(resp){
        this.quitarFiltro();
      }
      else{
        this.filtrarInformacion();
      }
    });
  }

  private quitarFiltro(){
    this.programas_filtrados = this.programas_totales;
    this.dataSource_programas_totales.data = this.programas_filtrados;
  }

  private filtrarInformacion(){
    this.programas_filtrados = [];
    this.programas_totales.forEach(elemento=>{
      if(elemento.pro_activo == 'Y'){
        this.programas_filtrados.push(elemento)
      }
    });
    this.dataSource_programas_totales.data = this.programas_filtrados;
  }

  private traerProgramas(){
    this.spinner.show();
    this.loadingService.loading$.next({opacity:0.5});
    this.programaServicio.listaProgramasAll().subscribe((resp:any) =>{

      this.programas_totales = resp; 
      this.filtrarInformacion();
      
      console.log("programas ",resp);
      this.spinner.hide();
    },(error)=>{
      //this.eventosServicio.hideLoading();
      this.spinner.hide();
      console.info("error programas: ",error);
    });
  }

  ngAfterViewInit(): void {
    this.paginator._intl.itemsPerPageLabel = "Registros por página";
    this.dataSource_programas_totales.paginator = this.paginator;
    this.dataSource_programas_totales.sort = this.sort;
  }

  applyFilter(filterValue:string){
    this.dataSource_programas_totales.filter = filterValue.trim().toLowerCase();
  }

  nuevoPrograma(){
    const dialogRef = this.dialog.open(ProgramaFormComponent, {
      width:'400px',
      data: {
        nombre: "crear",
        programa :null,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
        if( result==undefined){
          console.log("No se recibe nada");
        }
        else if(result.estado=='EXITO'){
          this.alertaServicio.creacionCorrecta();
          this.traerProgramas();
        }
        else{
          console.log("ha ocurrido un error");
        }
    });
  }

  editarPrograma(row){

    const dialogRef = this.dialog.open(ProgramaFormComponent, {
      width:'400px',
      data: {
        nombre: "editar",
        programa :row,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
        if( result==undefined){
          console.log("No se recibe nada");
        }
        else if(result.estado=='EXITO'){
          this.alertaServicio.actualizacionCorrecta();
          this.traerProgramas();
        }
        else{
          console.log("ha ocurrido un error");
        }
    });
  }

  eliminarPrograma(row){
    this.spinner.show()
    this.loadingService.loading$.next({opacity:0.5});
    let salida = {
      programaID: row.pro_id
    }
    console.log(salida)
    this.programaServicio.eliminarPrograma(salida).subscribe(resp => {
      this.spinner.hide()
      if(resp['status'] == 'OK'){
        this.alertaServicio.eliminacionCorrecta();
      }else{
        this.alertaServicio.errorInterno()
      }
    },(error) => {
      this.spinner.hide()
      this.alertaServicio.algoHaIdoMal()
      console.log(error);
      
    })
  }

  cambiarEstado(row){
    let nuevo_est = '';
    let codigo = row.pro_id;
    if(row.pro_activo == 'N'){
      nuevo_est = 'Y';
    }
    else{
      nuevo_est = 'N';
    }
    let body = {
      estadoNuevo : nuevo_est,
      programaID: codigo,
    }

    this.programaServicio.cambiarEstado(body).subscribe(resp=>{
      if(resp['status']=='OK'){
        this.filtro_control.setValue(false);
        this.traerProgramas();
      }
      else{
        //error
      }
    },(error)=>{
      console.log(error);
    });
  }

  arrayProgramasID: any[] = [];
  detalleCurso(row,index){
    if(this.dataSource_programas_totales.filter == ''){
      for(let index = 0; index < this.programas_filtrados.length; index++) {
        this.arrayProgramasID.push(this.programas_filtrados[index].pro_id);      
      }  
    }else{
      for(let index = 0; index < this.dataSource_programas_totales.filteredData.length; index++) {
        this.arrayProgramasID.push(this.dataSource_programas_totales.filteredData[index].pro_id);      
      }     
    }
    let salida = {
      programaID: row.pro_id,
      ventana: 'Programas'
    }
    localStorage.setItem('programaID',row.pro_id);  
    localStorage.setItem('indice',index);    
    localStorage.setItem('txtFiltro',this.dataSource_programas_totales.filter);

    localStorage.setItem('arrayListaPendiente',JSON.stringify(this.arrayProgramasID));
    this.ruteador.navigate(['maestros/detalle-curso'] ,{ state : {data: salida } });   
  }

  sortData(sort:Sort){
    const data = this.programas_filtrados.slice();
    let arreglo_temporal :any []= [];
    if (!sort.active || sort.direction === '') {
      arreglo_temporal = data;
      this.dataSource_programas_totales.data = arreglo_temporal;
      return;
    }

    arreglo_temporal = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'codigo': return this.compare(a.pro_id||'', b.pro_id||'', isAsc);
        case 'programa': return this.compare(a.pro_nombre||'', b.pro_nombre||'', isAsc);
        default: return 0;
      }
    });
    this.dataSource_programas_totales.data = arreglo_temporal;
  }
  private  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
  actualizarLista(){
    this.traerProgramas()
  }
}
