import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatriculaCajaComponent } from './matricula-caja.component';

describe('MatriculaCajaComponent', () => {
  let component: MatriculaCajaComponent;
  let fixture: ComponentFixture<MatriculaCajaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MatriculaCajaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MatriculaCajaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
