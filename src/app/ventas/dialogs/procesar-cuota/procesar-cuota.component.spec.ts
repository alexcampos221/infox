import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcesarCuotaComponent } from './procesar-cuota.component';

describe('ProcesarCuotaComponent', () => {
  let component: ProcesarCuotaComponent;
  let fixture: ComponentFixture<ProcesarCuotaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProcesarCuotaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcesarCuotaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
