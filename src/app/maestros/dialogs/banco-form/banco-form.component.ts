import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { BancoService } from 'src/app/shared/services/backend/banco.service';

@Component({
  selector: 'app-banco-form',
  templateUrl: './banco-form.component.html',
  styleUrls: ['./banco-form.component.sass']
})
export class BancoFormComponent implements OnInit {

  public addCusForm: FormGroup;

  public nombre : string = '';
  editar : boolean = false;

  constructor(private fb: FormBuilder, 
              public dialogRef: MatDialogRef<BancoFormComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private bancoServicio: BancoService) { }

  ngOnInit(): void {
    this.nombre = 'Crear Banco';
    this.crearFormulario();
    console.log(this.data.nombre)
    if(this.data.nombre == 'editar'){
      this.editar = true;
      this.nombre = 'Editar Banco';
      this.crearFormularioEditar();
    }
    else{

      this.crearFormulario();
    }
  }

  private crearFormulario(){    
    this.addCusForm = this.fb.group({
      nombre: ['',[Validators.required, Validators.pattern('[a-zA-Z]+([a-zA-Z ]+)*')]
      ],
    });
  }
  private crearFormularioEditar(){
    let codigo = this.data.banco.ban_id;
    let nombre =  this.data.banco.ban_nombre
    this.addCusForm = this.fb.group({
      codigo : [codigo,[Validators.required]],
      nombre : [nombre,[Validators.required]],
    });
  }

  private crearBanco(){

    let nombre = this.addCusForm.controls.nombre.value;
    let body = {
      nombre : nombre
    }
    console.log(body);
    this.bancoServicio.guardarBanco(body).subscribe(resp => {
      console.log(resp);
      if(resp['status']=='OK'){
        this.dialogRef.close({estado : 'EXITO'})
      }
      else{
        this.dialogRef.close({estado : 'FALLO'});
      }
    },(error)=>{
      console.log(error);
      this.dialogRef.close({estado : 'FALLO'});
    });
  }

  private editarBanco(){
    let codigo = this.addCusForm.controls.codigo.value;
    let nombre = this.addCusForm.controls.nombre.value;

    let body = {
      bancoID : codigo,
      nombre : nombre
    }
    console.log(body);
    
    this.bancoServicio.editarBanco(body).subscribe(resp=>{
      if(resp['status']=='OK'){
        this.dialogRef.close({estado : 'EXITO'})
      }
      else{
        this.dialogRef.close({estado : 'FALLO'});
      }
    },(error)=>{
      console.log(error);
      this.dialogRef.close({estado : 'FALLO'});
    });
    

  } 

  closeDialog(): void {
    this.dialogRef.close();
  }

  enviarFormulario(){
    console.log(this.editar)
    if(this.editar){
      this.editarBanco();
    }
    else{
      this.crearBanco();
    }
  }


}
