import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { Subscription } from 'rxjs';
import { AlertaService } from 'src/app/shared/services/general/alerta.service';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { MatriculaService } from 'src/app/shared/services/registros/matricula.service';
import { BancoFormComponent } from '../dialogs/banco-form/banco-form.component';
import { ProspectosService } from '../../shared/services/backend/prospectos.service';
import { ExcelService } from 'src/app/shared/services/general/excel.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-prospectos',
  templateUrl: './prospectos.component.html',
  styleUrls: ['./prospectos.component.sass'],
  providers:[DatePipe]
})
export class ProspectosComponent implements OnInit {

  prospectos_totales : any[] = [];
  prospectos_filtrados : any[] = [];
  lista_prospectos : any[] = [];
  lista_promociones : any[] = [];

  formdata: FormGroup;
  arreglo;

  date_hoy:Date=new Date()
  dataSource_prospectos_totales : MatTableDataSource<any>;

  //atributos para la tabla material
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;

  displayedColumns = [
    'fecha',
    'promocion',
    'dni',
    'nombre',
    'correo',
    'telefono',
    'universidad',
    'programa',
    'semestre',
    'pais',
    'acciones',
  ];

  filtro_control : FormControl;
  filtro$ : Subscription;

  usuario:any;
  constructor(private fb:FormBuilder,
    public dialog: MatDialog,
              private spinner: NgxSpinnerService,
              private loading:LoadingService,
              private servicios:MatriculaService,
              private alertaServicio : AlertaService,
              private datePipe:DatePipe,  
              private prospectosService: ProspectosService,
              private dateAdapter: DateAdapter<Date>,
              private excel: ExcelService,) { 
                this.dateAdapter.setLocale('es-PE');
                this.usuario = JSON.parse(localStorage.getItem('usu_actual'));  
              }

  ngOnDestroy(): void {
    if(this.filtro$){
      console.log("ubsuscribe")
      this.filtro$.unsubscribe()
    }
  }

  ngOnInit(): void {
    this.crearFormulario();
    this.filtro_control = new FormControl(false);
    this.dataSource_prospectos_totales = new MatTableDataSource();  
    this.f.desde.setValue(new Date(this.date_hoy.getFullYear(),this.date_hoy.getMonth(),1));
    this.f.hasta.setValue(this.date_hoy);  
    this.prepararRegistro();
    
  }

  ngAfterViewInit(): void {
    this.dataSource_prospectos_totales.paginator = this.paginator;
    this.dataSource_prospectos_totales.sort = this.sort;
  }

  crearFormulario(){
    this.formdata =this.fb.group({
      promocion:['0',[]],
      desde:['',[]],
      hasta:['',[]],
    });
  }
  private get f(){
    return this.formdata.controls;
  }
  selectedPromocion(event){
    console.log(event)
  }
  EndDateChange(evento){
    console.log(evento.value);
  }
  private pendienteFiltro(){
    this.filtro$ = this.filtro_control.valueChanges.subscribe(resp=>{
      console.log(resp);
      if(resp){
        this.quitarFiltro();
      } 
      else{
        this.filtrarInformacion();
      }
    });
  }
  prepararRegistro(){
    this.traerListaPromociones();
    this.pendienteFiltro();    
    this.realizarBusqueda();
  }
  traerListaPromociones(){
    this.prospectosService.listarPromociones().subscribe((resp:any)=>{
      console.log(resp);
      this.lista_promociones = resp;
      // function onlyUnique(value, index, self) { 
      //   return self.indexOf(value) === index;
      // }
      // this.lista_prospectos = this.lista_prospectos.filter( onlyUnique );
    })
  }
 
  // private traerProspectos(){
  //   this.spinner.show();
  //   this.loading.loading$.next({opacity:0.5});
  //   this.prospectosService.listarProspectos().subscribe((resp:any) =>{
  //     console.log(resp)
  //     this.prospectos_totales = resp; 
  //     // this.filtrarInformacion();
  //     // this.dataSource_prospectos_totales.data = this.prospectos_filtrados; 
  //     this.dataSource_prospectos_totales.data = this.prospectos_totales
  //     console.log("bancos ",resp);   
  //     this.spinner.hide(); 
  //   },(error)=>{
  //     this.spinner.hide();
  //     console.info("error bancos: ",error);
  //   });
  // }

  applyFilter(filterValue:string){
    this.dataSource_prospectos_totales.filter = filterValue.trim().toLowerCase();
    this.prospectos_totales = this.dataSource_prospectos_totales.filteredData
  }
  
  realizarBusqueda(){
    this.spinner.show();
    this.loading.loading$.next({opacity:0.5});
    let promocion = this.f.promocion.value 
    let salida = {
      desde: this.datePipe.transform(this.f.desde.value,'yyyy-MM-dd'),
      hasta: this.datePipe.transform(this.f.hasta.value,'yyyy-MM-dd'),
      promocionID: Number(promocion.pro_id) || 0,
    }  
    console.log(salida)
    this.prospectosService.listarProspectos(salida).subscribe((resp:any) =>{
      this.prospectos_totales = resp; 
      // this.filtrarInformacion();
      // this.dataSource_prospectos_totales.data = this.prospectos_filtrados; 
      this.dataSource_prospectos_totales.data = this.prospectos_totales
      console.log("prospectos ",resp);   
      this.spinner.hide(); 
    },(error)=>{
      this.spinner.hide();
      console.info("error bancos: ",error);
    });

  }
  nuevoBanco(){
    
    const dialogRef = this.dialog.open(BancoFormComponent, {
      data: {
        nombre: "agregar",
        banco : null
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
        if( result==undefined){
          console.log("No se recibe nada");
        }
        else if(result.estado=='EXITO'){
          // this.traerProspectos();
          this.alertaServicio.creacionCorrecta();

          console.log("Se ha creado el banco");
        }
        else{
          console.log("ha ocurrido un error");
        }
    });
    
  }

  editarBanco(row){
    console.log(row);
    const dialogRef = this.dialog.open(BancoFormComponent, {
      data: {
        nombre: "editar",
        banco : row,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
        if( result==undefined){
          console.log("No se recibe nada");
        }
        else if(result.estado=='EXITO'){
          // this.traerProspectos();
          this.alertaServicio.actualizacionCorrecta();
          console.log("Se ha editado el banco");
        }
        else{
          console.log("ha ocurrido un error");
        }
    });
    
  }

  private filtrarInformacion(){
    this.prospectos_filtrados = [];
    this.prospectos_totales.forEach(elemento=>{
      if(elemento.ban_activo == 'Y'){
        this.prospectos_filtrados.push(elemento)
      }
    });
    this.dataSource_prospectos_totales.data = this.prospectos_filtrados;
  }

  chageStateValue(data){
    if(data==='N') return 'Y';
    else if(data==='Y') return 'N';
    else throw 'error';  
  }

  public chageState(row){
    // console.log(row);
    this.spinner.show();
    this.loading.loading$.next({opacity:0.5});
    let body = {
      estadoNuevo: this.chageStateValue(row.ban_activo),
      bancoID: row.ban_id
    }
    console.log(body)
    
    this.servicios.status(body).subscribe((resp:any)=>{
      console.log(resp)
      // this.traerProspectos();
      this.spinner.hide();
    },(error)=>{
      console.log(error);
      this.spinner.hide();
    });
  }

  private quitarFiltro(){
    this.prospectos_filtrados = this.prospectos_totales;
    this.dataSource_prospectos_totales.data = this.prospectos_filtrados;
  }
  sortData(sort:Sort){
    const data = this.prospectos_totales.slice();
    let arreglo_temporal :any []= [];
    if (!sort.active || sort.direction === '') {
      arreglo_temporal = data;
      this.dataSource_prospectos_totales.data = arreglo_temporal;
      return;
    }

    arreglo_temporal = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {

        case 'fecha': return this.compare(a.pros_fecha||'', b.pros_fecha||'', isAsc);
        case 'promocion': return this.compare(a.promocion_nombre||'', b.promocion_nombre||'', isAsc);
        case 'dni': return this.compare(a.pros_dni||'', b.pros_dni||'', isAsc);
        case 'nombre': return this.compare(a.pros_nombre||'', b.pros_nombre||'', isAsc);
        case 'correo': return this.compare(a.pros_email||'', b.pros_email||'', isAsc);
        case 'telefono': return this.compare(a.pros_telefono||'', b.pros_telefono||'', isAsc);
        case 'universidad': return this.compare(a.univ_nombre||'', b.univ_nombre||'', isAsc);
        case 'programa': return this.compare(a.programa_nombre||'', b.programa_nombre||'', isAsc);
        case 'semestre': return this.compare(a.semestre_nombre||'', b.semestre_nombre||'', isAsc);
        case 'pais': return this.compare(a.pais_nombre||'', b.pais_nombre||'', isAsc);
        default: return 0;
      }
    });
    this.dataSource_prospectos_totales.data = arreglo_temporal;
  }
  private  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  downloadFileExcel(){
    let dataToExport = this.prospectos_totales;
  
    let desde = this.datePipe.transform(this.formdata.controls.desde.value,'dd-MM-yyyy');
    let hasta = this.datePipe.transform(this.formdata.controls.hasta.value,'dd-MM-yyyy');
    console.log(desde)
    console.log(hasta)
    let promocion;
    let tipo_n = this.formdata.controls.promocion.value;
    if(this.formdata.controls.promocion.value == '0'){
      promocion = 'Todos';
    }else{
      promocion = tipo_n.nombre;
    }

    let mapHeaders=new Map([   
      ["Fecha","pros_fecha"],
      ["Promocion","promocion_nombre"],   
      ["DNI","pros_dni"],
      ["Nombre","pros_nombre"],
      ["Correo","pros_email"],
      ["Telefono","pros_telefono"],
      ["Universidad","univ_nombre"],
      ["Programa","programa_nombre"],
      ["Semestre","semestre_nombre"],
      ["Pais","pais_nombre"],
    ]);
    let mapHeadersShow=[
      "Fecha",
      "Promocion", 
      "DNI",
      "Nombre",
      "Correo",
      "Telefono",
      "Universidad",
      "Programa",
      "Semestre",
      "Pais",
    ];

    let fecha = "Prospectos "+ this.datePipe.transform(new Date(),'dd-MM-yyyy');

    let informacion = {
      promocion: promocion,
      desde : desde,
      hasta: hasta,
    };

    let dataExcel ={
      title: fecha,
      data: dataToExport,
      mapHeaders:mapHeaders,
      mapHeadersShow:mapHeadersShow,
      filtro:"",
      informacion : informacion,
    }
    console.log(dataExcel)
    this.excel.crearReporteProspectos(dataExcel)
  }

}
