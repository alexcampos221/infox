import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { DateAdapter } from '@angular/material/core';
import { DatePipe } from '@angular/common';
import { UniversidadService } from 'src/app/shared/services/backend/universidad.service';
import { ProgramaService } from 'src/app/shared/services/backend/programa.service';
import { PromocionService } from 'src/app/shared/services/backend/promocion.service';
import { BancoService } from 'src/app/shared/services/backend/banco.service';
import { ProcesarPagoComponent } from 'src/app/pagos/dialogs/procesar-pago/procesar-pago.component';

@Component({
  selector: 'app-pago-detalle',
  templateUrl: './pago-detalle.component.html',
  styleUrls: ['./pago-detalle.component.sass'],
  providers : [DatePipe],
})
export class PagoDetalleComponent implements OnInit {

  pagoForm: FormGroup;
  editar: boolean = false;
  nombre_modal: string;

  universidades_lista : any[] = [];
  bancos_lista : any[] = [];
  programas_lista : any[]  = [];
  semestres_lista : any[] = [];
  vendedores_lista : any[] = [];
  modos_lista : any[] = [];
  cursos_lista : any[] = [
    {
      cur_id :'100',
      cur_nombre : 'Curso prueba 01',
    },
    {
      cur_id :'101',
      cur_nombre : 'Curso prueba 02',
    },
    {
      cur_id :'102',
      cur_nombre : 'Curso prueba 03',
    }
  ];

  matriculado : boolean = false;
  activo : boolean = true;

  constructor(private programaServicio: ProgramaService,
              private promocionServicio: PromocionService,
              private  dialog: MatDialog,
              private fb: FormBuilder,
              private dateAdapter : DateAdapter<Date>,
              private datePipe: DatePipe,
              private universidadServicio : UniversidadService,
              private bancoServicio : BancoService,) { }

  ngOnInit(): void {
    this.crearFormulario();
    this.traerUniversidades();
    this.traerProgramas();
  }



  private crearFormulario() {
    this.pagoForm = this.fb.group({
      nombre : ['',[Validators.required]],
      dni : ['',[Validators.required]],
      correo : ['',[Validators.required]],
      universidad : ['',[Validators.required]],
      pais :['',[Validators.required]],
      descripcionCombo :['',[Validators.required]],
      programa : ['',[Validators.required]],
      representante : ['',[Validators.required]],
      vendedor : ['',[Validators.required]],
      usuario : ['',[Validators.required]],
      banco : ['',[Validators.required]],
      monto : ['',[Validators.required]],
      modo : ['',[Validators.required]],


      telefono : ['',[Validators.required]],
      activo : [false,[Validators.required]],
      matriculado : [false,[Validators.required]],
      otros : ['',[Validators.required]],
      curso : ['',[Validators.required]],
      inicio_curso : ['',[Validators.required]],
      codigo_promocional : ['',[Validators.required]],
      clave : ['',[Validators.required]],
      numero : ['',[Validators.required]],
      fecha : ['',[Validators.required]],
      image_url : ['',[Validators.required]],
      
    });
  }

  private get f(){
    return this.pagoForm.controls;
  }

  enviarFormulario(){
    console.log(this.pagoForm.value);
  }

  volver(){

  }

  private traerUniversidades(){
    this.universidadServicio.listaUniversidades().subscribe((resp:any[]) =>{
      this.universidades_lista = resp;  
      console.log("universidades ",resp);
    },(error)=>{
      console.info("error universidades: ",error);
    });
  }

  private traerProgramas(){

    this.programaServicio.listaProgramas().subscribe((resp:any) =>{

      this.programas_lista = resp; 
      console.log("programas ",resp);
    },(error)=>{
      console.info("error programas: ",error);
    });
  }

  procesar(){
    const dialogRef = this.dialog.open(ProcesarPagoComponent, {
      data: {
        nombre: "formulario",
        banco : null
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
        if( result==undefined){
          console.log("No se recibe nada");
        }
        else if(result.estado=='exito'){
          console.log("Se ha creado el banco");
        }
        else{
          console.log("ha ocurrido un error");
        }
    });
  }
}
