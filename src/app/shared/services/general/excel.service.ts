import { Injectable } from '@angular/core';
import { Workbook } from 'exceljs';

import * as fs from 'file-saver';
import * as Global from '../../globals';

import { DatePipe } from '@angular/common';

import { couldStartTrivia } from 'typescript';

import * as Globales from 'src/app/shared/globals';

// import { Console } from 'console';

@Injectable({
  providedIn: 'root'
})
export class ExcelService {

  constructor() { }

  crearReporteGeneral(excelData){
    const title = excelData.title;

    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet("Reporte");
    
    let informacion = excelData.informacion;

    let titulo = [];    
    titulo[1] = title;

    let fechas = [];

    fechas[1] = `( ${informacion.desde} - ${informacion.hasta} )`;

    let titulo_general = worksheet.addRow(titulo);
    let fechas_reporte = worksheet.addRow(fechas);
    titulo_general.alignment = { vertical: 'middle', horizontal: 'center' };
    titulo_general.font = {bold:true}
    fechas_reporte.alignment = { vertical: 'middle', horizontal: 'center' };

    worksheet.addRow([]);
    let tipo = [];
    tipo[2] = "Tipo";
    tipo[3] = informacion.tipo;
    let vendedor = [];
    vendedor[2] = "Vendedor";
    vendedor[3] = informacion.vendedor;
    let banco = [];
    banco[2] = "Banco";
    banco[3] = informacion.banco;

    worksheet.addRow(tipo);
    worksheet.addRow(vendedor);
    worksheet.addRow(banco);

    worksheet.getCell('B4').font = {bold:true};
    worksheet.getCell('B5').font = {bold:true};
    worksheet.getCell('B6').font = {bold:true};
    
    let titulos = excelData.mapHeadersShow;
    let headerRow = worksheet.addRow(titulos);
    
    worksheet.mergeCells('A1','I1');
    worksheet.mergeCells('A2','C2');

    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: '4167B8' },
        bgColor: { argb: '' }
      }
      cell.font = {
        bold: true,
        color: { argb: 'FFFFFF' },
        size: 12
      }
    });

    excelData.data.forEach(element => {
      let fila = [];
      for (let index = 0; index < excelData.mapHeadersShow.length; index++) {
        let clave = excelData.mapHeaders.get(excelData.mapHeadersShow[index]);
        switch (clave) {
          default:
            fila[index+1] = element[clave];
            break;
        }
      }
      worksheet.addRow(fila);
    })

    worksheet.getColumn(1).width = 15;
    worksheet.getColumn(2).width = 20;
    worksheet.getColumn(3).width = 20;
    worksheet.getColumn(4).width = 50;
    worksheet.getColumn(5).width = 20;
    worksheet.getColumn(6).width = 20;
    worksheet.getColumn(7).width = 30;
    worksheet.getColumn(8).width = 25;
    worksheet.getColumn(9).width = 20;
    worksheet.getColumn(10).width = 20;
    worksheet.getColumn(11).width = 20;
    worksheet.addRow([]);
    
    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, title + '.xlsx');
    })
    console.log(excelData);
  }


  crearReporteConfirmadosByVendedor(excelData){
    const title = excelData.title;

    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet("Reporte");
    
    let informacion = excelData.informacion;

    let titulo = [];    
    titulo[1] = title;

    let fechas = [];

    fechas[1] = `( ${informacion.desde} - ${informacion.hasta} )`;

    let nombre = [];
    nombre[1] = informacion.data;

    let titulo_general = worksheet.addRow(titulo);
    let fechas_reporte = worksheet.addRow(fechas);
    let nombre_usuario = worksheet.addRow(nombre);

    titulo_general.alignment = { vertical: 'middle', horizontal: 'center' };
    titulo_general.font = {bold:true}
    fechas_reporte.alignment = { vertical: 'middle', horizontal: 'center' };

    nombre_usuario.alignment = { vertical: 'middle', horizontal: 'center' };

    worksheet.addRow([]);   

    let titulos = excelData.mapHeadersShow;
    let headerRow = worksheet.addRow(titulos);
    
    worksheet.mergeCells('A1','I1');
    worksheet.mergeCells('A2','C2');
    worksheet.mergeCells('A3','C3');

    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: '4167B8' },
        bgColor: { argb: '' }
      }
      cell.font = {
        bold: true,
        color: { argb: 'FFFFFF' },
        size: 12
      }
    });

    excelData.data.forEach(element => {
      let fila = [];
      for (let index = 0; index < excelData.mapHeadersShow.length; index++) {
        let clave = excelData.mapHeaders.get(excelData.mapHeadersShow[index]);
        switch (clave) {
          default:
            fila[index+1] = element[clave];
            break;
        }
      }
      worksheet.addRow(fila);
    })

    worksheet.getColumn(1).width = 15;
    worksheet.getColumn(2).width = 20;
    worksheet.getColumn(3).width = 10;
    worksheet.getColumn(4).width = 50;
    worksheet.getColumn(5).width = 20;
    worksheet.getColumn(6).width = 20;
    worksheet.getColumn(7).width = 35;
    worksheet.getColumn(8).width = 35;
    worksheet.getColumn(9).width = 20;
    worksheet.getColumn(10).width = 20;
    worksheet.addRow([]);
    
    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, title + '.xlsx');
    })
    console.log(excelData);
  }
  

  crearReporteConfirmadosByCaja(excelData){
    const title = excelData.title;

    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet("Reporte");
    
    let informacion = excelData.informacion;

    let titulo = [];    
    titulo[1] = title;

    let fechas = [];

    fechas[1] = `( ${informacion.desde} - ${informacion.hasta} )`;

    let titulo_general = worksheet.addRow(titulo);
    let fechas_reporte = worksheet.addRow(fechas);
    titulo_general.alignment = { vertical: 'middle', horizontal: 'center' };
    titulo_general.font = {bold:true}
    fechas_reporte.alignment = { vertical: 'middle', horizontal: 'center' };

    worksheet.addRow([]);   

    let titulos = excelData.mapHeadersShow;
    let headerRow = worksheet.addRow(titulos);
    
    worksheet.mergeCells('A1','I1');
    worksheet.mergeCells('A2','C2');

    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: '4167B8' },
        bgColor: { argb: '' }
      }
      cell.font = {
        bold: true,
        color: { argb: 'FFFFFF' },
        size: 12
      }
    });

    excelData.data.forEach(element => {
      let fila = [];
      for (let index = 0; index < excelData.mapHeadersShow.length; index++) {
        let clave = excelData.mapHeaders.get(excelData.mapHeadersShow[index]);
        switch (clave) {
          default:
            fila[index+1] = element[clave];
            break;
        }
      }
      worksheet.addRow(fila);
    })

    worksheet.getColumn(1).width = 15;
    worksheet.getColumn(2).width = 20;
    worksheet.getColumn(3).width = 10;
    worksheet.getColumn(4).width = 50;
    worksheet.getColumn(5).width = 20;
    worksheet.getColumn(6).width = 20;
    worksheet.getColumn(7).width = 35;
    worksheet.getColumn(8).width = 35;
    worksheet.getColumn(9).width = 15;
    worksheet.getColumn(10).width = 15;
    worksheet.addRow([]);
    
    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, title + '.xlsx');
    })
    console.log(excelData);
  }
  

  crearReporteSaldosByCaja(excelData){
    const title = excelData.title;

    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet("Reporte");
    
    let informacion = excelData.informacion;

    let titulo = [];    
    titulo[1] = title;

    let fechas = [];

    fechas[1] = `( ${informacion.desde} - ${informacion.hasta} )`;

    let titulo_general = worksheet.addRow(titulo);
    let fechas_reporte = worksheet.addRow(fechas);
    titulo_general.alignment = { vertical: 'middle', horizontal: 'center' };
    titulo_general.font = {bold:true}
    fechas_reporte.alignment = { vertical: 'middle', horizontal: 'center' };

    worksheet.addRow([]);   

    let titulos = excelData.mapHeadersShow;
    let headerRow = worksheet.addRow(titulos);
    
    worksheet.mergeCells('A1','I1');
    worksheet.mergeCells('A2','C2');

    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: '4167B8' },
        bgColor: { argb: '' }
      }
      cell.font = {
        bold: true,
        color: { argb: 'FFFFFF' },
        size: 12
      }
    });

    excelData.data.forEach(element => {
      let fila = [];
      for (let index = 0; index < excelData.mapHeadersShow.length; index++) {
        let clave = excelData.mapHeaders.get(excelData.mapHeadersShow[index]);
        switch (clave) {
          default:
            fila[index+1] = element[clave];
            break;
        }
      }
      worksheet.addRow(fila);
    })

    worksheet.getColumn(1).width = 15;
    worksheet.getColumn(2).width = 20;
    worksheet.getColumn(3).width = 10;
    worksheet.getColumn(4).width = 40;
    worksheet.getColumn(5).width = 15;
    worksheet.getColumn(6).width = 15;
    worksheet.getColumn(7).width = 30;
    worksheet.getColumn(8).width = 35;
    worksheet.getColumn(9).width = 15;
    worksheet.getColumn(10).width = 15;
    
    worksheet.addRow([]);
    
    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, title + '.xlsx');
    })
    console.log(excelData);
  }

  crearCertificados(excelData){
    const title = excelData.title;

    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet("Reporte");
    
    let informacion = excelData.informacion;

    let titulo = [];    
    titulo[1] = title;

    
    let titulo_general = worksheet.addRow(titulo);
  
    titulo_general.alignment = { vertical: 'middle', horizontal: 'center' };
    titulo_general.font = {bold:true}

    worksheet.addRow([]);
    
    let titulos = excelData.mapHeadersShow;
    let headerRow = worksheet.addRow(titulos);
    
    worksheet.mergeCells('A1','G1');

    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: '4167B8' },
        bgColor: { argb: '' }
      }
      cell.font = {
        bold: true,
        color: { argb: 'FFFFFF' },
        size: 12
      }
    });

    excelData.data.forEach(element => {
      let fila = [];
      for (let index = 0; index < excelData.mapHeadersShow.length; index++) {
        let clave = excelData.mapHeaders.get(excelData.mapHeadersShow[index]);
        switch (clave) {
          default:
            fila[index+1] = element[clave];
            break;
        }
      }
      worksheet.addRow(fila);
    })

    worksheet.getColumn(1).width = 15;
    worksheet.getColumn(2).width = 30;
    worksheet.getColumn(3).width = 20;
    worksheet.getColumn(4).width = 30;
    worksheet.getColumn(5).width = 30;
    worksheet.getColumn(6).width = 20;
    worksheet.getColumn(7).width = 20;
    worksheet.addRow([]);
    
    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, title + '.xlsx');
    })
    console.log(excelData);
  }

  crearReporteProspectos(excelData){
    const title = excelData.title;

    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet("Reporte");
    
    let informacion = excelData.informacion;

    let titulo = [];    
    titulo[1] = title;

    let fechas = [];

    fechas[1] = `( ${informacion.desde} - ${informacion.hasta} )`;

    let titulo_general = worksheet.addRow(titulo);
    let fechas_reporte = worksheet.addRow(fechas);
    titulo_general.alignment = { vertical: 'middle', horizontal: 'center' };
    titulo_general.font = {bold:true}
    fechas_reporte.alignment = { vertical: 'middle', horizontal: 'center' };

    worksheet.addRow([]);
    let promocion = [];
    promocion[2] = "Promocion";
    promocion[3] = informacion.promocion;

    worksheet.addRow(promocion);

    worksheet.getCell('B4').font = {bold:true};
    let titulos = excelData.mapHeadersShow;
    let headerRow = worksheet.addRow(titulos);
    
    worksheet.mergeCells('A1','J1');
    worksheet.mergeCells('A2','C2');

    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: '4167B8' },
        bgColor: { argb: '' }
      }
      cell.font = {
        bold: true,
        color: { argb: 'FFFFFF' },
        size: 12
      }
    });

    excelData.data.forEach(element => {
      let fila = [];
      for (let index = 0; index < excelData.mapHeadersShow.length; index++) {
        let clave = excelData.mapHeaders.get(excelData.mapHeadersShow[index]);
        switch (clave) {
          default:
            fila[index+1] = element[clave];
            break;
        }
      }
      worksheet.addRow(fila);
    })

    worksheet.getColumn(1).width = 15;
    worksheet.getColumn(2).width = 25;
    worksheet.getColumn(3).width = 15;
    worksheet.getColumn(4).width = 30;
    worksheet.getColumn(5).width = 30;
    worksheet.getColumn(6).width = 15;
    worksheet.getColumn(7).width = 15;
    worksheet.getColumn(8).width = 30;
    worksheet.getColumn(9).width = 15;
    worksheet.getColumn(10).width = 15;
    worksheet.addRow([]);
    
    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, title + '.xlsx');
    })
    console.log(excelData);
  }

  crearReporteVendedorEmbajador(excelData){
    const title = excelData.title;

    let workbook = new Workbook();
    
    let worksheet = workbook.addWorksheet("Vendedores");
    // let worksheet2 = workbook.addWorksheet("Embajadores");
    
    let informacion = excelData.informacion;

    let titulo = [];    
    titulo[1] = title;

    let fechas = [];

    fechas[1] = `( ${informacion.desde} - ${informacion.hasta} )`;

    let titulo_general = worksheet.addRow(titulo);
    let fechas_reporte = worksheet.addRow(fechas);
    titulo_general.alignment = { vertical: 'middle', horizontal: 'center' };
    titulo_general.font = {bold:true}
    fechas_reporte.alignment = { vertical: 'middle', horizontal: 'center' };

    
    let titulos = excelData.mapHeadersShow;
    let headerRow = worksheet.addRow(titulos);

    // let titulos2 = excelData.mapHeadersShow;
    // let headerRow2 = worksheet2.addRow(titulos2);
    
    worksheet.mergeCells('A1','D1');
    worksheet.mergeCells('A2','C2');

    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: '4167B8' },
        bgColor: { argb: '' }
      }
      cell.font = {
        bold: true,
        color: { argb: 'FFFFFF' },
        size: 12
      }
    });

    // headerRow2.eachCell((cell, number) => {
    //   cell.fill = {
    //     type: 'pattern',
    //     pattern: 'solid',
    //     fgColor: { argb: '4167B8' },
    //     bgColor: { argb: '' }
    //   }
    //   cell.font = {
    //     bold: true,
    //     color: { argb: 'FFFFFF' },
    //     size: 12
    //   }
    // });

    excelData.data.forEach(element => {
      let fila = [];
      for (let index = 0; index < excelData.mapHeadersShow.length; index++) {
        let clave = excelData.mapHeaders.get(excelData.mapHeadersShow[index]);
        switch (clave) {
          default:
            fila[index+1] = element[clave];
            break;
        }
      }
      worksheet.addRow(fila);
    });

    // excelData.data2.forEach(element => {
    //   let fila = [];
    //   for (let index = 0; index < excelData.mapHeadersShow.length; index++) {
    //     let clave = excelData.mapHeaders.get(excelData.mapHeadersShow[index]);
    //     switch (clave) {
    //       default:
    //         fila[index+1] = element[clave];
    //         break;
    //     }
    //   }
    //   worksheet2.addRow(fila);
    // });

    worksheet.getColumn(1).width = 15;
    worksheet.getColumn(2).width = 20;
    worksheet.getColumn(3).width = 20;
    worksheet.getColumn(4).width = 15;    
    worksheet.addRow([]);

    // worksheet2.getColumn(1).width = 15;
    // worksheet2.getColumn(2).width = 20;
    // worksheet2.getColumn(3).width = 20;
    // worksheet2.addRow([]);
    
    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, title + '.xlsx');
    })
    console.log(excelData);
  }

  crearReporteVentasAnuales(excelData){
    const title = excelData.title;

    let workbook = new Workbook();
    
    let worksheet = workbook.addWorksheet("Ventas");
    
    // let informacion = excelData.informacion;

    let titulo = [];    
    titulo[1] = title;

    // let fechas = [];

    // fechas[1] = `( ${informacion.desde} - ${informacion.hasta} )`;

    let titulo_general = worksheet.addRow(titulo);
    // let fechas_reporte = worksheet.addRow(fechas);
    titulo_general.alignment = { vertical: 'middle', horizontal: 'center' };
    titulo_general.font = {bold:true}
    // fechas_reporte.alignment = { vertical: 'middle', horizontal: 'center' };

    
    let titulos = excelData.mapHeadersShow;
    let headerRow = worksheet.addRow(titulos);

    
    worksheet.mergeCells('A1','C1');
    // worksheet.mergeCells('A2','C2');

    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: '4167B8' },
        bgColor: { argb: '' }
      }
      cell.font = {
        bold: true,
        color: { argb: 'FFFFFF' },
        size: 12
      }
    });

    excelData.data.forEach(element => {
      let fila = [];
      for (let index = 0; index < excelData.mapHeadersShow.length; index++) {
        let clave = excelData.mapHeaders.get(excelData.mapHeadersShow[index]);
        switch (clave) {
          default:
            fila[index+1] = element[clave];
            break;
        }
      }
      worksheet.addRow(fila);
    });


    worksheet.getColumn(1).width = 15;
    worksheet.getColumn(2).width = 20;
    worksheet.getColumn(3).width = 20;  
    worksheet.addRow([]);
    
    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, title + '.xlsx');
    })
    console.log(excelData);
  }

}
