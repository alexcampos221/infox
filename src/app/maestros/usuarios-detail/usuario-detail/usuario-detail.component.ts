import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { UsuarioService } from 'src/app/shared/services/backend/usuario.service';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { EditUsuarioComponent } from '../../dialogs/edit-usuario/edit-usuario.component';
import { FechasFormComponent } from '../../dialogs/fechas-form/fechas-form.component';

@Component({
  selector: 'app-usuario-detail',
  templateUrl: './usuario-detail.component.html',
  styleUrls: ['./usuario-detail.component.sass']
})
export class UsuarioDetailComponent implements OnInit {
  
  // formBusqueda : FormGroup;
  //paginator
  pageEvent: PageEvent;
  datasource: null;
  pageIndex:number;
  pageSize:number = 1;
  length:number;

  usuID:string;
  arrayPagination : any [] = [];

  rowUsu :any;

  current_user:any;

  displayedColumns = [
    'rol',
    'acciones',
  ];
  displayedColumns2 = [
    'mes',
    'anio',
    'valor',
    'acciones',
  ];
  
  @ViewChild(MatPaginator, { static: true }) paginator3: MatPaginator;
  @ViewChild(MatPaginator, { static: true }) paginator2: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;
  dataSource_locales_totales : MatTableDataSource<any>;
  dataSource_locales_meta : MatTableDataSource<any>;

  @ViewChild(MatPaginator, { static: true }) paginador: MatPaginator;

  locales_filtrados : any [] = [];

  constructor(
              private router:Router,
              private  dialog: MatDialog,
              public activatedRoute: ActivatedRoute,
              private loadingService: LoadingService,
              private spinner: NgxSpinnerService,
              private usuarioService:UsuarioService,
              private fb : FormBuilder,
              ) 
    { 
      if(JSON.parse(localStorage.getItem('info_usu')) != null){
        this.arrayPagination = JSON.parse(localStorage.getItem('info_usu'));
        this.pageIndex = JSON.parse(localStorage.getItem('indice'));
        this.length = this.arrayPagination.length;  
      }
      if(JSON.parse(localStorage.getItem('info_usu_id')) != null){
        this.usuID = JSON.parse(localStorage.getItem('info_usu_id'));
      }else{
        this.usuID=this.router.getCurrentNavigation().extras.state.data.usuID;
      }
    }
  txtNuevo:string;
  ngOnInit(): void {
    this.dataSource_locales_totales = new MatTableDataSource();
    this.dataSource_locales_meta = new MatTableDataSource();
    this.paginador._intl.getRangeLabel = (page: number, pageSize: number, length: number)=>{
      if (length == 0 || pageSize == 0) {
        return `0 de ${length}`;
      }
      const startIndex = page * pageSize;

      return `${startIndex+1} de ${length}`
    }; 
    this.traerInfoUsu();
    // this.traerRolesUsuario();
    
  }

  ngAfterViewInit():void{
    this.paginador._intl.getRangeLabel = (page: number, pageSize: number, length: number)=>{
      if (length == 0 || pageSize == 0) {
        return `0 de ${length}`;
      }
      const startIndex = page * pageSize;

      return `${startIndex+1} de ${length}`
    };    
  }

  traerInfoUsu(){
    let salida={
      usuarioID: this.usuID
    }
    this.usuarioService.infoUsuario(JSON.stringify(salida)).subscribe((resp : any)=>{
      console.log(resp);
      this.rowUsu = resp['info'];
      this.dataSource_locales_meta.data = resp['metas'] || []
      console.log(this.rowUsu);
      console.log(this.dataSource_locales_meta.data)
    })
  }

  // traerRolesUsuario(){
  //   let salida={
  //     usu: this.usuID
  //   }
  //   this.usuarioService.rolesUsuario(JSON.stringify(salida)).subscribe((resp : any)=>{
  //     this.rowUsu = resp;
  //     console.log(this.rowUsu)
  //   })
  // }

 
  applyFilter(filterValue){
    let filterV=filterValue.value
    this.dataSource_locales_meta.filter = filterV.trim().toLowerCase() || '';
  }

  addtable(){
    const dialogRef = this.dialog.open(FechasFormComponent,{
      data:{
        nombre:'Agregar',
        usuarioID: this.usuID
      }
    });
    dialogRef.afterClosed().subscribe((result) => {
      if( result == undefined){
        console.log("No se recibe nada");
      }
      else if(result.estado=='EXITO'){
      this.traerInfoUsu()
      // this.traerRolesUsuario();
      }
      else{
        console.log("ha ocurrido un error");
      }
    }); 
  }
  actualizarLista(){
    this.traerInfoUsu();
    // this.traerRolesUsuario();
  }
  sortData(sort: Sort){
    const data = this.locales_filtrados.slice();
    let arreglo_temporal :any []= [];
    if (!sort.active || sort.direction === '') {
      arreglo_temporal = data;
      this.dataSource_locales_totales.data = arreglo_temporal;
      return;
    }
    arreglo_temporal = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'rol': return this.compare(a.rol_nom||'', b.rol_nom||'', isAsc);        
        default: return 0;
      }
    });

    this.dataSource_locales_totales.data = arreglo_temporal;
  }
  sortData2(sort: Sort){
    const data = this.locales_filtrados.slice();
    let arreglo_temporal :any []= [];
    if (!sort.active || sort.direction === '') {
      arreglo_temporal = data;
      this.dataSource_locales_totales.data = arreglo_temporal;
      return;
    }
    arreglo_temporal = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'mes': return this.compare(a.mes_nombre||'', b.mes_nombre||'', isAsc);
        case 'anio': return this.compare(a.anio_nombre||'', b.anio_nombre||'', isAsc);
        case 'valor': return this.compare(Number(a.met_meta) || 0, Number(b.met_meta) || 0, isAsc);        
        default: return 0;
      }
    });

    this.dataSource_locales_totales.data = arreglo_temporal;
  }
  private  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
  deleteMeta(row){
    console.log(row)
  }
  editUser(){

  }

  cerrarDetalle(){
    // localStorage.setItem('cerrar_detalle',JSON.stringify(true));
    this.router.navigate(['/maestros/usuarios']);
  }
  public getServerData(event?:PageEvent){
    this.pageIndex = event.pageIndex;
    localStorage.setItem('indice',JSON.stringify(event.pageIndex));
    this.usuID = this.arrayPagination[event.pageIndex];    
    this.traerInfoUsu();
    return event;
  }

  editarInfoUsu(){
    console.log(this.rowUsu)
    const dialogRef = this.dialog.open(EditUsuarioComponent,{
      
      data:{
        nombre: "Editar",
        usuario: this.rowUsu
      }
    });
    dialogRef.afterClosed().subscribe((result) => {
      if( result == undefined){
        console.log("No se recibe nada");
      }
      else if(result.estado=='EXITO'){
      this.traerInfoUsu()
      }
      else{
        console.log("ha ocurrido un error");
      }
    });   
  }

}
