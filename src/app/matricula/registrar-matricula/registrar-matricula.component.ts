import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { MatriculaService } from 'src/app/shared/services/registros/matricula.service';
import { DateAdapter } from '@angular/material/core';
import { UtlitariosService } from 'src/app/shared/services/general/utlitarios.service';
import { Subscription } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { AlertaService } from 'src/app/shared/services/general/alerta.service';
import { FirebaseStorageService } from 'src/app/shared/services/storage/firebase-storage.service';
import { DatePipe } from '@angular/common';
import { ChangeDetectorRef } from '@angular/core';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-registrar-matricula',
  templateUrl: './registrar-matricula.component.html',
  styleUrls: ['./registrar-matricula.component.sass'],
  providers:[DatePipe]
})
export class RegistrarMatriculaComponent implements OnInit {

  codigo: any;
  formMatricula: FormGroup;

  universidades_lista: any[] = [];
  bancos_lista: any[] = [];
  programas_cursos_lista: any[] = [];
  programas_procesados: any[] = [];
  programas_filtrados : any[] = [];
  fecha_maxima = new Date();

  paises_lista: any[] = [];

  documentos: any[] = [];
  todos_doc: any[] = [
    {
      nombre: 'Otros', valor: 0, tamanio :0,
    },
    {
      nombre: 'DNI', valor: 1, tamanio : 8,
    },
    {
      nombre: 'RUC', valor: 2, tamanio : 11,
    }
  ];

  cursos_combo : any[] = [];
  meses_combo : any[] = [];
  lista_semestres : any[] = [];
  cursos_permitidos = 0;
  combo : boolean = false;
  mapa_cursos : Map<string,any>;
  buscador_programa : FormControl;
  codigo_valido : boolean = false;
  envio_codigo  : boolean = false;
  mapa_promocional : Map<string,any>;

  referido_valido : boolean = false;
  envio_referido : boolean = false;
  mapa_referido : Map<string,any>;
  ruta_multimedia : string="";

  asesor_completo :any;
  asesor_nombre : string = '';
  formDirective_actual: FormGroupDirective;
  otra_universidad : boolean = false;

  enviandoformulario : boolean = false;

  @ViewChild('form') form;
  nombre_documento : string = "Documento";

  mapa_meses : Map<string,any>;
  tipo_programa : boolean = false;

  estado_asesor : string = 'INICIO';

  constructor(private rutaActiva: ActivatedRoute,
              private dateAdapter: DateAdapter<Date>,
              private fb: FormBuilder,
              private matriculaServicio: MatriculaService,
              private utilitarios: UtlitariosService,
              private spinner:NgxSpinnerService,
              private loading:LoadingService,
              private alertaServicio:AlertaService,
              private ruteador: Router,
              private storage:FirebaseStorageService,
              private datePipe:DatePipe,
              private cdRef:ChangeDetectorRef
  ) {
    this.dateAdapter.setLocale('es-PE');
  }
  

  ngOnInit(): void {
    //this.codigo = this.rutaActiva.snapshot.params.codigo;
    this.prepararRegistro();
    this.obtenerAsesor();
    /*
    console.log(this.codigo);
    if (this.codigo) {
      this.crearFormulario();
    }
    else {
      console.log("No hay codigo de empleado");
    }
    */
  }

  ngAfterViewChecked(){
    this.cdRef.detectChanges();
  }

  private obtenerAsesor(){
    this.codigo = this.rutaActiva.snapshot.params.codigo;

    let salida = {
      code : this.codigo
    };
    console.log(salida)
    let body = JSON.stringify(salida);
    this.spinner.show();
    this.loading.loading$.next({opacity:0.5});
    this.matriculaServicio.getVendedorAct(body).subscribe((resp:any)=>{
      this.spinner.hide();
      if(resp['status']=='OK'){
        this.asesor_completo = resp;
        this.asesor_nombre = this.asesor_completo.result.usu_nombre;
        this.f.asesor.setValue(this.asesor_nombre);
        this.estado_asesor = 'VALIDO';
        this.f.codigo_empleado.setValue(this.codigo);
      }
      else{
        //this.ruteador.navigate(['/error']);
        this.estado_asesor = 'ERROR';
      }
    },(error)=>{
      //this.alertaServicio.errorInterno();
      //this.ruteador.navigate(['/error']);
      this.spinner.hide();
      console.log(error);
      this.estado_asesor = 'ERROR';
    });
  }


  private prepararRegistro(){
    this.crearFormulario();
    this.traerUniversidades();
    this.traerSemestres();
    this.listaProgramaCursos();
    this.listaBancos();
    this.traerPaises();
    this.prepararTiposDoc([this.todos_doc[0]]);
    this.mapa_cursos = new Map();
    this.mapa_meses  = new Map();
    this.mapa_promocional = new Map();
    this.mapa_referido = new Map();
  }

  private traerPaises(){
    this.utilitarios.listarPaises().subscribe((resp: any) => {
      console.log(resp);
      this.paises_lista = resp.filter((p)=>p.pai_activo=='Y');
    });
  }

  private crearFormulario() {
    this.formMatricula = this.fb.group({
      tipo_doc: ['', [Validators.required]],
      documento: ['', [Validators.required]],
      nombres: ['', [Validators.required]],
      correo: ['', [Validators.required]],
      telefono: ['', [Validators.required]],
      universidad: ['', [Validators.required]],
      programa: ['', [Validators.required]],
      cursos: ['',[]],
      semestre: ['', [Validators.required]],
      banco: ['', [Validators.required]],
      operacion: ['', [Validators.required]],
      monto: ['', [Validators.required]],
      fecha: ['', [Validators.required]],
      fecha_inicio : ['',[Validators.required]],
      imagen: ['', [Validators.required]],
      codigo_promo: ['', []],
      codigo_referido: ['', []],
      codigo_empleado: ['', [Validators.required]],
      otros: ['', []],
      nombre_referido: ['', []],
      pais: ['', []],
      asesor : ['',[]],
      cod_pais : ['',[]],
      meses_programa : ['',[Validators.required,Validators.minLength(1)]]
    });
    this.buscador_programa = new FormControl();
  }

  private get f() {
    return this.formMatricula.controls;
  }

  private prepararCursosCombo(){
    let flag = 1;
    let salida="";
    console.log(this.mapa_cursos)
    this.mapa_cursos.forEach(elemento=>{
      salida = salida + elemento.cur_nombre;
      if(flag<this.mapa_cursos.size){
        salida=salida+",";
      }
      flag++;
    });
    return salida;
  }

  private traerUniversidades() {
    this.matriculaServicio.listaUniversidades().subscribe((resp: any) => {
      console.log(resp);
      this.universidades_lista = resp.filter((m)=>m.uni_activo=='Y');
    });
  }
  private traerSemestres(){
    this.matriculaServicio.listaSemestres().subscribe((resp: any) => {
      console.log(resp);
      this.lista_semestres = resp.filter((m)=>m.sem_activo=='Y');
    },(error) => {
      console.log(error);
      this.spinner.hide();
    });
  }
  private listaProgramaCursos() {

    this.matriculaServicio.listaProgramaCursos().subscribe((resp: any) => {
      console.log(resp);
      this.programas_cursos_lista = resp['result'];
      this.procesarProgramas();
    });
  }

  private listaBancos() {
    this.matriculaServicio.listaBancos().subscribe((resp: any) => {
      console.log(resp);
      this.bancos_lista = resp['result'].filter((m)=>m.ban_activo=='Y');
    });
  }


  private procesarProgramas() {
    let arreglo_salida: any[] = [];
    //console.log(this.programas_cursos_lista); 
    this.programas_cursos_lista.forEach(elemento => {

      //meses ofrecidos
      let arreglo_meses :any[]= [];

      if(elemento.pro_1m =="Y"){
        arreglo_meses.push({
          duracion : "1 MES",
          numerico : 1,
          valor : "pro_1m",
        });
      }

      if(elemento.pro_3m=="Y"){
        arreglo_meses.push({
          duracion : "3 MESES",
          numerico : 3,
          valor : "pro_3m",
        });
      }

      if(elemento.pro_6m=="Y"){
        arreglo_meses.push({
          duracion : "6 MESES",
          numerico : 6,
          valor : "pro_6m",
        });
      }

      if(elemento.pro_12m=="Y"){
        arreglo_meses.push({
          duracion : "12 MESES",
          numerico : 12,
          valor : "pro_12m",
        });
      }

    
      if(arreglo_meses.length == 0){
        arreglo_meses.push({
          duracion : "1 MES",
          numerico : 1,
          valor : "pro_1m",
        });
      }
      

      let tipo = elemento.pro_tipo;
      let arreglo:any[] = [];
      switch (tipo) {
        case 'PR':
          if(elemento.pro_cursos.length>0){
            let arreglo_nuevo:any[] =[];
            let curso_completo = null;
            elemento.pro_cursos.forEach(curso => {
              if(curso.cur_nombre=="PROGRAMA COMPLETO" || curso.cur_nombre=="PAQUETE COMPLETO"){
                curso_completo = curso;
                //revisar el numero de meses disponible
              }
              else{
                let objeto = {
                  nombre : '-' +' '+curso.cur_nombre,
                  programa_id : elemento.pro_id,
                  curso_id : curso.cur_id,
                  tipo : 'CU',
                  bloqueado : false,
                  tipo_origen : elemento.pro_tipo,
                };
                arreglo_nuevo.push(objeto);
              }
            });
    
            if(curso_completo!=null){
              let objeto_completo = {
                nombre : `${elemento.pro_nombre.toUpperCase()} (Completo)`,
                programa_id : elemento.pro_id,
                curso_id : curso_completo.cur_id,
                tipo: 'PR',
                bloqueado : false,
                tipo_origen : elemento.pro_tipo,
                meses : arreglo_meses,
              }
              arreglo_nuevo.unshift(objeto_completo);
            }
            else{
              let objeto_completo = {
                nombre : `${elemento.pro_nombre.toUpperCase()}`,
                programa_id : elemento.pro_id,
                curso_id : '000',
                tipo: 'PR',
                bloqueado : true,
                tipo_origen : elemento.pro_tipo,
                meses : arreglo_meses,
              }
              arreglo_nuevo.unshift(objeto_completo);
            }
    
            arreglo = arreglo_nuevo;
          }
          else{
            let objeto_programa = {
              nombre : `${elemento.pro_nombre.toUpperCase()} (Completo)`,
              programa_id : elemento.pro_id,
              curso_id : '000',
              tipo: 'PR',
              bloqueado : false,
              tipo_origen : elemento.pro_tipo,
              meses : arreglo_meses,
            };
            arreglo.push(objeto_programa);
          }
          break;
      
        case 'CO':
          let objeto_combo = {
            nombre : `${elemento.pro_nombre.toUpperCase()}`,
            programa_id : elemento.pro_id,
            curso_id : '000',
            tipo: 'CO',
            bloqueado : false,
            tipo_origen : elemento.pro_tipo,
            cantidad : Number(elemento.pro_qty),
            cursos : elemento.pro_cursos||[],
          }
          arreglo.push(objeto_combo);
          break;

        default:
          break;
      }
      arreglo.forEach(elemento=>{
        arreglo_salida.push(elemento);
      });
    });


    this.programas_procesados = arreglo_salida;
    this.programas_filtrados = this.programas_procesados;
    console.log(this.programas_procesados);
  }

  private prepararTiposDoc(arreglo: any[]) {
    this.documentos = arreglo;
  }

  public selecPais(evento) {
    let pais = evento;
    this.f.nombres.setValue('');
    this.f.documento.setValue('');
    this.f.universidad.setValue('');
    this.f.cod_pais.setValue(evento.pai_codigo||'');

    this.otra_universidad = false;

    this.nombre_documento = evento.pais_nombre_documento;

    this.universidadesByPais(evento);

    if (pais.pai_id == "1000000") {
      this.todos_doc[0].nombre = 'Otros';
      this.prepararTiposDoc(this.todos_doc);
      this.f.tipo_doc.enable();
    }
    else {
      this.todos_doc[0].nombre = evento.pais_nombre_documento||'Documento';
      this.prepararTiposDoc([this.todos_doc[0]]);
      this.f.tipo_doc.setValue(this.todos_doc[0]);
      this.f.tipo_doc.disable();
      
      //actualizar el numero
    }
  }

  private universidadesByPais(pais){
    let body = {
      paisID : pais.pai_id
    };
    console.log(body);
    this.universidades_lista = [];
    this.matriculaServicio.universidadListarByPais(body).subscribe((resp:any)=>{
      console.log(resp);
      this.universidades_lista = resp;
      let otros=null;
      resp.forEach(element => {
        if(element.uni_nombre=="OTROS"){
          otros = element;
        }
      });

      if(otros==null){
        this.universidades_lista.push(
          {
            uni_nombre : 'OTROS',
            uni_id : "1000005"
          }
        );
      }      
    },(error)=>{
      console.log(error);
      this.universidades_lista.push(
        {
          uni_nombre : 'OTROS',
          uni_id : "1000005"
        }
      );
    });
  }
  selectSemestre(event){
    console.log(event)
  }
  public selectTipoDoc(evento) {
    let documento = evento;
    switch (documento.valor) {
      case 1:
        //setear el validator 
        console.log("Validator dni");
        this.f.documento.setValidators([]);
        this.f.documento.setValidators([Validators.required,Validators.maxLength(8), Validators.minLength(8)]);
        this.f.documento.setValue('');
        this.f.nombres.setValue('');
        break;
      case 2:
        //setear el validator 
        console.log("Validator RUC");
        this.f.documento.setValidators([]);
        this.f.documento.setValidators([Validators.required,Validators.maxLength(11), Validators.minLength(11)])
        this.f.documento.setValue('');
        this.f.nombres.setValue('');
        break;
      case 0:
        this.f.documento.setValidators([]);
        this.f.documento.setValidators(Validators.required);
        this.f.documento.setValue('');
        this.f.nombres.setValue('');
      default:
        break;
    }
  }

  public buscarDocumento(resp){
      if(resp.length  == this.f.tipo_doc.value.tamanio && this.f.tipo_doc.value.valor>0){
        this.f.documento.disable();
        this.f.nombres.setValue('');
        this.consultarDocumento(resp,this.f.tipo_doc.value.valor);
      }
  }

  private consultarDocumento(cadena,valor){
    let body = {
      valor : cadena
    }

    if(valor==1){
      this.consultarDNI(body);
    }

    else if(valor == 2){
      this.consultarRUC(body);
    }
    else{

    }
  }

  private consultarDNI(body){
    this.spinner.show();
    this.loading.loading$.next({opacity:0.5});
    this.utilitarios.traerNombresConDNI(body).subscribe((resp:any)=>{
      console.log(resp);
      if(resp.estado){
        this.f.nombres.setValue(resp.nombre_completo);
      }
      else{
        this.alertaServicio.sinResultados('DNI');
      }

      this.f.documento.enable();
      this.spinner.hide();
    },(error)=>{
      console.log(error);
      this.f.documento.enable();
      this.spinner.hide();
    });
  }

  private consultarRUC(body){
    this.spinner.show();
    this.loading.loading$.next({opacity:0.5,});
    this.utilitarios.traerRazoSocialConRuc(body).subscribe((resp:any)=>{
      console.log(resp);
      if(resp.estado!="false"){
        this.f.nombres.setValue(resp.razon_social);
      }
      else{
        this.alertaServicio.sinResultados('RUC');
      }
      this.f.documento.enable();
      this.spinner.hide();
    },(error)=>{
      console.log(error);
      this.f.documento.enable();
      this.spinner.hide();
    });
  }

  onKey(valor){
    this.buscarPrograma(valor);

  }

  private buscarPrograma(cad){
    let val = cad;
    if (val != "") {
      val = val.toLowerCase();
    } else {
      this.programas_filtrados = this.programas_procesados;
    }
    
    const rows = this.programas_procesados.filter(function(d) {        
      //console.log(d);
      if (d.nombre.toString().toLowerCase().indexOf(val) > -1 
          ) {
        return d;
      }        
    });
    this.programas_filtrados = rows;
  }

  selectPrograma(evento){
    //this.buscador_programa.setValue('');
    this.f.cursos.setValue('');
    this.mapa_cursos.clear();
    this.mapa_meses.clear();
    this.cursos_permitidos = 0;

    console.log(this.mapa_meses);
    console.log(evento);

    let tipo = evento.tipo;
    switch (tipo) {
      case 'CO':
        this.combo = true;
        this.tipo_programa = false;
        this.cursos_combo = evento.cursos;
        this.cursos_permitidos = Number(evento.cantidad)||0;
        this.f.cursos.setValidators([Validators.minLength(this.cursos_permitidos),Validators.required]);
        //this.anularMeses();
        this.f.meses_programa.setValue([{
          duracion : "1 MES",
          numerico : 1,
          valor : "pro_1m",
        }]);
        break;

      case 'PR':
        this.anularCursos();
        this.combo = false;
        this.tipo_programa = true;
        this.meses_combo = evento.meses;
        this.f.meses_programa.setValue([]);
        //this.f.meses_programa.setValidators([Validators.minLength(1),Validators.required]);
        break;
    
      default:
        this.combo = false;
        this.tipo_programa = false;
        this.f.cursos.clearValidators();
        this.f.cursos.setValue('');
      
        this.f.cursos.updateValueAndValidity();
    
        this.f.meses_programa.setValue([{
          duracion : "1 MES",
          numerico : 1,
          valor : "pro_1m",
        }]);
        break;
    }
    
    /*
    if(evento.tipo_origen=='CO'){
      this.combo = true;
      this.cursos_combo = evento.cursos;
      this.cursos_permitidos = Number(evento.cantidad)||0;
      this.f.cursos.setValidators([Validators.minLength(this.cursos_permitidos),Validators.required]);
    }
    else{
      this.combo = false;
      this.f.cursos.clearValidators();
      this.f.cursos.setValue('');
      this.f.cursos.updateValueAndValidity();
    }
    */
  }

  private anularCursos(){
    this.f.cursos.clearValidators();
    this.f.cursos.setValue('');
    this.f.cursos.updateValueAndValidity();

  }

  private anularMeses(){
    this.f.meses_programa.clearValidators();
    this.f.meses_programa.setValue('');
    this.f.meses_programa.updateValueAndValidity();
  }

  selectCurso(evento){
    let objto = evento.source.value;
    console.log(evento)
    if(this.mapa_cursos.get(objto.cur_id)==null && this.mapa_cursos.size<this.cursos_permitidos){
      this.mapa_cursos.set(objto.cur_id,objto);
    }
    else{
      this.mapa_cursos.delete(objto.cur_id);
    }
  }

  desabilitado(curso){
    if(this.mapa_cursos.size==this.cursos_permitidos && this.mapa_cursos.get(curso.cur_id)==null){
      return true;
    }
    else{
      return false;
    }
  }

  selectMeses(evento){
    let objto = evento.source.value;

    if(this.mapa_meses.get(objto.valor)==null && this.mapa_meses.size<1){
      this.mapa_meses.set(objto.valor,objto);
    }
    else{
      this.mapa_meses.delete(objto.valor);
    }
  }

  desabilitadoMeses(mes){
    //
    if(this.mapa_meses.size==1 && this.mapa_meses.get(mes.valor)==null){
      return true;
    }
    else{
      return false;
    }
  }

  selectUniversidad(evento){
    if(evento.uni_nombre=="OTROS"){
      this.otra_universidad = true;
      this.f.otros.setValidators([Validators.required]);
      this.f.otros.setValue('');
    }
    else{
      this.otra_universidad = false;
      this.f.otros.clearValidators();
      this.f.otros.setValue('');
      this.f.otros.updateValueAndValidity()
    }
  }

  probarCodigo(){
    this.f.codigo_promo.disable();
    this.envio_codigo = true;
    let salida = {
      code : this.f.codigo_promo.value
    };
    let body = JSON.stringify(salida);
    this.spinner.show();
    this.loading.loading$.next({opacity:0.5});
    this.matriculaServicio.buscarPromocion(body).subscribe((resp:any)=>{
      console.log(resp);
      if(resp.status=='OK'){
        console.log("Codigo Valido");
        this.mapa_promocional.set(this.f.codigo_promo.value,resp);
        this.codigo_valido = true;
      }
      else{
        console.log("Codigo Invalido");
        this.codigo_valido = false;
      }
      this.spinner.hide();
    },(error)=>{
      console.log(error);
      this.spinner.hide();
    });

  }

  private obtenerReferido(){
    this.f.codigo_referido.disable();
    let salida = {
      code : this.f.codigo_referido.value
    };
    console.log(salida)
    let body = JSON.stringify(salida);
    this.spinner.show();
    this.loading.loading$.next({opacity:0.5});
    this.matriculaServicio.buscarReferido(body).subscribe((resp:any)=>{
      console.log(resp);
      if(resp.status=='OK'){
        this.mapa_referido.set(this.f.codigo_referido.value,resp);
        this.envio_referido = true;
        this.referido_valido = true;
        this.f.nombre_referido.setValue(resp);
      }
      else{
        this.envio_referido = true;
        this.referido_valido = false;
        this.f.nombre_referido.setValue("");
      }

      this.spinner.hide();
    },(error)=>{
      this.spinner.hide();
      console.log(error);
    });
  }

  buscarReferido(resp:string){
    console.log(resp)
    if(resp.length  == 6){
      this.f.nombre_referido.setValue('');
      this.obtenerReferido();
    }
  }

  quitarCodigo(){
    this.f.codigo_promo.enable();
    this.envio_codigo = false;
    this.codigo_valido = false;
    this.f.codigo_promo.setValue('');
    if(this.mapa_promocional.size>0){
      this.mapa_promocional.clear();
      console.log("Quitando codigo");
    }
  }

  quitarReferido(){
    this.f.codigo_referido.enable();
    this.envio_referido = false;
    this.referido_valido = false;
    this.f.codigo_referido.setValue('');
    this.f.nombre_referido.setValue('');
    if(this.mapa_referido.size>0){
      this.mapa_referido.clear();
      console.log("Quitando referido");
    }
  }

  enviarFormulario() {
    
    this.enviandoformulario = true;
    console.log(this.formMatricula.value);
    //console.log("duracion : ",this.prepararDuracion());
    
    this.spinner.show();
    this.loading.loading$.next({opacity:0.5});
    this.storage.uploadFile(this.f.imagen.value._files,"temporales").then((publicURL:any)=>{
      this.ruta_multimedia = publicURL;
      console.log(this.ruta_multimedia);
      this.spinner.hide();
      this.enviarMatricula();
    },(error)=>{
      console.log(error);
      this.enviandoformulario = false;
      this.spinner.hide();
      this.alertaServicio.algoHaIdoMal();
    });
    
  }

  private enviarMatricula(){
    console.log(this.f.value)
    let dni = this.f.documento.value;
    let nombres = this.f.nombres.value;
    let correo = this.f.correo.value;
    let telefono = this.f.cod_pais.value+this.f.telefono.value;
    let universidad = this.f.universidad.value.uni_id;
    let programa = this.f.programa.value.programa_id;
    let curso;
    if(this.f.programa.value.curso_id == '000'){
      curso = '0';
    }else{
      curso = this.f.programa.value.curso_id;
    }
    
    let otros = this.f.otros.value;
    let url  = this.ruta_multimedia;
    let banco = this.f.banco.value.ban_id;
    let operacion = this.f.operacion.value;
    let monto = this.f.monto.value;
    let fecha = this.datePipe.transform(this.f.fecha.value,'yyyy-MM-dd');
    let codigo_promo = this.f.codigo_promo.value;
    let usuario = this.asesor_completo.result.usu_id;
    let referido = "";
    if(this.referido_valido){
      referido = this.f.codigo_referido.value;
    }

    let pais = this.f.pais.value.pai_id;
    let fecha_inicio = this.datePipe.transform(this.f.fecha_inicio.value,'yyyy-MM-dd');
    let comboCursos = this.prepararCursosCombo();
    let duracion = this.prepararDuracion();

    let salida = {
      txtDni: dni, 
      txtNombres: nombres,
      txtEmail : correo,
      txtTelefono : telefono,
      universidadID: universidad,
      programaID: programa,
      cursoID: curso,
      txtOtros: otros,
      url: url,
      bancoID: banco,
      txtNumOperacion: operacion,
      txtMonto: monto,
      txtFechaAbono: fecha,
      txtCodPromocion: codigo_promo,
      usuarioID: usuario,
      codReferido: referido, 
      paisID: pais,
      fechaInicio: fecha_inicio,
      comboCursos: comboCursos,
      tiempo : duracion,
      semestreID : this.f.semestre.value.sem_id
    }
    console.log(salida);
    let body = JSON.stringify(salida);
    
    
    this.spinner.show();
    this.loading.loading$.next({opacity:0.5});
    this.matriculaServicio.guardarRegistroPagoNew(body).subscribe((resp:any)=>{
      this.spinner.hide();
      
      console.log(resp);
      if(resp['status']=='OK'){
        this.enviarCorreo();
      }
      else{
        this.alertaServicio.algoHaIdoMal();
        this.enviandoformulario = false;
      }      
    },(error)=>{
      console.log(error);
      this.spinner.hide();
      this.alertaServicio.algoHaIdoMal();
      this.enviandoformulario = false;
    });
  }

  private prepararDuracion(){
    let salida;
    let valor = this.f.meses_programa.value;
    if(valor==""){
      salida = valor;
    }
    else{
      salida = this.f.meses_programa.value[0].numerico;
    }
    return salida;
  }

  private enviarCorreo(){
    let body = {
      destinatario : this.f.correo.value,
      nombre : this.f.nombres.value,
    };
    console.log(body);
    this.spinner.show();
    this.loading.loading$.next({opacity:0.5});
    this.utilitarios.enviarEmailPago(body).subscribe((resp:any)=>{
      this.spinner.hide();
      this.alertaServicio.matriculaCorrecta();
      console.log(resp);
      window.location.href='https://www.infoxeduca.com/';
    },(error)=>{
      this.spinner.hide();
      this.alertaServicio.matriculaCorrecta();
      console.log(error);
      window.location.href='https://www.infoxeduca.com/';
    });  
  }

  public reintentar(){
    this.obtenerAsesor();
  }
}