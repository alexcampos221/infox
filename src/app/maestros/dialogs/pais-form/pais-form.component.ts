import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PaisesService } from 'src/app/shared/services/backend/paises.service';
import { CursoFormComponent } from '../curso-form/curso-form.component';

@Component({
  selector: 'app-pais-form',
  templateUrl: './pais-form.component.html',
  styleUrls: ['./pais-form.component.sass']
})
export class PaisFormComponent implements OnInit {
  nombre:any;

  editar : boolean = false;

  paisForm : FormGroup;
  constructor(private fb: FormBuilder,               
    public dialogRef: MatDialogRef<CursoFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,              
    private paisesService: PaisesService,
    ) { 
      console.log(data)
    }

  ngOnInit(): void {
    
    if(this.data.nombre == 'editar'){
      this.editar = true;
      this.crearFormularioEditar();
      this.nombre = 'Editar Pais';
      this.f.pais.setValue(this.data.pais.pai_nombre);   
      this.f.glosaDoc.setValue(this.data.pais.pais_nombre_documento); 
      this.f.codPais.setValue(this.data.pais.pai_codigo); 
    }
    else{
      this.nombre = "Crear Pais";
      this.crearFormulario();
    }
    // this.traerPaises();
  }

  private crearFormulario(){
    this.paisForm = this.fb.group({
      pais: ['',[Validators.required]],
      glosaDoc: ['',[Validators.required]],
      codPais: ['',[Validators.required]],
    });
  }

  private crearFormularioEditar(){
    this.paisForm = this.fb.group({
      pais: ['',[Validators.required]],
      glosaDoc: ['',[Validators.required]],
      codPais: ['',[Validators.required]],
    });
  }

  private get f(){
    return this.paisForm.controls;
  }

  enviarFormulario(){
    if(this.editar==true){
      this.editarPais();
    }else{
      this.crearPais();
    }
  }
  editarPais(){
    let nombre = this.paisForm.controls.pais.value;
    let glosaDoc = this.paisForm.controls.glosaDoc.value;
    let codPais = this.paisForm.controls.codPais.value;
    let body = {
      nombre : nombre,
      glosaDoc : glosaDoc,
      codPais: codPais,
      paisID: this.data.pais.pai_id,
    }
    console.log(body)
    
    this.paisesService.editarPais(body).subscribe(resp=>{
      if(resp['status']=='OK'){
        this.dialogRef.close({estado : 'EXITO'})
      }
      else{
        this.dialogRef.close({estado : 'FALLO'});
      }
    },(error)=>{
      console.log(error);
      this.dialogRef.close({estado : 'FALLO'});
    });
  }
  crearPais(){
    let nombre = this.paisForm.controls.pais.value;
    let glosaDoc = this.paisForm.controls.glosaDoc.value;
    let codPais = this.paisForm.controls.codPais.value;
    let body = {
      nombre : nombre,
      glosaDoc : glosaDoc,
      codPais:codPais,
    }
    console.log(body)
    
    this.paisesService.crearPais(body).subscribe(resp=>{
      if(resp['status']=='OK'){
        this.dialogRef.close({estado : 'EXITO'})
      }
      else{
        this.dialogRef.close({estado : 'FALLO'});
      }
    },(error)=>{
      console.log(error);
      this.dialogRef.close({estado : 'FALLO'});
    });
  }

  closeDialog(): void {
    this.dialogRef.close();
  }
}
