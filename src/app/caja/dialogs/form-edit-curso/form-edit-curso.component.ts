import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CertificadoService } from 'src/app/shared/services/backend/certificado.service';
import { UniversidadService } from 'src/app/shared/services/backend/universidad.service';
import { AlertaService } from 'src/app/shared/services/general/alerta.service';
import { MatriculaService } from 'src/app/shared/services/registros/matricula.service';

@Component({
  selector: 'app-form-edit-curso',
  templateUrl: './form-edit-curso.component.html',
  styleUrls: ['./form-edit-curso.component.sass'],
  providers: [ DatePipe ]
})
export class  FormEditCursoComponent implements OnInit {
  formEditar:FormGroup;
  ventana:any;

  programas : any[] = [];
  cursos : any[] = [];

  curso:any;
  combo:boolean;
  tipo_programa:boolean;

  meses_combo : any[] = [];

  combo_descripcion:any[]=[];
  showMeses:boolean=false
  cambioPrograma:boolean = true;
  isPrograma:boolean = false;
  isMeses:boolean = true;

  cursos_permitidos = 0
  mapa_cursos : Map<string,any>;
  mapa_meses : Map<string,any>;

  lista_meses : any;

  lista_universidades : any;
  lista_semestres : any [] = [];
  date_hoy : Date;
  tipo : string = '';
  info:any;

  constructor(private dialogRef: MatDialogRef<FormEditCursoComponent>,
    private fb: FormBuilder,
    private certificadoService:CertificadoService,
    private alertaServicio:AlertaService,
    private matriculaServicio:MatriculaService,
    private universidadServicio : UniversidadService,
    private dateAdapter:DateAdapter<Date>,
    private datePipe: DatePipe,
    @Inject(MAT_DIALOG_DATA) public data: any,) {
      console.log(data)
      this.dateAdapter.setLocale('es-PE')
      this.ventana = this.data.ventana;
    }

  ngOnInit(): void {
    this.date_hoy = new Date()
    this.crearFormulario();
    this.combo_descripcion = this.data.curso_nombre
    this.tipo = this.data.tipo;
    this.info = this.data.info;

    if(this.ventana == 'reporte'){
      this.f.usuario.setValue(this.data.usuario)
      this.f.clave.setValue(this.data.clave)
      this.f.meses_programa.setValue(this.data.meses + ' Meses'  );
      this.f.correoCaja.setValue(this.info.pag_email)
    }
    if(this.tipo == 'vendedor'){

    }
    if(this.tipo == 'SSS'){
      this.f.dni.setValue(this.info.pag_dni)
      this.f.correo.setValue(this.info.pag_email)
      this.f.telefono.setValue(this.info.pag_telefono)
      this.f.meses_programa.setValue(this.data.tiempo + ' Meses');
      let dia =this.info.pag_inicio_curso.substring(0,2);
      let mes =this.info.pag_inicio_curso.substring(3,5)
      let anio = this.info.pag_inicio_curso.substring(6,10)
      let fecha_l =  anio + '-' + mes + '-' + dia
      let fecha_i = this.datePipe.transform(fecha_l,'shortDate') //this.info.pag_inicio_curso.replaceAll('/','-')
      let fecha = new Date(fecha_i)
      this.f.ini_curso.setValue(fecha)
    }
    this.listarProgramas();
    this.listarUniversidades();
    this.listarSemestres()
    this.mapa_cursos = new Map();
    this.mapa_meses  = new Map();
  }
  private crearFormulario(){
    this.formEditar = this.fb.group({
      cursos: ['',[]],
      programa: ['', [Validators.required]],
      curso:['',[]],
      usuario:['',[]],
      correoCaja:['',[]],
      clave:['',[]],
      meses_programa:['',[]],

      dni:['',[Validators.required]],
      correo:['',[Validators.required]],
      telefono:['',[Validators.required]],
      univerdad:['',[Validators.required]],
      ini_curso:['',[Validators.required]],
      semestre:['',[]],
    });
  }
  private get f(){
    return this.formEditar.controls;
  }
  private listarProgramas(){
    this.certificadoService.listarProgramas().subscribe((resp:any)=>{
      // console.log(resp);
      this.programas = resp || [];
      this.programas = this.programas.filter((m) => m.pro_activo=='Y');
      // this.programas.forEach(ele => {
      //   ele.pro_1m = 'Y'
      // })
      this.programas.forEach(elemento=>{
        if(elemento.pro_id == this.data.programaID && elemento.pro_nombre == this.data.programa_nombre){
          this.f.programa.setValue(elemento);
          this.isPrograma=true
        }
      });
      if(!this.isPrograma){
        this.programas.forEach(elemento => {
          if(elemento.pro_id == this.data.cursoID){
            this.f.programa.setValue(elemento);
          }
        })
      }
      console.log(this.programas)
      this.listarCursos();
    });
  }

  private listarCursos(){
    // console.log(this.f.programa.value.pro_qty_combo)
    let programaID = this.data.programaID
    let salida ={
      programaID: programaID
    }
    this.certificadoService.listarCursos(salida).subscribe(resp=>{
      console.log(resp)
      this.cursos = resp['cursos'] || [];
      if(this.data.cursoID != null){
        this.cursos.forEach(elemento => {
          if(elemento.cur_id == this.data.cursoID){
            if(this.f.programa.value.pro_tipo == 'PR'){
              this.f.curso.setValue(elemento)
            }
          }
        })
      }
      if(this.f.programa.value.pro_tipo == 'PR'){
        this.cursos_permitidos = Number(1);
        this.f.curso.setValidators([Validators.minLength(this.cursos_permitidos),Validators.required]);
      }else{
        this.cursos_permitidos = Number(this.f.programa.value.pro_qty_combo) || 0;
        this.f.cursos.setValidators([Validators.minLength(this.cursos_permitidos),Validators.required]);
      }
    })
  }
  listarUniversidades(){
    this.universidadServicio.listaUniversidades().subscribe(resp => {
      console.log(resp);
      this.lista_universidades = resp;
      this.lista_universidades = this.lista_universidades.filter(m => m.uni_activo == 'Y')
      if(this.tipo == 'SSS'){
        this.lista_universidades.forEach(ele => {
          if(ele.uni_nombre == this.info.uni_nombre){
            this.f.univerdad.setValue(ele)
          }
        });
      }
    });
  }
  
  selectUniversidad( evento ){
    console.log(evento)
  }
  listarSemestres(){
    this.matriculaServicio.listaSemestres().subscribe((resp:any) => {
      console.log(resp);
      this.lista_semestres = resp;
      this.lista_semestres = this.lista_semestres.filter(m => m.sem_activo == 'Y');
      if(this.tipo == 'SSS'){
        this.lista_semestres.forEach(ele => {
          if(ele.sem_id == this.info.sem_id){
            this.f.semestre.setValue(ele)
          }
        });
      }
    },(error) => {
      console.log(error);
    })
  }
  selectSemestre( evento ){
    console.log(evento)
  }
  private anularCursos(){
    this.f.curso.clearValidators();
    this.f.curso.setValue('');
    this.f.curso.updateValueAndValidity();
    this.f.cursos.clearValidators();
    this.f.cursos.setValue('');
    this.f.cursos.updateValueAndValidity();
  }

  selectPrograma(event){
    console.log(event)

    this.f.cursos.setValue('');
    this.mapa_cursos.clear();
    this.mapa_meses.clear();
    this.cursos_permitidos = 0;
    this.cambioPrograma = false;
    this.isMeses = false
    // this.f.curso.setValue('')
    this.buscarCursos()
    this.f.programa.setValue(event);
    let tipo = event.pro_tipo
    // debugger
    switch (tipo) {
      case 'CO':
        this.combo = true;
        this.tipo_programa = false;
        this.cursos_permitidos = Number(this.f.programa.value.pro_qty_combo) || 0;
        this.f.cursos.setValidators([Validators.minLength(this.cursos_permitidos),Validators.required]);

        break;
      case 'PR':
        this.anularCursos();
        this.combo = false;
        this.tipo_programa = true;
        this.cursos_permitidos = Number(1)
        this.f.curso.setValidators([Validators.minLength(this.cursos_permitidos),Validators.required]);
        this.lista_meses = event
        break;
      default:
        this.combo = false;
        this.tipo_programa = false;
        this.f.cursos.clearValidators();
        this.f.cursos.setValue('');
        this.f.cursos.updateValueAndValidity();
        this.f.curso.setValue('')
        this.f.curso.updateValueAndValidity();
        break;
    }
  }

  prepararMeses(array){
    let array_meses  : any [] = [];
    let obj
    if (array.pro_1m == 'Y') {
      obj = {
        duracion: "1 MES",
        numerico: 1,
        valor: "pro_1m",
      }
      array_meses.push(obj)
    }
    if (array.pro_3m == 'Y') {
      obj = {
        duracion: "3 MESES",
        numerico: 3,
        valor: "pro_3m",
      }
      array_meses.push(obj)
    }
    if (array.pro_6m == 'Y') {
      obj = {
        duracion: "6 MESES",
        numerico: 6,
        valor: "pro_6m",
      }
      array_meses.push(obj)
    }
    if (array.pro_12m == 'Y') {
      obj = {
        duracion: "12 MESES",
        numerico: 12,
        valor: "pro_12m",
      }
      array_meses.push(obj)
    }
    // console.log(array_meses)
    return array_meses
  }

  buscarCursos(){
    let salida = {
      programaID: this.f.programa.value.pro_id
    }
    this.certificadoService.listarCursos(salida).subscribe((resp:any)=>{
      console.log(resp);
      this.cursos = resp['cursos'] || []
    })
  }

  selectCurso(evento){
    this.f.meses_programa.setValue('')
    // let objto = evento.source.value;
    let obj = evento;
    console.log(obj)
    if(this.mapa_cursos.get(obj.cur_id)==null && this.mapa_cursos.size<this.cursos_permitidos){
      this.mapa_cursos.set(obj.cur_id,obj);
    }
    else{
      this.mapa_cursos.delete(obj.cur_id);
    }
    if(obj.cur_nombre == 'PROGRAMA COMPLETO'){
      this.f.meses_programa.setValidators(Validators.required);
      this.f.meses_programa.updateValueAndValidity();
      this.showMeses = true
      this.meses_combo = this.prepararMeses(this.lista_meses);
    }
    else{
      this.f.meses_programa.clearValidators();
      this.f.meses_programa.setValue('')
      this.f.meses_programa.updateValueAndValidity();
      this.meses_combo = []
      this.showMeses = false
    }

  }

  closeDialog():void{
    this.dialogRef.close();
  }
  desabilitado(curso){
    // console.log(curso)
    if(this.mapa_cursos.size==this.cursos_permitidos && this.mapa_cursos.get(curso.cur_id)==null){
      return true;
    }
    else{
      return false;
    }
  }

  onSubmitClick(){
    console.log(this.formEditar.value)
    if(this.ventana == 'reporte'){
      this.editarReporte()
    }else{
      this.editarByCaja()
    }
  }
  editarReporte(){
    let programa = this.f.programa.value;
    console.log(this.f.meses_programa.value)
    let curso;
    if(programa.pro_tipo=='PR'){
      curso = this.f.curso.value.cur_id;
    }else{
      curso = '0';
    }
    let comboCursos = this.prepararCursosCombo();
    let body = {
      pagoID: Number(this.data.pagoID),
      usuarioID:Number(this.data.usuarioID),
      programaID:programa.pro_id,
      cursoID:Number(curso),
      descripcionCombo: comboCursos || '',
      usuarioNombre:this.f.usuario.value,
      usuarioClave:this.f.clave.value,
      tiempo: this.f.meses_programa.value[0]?.numerico || '0',
      correo: this.formEditar.controls.correoCaja.value,//this.info.pag_email,
      usuario_nombre: this.formEditar.controls.usuario.value,
      usuario_clave: this.formEditar.controls.clave.value,
      fecha_curso: this.info.pag_inicio_curso,
      semestreID: this.info?.sem_id || null,
      universidadID: this.info?.uni_id || null,
      telefono : this.info.pag_telefono,
      dni: this.info.pag_dni
    }
    console.log(body);
    this.matriculaServicio.editarMatriculaCurso(JSON.stringify(body)).subscribe(resp => {
      console.log(resp);
      if(resp['status']=='OK'){
        this.dialogRef.close({estado : 'EXITO'});
      }else{
        this.alertaServicio.algoHaIdoMal();
        this.dialogRef.close({estado : 'FALLO'});
      }
    },(error)=>{
      console.log(error);

    })
  }
  editarByCaja(){
    console.log(this.formEditar.value)
    let programa = this.f.programa.value;
    let curso;
    let duracion;
    if(this.data.programaID != programa.pro_id){
      duracion = this.prepararDuracion()
    }else{
      duracion = this.data.tiempo
    }
    if(programa.pro_tipo == "PR" ){
      curso = this.f.curso.value.cur_id || '';
    }else{
      curso = '0';
    }
    let comboCursos = this.prepararCursosCombo();
    // if(this.formEditar.valid){
      let body = {
        pagoID: Number(this.data.pagoID),
        usuarioID:Number(this.data.usuarioID),
        programaID:programa.pro_id,
        cursoID:Number(curso),
        descripcionCombo: comboCursos || '',
        tiempo: duracion || "0",
        correo: this.f.correo.value,
        dni: this.f.dni.value,
        telefono: this.f.telefono.value,
        universidadID: this.f.univerdad.value.uni_id,
        fecha_curso: this.datePipe.transform(this.f.ini_curso.value,'yyyy-MM-dd'),
        semestreID: this.f.semestre.value.sem_id
      }
      console.log(body);
      this.matriculaServicio.editarMatriculaCurso(JSON.stringify(body)).subscribe(resp => {
        console.log(resp);
        if(resp['status']=='OK'){
          this.dialogRef.close({estado : 'EXITO'});
        }else{
          this.alertaServicio.algoHaIdoMal();
          this.dialogRef.close({estado : 'FALLO'});
        }
      },(error)=>{
        console.log(error);

      })
  }
  private prepararDuracion(){
    let salida;
    let valor = this.f.meses_programa.value;
    if(valor==""){
      salida = valor;
    }
    else{
      salida = this.f.meses_programa.value[0].numerico;
    }
    return salida;
  }
  private prepararCursosCombo(){
    let flag = 1;
    let salida="";
    let array_tmp:any[] = this.f.cursos.value
    console.log(array_tmp)

    for (let index = 0; index < array_tmp.length; index++) {
      salida = salida + array_tmp[index].cur_nombre;
      if(flag<array_tmp.length){
        salida=salida+",";
      }
      flag++;
    }
    // this.mapa_cursos[0].forEach(element => {
    //   salida = salida + element.cur_nombre;
    //   if(flag<this.mapa_cursos.size){
    //     salida=salida+",";
    //   }
    // });
    // this.mapa_cursos.forEach(elemento=>{
    //   salida = salida + elemento.cur_nombre;
    //   if(flag<this.mapa_cursos.size){
    //     salida=salida+",";
    //   }
    //   flag++;
    // });
    return salida;
  }

  selectMeses(evento){
    let objto = evento.source.value;

    if(this.mapa_meses.get(objto.valor)==null && this.mapa_meses.size<1){
      this.mapa_meses.set(objto.valor,objto);
    }
    else{
      this.mapa_meses.delete(objto.valor);
    }
  }
  desabilitadoMeses(mes){
    if(this.mapa_meses.size==1 && this.mapa_meses.get(mes.valor)==null){
      return true;
    }
    else{
      return false;
    }
  }
}
