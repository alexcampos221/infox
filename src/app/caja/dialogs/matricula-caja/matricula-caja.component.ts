import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AlertaService } from 'src/app/shared/services/general/alerta.service';
import { UtlitariosService } from 'src/app/shared/services/general/utlitarios.service';

@Component({
  selector: 'app-matricula-caja',
  templateUrl: './matricula-caja.component.html',
  styleUrls: ['./matricula-caja.component.sass']
})
export class MatriculaCajaComponent implements OnInit {
  
  matriculaForm:FormGroup;

  reenviarEmail:boolean=true;

  constructor(private dialogRef: MatDialogRef<MatriculaCajaComponent>,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private utlitariosService:UtlitariosService,
    private router:Router,
    private alertaServicio:AlertaService,) {
      console.log(data);
     }


  ngOnInit(): void {
    this.crearFormulario();
    this.f.usuario_correo.setValue(this.data.usuario_correo);
    this.f.programa_nombre.setValue(this.data.programa_nombre);
    this.f.curso_nombre.setValue(this.data.curso_nombre);
    this.f.tiempo_contratado.setValue(this.data.tiempo_contratado);
    this.f.inicio_curso.setValue(this.data.ini_cur);
    console.log(this.f);
  }
  private crearFormulario(){    
    this.matriculaForm = this.fb.group({
      usu_nombre: ['',[Validators.required]],
      usu_clave: ['',[Validators.required]],
      usuario_correo: ['',[Validators.required]],
      // alumno_nombre: ['',[Validators.required]],
      programa_nombre: ['',[Validators.required]],
      curso_nombre: ['',[Validators.required]],
      tiempo_contratado: ['',[Validators.required]],
      inicio_curso: ['',[Validators.required]],
    });
  }
  private get f(){
    return this.matriculaForm.controls;
  }


  onSubmitClick() {    
    // if(this.matriculaForm.valid){
      let enviarEmail;
      if(this.reenviarEmail == true){
        enviarEmail='Y'
      }else{
        enviarEmail='N'
      }
      let email = this.f.usuario_correo.value
      let salida = {
        usuario_nombre: this.f.usu_nombre.value,
        usuario_clave: this.f.usu_clave.value,
        usuario_correo: email.trim(),
        pagoID: this.data.pagoID,
        usuarioID:Number(this.data.usuarioID),
        alumno_nombre: this.data.alumno_nombre,
        programa_nombre: this.data.programa_nombre,
        alumno_dni: this.data.dni,
        enviarEmail: enviarEmail
      } 
      let body=JSON.stringify(salida)
      console.log(salida);
      this.utlitariosService.matriculaByCaja(body).subscribe(resp=>{
        if(resp['status']=='OK'){        
          if(resp['enviado']=='OK'){
            this.dialogRef.close({estado : 'EXITO'});
          }         
        }else{
          this.alertaServicio.algoHaIdoMal();
          this.dialogRef.close({estado : 'FALLO'});
        }
      },(error)=>{
        console.log(error);
        this.alertaServicio.algoHaIdoMal();
        this.dialogRef.close({estado : 'FALLO'});
      })
    // }
  }
  changeEmail(event){
    console.log(event)
    this.reenviarEmail = event.checked
  }

  closeDialog(): void {
    this.dialogRef.close();
  }
  cerrar_popup(): void{
    this.dialogRef.close();
  }

}
