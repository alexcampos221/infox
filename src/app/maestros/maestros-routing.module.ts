import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BancosComponent } from './bancos/bancos.component';
import { ProgramasComponent } from './programas/programas.component';
import { UniversidadesComponent } from './universidades/universidades.component';
import { CursosComponent } from './cursos/cursos.component';
import { SemestresComponent } from './semestres/semestres.component';
import { PromocionesComponent } from './promociones/promociones.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { UsuarioDetailComponent } from './usuarios-detail/usuario-detail/usuario-detail.component';
import { AniosComponent } from './anios/anios.component';
import { ProgramaDetailComponent } from './programa-detail/programa-detail.component'
import { PaisesComponent } from './paises/paises.component';
import { ProspectosComponent } from './prospectos/prospectos.component';
import { CertificadosComponent } from './certificados/certificados.component';
const routes: Routes = [
  
  {
    path : 'bancos',
    component : BancosComponent,
  },
  {
    path : 'programas',
    component : ProgramasComponent,
  },
  {
    path : 'universidades',
    component : UniversidadesComponent,
  },
  // {
  //   path : 'cursos',
  //   component : CursosComponent,
  // },
  {
    path : 'semestres',
    component : SemestresComponent,
  },
  {
    path : 'promociones',
    component : PromocionesComponent,
  },
  {
    path : 'anos',
    component : AniosComponent,
  },
  {
    path : 'usuarios',
    component : UsuariosComponent,
  },
  {
    path : 'detalle',
    component : UsuarioDetailComponent,
  },  
  {
    path: 'detalle-curso',
    component: ProgramaDetailComponent,
  },
  {
    path: 'paises',
    component: PaisesComponent,
  }, 
  {
    path: 'prospectos',
    component: ProspectosComponent,
  }, 
  {
    path: 'certificados',
    component: CertificadosComponent,
  }, 
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MaestrosRoutingModule { }
