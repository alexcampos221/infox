import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { MatriculaService } from 'src/app/shared/services/registros/matricula.service';
import { DateAdapter } from '@angular/material/core';
import { Subscription } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { AlertaService } from 'src/app/shared/services/general/alerta.service';
import { UtlitariosService } from 'src/app/shared/services/general/utlitarios.service';
import { PromocionService } from 'src/app/shared/services/promociones/promocion.service';

@Component({
  selector: 'app-promocion',
  templateUrl: './promocion.component.html',
  styleUrls: ['./promocion.component.sass']
})
export class PromocionComponent implements OnInit {

  codigo : any;
  formPromo : FormGroup;

  promocion : string;
  promocion_completa : any;
  paises_lista : any[] = [];
  universidades_lista : any[] = [];

  semestres_lista : any[] = [];

  documentos: any[] = [];
  todos_doc: any[] = [
    {
      nombre: 'Otros', valor: 0, tamanio :0,
    },
    {
      nombre: 'DNI', valor: 1, tamanio : 8,
    },
    {
      nombre: 'RUC', valor: 2, tamanio : 11,
    }
  ];

  nombre_documento : string = "Documento";
  otra_universidad : boolean = false;
  enviando : boolean = false;

  constructor(private rutaActiva: ActivatedRoute,
              private fb: FormBuilder,
              private matriculaServicio :MatriculaService,
              private spinner:NgxSpinnerService,
              private loading:LoadingService,
              private alertaServicio:AlertaService,
              private ruteador: Router,
              private utilitarios: UtlitariosService,
              private promocionServicio : PromocionService,
  ) { }

  ngOnInit(): void {
    this.crearFormulario();
    this.prepararRegistro();
    this.verificarPromo();
  }

  private verificarPromo(){
    this.codigo = this.rutaActiva.snapshot.params.codigo;
    console.log(this.codigo);

    let salida = {
      code : this.codigo
    };
    let body = JSON.stringify(salida);
    this.spinner.show();
    this.loading.loading$.next({opacity:0.5});
    this.matriculaServicio.buscarPromocion(body).subscribe((resp:any)=>{
      this.spinner.hide();
      console.log(resp);
      if(resp['status']=='OK'){
        this.promocion_completa = resp;
        this.promocion = this.promocion_completa.result.pro_nombre;
        this.f.sorteo.setValue(this.promocion_completa.result.programa_nombre);
      }
      else{
        this.rutearFuera();
      }
    },(error)=>{
      console.log(error);
      this.spinner.hide();
      this.rutearFuera();
    });
  }

  private prepararRegistro(){
    this.traerPaises();
    this.traerUniversidades();
    this.traerSemestres();
  }

  private crearFormulario(){
      this.formPromo = this.fb.group({
        documento : ['',[Validators.required]],
        tipo_doc: ['', [Validators.required]],
        nombres : ['',[Validators.required]],
        correo : ['',[Validators.required,Validators.email]],
        telefono : ['',[Validators.required]],
        pais : ['',[Validators.required]],
        universidad : ['',[Validators.required]],
        semestre : ['',Validators.required],
        sorteo : ['',],
        cod_pais : ['',],
        otros : [''],
      });
  }

  private rutearFuera(){
    this.ruteador.navigate(['/ruta']);
  }

  private traerPaises() {
    this.utilitarios.listarPaises().subscribe((resp: any) => {
      console.log(resp);
      this.paises_lista = resp;
    });
  }

  private traerUniversidades() {
    this.matriculaServicio.listaUniversidades().subscribe((resp: any) => {
      console.log(resp);
      this.universidades_lista = resp['result'];
    });
  }

  private traerSemestres(){
    this.matriculaServicio.listarSemestres().subscribe((resp: any) => {
      console.log(resp);
      this.semestres_lista = resp.filter((el) =>
      el.sem_activo =='Y');
    },(error)=>{
      console.log(error);
      
    });
  }

  private get f(){
    return this.formPromo.controls;
  }

  selectUniversidad(evento){
    if(evento.uni_nombre=="OTROS"){
      this.otra_universidad = true;
      this.f.otros.setValidators([Validators.required]);
      this.f.otros.setValue('');
    }
    else{
      this.otra_universidad = false;
      this.f.otros.clearValidators();
      this.f.otros.setValue('');
      this.f.otros.updateValueAndValidity()
    }
  }

  public selecPais(evento) {
    let pais = evento;
    console.log(evento);
    this.otra_universidad = false;
    this.nombre_documento = evento.pais_nombre_documento;
    this.f.cod_pais.setValue(evento.pai_codigo||'');
    this.f.documento.setValue('');
    this.f.nombres.setValue('');
    this.f.otros.setValue('');
    this.universidadesByPais(evento);
    if (pais.pai_id == "1000000") {
      this.todos_doc[0].nombre="Otros";
      this.prepararTiposDoc(this.todos_doc);
      this.f.tipo_doc.enable();
    }
    else {
      this.todos_doc[0].nombre = evento.pais_nombre_documento||'Documento';
      this.prepararTiposDoc([this.todos_doc[0]]);
      this.f.tipo_doc.setValue(this.todos_doc[0]);
      this.f.tipo_doc.disable();
    }
  }

  private prepararTiposDoc(arreglo: any[]) {
    this.documentos = arreglo;
  }

  private universidadesByPais(pais){
    let body = {
      paisID : pais.pai_id
    };
    console.log(body);
    this.universidades_lista = [];
    this.matriculaServicio.universidadListarByPais(body).subscribe((resp:any)=>{
      console.log(resp);
      this.universidades_lista = resp;
      let otros=null;
      resp.forEach(element => {
        if(element.uni_nombre=="OTROS"){
          otros = element;
        }
      });

      if(otros==null){
        this.universidades_lista.push(
          {
            uni_nombre : 'OTROS',
            uni_id : "1000005"
          }
        );
      }      
    },(error)=>{
      console.log(error);
      this.universidades_lista.push(
        {
          uni_nombre : 'OTROS',
          uni_id : "1000005"
        }
      );
    });
  }

  public selectTipoDoc(evento){
    let documento = evento;
    switch (documento.valor) {
      case 1:
        //setear el validator 
        console.log("Validator dni");
        this.f.documento.setValidators([]);
        this.f.documento.setValidators([Validators.required,Validators.maxLength(8), Validators.minLength(8)]);
        this.f.documento.setValue('');
        this.f.nombres.setValue('');
        break;
      case 2:
        //setear el validator 
        console.log("Validator RUC");
        this.f.documento.setValidators([]);
        this.f.documento.setValidators([Validators.required,Validators.maxLength(11), Validators.minLength(11)])
        this.f.documento.setValue('');
        this.f.nombres.setValue('');
        break;
      case 0:
        this.f.documento.setValidators([]);
        this.f.documento.setValidators(Validators.required);
        this.f.documento.setValue('');
        this.f.nombres.setValue('');
      default:
        break;
    }
  }

  public buscarDocumento(resp){
    if(resp.length  == this.f.tipo_doc.value.tamanio && this.f.tipo_doc.value.valor>0){
      this.f.documento.disable();
      this.f.nombres.setValue('');
      this.consultarDocumento(resp,this.f.tipo_doc.value.valor);
    }
  }

  private consultarDocumento(cadena,valor){
    let body = {
      valor : cadena
    }

    if(valor==1){
      this.consultarDNI(body);
    }

    else if(valor == 2){
      this.consultarRUC(body);
    }
    else{

    }
  }

  private consultarDNI(body){
    this.spinner.show();
    this.loading.loading$.next({opacity:0.5});
    console.log(body);
    this.utilitarios.traerNombresConDNI(body).subscribe((resp:any)=>{
      console.log(resp);
      if(resp.estado){
        this.f.nombres.setValue(resp.nombre_completo);
      }
      else{
        this.alertaServicio.sinResultados('DNI');
      }

      this.f.documento.enable();
      this.spinner.hide();
    },(error)=>{
      console.log(error);
      this.f.documento.enable();
      this.spinner.hide();
    });
  }

  private consultarRUC(body){
    this.spinner.show();
    this.loading.loading$.next({opacity:0.5,});
    console.log(body);
    this.utilitarios.traerRazoSocialConRuc(body).subscribe((resp:any)=>{
      console.log(resp);
      if(resp.estado!="false"){
        this.f.nombres.setValue(resp.razon_social);
      }
      else{
        this.alertaServicio.sinResultados('RUC');
      }
      this.f.documento.enable();
      this.spinner.hide();
    },(error)=>{
      console.log(error);
      this.f.documento.enable();
      this.spinner.hide();
    });
  }

  selectSemestre(evento){
    console.log(evento);
  }

  enviarFormulario(){
    //console.log(this.formPromo.value);
    this.prepararConsulta();
    this.enviando = true;
  }

  private prepararConsulta(){
    let documento = this.f.documento.value;
    let pais = this.f.pais.value.pai_id;
    let nombres = this.f.nombres.value;
    let telefono = this.f.cod_pais.value+this.f.telefono.value.toString();
    let universidad = this.f.universidad.value.uni_id;
    let correo = this.f.correo.value;
    let semestre = this.f.semestre.value.sem_id;
    let sorteo = this.f.sorteo.value;
    let otros = this.f.otros.value;

    let consulta = {
      txtDni : documento, 
      txtNombres : nombres,
      txtEmail : correo,
      txtTelefono : telefono,
      universidadID : universidad,
      programaID : this.promocion_completa.result.programa_id,
      txtOtros : otros,
      promocionID : this.promocion_completa.result.pro_id,
      semestreID : semestre,
      paisID : pais,
    }

    let body = JSON.stringify(consulta);
    this.enviarPromo(body);
  }

  private enviarPromo(body){
    console.log(body);
    this.spinner.show();
    this.loading.loading$.next({opacity:0.5});
    this.promocionServicio.guardarRegistroPromoNew(body).subscribe((resp:any)=>{
      console.log(resp);
      this.spinner.hide();
      if(resp['status']=='OK'){
        this.alertaServicio.registroCorrecto();      
        window.location.href='https://www.infoxeduca.com/';
      }
      else{
        this.alertaServicio.algoHaIdoMal();
        this.enviando = false;
      }
    },(error)=>{
      console.log(error);
      this.spinner.hide();
      this.alertaServicio.algoHaIdoMal();
      this.enviando = false;
    });
  }

}