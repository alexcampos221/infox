import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistroPagoComponent } from './registro-pago/registro-pago.component';

const routes: Routes = [
  {
    path : ':codigo',
    component : RegistroPagoComponent,
  },
  // {
  //   path: '**', 
  //   component: Page404Component,
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CuotasRoutingModule { }
