import { Component, OnInit, ElementRef,AfterViewInit, ViewChild, OnDestroy} from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormControl, FormControlName, FormGroup, Validators } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { DatePipe } from '@angular/common';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { Utils } from 'src/app/shared/utils';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatriculaService } from 'src/app/shared/services/registros/matricula.service';
import { BancoFormComponent } from '../dialogs/banco-form/banco-form.component';
import { Subscription } from 'rxjs';
import { AlertaService } from 'src/app/shared/services/general/alerta.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { LoadingService } from 'src/app/shared/services/loading.service';

@Component({
  selector: 'app-bancos',
  templateUrl: './bancos.component.html',
  styleUrls: ['./bancos.component.sass'],
  providers: [DatePipe]
})
export class BancosComponent implements OnInit, OnDestroy {

  bancos_totales : any[] = [];
  bancos_filtrados : any[] = [];


  arreglo;
  dataSource_bancos_totales : MatTableDataSource<any>;

  //atributos para la tabla material
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;

  displayedColumns = [
    'codigo',
    'banco',
    'acciones',
  ];

  filtro_control : FormControl;
  filtro$ : Subscription;

  usuario:any;
  constructor(
              private fb: FormBuilder,
              private adpatadorFecha: DateAdapter<Date>,
              private pipeFecha : DatePipe,
              public dialog: MatDialog,
              private snackBar: MatSnackBar,
              private ruteador: Router,
              private spinner: NgxSpinnerService,
              private loading:LoadingService,
              private servicios:MatriculaService,
              private alertaServicio : AlertaService) { 
                this.usuario = JSON.parse(localStorage.getItem('usu_actual'));  
              }

  ngOnDestroy(): void {
    if(this.filtro$){
      console.log("ubsuscribe")
      this.filtro$.unsubscribe()
    }
  }

  ngOnInit(): void {
    
    this.filtro_control = new FormControl(false);
    this.dataSource_bancos_totales = new MatTableDataSource();
    this.traerBancos();
    this.pendienteFiltro();
  }

  ngAfterViewInit(): void {
    this.dataSource_bancos_totales.paginator = this.paginator;
    this.dataSource_bancos_totales.sort = this.sort;
  }

  private pendienteFiltro(){
    this.filtro$ = this.filtro_control.valueChanges.subscribe(resp=>{
      console.log(resp);
      if(resp){
        this.quitarFiltro();
      } 
      else{
        this.filtrarInformacion();
      }
    });
  }

  private traerBancos(){
    this.spinner.show();
    this.loading.loading$.next({opacity:0.5});
    this.servicios.listaBancos().subscribe((resp:any) =>{
      console.log(resp)
      this.bancos_totales = resp.result; 
      this.filtrarInformacion();
      this.dataSource_bancos_totales.data = this.bancos_filtrados; 
      console.log("bancos ",resp);   
      this.spinner.hide(); 
    },(error)=>{
      //this.eventosServicio.hideLoading();
      this.spinner.hide();
      console.info("error bancos: ",error);
    });
  }

  applyFilter(filterValue:string){
    this.dataSource_bancos_totales.filter = filterValue.trim().toLowerCase();
  }

  nuevoBanco(){
    
    const dialogRef = this.dialog.open(BancoFormComponent, {
      data: {
        nombre: "agregar",
        banco : null
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
        if( result==undefined){
          console.log("No se recibe nada");
        }
        else if(result.estado=='EXITO'){
          this.traerBancos();
          this.alertaServicio.creacionCorrecta();

          console.log("Se ha creado el banco");
        }
        else{
          console.log("ha ocurrido un error");
        }
    });
    
  }

  editarBanco(row){
    console.log(row);
    const dialogRef = this.dialog.open(BancoFormComponent, {
      data: {
        nombre: "editar",
        banco : row,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
        if( result==undefined){
          console.log("No se recibe nada");
        }
        else if(result.estado=='EXITO'){
          this.traerBancos();
          this.alertaServicio.actualizacionCorrecta();
          console.log("Se ha editado el banco");
        }
        else{
          console.log("ha ocurrido un error");
        }
    });
    
  }

  private filtrarInformacion(){
    this.bancos_filtrados = [];
    this.bancos_totales.forEach(elemento=>{
      if(elemento.ban_activo == 'Y'){
        this.bancos_filtrados.push(elemento)
      }
    });
    this.dataSource_bancos_totales.data = this.bancos_filtrados;
  }

  chageStateValue(data){
    if(data==='N') return 'Y';
    else if(data==='Y') return 'N';
    else throw 'error';  
  }

  public chageState(row){
    // console.log(row);
    this.spinner.show();
    this.loading.loading$.next({opacity:0.5});
    let body = {
      estadoNuevo: this.chageStateValue(row.ban_activo),
      bancoID: row.ban_id
    }
    console.log(body)
    
    this.servicios.status(body).subscribe((resp:any)=>{
      console.log(resp)
      this.traerBancos();
      this.spinner.hide();
    },(error)=>{
      console.log(error);
      this.spinner.hide();
    });
  }

  private quitarFiltro(){
    this.bancos_filtrados = this.bancos_totales;
    this.dataSource_bancos_totales.data = this.bancos_filtrados;
  }
  sortData(sort:Sort){
    const data = this.bancos_filtrados.slice();
    let arreglo_temporal :any []= [];
    if (!sort.active || sort.direction === '') {
      arreglo_temporal = data;
      this.dataSource_bancos_totales.data = arreglo_temporal;
      return;
    }

    arreglo_temporal = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'codigo': return this.compare(a.ban_id||'', b.ban_id||'', isAsc);
        case 'banco': return this.compare(a.ban_nombre||'', b.ban_nombre||'', isAsc);
        default: return 0;
      }
    });
    this.dataSource_bancos_totales.data = arreglo_temporal;
  }
  private  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
  actualizarLista(){
    this.traerBancos();
  }

}
