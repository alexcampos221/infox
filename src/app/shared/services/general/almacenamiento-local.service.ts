import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AlmacenamientoLocalService {

  constructor() { }

  setItem(name, value){
    localStorage.setItem(name,JSON.stringify(value));
  }

  removeItem(name){
    localStorage.removeItem(name);
  }

  getItem(name){
    if(localStorage.getItem(name)){
      return JSON.parse(localStorage.getItem(name));
    }else{
      return undefined;
    }
  }

  clearLS(){
    localStorage.clear();
  }
  
  jsonToFormData(item){
    var form_data = new FormData();
    for (var key in item ) {
      form_data.append(key, item[key]);
    }
    return form_data;
  }

}
