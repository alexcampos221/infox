import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CajaRoutingModule } from './caja-routing.module';
import { PagosComponent } from './pagos/pagos.component';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSortModule } from '@angular/material/sort';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MaterialFileInputModule } from 'ngx-material-file-input';
import { MatMenuModule } from '@angular/material/menu';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatStepperModule } from '@angular/material/stepper';
import { PendienteDetalleComponent } from './pendiente-detalle/pendiente-detalle.component';
import { EditarCuotaComponent } from './dialogs/editar-cuota/editar-cuota.component';
import { ReporteGeneralComponent } from './reporte-general/reporte-general.component';
import { ReporteDetalleComponent } from './reporte-detalle/reporte-detalle.component';
import { MatriculaCajaComponent } from './dialogs/matricula-caja/matricula-caja.component';
import { AnuladosComponent } from './anulados/anulados.component';
import { FormReenviarCredencialesComponent } from './dialogs/form-reenviar-credenciales/form-reenviar-credenciales.component';
import { SaldosComponent } from './saldos/saldos.component';
import { AprobadosComponent } from './aprobados/aprobados.component';
import { FormEditCursoComponent } from './dialogs/form-edit-curso/form-edit-curso.component';
import { FormUsuariosComponent } from './dialogs/form-usuarios/form-usuarios.component';


@NgModule({
  declarations: [PagosComponent, PendienteDetalleComponent, EditarCuotaComponent, ReporteGeneralComponent, ReporteDetalleComponent, MatriculaCajaComponent, AnuladosComponent, FormReenviarCredencialesComponent, SaldosComponent, AprobadosComponent, FormEditCursoComponent, FormUsuariosComponent],
  imports: [
    CommonModule,
    CommonModule,
    MatButtonToggleModule,
    MatButtonModule,
    MatCheckboxModule,
    FormsModule,
    ReactiveFormsModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTabsModule,

    MatTableModule,
    MatPaginatorModule,
    MatSnackBarModule,
    MatDialogModule,
    MatSortModule,
    MatToolbarModule,
    MatSelectModule,
    MatDatepickerModule,
    MaterialFileInputModule,
    MatMenuModule,
    MatSlideToggleModule,
    MatStepperModule,
    CajaRoutingModule
  ]
})
export class CajaModule { }
