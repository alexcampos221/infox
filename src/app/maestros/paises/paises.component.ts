import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { Subscription } from 'rxjs';
import { AlertaService } from 'src/app/shared/services/general/alerta.service';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { MatriculaService } from 'src/app/shared/services/registros/matricula.service';
import { PaisesService } from '../../shared/services/backend/paises.service'
import {PaisFormComponent} from '../dialogs/pais-form/pais-form.component'
@Component({
  selector: 'app-paises',
  templateUrl: './paises.component.html',
  styleUrls: ['./paises.component.sass']
})
export class PaisesComponent implements OnInit {

  paises_totales : any[] = [];
  paises_filtrados : any[] = [];

  arreglo;

  dataSource_paises_totales : MatTableDataSource<any>;

  //atributos para la tabla material
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;

  displayedColumns = [
    'codigo',
    'pais',
    'tipo',
    'estado',
    'acciones',
  ];

  filtro_control : FormControl;
  filtro$ : Subscription;

  usuario:any;
  constructor(public dialog: MatDialog,
              private spinner: NgxSpinnerService,
              private loading:LoadingService,
              private servicios:MatriculaService,
              private paisesService:PaisesService,
              private alertaServicio : AlertaService) { 
                this.usuario = JSON.parse(localStorage.getItem('usu_actual'));  
              }

  ngOnDestroy(): void {
    if(this.filtro$){
      console.log("ubsuscribe")
      this.filtro$.unsubscribe()
    }
  }

  ngOnInit(): void {
    
    this.filtro_control = new FormControl(false);
    this.dataSource_paises_totales = new MatTableDataSource();
    this.traerPaises();
    this.pendienteFiltro();
  }

  ngAfterViewInit(): void {
    this.dataSource_paises_totales.paginator = this.paginator;
    this.dataSource_paises_totales.sort = this.sort;
  }

  private pendienteFiltro(){
    this.filtro$ = this.filtro_control.valueChanges.subscribe(resp=>{
      console.log(resp);
      if(resp){
        this.quitarFiltro();
      } 
      else{
        this.filtrarInformacion();
      }
    });
  }

  private traerPaises(){
    this.spinner.show();
    this.loading.loading$.next({opacity:0.5});
    this.paisesService.listarPaises().subscribe((resp:any) =>{
      console.log(resp)
      this.paises_totales = resp; 
      this.filtrarInformacion();
      this.dataSource_paises_totales.data = this.paises_filtrados; 
      console.log("paises ",resp);   
      this.spinner.hide(); 
    },(error)=>{
      //this.eventosServicio.hideLoading();
      this.spinner.hide();
      console.info("error bancos: ",error);
    });
  }

  applyFilter(filterValue:string){
    this.dataSource_paises_totales.filter = filterValue.trim().toLowerCase();
  }

  nuevoPais(){
    const dialogRef = this.dialog.open(PaisFormComponent, {
      width:'400px',
      data: {
        nombre: "agregar",
        pais : null
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
        if( result==undefined){
          console.log("No se recibe nada");
        }
        else if(result.estado=='EXITO'){
          this.traerPaises();
          this.alertaServicio.creacionCorrecta();

          console.log("Se ha creado el banco");
        }
        else{
          console.log("ha ocurrido un error");
        }
    });
    
  }

  editarPais(row){
    console.log(row);
    const dialogRef = this.dialog.open(PaisFormComponent, {
      width:'400px',
      data: {
        nombre: "editar",
        pais : row,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
        if( result==undefined){
          console.log("No se recibe nada");
        }
        else if(result.estado=='EXITO'){
          this.traerPaises();
          this.alertaServicio.actualizacionCorrecta();
          console.log("Se ha editado el banco");
        }
        else{
          console.log("ha ocurrido un error");
        }
    });
    
  }

  private filtrarInformacion(){
    this.paises_filtrados = [];
    this.paises_totales.forEach(elemento=>{
      if(elemento.pai_activo == 'Y'){
        this.paises_filtrados.push(elemento)
      }
    });
    this.dataSource_paises_totales.data = this.paises_filtrados;
  }

  chageStateValue(data){
    if(data==='N') return 'Y';
    else if(data==='Y') return 'N';
    else throw 'error';  
  }

  public chageState(row){
    // console.log(row);
    this.spinner.show();
    this.loading.loading$.next({opacity:0.5});
    let body = {
      estadoNuevo: this.chageStateValue(row.pai_activo),
      paisID: row.pai_id
    }
    console.log(body)
    
    this.paisesService.status(body).subscribe((resp:any)=>{
      console.log(resp)
      this.traerPaises();
      this.spinner.hide();
    },(error)=>{
      console.log(error);
      this.spinner.hide();
    });
  }

  private quitarFiltro(){
    this.paises_filtrados = this.paises_totales;
    this.dataSource_paises_totales.data = this.paises_filtrados;
  }

  sortData(sort:Sort){
    const data = this.paises_filtrados.slice();
    let arreglo_temporal :any []= [];
    if (!sort.active || sort.direction === '') {
      arreglo_temporal = data;
      this.dataSource_paises_totales.data = arreglo_temporal;
      return;
    }

    arreglo_temporal = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'codigo': return this.compare(a.ban_id||'', b.ban_id||'', isAsc);
        case 'banco': return this.compare(a.ban_nombre||'', b.ban_nombre||'', isAsc);
        default: return 0;
      }
    });
    this.dataSource_paises_totales.data = arreglo_temporal;
  }
  private  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
  actualizarLista(){
    this.traerPaises();
  }

}
