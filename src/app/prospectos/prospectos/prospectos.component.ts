import { Component, OnInit, ElementRef, AfterViewInit, ViewChild, OnDestroy  } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { MatriculaService } from 'src/app/shared/services/registros/matricula.service';
import { DateAdapter } from '@angular/material/core';
import { ProgramaService } from 'src/app/shared/services/backend/programa.service';
import { PromocionService } from 'src/app/shared/services/backend/promocion.service';
import { SemestreService } from 'src/app/shared/services/backend/semestre.service';
import { UniversidadService } from 'src/app/shared/services/backend/universidad.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { ProspectoFormComponent } from '../dialogs/prospecto-form/prospecto-form.component';
import { AlertaService } from 'src/app/shared/services/general/alerta.service';

@Component({
  selector: 'app-prospectos',
  templateUrl: './prospectos.component.html',
  styleUrls: ['./prospectos.component.sass']
})
export class ProspectosComponent implements OnInit {



  promociones_totales: any[] = [];
  promociones_filtrados: any[] = [];
  arreglo;
  dataSource_promociones_totales: MatTableDataSource<any>;

  //atributos para la tabla material
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;

  /*
  displayedColumns = [
    'proNombre',
    'programa',
    'proCode',
    'proDescripcion',
    'periodo',
    'acciones'
  ];
  */
  displayedColumns = [
    'dni',
    'telefono',
    'correo',
    'activo',
    'matriculado',
    'universidad',
    'otros',
  ];


  promociones_procesados: any[] = [];

  filtro_control: FormControl;
  filtro$: Subscription;





  formBusqueda : FormGroup;
  universidades_lista : any[] = [];
  promociones_lista : any[] = [];
  programas_lista : any[]  = [];
  semestres_lista : any[] = [];

  constructor(private fb : FormBuilder,
              private dateAdapter: DateAdapter<Date>,
              private programaServicio : ProgramaService,
              private promocionServicio:PromocionService,
              private semestreServicio: SemestreService,
              public dialog: MatDialog,
              private alertaServicio: AlertaService,
              private universidadServicio : UniversidadService) { 
                this.dateAdapter.setLocale('es-PE');
              }

  ngOnInit(): void {
    this.dataSource_promociones_totales = new MatTableDataSource();
    this.filtro_control = new FormControl(false);
    this.crearFormulario();
    this.traerUniversidades();
    this.traerProgramas();
    this.traerSemestres();
    this.traerPromociones();
  }

  ngAfterViewInit(): void {
    this.paginator._intl.itemsPerPageLabel = "Registros por página";
    this.dataSource_promociones_totales.paginator = this.paginator;
    this.dataSource_promociones_totales.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource_promociones_totales.filter = filterValue.trim().toLowerCase();
  }

  private crearFormulario(){
    this.formBusqueda = this.fb.group({
      desde : ['',[]],
      hacia : ['',[]],
      universidad : ['',[]],
      programa : ['',[]],
      promocion : ['',[]],
      semestre : ['',[]],
      dni : ['',[]],
      nombre : ['',[]]
    });
  }

  private traerUniversidades(){
    this.universidadServicio.listaUniversidades().subscribe((resp:any[]) =>{
      this.universidades_lista = resp;  
      console.log("universidades ",resp);
    },(error)=>{
      console.info("error universidades: ",error);
    });
  }

  private traerSemestres(){
    this.semestreServicio.listaSemestres().subscribe((resp:any) =>{
      this.semestres_lista = resp;       
      console.log("semestres ",resp);
    },(error)=>{
      console.info("error semestres: ",error);
    });
  }

  private traerProgramas(){

    this.programaServicio.listaProgramas().subscribe((resp:any) =>{

      this.programas_lista = resp; 
      console.log("programas ",resp);
    },(error)=>{
      console.info("error programas: ",error);
    });
  }

  private traerPromociones() {
    this.promocionServicio.listaPromociones().subscribe((resp: any) => {
      this.promociones_lista = resp;
      console.log("promociones ", resp);
    }, (error) => {
      console.info("error promociones: ", error);
    });
  }

  EndDateChange($event){
    console.log(event);
  }

  nuevoProspecto(){
    
    const dialogRef = this.dialog.open(ProspectoFormComponent, {
      data: {
        nombre: "editar",
        prospecto: 'row',
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result == undefined) {
        console.log("No se recibe nada");
      }
      else if (result.estado == 'EXITO') {
        this.alertaServicio.actualizacionCorrecta();
        this.traerPromociones();
      }
      else {
        console.log("ha ocurrido un error");
      }
    });
  }

  createPDF(){
    
  }

}

