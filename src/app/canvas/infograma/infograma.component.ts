import { Component, ElementRef, OnInit } from '@angular/core';
import { Objeto } from '../models/objeto';
// import { FlowerCenter } from './models/flower-center';
import { Item } from '../models/item';
import { Point } from '../models/points';

@Component({
  selector: 'app-infograma',
  templateUrl: './infograma.component.html',
  styleUrls: ['./infograma.component.sass']
})
export class InfogramaComponent implements OnInit {

  poligono: any [] = [
    {
      nombre:'canvas 1',
      color:'Y',
    },
    {
      nombre:'canvas 2',
      color:'N',
    },
    // {
    //   nombre:'canvas3',
    //   color:'Y',
    // },
    // {
    //   nombre:'canvas4',
    //   color:'Y',
    // },
    // {
    //   nombre:'canvas5',
    //   color:'N',
    // },
    // {
    //   nombre:'canvas6',
    //   color:'N',
    // },
  ]

  trackByFn(index:number){
    return (index);
  }

  constructor(private el:ElementRef,) { }

  ngOnInit(): void {
    
    // const canvas = <HTMLCanvasElement>document.getElementById('canvas');
    // const context = canvas.getContext('2d');

    // const petal = new Item(new Point(200, 250), 250, 1.2, 85, '#ff1493');
    // // const center = new FlowerCenter(new Point(75, 80), 20, '#ff5a02');
    // // const flower = new Flower(center, 4, petal);
    // const flower = new Objeto(6, petal);

    // // first flower
    // // flower.draw(context);

    // // second flower
    // context.save();
    // context.translate(140, 120);

    // // shadow effects
    // context.shadowBlur = 5;
    // context.shadowOffsetX = 2;
    // context.shadowOffsetY = 2;
    // context.shadowColor = '#333';
    // context.globalAlpha = 0.85;

    // flower.draw(context);
    // context.restore();
  }
  ngAfterViewInit() : void{

    for (let index = 0; index < this.poligono.length; index++) {
      let canvas = <HTMLCanvasElement>this.el.nativeElement.querySelector('#canvas-' + index)
      if (canvas.getContext) {
        var ctx = canvas.getContext('2d');

        // Quadratric curves example
        ctx.beginPath();
        ctx.moveTo(75,20);
        ctx.quadraticCurveTo(35,15,25,50);
        ctx.quadraticCurveTo(15,100,70,100);
        ctx.quadraticCurveTo(75,100,80,110);
        ctx.quadraticCurveTo(75,100,85,120);
        ctx.quadraticCurveTo(85,100,120,100);
        ctx.quadraticCurveTo(180,110,180,50);
        ctx.quadraticCurveTo(180,15,70,20);
        ctx.stroke();
      }
    }
  }
  
  selectCanvas(i){
    console.log(i)
  }

}
