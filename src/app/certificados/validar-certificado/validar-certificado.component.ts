import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { MatriculaService } from 'src/app/shared/services/registros/matricula.service';
import { DateAdapter } from '@angular/material/core';
import { CertificadoService } from 'src/app/shared/services/backend/certificado.service';
import { AlertaService } from 'src/app/shared/services/general/alerta.service';

@Component({
  selector: 'app-validar-certificado',
  templateUrl: './validar-certificado.component.html',
  styleUrls: ['./validar-certificado.component.sass']
})
export class ValidarCertificadoComponent implements OnInit {

  codigo: any;
  formMatricula: FormGroup;
  

  universidades_lista: any[] = [];
  bancos_lista: any[] = [];
  programas_cursos_lista: any[] = [];
  programas_procesados: any[] = [];

  estado : boolean = false;
  valido : boolean = false;
  enviando : boolean = false;

  respuestaForm : FormGroup;
  envioForm : FormGroup;

  url_multimedia ='https://drive.google.com/file/d/1dlwxWDTyTtPYvbTx0Iz2FNbjtYXKBKpB/view';

  titulo = 'Validar Certificado';
  respuesta:any = {};
  constructor(private rutaActiva: ActivatedRoute,
              private dateAdapter: DateAdapter<Date>,
              private fb: FormBuilder,
              private matriculaServicio: MatriculaService,
              private certificadoServicio:CertificadoService,
              private alertaServicio:AlertaService) {
              this.dateAdapter.setLocale('es-PE');
  }

  ngOnInit(): void {
   
    this.formularioEnvio();
    //this.traerUniversidades();
    //this.listaProgramaCursos();
    //this.listaBancos();
    //this.formularioRespuesta();
  }

  private formularioEnvio(){
    this.envioForm = this.fb.group({
      dni :['',[Validators.required]],
      numero : ['',[Validators.required]]
    });
  }

  private crearFormulario() {
    this.formMatricula = this.fb.group({
      tipo_doc: ['', [Validators.required]],
      documento: ['', [Validators.required]],
      nombres: ['', [Validators.required]],
      correo: ['', [Validators.required, Validators.email]],
      telefono: ['', [Validators.required]],
      universidad: ['', [Validators.required]],
      programa: ['', [Validators.required]],
      banco: ['', [Validators.required]],
      operacion: ['', [Validators.required]],
      monto: ['', [Validators.required]],
      fecha: ['', [Validators.required]],
      imagen: ['', [Validators.required]],
      codigo_promo: ['', []],
      codigo_referido: ['', []],
      codigo_empleado: [this.codigo, [Validators.required]],
      otros: ['', []],
      nombre_referido: ['', []],
    });
  }

  enviarFormulario() {
    //console.log(this.formMatricula.value);

    let programa = this.formMatricula.controls.programa.value.pro_id;
    let curso = this.formMatricula.controls.programa.value;
    console.log(curso);

    let body = {
      txtDni: this.formMatricula.controls.documento.value,
      txtNombres: this.formMatricula.controls.nombres.value,
      txtEmail: this.formMatricula.controls.correo.value,
      txtTelefono: this.formMatricula.controls.telefono.value,
      universidadID: this.formMatricula.controls.universidad.value.uni_id,
      programaID: programa,
      cursoID: '',
      txtOtros: this.formMatricula.controls.otros.value,
      bancoID: this.formMatricula.controls.banco.value.ban_id,
      txtNumOperacion: this.formMatricula.controls.operacion.value,
      txtMonto: this.formMatricula.controls.monto.value,
      txtCodPromocion: this.formMatricula.controls.codigo_promo.value,
      codReferido: this.formMatricula.controls.codigo_empleado.value,
    };


    console.log(body);

  }

  get f (){
    return this.envioForm.controls;
  }
  limipiarEspaciosBlanco(txt:string){
    return txt.replace(/\s{2,}/g, ' ').trim()
  }
  enviarCertificado(){
    this.enviando = true;
    let codigo = this.limipiarEspaciosBlanco(this.f.numero.value);
    let dni = this.limipiarEspaciosBlanco(this.f.dni.value) ;
    
    let body = {
      codigoCertificado : codigo,
      dniAlumno : dni
    }
    console.log(body);
    this.certificadoServicio.validarCertificado(body).subscribe((resp:any)=>{
      console.log(resp);
      this.enviando = false;
      if(resp?.cer_alumno_nombre){
        console.log("exito");
        this.respuesta = resp;
        this.respuestaValida();
      }
      else{
        console.log("error");
        this.respuestaInvalida();
      }
    },(error)=>{
      console.log(error);
      this.cancelarOperacion();
      this.alertaServicio.algoHaIdoMal();
      this.enviando = false;
    });
  }

  private respuestaValida(){
    this.titulo = 'Certificado Válido';
    this.valido = true;
    this.estado = true;
  }

  private respuestaInvalida(){
    this.estado = true;
    this.valido=false;
    this.titulo = 'Volver a Intentar';
  }

  private traerUniversidades() {
    this.matriculaServicio.listaUniversidades().subscribe((resp: any) => {
      console.log(resp);
      this.universidades_lista = resp['result'];
    });
  }

  private listaProgramaCursos() {

    this.matriculaServicio.listaProgramaCursos().subscribe((resp: any) => {
      //console.log(resp);
      this.programas_cursos_lista = resp['result'];
      this.procesarProgramas();
    });
  }

  private listaBancos() {
    this.matriculaServicio.listaBancos().subscribe((resp: any) => {
      this.bancos_lista = resp['result'];
    });
  }


  private procesarProgramas() {
    let arreglo_salida: any[] = [];
    //console.log(this.programas_cursos_lista); 
    this.programas_cursos_lista.forEach(elemento => {

      let objeto_programa = {
        pro_id: elemento.pro_id,
        nombre: elemento.pro_nombre,
        cur_id: '',
        tipo: 'P'
      }

      arreglo_salida.push(objeto_programa);

      elemento.pro_cursos.forEach(curso => {
        let objeto = {
          pro_id: elemento.pro_id,
          cur_id: curso.cur_id,
          nombre: curso.cur_nombre,
          tipo: 'C'
        }
        arreglo_salida.push(objeto);
      });
    });
    this.programas_procesados = arreglo_salida;
  }

  private formularioRespuesta(){
    this.respuestaForm = this.fb.group({
      programa : ['',[]],
      curso : ['',[]],
      fecha:['',[]],
    });
  }

  cancelarOperacion(){
    this.estado = false;
    this.valido = false;
    this.titulo = 'Validar Certificado';
    this.envioForm.reset();
    this.formularioEnvio();
  }

  obtenerFecha(cad:string){
    return cad.substr(0,10);
  }

}
