import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Subscription } from 'rxjs';
import { UsuarioService } from 'src/app/shared/services/backend/usuario.service';
import { AlertaService } from 'src/app/shared/services/general/alerta.service';
import { ExcelService } from 'src/app/shared/services/general/excel.service';
import { UtlitariosService } from 'src/app/shared/services/general/utlitarios.service';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { Utils } from 'src/app/shared/utils';

@Component({
  selector: 'app-equipo',
  templateUrl: './equipo.component.html',
  styleUrls: ['./equipo.component.sass'],
  providers: [DatePipe]
})
export class EquipoComponent implements OnInit {
  codigo:any;
  usuario: any;
  ListaPagoPendientes_totales: any[] = [];
  promociones_filtrados: any[] = [];

  DSlistaPagosPendientes: MatTableDataSource<any>;

  //atributos para la tabla material
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  //@ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;

  Utils: Utils;
  
  displayedColumns = [
    'infoPago',
    'fecha',  
    'vendedor',
    'nombre',
    'dni',
    'telefono',
    'programa',
    'curso',
  ];

  promociones_procesados: any[] = [];
  lista_vendedores: any[] = [];

  filtro_control: FormControl;
  filtro$: Subscription;

  formdata : FormGroup;

  maximo : Date = new Date();
  date_hoy : Date;

  isEstado:boolean;
  
  constructor(
    private fb : FormBuilder,
    public dialog: MatDialog,
    private alertaServicio : AlertaService,
    private ruteador : Router,
    private utilitarios: UtlitariosService,
    private rutaActiva: ActivatedRoute,
    private loadingService: LoadingService,
    private dateAdapter:DateAdapter<Date>,
    private spinner: NgxSpinnerService,
    private datePipe: DatePipe,
    private excel:ExcelService,
    private usuarioService:UsuarioService,
  ) { 
    this.dateAdapter.setLocale('es-PE'); 
  }

  private crearFormulario(){
    this.formdata = this.fb.group({
      id:['',[Validators.required]],
      fecha:['',[Validators.required]],
      nombre:['',[Validators.required],],
      banco:['',[Validators.required],],
      url:['',[Validators.required]],
      monto:['',[Validators.required]],
      dni: ['',[Validators.required]],
      telefono: ['',[Validators.required]], 
      estado: ['',[Validators.required]], 
      desde:[Utils.primerdiaMes(),[]],
      hasta:[new Date(),[]],
      vendedor:[0,[]]
    });
  }
  private get f (){
    return this.formdata.controls;
  }
  txtNuevo:string = '';
  txtFiltro:String ;
  
  ngOnInit(): void {
    this.obtenerAsesor();
    this.crearFormulario();
    this.DSlistaPagosPendientes = new MatTableDataSource();
    this.traerVendedores();
    this.traerListaEquipo();
    // this.traerListaConfirmados();
    if(JSON.parse(localStorage.getItem('cerrar_detalle')) == true){
     
      this.txtFiltro = JSON.stringify(localStorage.getItem("txtFiltro")); 
      var reg= /"/gi
      this.txtNuevo = this.txtFiltro.replace(reg,'');
      this.applyFilter(this.txtNuevo);  
    }
  }
  ngAfterViewInit(): void {
    this.paginator._intl.itemsPerPageLabel = "Registros por página";
    this.DSlistaPagosPendientes.paginator = this.paginator;
    this.paginator._intl.getRangeLabel = (page: number, pageSize: number, length: number)=>{
      if (length == 0 || pageSize == 0) {
        return `0 de ${length}`;
      }
      const startIndex = page * pageSize;

      return `${startIndex+1} de ${length}`
    };
    this.DSlistaPagosPendientes.sort = this.sort;
  }
  private obtenerAsesor(){
    this.usuario = JSON.parse(localStorage.getItem('usu_actual'));    
    this.codigo=this.usuario.usu_id;
  }

  traerListaEquipo(){
    let salida = {
      // usuarioID : this.codigo,
      vendedorID: 0,//this.f.vendedor.value.usu_id,
      desde: this.datePipe.transform(this.f.desde.value,'yyyy-MM-dd'),
      hasta: this.datePipe.transform(this.f.hasta.value,'yyyy-MM-dd')
    };
    console.log(salida);
    this.spinner.show();
    this.loadingService.loading$.next({message:'', opacity:0.5});
    this.utilitarios.getListaPagosAprobadosByVendor(JSON.stringify(salida)).subscribe((resp:any)=>{
      console.log(resp);
      this.ListaPagoPendientes_totales = resp;
      this.formdata.controls.estado.setValue(resp.validado_estado);
      if(this.formdata.controls.estado.value=="Pendiente"){
        this.isEstado=true;
      }
     this.DSlistaPagosPendientes.data = this.ListaPagoPendientes_totales;    
     this.spinner.hide();
    },(error)=>{
      this.spinner.hide();
    });
  }
  
  traerVendedores(){
    let salida={
      tipo: '0'
    }
    this.loadingService.loading$.next({opacity:0.5});
    this.spinner.show();
    this.usuarioService.listarVendors(JSON.stringify(salida)).subscribe((resp : any)=>{
      console.log(resp);
      this.lista_vendedores = resp || [];
      // this.lista_vendedores = this.lista_vendedores.filter((m) => m.usu_activo == 'Y')
      // this.lista_vendedores = this.lista_vendedores.filter((m) => m.usu_tipo == '000')
      this.spinner.hide();
    },(error) => {
      console.log(error);
      this.spinner.hide();
    })
  }

  applyFilter(filterValue: string){
    this.DSlistaPagosPendientes.filter = filterValue.trim().toLowerCase() || '';
  }
  arreglo_temporal:any[] = []
  sortData(sort: Sort){
    const data = this.ListaPagoPendientes_totales.slice();
    console.log(data);
    this.arreglo_temporal = [];
    if (!sort.active || sort.direction === '') {
      this.arreglo_temporal = data;
      this.DSlistaPagosPendientes.data = this.arreglo_temporal;
      return;
    }
  
      this.arreglo_temporal = data.sort((a, b) => {
        console.log(this.ListaPagoPendientes_totales)
        const isAsc = sort.direction === 'asc';
        switch (sort.active) {
          case 'vendedor': return this.compare(a.usu_vendedor||'', b.usu_vendedor||'', isAsc);
          case 'nombre': return this.compare(a.pag_nombre||'', b.pag_nombre||'', isAsc);
          case 'dni': return this.compare(a.pag_dni||'', b.pag_dni||'', isAsc);
          case 'telefono': return this.compare(a.pag_telefono||'', b.pag_telefono||'', isAsc);
          case 'programa': return this.compare(a.pro_nombre||0, b.pro_nombre||0, isAsc);
          case 'curso': return this.compare(a.cur_nombre||0, b.cur_nombre||0, isAsc);
          case 'fecha': return this.compare(new Date(a.pag_fecha_abono),new Date(a.pag_fecha_abono), isAsc);
      
          default: return 0;
        }
    });
  
    this.DSlistaPagosPendientes.data = this.arreglo_temporal;
  }
  
  private compare(a: number | string | Date, b: number | string | Date, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
  EndDateChange(evento){
    console.log(evento);
  }

  filtrarByFechas(){
    let salida = {
      // usuarioID : this.codigo,
      vendedorID:this.f.vendedor.value.usu_id,
      desde: this.datePipe.transform(this.f.desde.value,'yyyy-MM-dd'),
      hasta: this.datePipe.transform(this.f.hasta.value,'yyyy-MM-dd')
    };
    console.log(salida);
    this.spinner.show();
    this.loadingService.loading$.next({message:'', opacity:0.5});
    if(this.f.desde.value > this.f.hasta.value){
      this.alertaServicio.FechaEsMayor();
      this.spinner.hide();
    }else{
      this.utilitarios.getListaPagosAprobadosByVendor(JSON.stringify(salida)).subscribe((resp:any)=>{
        console.log(resp);
        this.ListaPagoPendientes_totales = resp;
        this.formdata.controls.estado.setValue(resp.validado_estado);
        if(this.formdata.controls.estado.value=="Pendiente"){
          this.isEstado=true;
        }
        this.DSlistaPagosPendientes.data = this.ListaPagoPendientes_totales;
        this.spinner.hide();
      },(error)=>{
        this.spinner.hide();
      });
    }
  }

  arrayPagosID: any[] = [];
  detallePendiente(row,index){
    console.log(row,index);
    if(this.DSlistaPagosPendientes.filter == ''){
      for(let index = 0; index < this.ListaPagoPendientes_totales.length; index++) {
        this.arrayPagosID.push(this.ListaPagoPendientes_totales[index].pag_id);      
      }  
    }else{
      for(let index = 0; index < this.DSlistaPagosPendientes.filteredData.length; index++) {
        this.arrayPagosID.push(this.DSlistaPagosPendientes.filteredData[index].pag_id);      
      }     
    }
    
    let salida={
      pagoID:row.pag_id,
      ventana:'Confirmados'
    }

    localStorage.setItem('pago',row.pag_id);  
    localStorage.setItem('indice',index);    

    localStorage.setItem('txtFiltro',this.DSlistaPagosPendientes.filter);

    localStorage.setItem('arrayListaPendiente',JSON.stringify(this.arrayPagosID));
    this.ruteador.navigate(['ventas/detalle-equipo'] ,{ state : {data: salida } });
  }
  
  downloadFileExcel(){

  }
  
  selectedTipo(event){   
    // this.f.tipo.setValue(event)
    console.log(event);
  }

}
