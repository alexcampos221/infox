import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CanvasComponent } from './canvas/canvas.component'
import { InfogramaComponent } from './infograma/infograma.component';
const routes: Routes = [
  // {
  //   path : '',
  //   component : CanvasComponent
  // },
  {
    path : '',
    component : InfogramaComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CanvasRoutingModule { }
