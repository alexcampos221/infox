import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProgramaService } from 'src/app/shared/services/backend/programa.service';
import { PromocionService } from 'src/app/shared/services/backend/promocion.service';
import { DateAdapter } from '@angular/material/core';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-prospecto-form',
  templateUrl: './prospecto-form.component.html',
  styleUrls: ['./prospecto-form.component.sass'],
  providers : [DatePipe],
})
export class ProspectoFormComponent implements OnInit {

  prospectoForm: FormGroup;
  editar: boolean = false;
  nombre_modal: string;
  programas : any[] = [];

  constructor(private programaServicio: ProgramaService,
              private promocionServicio: PromocionService,
              private fb: FormBuilder,
              public dialogRef: MatDialogRef<ProspectoFormComponent>,
              private dateAdapter : DateAdapter<Date>,
              private datePipe: DatePipe,
              @Inject(MAT_DIALOG_DATA) public data: any,){ 
                this.dateAdapter.setLocale('es-PE');
              }

  ngOnInit(): void {
    this.crearFormulario();
  }

  private crearFormulario() {
    this.prospectoForm = this.fb.group({
      nombre: ['', [Validators.required]],
      desde: ['', []],
      hacia: ['', []],
      programa : ['', [Validators.required]],
      codigo_pro : ['', [Validators.required]],      
      descripcion : ['', [Validators.required]],
      dni : ['',[]],
    });
  }

  closeDialog(): void {
    this.dialogRef.close();
  }

  enviarFormulario() {
    console.log(this.prospectoForm.value);
  }
    
}
