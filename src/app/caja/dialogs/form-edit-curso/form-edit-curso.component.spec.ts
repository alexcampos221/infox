import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormEditCursoComponent } from './form-edit-curso.component';

describe('FormEditCursoComponent', () => {
  let component: FormEditCursoComponent;
  let fixture: ComponentFixture<FormEditCursoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormEditCursoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormEditCursoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
