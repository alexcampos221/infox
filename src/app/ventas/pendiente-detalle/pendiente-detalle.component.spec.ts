import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PendienteDetalleComponent } from './pendiente-detalle.component';

describe('PendienteDetalleComponent', () => {
  let component: PendienteDetalleComponent;
  let fixture: ComponentFixture<PendienteDetalleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PendienteDetalleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PendienteDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
