import { ParsedHostBindings } from '@angular/compiler';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartComponent, ApexAxisChartSeries, ApexChart, ApexXAxis, ApexDataLabels, ApexTooltip, ApexYAxis, ApexStroke, ApexLegend, ApexMarkers, ApexGrid, ApexFill, ApexTitleSubtitle, ApexNonAxisChartSeries, ApexResponsive } from 'ng-apexcharts';
import { areaChartOptions, barChartOptions } from 'src/app/admin/dashboard/main/main.component';
import { ProgramaService } from 'src/app/shared/services/backend/programa.service';
import { DatePipe } from '@angular/common';
import { DateAdapter } from '@angular/material/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { EChartOption } from 'echarts';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { AlmacenamientoLocalService } from 'src/app/shared/services/general/almacenamiento-local.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass'],
  providers:[DatePipe]
})
export class DashboardComponent implements OnInit {

  public areaChartOptions: Partial<areaChartOptions>;
  public vendedores_mes : EChartOption;

  formSearch : FormGroup;
  formAnio : FormGroup;
  
  date_hoy = new Date();

  estado:string;
  porcentaje :number;

  //graficos de meses
  arreglo_vendedores :any[] = [];
  paleta_colores = [    
    '#138D75',
    '#F7B105',
    '#F8FF00',
    '#00F5FF',
    '#9400FF',
    '#FF9200',
    '#656561',
    '#FF1700',
  ];
  usuario_actual : any;

  constructor(private programaService:ProgramaService,
              private datePipe: DatePipe,
              private dateAdapter: DateAdapter<Date>,
              private fb:FormBuilder,
              private localServicio : AlmacenamientoLocalService,
              ) 
              {
      this.dateAdapter.setLocale('es-PE');
    }

  public chartGauge1: any;
  line_bar_chart: EChartOption
  // public chartBar1 : EChartOption;
  // public chartBar2 : EChartOption;
  // public chartBar3 : EChartOption;
  

  public info_cards : any [] = [];
  public info_chart_mes : any [] = [];
  public info_chart_anio : any [] = [];
  public infoCard : any;
  anios_lista:any[]=[]

  barraPorcentaje:any;

  ngOnInit() {    
    this.usuario_actual = this.localServicio.getItem('usu_actual');
    this.traerInfoCharts();   
    this.createForm();
    this.createForm2();
    this.traerAnios();
    this.f.desde.setValue( new Date(this.date_hoy.getFullYear(), this.date_hoy.getMonth(), 1));
    this.f.hasta.setValue(new Date());
  }
  createForm(){
    this.formSearch = this.fb.group({
      desde:['',[]],
      hasta:['',[]],     
    })
  }
  createForm2(){
    this.formAnio = this.fb.group({      
      anios:['',[]],
    })
  }

  private get f (){
    return this.formSearch.controls;
  }
  traerAnios(){    
    this.programaService.listarAnios().subscribe((resp:any[])=>{
      console.log(resp);
      this.anios_lista = resp;
      for (let index = 0; index < this.anios_lista.length; index++) {
        if(this.anios_lista[index].anio_nombre == (new Date().getFullYear()).toString()){
          this.formAnio.controls.anios.setValue(this.anios_lista[index]);
          console.log(this.formAnio.controls.anios.value)
          break;
        }  else{
          console.log('else')
        }
      }
      },(error)=>{
        console.log(error)
      })
  }
  selectAnio(event){
    this.formAnio.controls.anios.setValue(event);
  }
  realizarBusqueda(){
    let vendedorID = JSON.parse(localStorage.getItem('usu_actual')).usu_id;
    let salida = {
      desde: this.datePipe.transform(this.f.desde.value,'yyyy-MM-dd'),
      hasta: this.datePipe.transform(this.f.hasta.value,'yyyy-MM-dd'),
      vendedorID: Number(vendedorID),
      tipo: this.usuario_actual.usu_tipo
    }
    console.log(salida)
    this.programaService.realizarBusqueda(JSON.stringify(salida)).subscribe((resp:any)=>{
      console.log(resp)
      this.info_chart_mes = resp.individual || []; 
      console.log("eeeeeeee ",this.info_chart_mes);  

      this.arreglo_vendedores = resp.grupal ||[];
      this.actualizarVendedores();
      this.chart1();
    })
  }
  busquedaAnio(){
    let vendedorID = JSON.parse(localStorage.getItem('usu_actual')).usu_id;
    let anio = this.formAnio.controls.anios.value
    let salida = {
      anioID: anio.anio_id,
      vendedorID: Number(vendedorID),
    }
    console.log(salida)
    this.programaService.realizarBusquedaAnio(JSON.stringify(salida)).subscribe((resp:any)=>{
      console.log(resp)
      this.info_chart_anio = resp || []; 
      console.log(this.info_chart_anio);
      this.chart5();
    })
  }

  EndDateChange(evento){
    console.log(evento);
  }
  public traerInfoCharts(){    
    let vendedorID = JSON.parse(localStorage.getItem('usu_actual')).usu_id;
    let desde = new Date(this.date_hoy.getFullYear(), this.date_hoy.getMonth(), 1)
    let salida = {
      vendedorID: vendedorID,
      desde: this.datePipe.transform(desde,'yyyy-MM-dd'),
      hasta: this.datePipe.transform(this.date_hoy,'yyyy-MM-dd'),
      tipo: this.usuario_actual.usu_tipo
    }
    console.log(salida);
    let body=JSON.stringify(salida);
    this.programaService.listarInfoCards(body).subscribe((resp:any[]) =>{
      console.log(resp)
      this.info_cards = resp['info'] || [];
      this.info_chart_mes = resp['serie_mes'] || [];
      this.info_chart_anio = resp['serie_anio'] || [];
      this.infoCard= this.info_cards;
      if(this.infoCard.meta_mes == 0){
        this.porcentaje = 100
      }else{
        this.porcentaje = Math.round((this.infoCard.venta_mes /this.infoCard.meta_mes )*100) || 0;
      }
      
      this.barraPorcentaje = this.porcentaje;
      console.log(this.porcentaje)
      if(this.barraPorcentaje > 100) {this.barraPorcentaje = 100;}
      
      if(this.porcentaje > 66){
        this.estado='success'
      }else if(this.porcentaje > 33){
        this.estado='warning'
      }else{
        this.estado='danger'
      }
      this.chart1();
      // this.chart2();
      this.chart3();
      this.chart5();
      // this.chart4();

      this.arreglo_vendedores = resp['serie_vendedores'] || [];
      this.actualizarVendedores();

    },(error)=>{
      console.log(error);
    });
      

  }
  private chart5(){
    let data_data: any [] = [];
    let data_mes: any [] = [];
    let data_promedio: any [] = [];
    this.info_chart_anio.forEach(elemento=>{
      let meses = elemento.MES
      let proVenta = elemento.meta
      let data = elemento.total_ventas;
      data_data.push(data);
      data_mes.push(meses);
      data_promedio.push(proVenta)
    });
    this.line_bar_chart = {
      grid: {
        containLabel: true,
        top: '6',
        right: '0',
        bottom: '17',
        left: '25'
      },
      xAxis: {
        data: data_mes,
        axisLine: {
          lineStyle: {
            color: '#eaeaea'
          }
        },
        axisLabel: {
          fontSize: 10,
          color: '#9aa0ac'
        }
      },
      tooltip: {
        show: true,
        showContent: true,
        alwaysShowContent: false,
        triggerOn: 'mousemove',
        trigger: 'axis'
      },
      yAxis: {
        splitLine: {
          lineStyle: {
            color: '#eaeaea'
          }
        },
        axisLine: {
          lineStyle: {
            color: '#eaeaea'
          }
        },
        axisLabel: {         
          color: '#9aa0ac'
        },
        
      },
      series: [
        {
          name: 'Ventas',
          type: 'bar',
          data: data_data,
        },
        {
          name: 'Metas',
          type: 'line',
          smooth: true,
          lineStyle: {
            width: 4,
            shadowColor: 'rgba(0,0,0,0.4)',
            shadowBlur: 10,
            shadowOffsetY: 10
          },
          
          data: data_promedio,
          symbolSize: 10,
        },
        
      ],
      color: ['#9f78ff', '#E74C3C', '#F6A025']
    };
  }
  private chart1() {
    let data_data: any [] = [];
    let data_dia: any [] = [];
    let data_promedio: any [] = [];
    this.info_chart_mes.forEach(elemento=>{
      let dia = elemento.dia_l
      let promVenta = elemento.meta_dia
      let data = elemento.venta_dia;
      data_data.push(data);
      data_dia.push(dia);
      data_promedio.push(promVenta)
    });

    this.areaChartOptions = {
      series: [
        {
          name: 'Venta',
          data: data_data,
        },
        {
          name: 'Meta',
          data: data_promedio,
        },
      ],
      chart: {
        height: 350,
        type: 'area',
        toolbar: {
          show: false,
        },
        foreColor: '#9aa0ac',
      },
      colors: ['#9F8DF1', '#E74C3C'],
      dataLabels: {
        enabled: false,
      },
      stroke: {
        curve: 'smooth',
      },
      xaxis: {
        type: 'category',
        categories: data_dia
      },
      legend: {
        show: true,
        position: 'top',
        horizontalAlign: 'center',
        offsetX: 0,
        offsetY: 0,
      },

      // tooltip: {
      //   x: {
      //     format: 'dd/MM/yy HH:mm',
      //   },
      // },
    };
  }


  // public chart4(){
  //   const colors = ['#5470C6', '#91CC75', '#EE6666'];
  //   let data_data: any [] = [];
  //   let data_mes: any [] = [];
  //   let data_promedio: any [] = [];
  //   this.info_chart_anio.forEach(elemento=>{
  //     let meses = elemento.MES
  //     let proVenta = elemento.meta
  //     let data = elemento.total_ventas;
  //     data_data.push(data);
  //     data_mes.push(meses);
  //     data_promedio.push(proVenta)
  //   });
  //   this.chartBar3 = {
  //     color: colors,

  //     tooltip: {
  //       trigger: 'axis',
  //       axisPointer: {
  //         type: 'cross'
  //       }
  //     },
  //     grid: {
  //       right: '5%'
  //     },
  //     toolbox: {
  //       feature: {
  //         dataView: { show: false, readOnly: false },
  //         restore: { show: false },
  //         saveAsImage: { show: true }
  //       }
  //     },
  //     legend: {
  //       data: ['Meta', 'Ventas', '']
  //     },
  //     xAxis: [
  //       {
  //         type: 'category',
  //         axisTick: {
  //           alignWithLabel: true
  //         },
  //         // prettier-ignore
  //         data: data_mes
  //       }
  //     ],
  //     yAxis: [
  //       {
  //         type: 'value',
  //         name: '',
  //         show:false,
  //         position: 'right',
  //         axisLine: {
  //           show: false,
  //           lineStyle: {
  //             color: colors[0]
  //           }
  //         },
  //         axisLabel: {
  //           formatter: '{value}'
  //         }
  //       },
  //       {
  //         type: 'value',
  //         name: '',
  //         show:false,
  //         position: 'right',
  //         offset: 80,
  //         axisLine: {
  //           show: false,
  //           lineStyle: {
  //             color: colors[1]
  //           }
  //         },
  //         axisLabel: {
  //           formatter: '{value} ml'
  //         }
  //       },
  //       {
  //         type: 'value',
  //         name: '', 
  //         min : 0,
  //         max : 100000,    
  //         position: 'left',
  //         axisLine: {
  //           show: true,
  //           lineStyle: {
  //             color: colors[2]
  //           }
  //         },
  //         axisLabel: {
  //           formatter: '{value}'
  //         }
  //       }
  //     ],
  //     series: [
  //       {
  //         name: 'Ventas',
  //         type: 'bar',
  //         data: data_data
  //       },
  //       {
  //         name: 'Meta',
  //         type: 'line',
  //         yAxisIndex: 2,
  //         data: data_promedio
  //       }
  //     ]
  //   };
  // }

  // public chart2(){
  //   let data_data: any [] = [];
  //   let data_mes: any [] = [];
  //   let data_promedio: any [] = [];
  //   this.info_chart_anio.forEach(elemento=>{
  //     let meses = elemento.MES
  //     let proVenta = elemento.meta
  //     let data = elemento.total_ventas;
  //     data_data.push(data);
  //     data_mes.push(meses);
  //   });
  //   console.log(data_data)
  //   console.log(data_mes)
  //   let chart2 = {
  //     tooltip: {
  //       trigger: 'axis',
  //       axisPointer: {
  //         type: 'shadow'
  //       }
  //     },
  //     grid: {
  //       left: '3%',
  //       right: '4%',
  //       bottom: '3%',
  //       containLabel: true
  //     },
  //     xAxis: [
  //       {
  //         type: 'category',
  //         data: data_mes,
  //         axisTick: {
  //           alignWithLabel: true
  //         }
  //       }
  //     ],
  //     yAxis: [
  //       {
  //         type: 'value'
  //       }
  //     ],
  //     series: [
  //       {
  //         name: 'Venta',
  //         type: 'bar',
  //         barWidth: '60%',
  //         data: data_data
  //       },        
  //     ]
  //   };
  //   this.chartBar2 = chart2
  // }
  private chart3( ){
    let valor = this.porcentaje
    let nuevaMeta = {
      series: [
        {
          type: 'gauge',
          startAngle: 180,
          endAngle: 0,
          min: 0,
          max: 100,
          splitNumber: 8,
          axisLine: {
            lineStyle: {
              width: 15,
              color: [
                [0.333, '#FF6E76'],
                [0.666, '#FDDD60'],
                [1, '#7CFFB2']
              ]
            }
          },
          // pointer: {
          //   icon: 'path://M12.8,0.7l12,40.1H0.7L12.8,0.7z',
          //   length: '15%',
          //   width: 20,
          //   offsetCenter: [0, '-75%'],
          //   itemStyle: {
          //     color: 'auto'
          //   }
          // },
          axisTick: {
            length: 0,
            lineStyle: {
              color: 'auto',
              width: 0
            }
          },
          splitLine: {
            length: 0,
            lineStyle: {
              color: 'auto',
              width: 0
            }
          },
          axisLabel: {
            color: '#464646',
            fontSize: 20,
            distance: -60,
            formatter: function (value) {
              if (value === 100) {
                return '';
              } else if (value == 50) {
                return '';
              } else if (value === 0) {
                return '';
              } 
              return '';
            }
          },
          title: {
            offsetCenter: [0, '35%'],
            fontSize: 10
          },
          detail: {
            fontSize: 18,
            offsetCenter: [0, '65%'],
            valueAnimation: true,
            formatter: function (value) {
              return Math.round(value)  + '%';
            },
            color: 'auto'
          },
          data: [
            {
              value: valor,
              name: 'Meta'
            }
          ]
        }
      ]
    };   

    this.chartGauge1 = nuevaMeta;
  }


  private actualizarVendedores(){

    let data_x: any [] = [];
    let data_data: any [] = [];
    this.arreglo_vendedores.forEach(elemento=>{
      //let proVenta = elemento
      let data = {
        value : elemento.vendedor_dia,       
        itemStyle: {
          color : this.conseguirColor(elemento.usu_id),
        }
      };
      data_data.push(data);
      data_x.push(elemento.vendedor_l);
    });


   let option = {
      grid: {
        containLabel: true,
        top: '6',
        right: '0',
        bottom: '17',
        left: '25'
      },
      xAxis: {
        data: data_x,
        axisLine: {
          lineStyle: {
            color: '#eaeaea'
          }
        },
        axisLabel: {
          fontSize: 10,
          color: '#9aa0ac'
        },
      },
      tooltip: {
        show: true,
        showContent: true,
        alwaysShowContent: false,
        triggerOn: 'mousemove',
        trigger: 'axis'
      },
      yAxis: {
        splitLine: {
          lineStyle: {
            color: '#eaeaea'
          }
        },
        axisLine: {
          lineStyle: {
            color: '#eaeaea'
          }
        },
        axisLabel: {
          fontSize: 10,
          color: '#9aa0ac'
        }
      },
      
      series: [
        {
          name: 'Ventas',
          type: 'bar',
          data: data_data
        },
        {
          name: 'Meta',
          type: 'line',
          smooth: true,
          lineStyle: {
            width: 3,
            shadowColor: 'rgba(0,0,0,0.4)',
            shadowBlur: 10,
            shadowOffsetY: 10
          },
          data: [],
          symbolSize: 10
        },
        
      ],
      
      //color: this.paleta_colores,
    };

    this.vendedores_mes = option;
  }

  private conseguirColor(id){
    if(Number(id)==Number(this.usuario_actual.usu_id)){
      return this.paleta_colores[0];
    }
    else{
      return this.paleta_colores[1];
    }
  }

}
