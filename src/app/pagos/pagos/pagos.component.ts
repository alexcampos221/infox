import { Component, OnInit, ElementRef, AfterViewInit, ViewChild, OnDestroy} from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { MatriculaService } from 'src/app/shared/services/registros/matricula.service';
import { DateAdapter } from '@angular/material/core';
import { ProgramaService } from 'src/app/shared/services/backend/programa.service';
import { PromocionService } from 'src/app/shared/services/backend/promocion.service';
import { SemestreService } from 'src/app/shared/services/backend/semestre.service';
import { UniversidadService } from 'src/app/shared/services/backend/universidad.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { AlertaService } from 'src/app/shared/services/general/alerta.service';
import { PagoFormComponent } from '../dialogs/pago-form/pago-form.component';

@Component({
  selector: 'app-pagos',
  templateUrl: './pagos.component.html',
  styleUrls: ['./pagos.component.sass']
})
export class PagosComponent implements OnInit {

  promociones_totales: any[] = [];
  promociones_filtrados: any[] = [];
  arreglo;
  dataSource_promociones_totales: MatTableDataSource<any>;

  //atributos para la tabla material
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;

  /*
  displayedColumns = [
    'proNombre',
    'programa',
    'proCode',
    'proDescripcion',
    'periodo',
    'acciones'
  ];
  */

  displayedColumns = [
    'dni',
    'telefono',
    'correo',
    'activo',
    'matriculado',
    'universidad',
    'otros',
  ];

  promociones_procesados: any[] = [];

  filtro_control: FormControl;
  filtro$: Subscription;

  formBusqueda : FormGroup;
  universidades_lista : any[] = [];
  programas_lista : any[]  = [];
  bancos_lista : any[] = [];
  vendedores_lista : any[] = [];
  cursos_lista : any[] = [];
  clientes_lista :any[] = [];

  constructor(private fb : FormBuilder,
              public dialog: MatDialog,
              private servicios:MatriculaService,
              private alertaServicio : AlertaService,
              private ruteador : Router ) { }

  ngOnInit(): void {
    this.crearFormulario();
    this.dataSource_promociones_totales = new MatTableDataSource();
    this.filtro_control = new FormControl(false);
  }

  private crearFormulario(){
    this.formBusqueda = this.fb.group({
      desde : ['',[]],
      hacia : ['',[]],
      universidad : ['',[]],
      programa : ['',[]],
      promocion : ['',[]],
      //cliente : ['',[]],
      vendedor : ['',[]],
      banco :['',[]],
      curso:['',[]],
    });
  }

  ngAfterViewInit(): void {
    this.paginator._intl.itemsPerPageLabel = "Registros por página";
    this.dataSource_promociones_totales.paginator = this.paginator;
    this.dataSource_promociones_totales.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource_promociones_totales.filter = filterValue.trim().toLowerCase();
  }

  EndDateChange(event){
    console.log(event);
  }

  createPDF(){

  }

  nuevoPago(){

    this.ruteador.navigate(['/pagos/detalle']);
    /*
    const dialogRef = this.dialog.open(PagoFormComponent, {
      data: {
        nombre: "formulario",
        banco : null
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
        if( result==undefined){
          console.log("No se recibe nada");
        }
        else if(result.estado=='exito'){
          this.alertaServicio.creacionCorrecta();
          console.log("Se ha creado el banco");
        }
        else{
          console.log("ha ocurrido un error");
        }
    });
    */
  }

}
