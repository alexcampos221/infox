import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VentasRoutingModule } from './ventas-routing.module';
import { VentasComponent } from './ventas/ventas.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSortModule } from '@angular/material/sort';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MaterialFileInputModule } from 'ngx-material-file-input';
import { MatMenuModule } from '@angular/material/menu';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatStepperModule } from '@angular/material/stepper';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { PendienteDetalleComponent } from './pendiente-detalle/pendiente-detalle.component';
import { PendienteDetalleFormComponent } from './dialogs/pendiente-detalle-form/pendiente-detalle-form.component';
import { DetallePagoComponent } from './detalle-pago/detalle-pago.component';
import { ProcesarCuotaComponent } from './dialogs/procesar-cuota/procesar-cuota.component';
import { EditarCuotaComponent } from './dialogs/editar-cuota/editar-cuota.component';
import { AgregarCuotaComponent } from './dialogs/agregar-cuota/agregar-cuota.component'
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { AprobadosComponent } from './aprobados/aprobados.component';
import { SaldosComponent } from './saldos/saldos.component';
import { EquipoComponent } from './equipo/equipo.component';
import { DetalleEquipoComponent } from './equipo/detalle-equipo/detalle-equipo.component';

@NgModule({
  declarations: [VentasComponent, PendienteDetalleComponent, PendienteDetalleFormComponent, DetallePagoComponent, ProcesarCuotaComponent, EditarCuotaComponent, AgregarCuotaComponent, AprobadosComponent, SaldosComponent, EquipoComponent, DetalleEquipoComponent],
  imports: [
    
    CommonModule,
    MatButtonToggleModule,
    MatButtonModule,
    MatCheckboxModule,
    FormsModule,
    ReactiveFormsModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTabsModule,

    MatTableModule,
    MatPaginatorModule,
    MatSnackBarModule,
    MatDialogModule,
    MatSortModule,
    MatToolbarModule,
    MatSelectModule,
    MatDatepickerModule,
    MaterialFileInputModule,
    MatMenuModule,
    MatSlideToggleModule,
    MatStepperModule,
    VentasRoutingModule
  ]
})
export class VentasModule { }
