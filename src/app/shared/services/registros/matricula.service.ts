import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

import * as Global from '../../globals';
import { Utils } from '../../utils';

@Injectable({
  providedIn: 'root'
})
export class MatriculaService {

  constructor(public http: HttpClient) { }

  listaUniversidades(){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Universidad/listar', requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

  listaSemestres(){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Semestre/listar', requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

  listaProgramaCursos(){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.get(Global.ruta_base_infox+ 'listaProgramaCursos', requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

  listaBancos(){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.get(Global.ruta_infox+ 'Banco/listar', requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

  buscarPromocion(body){
    const requestOptions = {
      headers: new HttpHeaders({
        //'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_base_infox+ 'buscarPromocion',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  editarMatriculaCurso(body){
    const requestOptions = {
      headers: new HttpHeaders({
        //'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Matricula/editarMatricula',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

  buscarReferido(body){
    const requestOptions = {
      headers: new HttpHeaders({
        //'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_base_infox+ 'buscarReferido',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

  getVendedorAct(body){
    const requestOptions = {
      headers: new HttpHeaders({
        //'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_base_infox+ 'getVendedorAct',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

  guardarRegistroPagoNew(body){
    const requestOptions = {
      headers: new HttpHeaders({
        //'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_base_infox+ 'guardarRegistroPagoNew',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

  listarSemestres(){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.get(Global.ruta_infox+ 'Semestre/listar', requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

  universidadListarByPais(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Universidad/listarByPais',Utils.jsonToFormDataV2(body,false), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

  status(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Banco/cambiarEstado',Utils.jsonToFormDataV2(body,false), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

}