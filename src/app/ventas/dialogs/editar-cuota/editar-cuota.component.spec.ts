import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarCuotaComponent } from './editar-cuota.component';

describe('EditarCuotaComponent', () => {
  let component: EditarCuotaComponent;
  let fixture: ComponentFixture<EditarCuotaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditarCuotaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarCuotaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
