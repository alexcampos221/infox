import { Router, NavigationEnd } from '@angular/router';
import { Role } from './../../shared/security/role';
import { DOCUMENT } from '@angular/common';
import {
  Component,
  Inject,
  ElementRef,
  OnInit,
  Renderer2,
  HostListener,
} from '@angular/core';
import { ROUTES } from './sidebar-items';
import { AuthService } from 'src/app/shared/security/auth.service';
import { AlmacenamientoLocalService } from 'src/app/shared/services/general/almacenamiento-local.service';
import * as Globales from 'src/app/shared/globals';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.sass'],
})
export class SidebarComponent implements OnInit {
  public sidebarItems: any[];
  homePage: string;
  showMenu = 'dashboard';
  showSubMenu = '';
  showSubSubMenu = '';
  public innerHeight: any;
  public bodyTag: any;
  listMaxHeight: string;
  listMaxWidth: string;
  userFullName: string;
  userImg: string;
  userType: string;
  headerHeight = 60;
  currentRoute: string;
  constructor(
    @Inject(DOCUMENT) private document: Document,
    private renderer: Renderer2,
    public elementRef: ElementRef,
    private authService: AuthService,
    private router: Router,
    private localServicio : AlmacenamientoLocalService,
  ) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        if (event.url.split('/')[1] === 'multilevel') {
          this.showMenu = event.url.split('/')[1];
        } else {
          this.showMenu = event.url.split('/').slice(-2)[0];
        }
        this.showSubMenu = event.url.split('/').slice(-2)[0];
        // console.log(event.url)
      }
    });
  }
  @HostListener('window:resize', ['$event'])
  windowResizecall(event) {
    this.setMenuHeight();
    this.checkStatuForResize(false);
  }
  @HostListener('document:mousedown', ['$event'])
  onGlobalClick(event): void {
    if (!this.elementRef.nativeElement.contains(event.target)) {
      this.renderer.removeClass(this.document.body, 'overlay-open');
    }
  }
  callMenuToggle(event: any, element: any) {
    if (element === this.showMenu) {
      this.showMenu = '0';
    } else {
      this.showMenu = element;
    }
    const hasClass = event.target.classList.contains('toggled');
    if (hasClass) {
      this.renderer.removeClass(event.target, 'toggled');
    } else {
      this.renderer.addClass(event.target, 'toggled');
    }

  }
  callSubMenuToggle(event: any, element: any) {

    if (element === this.showSubMenu) {
      this.showSubMenu = '0';
    } else {
      this.showSubMenu = element;
    }
    console.log(this.showMenu)
  }
  ngOnInit() {


    // if (this.authService.isLoggedIn()) {
    //   const userRole = this.authService.getRole();
    //   this.userFullName = this.authService.getUserFullName();
    //   this.userImg = this.authService.getUserImg();

    //   this.sidebarItems = ROUTES.filter(
    //     (x) => x.role.indexOf(userRole) !== -1 || x.role.indexOf('All') !== -1
    //   );
    //   if (userRole === Role.Admin) {
    //     this.userType = Role.Admin;
    //   } else if (userRole === Role.Teacher) {
    //     this.userType = Role.Teacher;
    //   } else if (userRole === Role.Student) {
    //     this.userType = Role.Student;
    //   } else {
    //     this.userType = Role.Admin;
    //   }
    // }

    // // this.sidebarItems = ROUTES.filter((sidebarItem) => sidebarItem);
    // this.initLeftSidebar();
    // this.bodyTag = this.document.body;


    let numero_rol =  this.localServicio.getItem('usu_actual').usu_tipo;
    let rol_actual = Globales.roles.get(numero_rol);
    this.userType = rol_actual;

    this.userImg = this.localServicio.getItem('usu_actual').usu_img || '/assets/images/empty-avatar.jpg';
    this.userFullName = this.localServicio.getItem('usu_actual').usu_dni;
    this.traerOpciones();
    this.initLeftSidebar();
    this.bodyTag = this.document.body;
  }

  private traerOpciones(){
    this.sidebarItems = ROUTES.filter(
      (x) => x.role.indexOf(this.userType) !== -1 || x.role.indexOf('All') !== -1
    );
  }


  initLeftSidebar() {
    const _this = this;
    // Set menu height
    _this.setMenuHeight();
    _this.checkStatuForResize(true);
  }
  setMenuHeight() {
    this.innerHeight = window.innerHeight;
    const height = this.innerHeight - this.headerHeight;
    this.listMaxHeight = height + '';
    this.listMaxWidth = '500px';
  }
  isOpen() {
    return this.bodyTag.classList.contains('overlay-open');
  }
  checkStatuForResize(firstTime) {
    if (window.innerWidth < 1170) {
      this.renderer.addClass(this.document.body, 'ls-closed');
    } else {
      this.renderer.removeClass(this.document.body, 'ls-closed');
    }
  }
  mouseHover(e) {
    const body = this.elementRef.nativeElement.closest('body');
    if (body.classList.contains('submenu-closed')) {
      this.renderer.addClass(this.document.body, 'side-closed-hover');
      this.renderer.removeClass(this.document.body, 'submenu-closed');
    }
  }
  mouseOut(e) {
    const body = this.elementRef.nativeElement.closest('body');
    if (body.classList.contains('side-closed-hover')) {
      this.renderer.removeClass(this.document.body, 'side-closed-hover');
      this.renderer.addClass(this.document.body, 'submenu-closed');
    }
  }

  funcionDeFiltrado(rutas){
    let M_rutas =rutas.filter(
      // (x) => x.role.indexOf(rol_nuevo) !== -1 || x.role.indexOf(rol_nuevo) !== -1
      (x) => x.role.indexOf(this.userType) !== -1
    );
    return M_rutas;
  }

}
