import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

import * as Global from '../../globals';
import { Utils } from '../../utils';

@Injectable({
  providedIn: 'root'
})
export class BancoService {

  constructor(public http : HttpClient) { }

  listaBancos(){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Banco/listar', Utils.jsonToFormDataV2(false), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

  editarBanco(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Banco/editar',Utils.jsonToFormDataV2(body,false), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

  cambiarEstado(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Banco/cambiarEstado', Utils.jsonToFormDataV2(body,false), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

  guardarBanco(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Banco/guardar',Utils.jsonToFormDataV2(body,false), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

}
