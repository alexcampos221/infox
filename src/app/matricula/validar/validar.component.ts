import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';

@Component({
  selector: 'app-validar',
  templateUrl: './validar.component.html',
  styleUrls: ['./validar.component.sass']
})
export class ValidarComponent implements OnInit {

  validarForm : FormGroup;

  constructor(private fb : FormBuilder,) { }

  ngOnInit(): void {
    this.crearFormulario();
  }

  private crearFormulario(){
    this.validarForm = this.fb.group({
      dni : ['',[Validators.required]]
    });
  }

  enviarFormulario(){
    
  }
}
