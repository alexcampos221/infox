import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CanvasRoutingModule } from './canvas-routing.module';
import { CanvasComponent } from './canvas/canvas.component';
import { InfogramaComponent } from './infograma/infograma.component';


@NgModule({
  declarations: [CanvasComponent, InfogramaComponent],
  imports: [
    CommonModule,
    CanvasRoutingModule
  ]
})
export class CanvasModule { }
