import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SemestreService } from 'src/app/shared/services/backend/semestre.service';

@Component({
  selector: 'app-semestre-form',
  templateUrl: './semestre-form.component.html',
  styleUrls: ['./semestre-form.component.sass']
})
export class SemestreFormComponent implements OnInit {
  semestreForm: FormGroup;
  editar: boolean = false;
  nombre_modal: string;

  constructor(private semestreServicio: SemestreService,
              private fb: FormBuilder,
              public dialogRef: MatDialogRef<SemestreFormComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,) { }

  ngOnInit(): void {
    this.nombre_modal = 'Crear Semestre';
    if (this.data.nombre == 'editar') {
      this.nombre_modal = 'Editar Semestre';
      this.editar = true;
      this.crearFormularioEditar();
    }
    else {
      this.crearFormulario();
    }

  }

  private crearFormulario() {
    this.semestreForm = this.fb.group({
      nombre: ['', [Validators.required]],
    });
  }

  private crearFormularioEditar() {
    let nombre = this.data.programa.sem_nombre;
    let codigo = this.data.programa.sem_id;
    this.semestreForm = this.fb.group({
      nombre: [nombre, [Validators.required]],
      codigo: [codigo, [Validators.required]],
    });
  }

  enviarFormulario() {
    console.log(this.semestreForm.value);
    if (this.editar) {
      this.editarSemestre();
    }
    else {
      this.crearSemestre();
    }
  }

  closeDialog(): void {
    this.dialogRef.close();
  }

  private editarSemestre() {
    let codigo = this.semestreForm.controls.codigo.value;
    let nombre = this.semestreForm.controls.nombre.value;

    let body = {
      nombre: nombre,
      semestreID: codigo,
    }

    this.semestreServicio.editarSemestre(body).subscribe(resp => {
      console.log(resp);
      if (resp['status'] == 'OK') {
        this.dialogRef.close({ estado: 'EXITO' })
      }
      else {
        this.dialogRef.close({ estado: 'FALLO' });
      }
    }, (error) => {
      console.log(error);
      this.dialogRef.close({ estado: 'FALLO' });
    });

  }

  private crearSemestre() {
    let nombre = this.semestreForm.controls.nombre.value;
    let body = {
      nombre: nombre,
    }

    this.semestreServicio.guardarSemestre(body).subscribe(resp => {
      console.log(resp);
      if (resp['status'] == 'OK') {
        this.dialogRef.close({ estado: 'EXITO' })
      }
      else {
        this.dialogRef.close({ estado: 'FALLO' });
      }
    }, (error) => {
      console.log(error);
      this.dialogRef.close({ estado: 'FALLO' });
    });
  }

}
