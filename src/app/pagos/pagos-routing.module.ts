import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagosComponent } from './pagos/pagos.component';
import { PagoDetalleComponent } from './pago-detalle/pago-detalle.component';
import { DocumentoComponent } from './documento/documento.component';

const routes: Routes = [
  {
    path : 'pagos',
    component : PagosComponent,
  },
  {
    path : 'detalle',
    component : DocumentoComponent,
  },
  {
    path : 'nuevo',
    component : PagoDetalleComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagosRoutingModule { }
