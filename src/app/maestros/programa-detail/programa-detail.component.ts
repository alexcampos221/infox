import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ProgramaService } from 'src/app/shared/services/backend/programa.service';
import { AlertaService } from 'src/app/shared/services/general/alerta.service';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { CursoFormComponent } from '../dialogs/curso-form/curso-form.component';
import { NotificacionesFormComponent } from '../dialogs/notificaciones-form/notificaciones-form.component';
import { ProgramaFormComponent } from '../dialogs/programa-form/programa-form.component';

@Component({
  selector: 'app-programa-detail',
  templateUrl: './programa-detail.component.html',
  styleUrls: ['./programa-detail.component.sass']
})
export class ProgramaDetailComponent implements OnInit {

  pageEvent: PageEvent;
  datasource: null;
  pageIndex:number;
  pageSize:number = 1;
  length:number;

  formCurso:FormGroup;

  arrayPagination : any [] = [];
 
  rowPrograma : any;
  rowInfoCurso : any;

  isActivo:boolean = false
  
  isPro0:boolean = false
  isPro1:boolean = false
  isPro2:boolean = false
  isPro3:boolean = false
  
  programaID:any
  ventana:any='';

  cursos_filtrados : any [] = [];

  displayedColumns=[
    'curso',
    'descripcion',
    'estado',
    'acciones',
  ]

  displayedColumns2=[
    'tipo',
    'dia',
    'hora',
    'mensaje',
    'vendedor',
    'estado',
    'acciones',
  ]

  dsCursos:MatTableDataSource<any>;
  dsNotificaciones : MatTableDataSource<any>;
  lista_notificaciones : any [];
  //atributos para la tabla material
  @ViewChild(MatPaginator, { static: true }) paginadorCurso: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;

  //atributos para la tabla material
  @ViewChild(MatPaginator, { static: true }) paginadorNotificacion : MatPaginator;
  @ViewChild(MatSort) sort2: MatSort;
  @ViewChild('filter', { static: true }) filter2 : ElementRef;

  
  constructor(private fb: FormBuilder,
    public activatedRoute: ActivatedRoute,
    private router:Router,
    private alertaServicio:AlertaService,
    private  dialog: MatDialog,
    private programaService:ProgramaService,
    private loadingService: LoadingService,
    private spinner: NgxSpinnerService,  ) { 
      if(JSON.parse(localStorage.getItem('programaID')) != null){
        this.programaID = JSON.parse(localStorage.getItem('programaID'));
      }else{        
        this.programaID = this.router.getCurrentNavigation().extras.state.data.pagoID;
        
      }
      if(JSON.parse(localStorage.getItem('arrayListaPendiente')) != null){
        this.arrayPagination = JSON.parse(localStorage.getItem('arrayListaPendiente'));
        this.pageIndex = JSON.parse(localStorage.getItem('indice'));
        this.length=this.arrayPagination.length;     
      }
    }

  ngOnInit(): void {
    this.paginadorCurso._intl.getRangeLabel = (page: number, pageSize: number, length: number)=>{
      if (length == 0 || pageSize == 0) {
        return `0 de ${length}`;
      }
      const startIndex = page * pageSize;

      return `${startIndex+1} de ${length}`
    };  
    this.crearFormulario();
    this.traerInfoCurso();
    this.traerNotificaciones();
    this.dsCursos = new MatTableDataSource();
    this.dsNotificaciones = new MatTableDataSource();
  }
  // ngAfterViewInit():void{
  //   this.paginadorCurso._intl.getRangeLabel = (page: number, pageSize: number, length: number)=>{
  //     if (length == 0 || pageSize == 0) {
  //       return `0 de ${length}`;
  //     }
  //     const startIndex = page * pageSize;
  //     return `${startIndex+1} de ${length}`
  //   };    
  // }
  crearFormulario(){
    this.formCurso = this.fb.group({
      nombre_programa:['',[Validators.required]],
      activo:['',[Validators.required]],
      pro_tiempo0:['',[Validators.required]],
      pro_tiempo1:['',[Validators.required]],
      pro_tiempo2:['',[Validators.required]],
      pro_tiempo3:['',[Validators.required]],
      descripcion:['',[Validators.required]],
      tipo:['',[Validators.required]],
      cantidad_combo:['',[Validators.required]],
    })
  }
  private get f (){
    return this.formCurso.controls;
  }
  tipo_programa: string;

  traerInfoCurso(){
    this.spinner.show();
    this.loadingService.loading$.next({opacity:0.5});
    let salida = {
      programaID:this.programaID
    }
    console.log(salida)
    this.programaService.infoCurso(salida).subscribe((resp:any) => {
      console.log(resp);
      this.rowPrograma = Object.assign({}, ...resp.info);
      this.rowInfoCurso = resp.cursos || '';
      this.dsCursos.data = this.rowInfoCurso;
      console.log(this.rowInfoCurso)
      if(this.rowPrograma != null){
        this.f.nombre_programa.setValue(this.rowPrograma.pro_nombre);
        this.f.activo.setValue(this.rowPrograma.pro_activo);
        this.f.pro_tiempo0.setValue(this.rowPrograma.pro_1m);
        this.f.pro_tiempo1.setValue(this.rowPrograma.pro_3m);
        this.f.pro_tiempo2.setValue(this.rowPrograma.pro_6m);
        this.f.pro_tiempo3.setValue(this.rowPrograma.pro_12m);
        this.f.descripcion.setValue(this.rowPrograma.pro_descripcion)
        this.f.tipo.setValue(this.rowPrograma.pro_tipo);
        this.f.cantidad_combo.setValue(this.rowPrograma.pro_qty_combo);
        if(this.f.activo.value =='Y'){this.isActivo=true}else{this.isActivo=false}
        if(this.f.pro_tiempo0.value =='Y'){this.isPro0=true}else{this.isPro0=false}
        if(this.f.pro_tiempo1.value =='Y'){this.isPro1=true}else{this.isPro1=false}
        if(this.f.pro_tiempo2.value =='Y'){this.isPro2=true}else{this.isPro2=false}
        if(this.f.pro_tiempo3.value =='Y'){this.isPro3=true}else{this.isPro3=false}
        if(this.f.tipo.value == 'PR') {this.tipo_programa = 'Programa'} else{this.tipo_programa = 'Combo'}
      }
      this.spinner.hide();
    },(error)=>{
      console.log(error);
      this.spinner.hide();
    })
  }

  traerNotificaciones(){
    this.spinner.show();
    this.loadingService.loading$.next({opacity:0.5});
    let salida = {
      programaID:this.programaID
    }
    this.programaService.listaProgramaNotificaciones(salida).subscribe((resp:any) => {
      console.log(resp)
      this.lista_notificaciones = resp['data']
      this.dsNotificaciones.data = this.lista_notificaciones.filter((m) => m.isActive == 'Y'); 
      this.spinner.hide();
    },(error) => {
      console.log(error)
      this.spinner.hide();
    })
  }

  public getServerData(event?:PageEvent){
    this.pageIndex = event.pageIndex;
    localStorage.setItem('indice',JSON.stringify(event.pageIndex));
    this.programaID = this.arrayPagination[event.pageIndex];    
    this.traerInfoCurso();
    this.traerNotificaciones();
    return event;
  }
  cerrarDetalle(){
    localStorage.setItem('cerrar_detalle',JSON.stringify(true));
    this.router.navigate(['/maestros/programas']);
  }
  sortData(sort : Sort){
    
  }
  sortData2(sort : Sort){

  }

  editarPrograma(){
    const dialogRef = this.dialog.open(ProgramaFormComponent, {
      width:'400px',
      data: {
        nombre: "editar",
        programa : this.rowPrograma,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
        if( result==undefined){
          console.log("No se recibe nada");
        }
        else if(result.estado=='EXITO'){
          this.alertaServicio.actualizacionCorrecta();
          this.traerInfoCurso();
        }
        else{
          console.log("ha ocurrido un error");
        }
    });
  }
  addCurso(){
    const dialogRef = this.dialog.open(CursoFormComponent, {
      data: {
        nombre: "agregar",
        curso :  null,
        programaID:this.rowPrograma.pro_id
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
        if( result==undefined){
          console.log("No se recibe nada");
        }
        else if(result.estado=='EXITO'){
          this.traerInfoCurso();
          console.log("Se ha creado el curso");
        }
        else{
          console.log("ha ocurrido un error");
        }
    });
  }

  editCurso(row){
    console.log(row)
    const dialogRef = this.dialog.open(CursoFormComponent, {
      data: {
        nombre: "editar",
        curso : row,
        programaID:this.rowPrograma.pro_id
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
        if( result==undefined){
          console.log("No se recibe nada");
        }
        else if(result.estado=='EXITO'){
          this.traerInfoCurso();
          console.log("Se ha actualizado el curso");
        }
        else{
          console.log("ha ocurrido un error");
        }
    });
  }
  applyFilter(filterValue:string){
    this.dsCursos.filter = filterValue.trim().toLowerCase() || '';
    this.cursos_filtrados = this.dsCursos.filteredData;
  }
  chageStateValue(data){
    if(data==='N') return 'Y';
    else if(data==='Y') return 'N';
    else throw 'error';  
  }

  addNotificacion(){
    const dialogRef = this.dialog.open(NotificacionesFormComponent, {
      width:'800px',
      data: {
        nombre: "Agregar Notificacion",
        notificacion: null,
        programaID: this.programaID
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
        if( result==undefined){
          console.log("No se recibe nada");
        }
        else if(result.estado=='EXITO'){
          this.traerNotificaciones();
          console.log("Se ha creado el curso");
        }
        else{
          console.log("ha ocurrido un error");
        }
    });
  }
  editarNotificacion(item){
    const dialogRef = this.dialog.open(NotificacionesFormComponent, {
      width:'600px',
      data: {
        nombre: "Editar Notificación",
        notificacion : item,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
        if( result==undefined){
          console.log("No se recibe nada");
        }
        else if(result.estado=='EXITO'){
          this.alertaServicio.actualizacionCorrecta();
          this.traerNotificaciones();
        }
        else{
          console.log("ha ocurrido un error");
        }
    });
  }
  public chageState(row){
    // console.log(row);
    this.spinner.show();
    this.loadingService.loading$.next({opacity:0.5});
    let body = {
      estadoNuevo: this.chageStateValue(row.pro_activo),
      cursoID: row.cur_id
    }
    console.log(body)
    this.programaService.status(body).subscribe((resp:any)=>{
      console.log(resp)
      this.traerInfoCurso();
      this.spinner.hide();
    },(error)=>{
      console.log(error);
      this.spinner.hide();
    });
  }
}
