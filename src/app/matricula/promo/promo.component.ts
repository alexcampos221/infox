import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { MatriculaService } from 'src/app/shared/services/registros/matricula.service';
import { DateAdapter } from '@angular/material/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-promo',
  templateUrl: './promo.component.html',
  styleUrls: ['./promo.component.sass']
})
export class PromoComponent implements OnInit,OnDestroy {

  codigo: any;
  formPromo: FormGroup;

  universidades_lista: any[] = [];
  programas_cursos_lista: any[] = [];

  univer$: Subscription;

  constructor(private rutaActiva: ActivatedRoute,
              private dateAdapter: DateAdapter<Date>,
              private fb: FormBuilder,
              private matriculaServicio: MatriculaService) {
    this.dateAdapter.setLocale('es-PE');
   }
  ngOnDestroy(): void {
    if(this.univer$){
      this.univer$.unsubscribe();
    }
  }

  ngOnInit(): void {
    this.crearFormulario();
    this.traerUniversidades();
    this.pendienteCambios();
  }

  private pendienteCambios(){
    this.univer$ = this.formPromo.controls.universidad.valueChanges.subscribe((resp:any)=>{
      console.log(resp);
      if(resp.uni_nombre == 'OTROS'){
        this.formPromo.controls.otros.enable();
      }
      else{
        this.formPromo.controls.otros.disable();
      }
    });
  }

  private crearFormulario(){
    this.formPromo = this.fb.group({
      dni : ['',[Validators.required]],
      nombres : ['',[Validators.required]],
      correo : ['',[Validators.required]],
      telefono : ['',[Validators.required]],
      universidad : ['',[Validators.required]],
      otros : ['',[Validators.required]],
      curso : ['',Validators.required],
    });
    this.formPromo.controls.otros.disable();
  }

  private traerUniversidades() {
    this.matriculaServicio.listaUniversidades().subscribe((resp: any) => {
      console.log(resp);
      this.universidades_lista = resp['result'];
    });
  }

  private listaProgramaCursos() {

    this.matriculaServicio.listaProgramaCursos().subscribe((resp: any) => {
      console.log(resp);
      this.programas_cursos_lista = resp['result'];
    });
  }

  enviarFormulario(){

  }

}