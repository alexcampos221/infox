import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ProgramaService } from 'src/app/shared/services/backend/programa.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { Console } from 'console';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-programa-form',
  templateUrl: './programa-form.component.html',
  styleUrls: ['./programa-form.component.sass']
})
export class ProgramaFormComponent implements OnInit {

  programaForm : FormGroup;
  editar: boolean = false;
  nombre_modal : string;

  seleccion_meses:any[]=[
    {
      nombre:'1 M',
    },
    {
      nombre:'3 M',
    },
    {
      nombre:'6 M',
    },
    {
      nombre:'12 M',
    },
  ]
  tipo_programa:any[]=[
    {
      nombre:'Programa',
      codigo:'PR'
    },
    {
      nombre:'Combo',
      codigo:'CO'
    },
  ]
  isMes1:boolean=false;
  isMes3:boolean=false;
  isMes6:boolean=false;
  isMes12:boolean=false;
  isCombo: boolean = false;

  constructor(private programaServicio:ProgramaService,
              private fb: FormBuilder,               
              public dialogRef: MatDialogRef<ProgramaFormComponent>,
              private spinner:NgxSpinnerService,
              private loading:LoadingService,
              @Inject(MAT_DIALOG_DATA) public data: any,) {
                console.log(data)
               }

  ngOnInit(): void {
    this.nombre_modal = 'Crear Programa';
    if(this.data.nombre == 'editar'){
      this.nombre_modal = 'Editar Programa';
      this.editar = true;
      this.crearFormularioEditar();
      this.f.programa.setValue(this.data.programa.pro_nombre);
      this.f.descripcion.setValue(this.data.programa.pro_nombre);
      this.f.mes_1.setValue(this.data.programa.pro_1m);
      if(this.f.mes_1.value == 'Y'){this.isMes1 =true}else{this.isMes1 =false}
      this.f.mes_3.setValue(this.data.programa.pro_3m);
      if(this.f.mes_3.value == 'Y'){this.isMes3 =true}else{this.isMes3 =false}
      this.f.mes_6.setValue(this.data.programa.pro_6m);
      if(this.f.mes_6.value == 'Y'){this.isMes6 =true}else{this.isMes6 =false}
      this.f.mes_12.setValue(this.data.programa.pro_12m);
      if(this.f.mes_12.value == 'Y'){this.isMes12 =true}else{this.isMes12 =false}
      for (let index = 0; index < this.tipo_programa.length; index++) {
        if(this.tipo_programa[index].codigo == this.data.programa.pro_tipo){
          this.f.tipo.setValue(this.tipo_programa[index]);
        }
      }
      if(this.data.programa.pro_tipo == 'CO'){
        this.isCombo=true
      }
      this.f.cantidad.setValue(this.data.programa.pro_qty_combo);
    }
    else{
      this.crearFormulario();
    }
  }

  private crearFormulario(){
    this.programaForm = this.fb.group({
      programa : ['',[Validators.required]],
      descripcion: ['',[]],
      mes_1: ['',[]],
      mes_3: ['',[]],
      mes_6: ['',[]],
      mes_12: ['',[]],
      tipo: ['',[]],
      cantidad:['',[Validators.required]],
    });
  }

  private crearFormularioEditar(){
    this.programaForm = this.fb.group({
      programa : ['',[Validators.required]],
      descripcion: ['',[]],
      mes_1: ['',[]],
      mes_3: ['',[]],
      mes_6: ['',[]],
      mes_12: ['',[]],
      tipo: ['',[]],
      cantidad:['',[Validators.required]],  
    });
  }
  private get f (){
    return this.programaForm.controls;
  }
  selectPrograma(event){
    console.log(event)
    if(event.nombre == 'Combo'){
      this.isCombo = true;
    }else if(event.nombre == 'Programa'){
      this.isCombo = false;
    }
  }
  enviarFormulario(){
    if(this.data.programa == null || this.data.programa.undefined){
      this.crearPrograma();
    }else{
      this.editarPrograma();
    }
  //  console.log(this.programaForm.value)
  }
  crearPrograma(){
    let tipo = this.f.tipo.value
    let is1m;
    let is3m;
    let is6m;
    let is12m;
    if(this.f.mes_1.value == true){is1m='Y'}else{is1m='N'}
    if(this.f.mes_3.value == true){is3m='Y'}else{is3m='N'}
    if(this.f.mes_6.value == true){is6m='Y'}else{is6m='N'}
    if(this.f.mes_12.value == true){is12m='Y'}else{is12m='N'}
    let salida = {
      nombre: this.f.programa.value,
      tipo: tipo.codigo,
      maxSeleccion: this.f.cantidad.value || 0,
      is1m:is1m,
      is3m:is3m,
      is6m:is6m,
      is12m:is12m,
      descripcion:this.f.descripcion.value,
    }
    console.log(salida)
    this.programaServicio.guardarPrograma(salida).subscribe(resp=>{
      console.log(resp)
        if(resp['status'] == 'OK'){
          this.dialogRef.close({estado : 'EXITO'})
        }else{
          this.dialogRef.close({estado : 'FALLO'})
        }
      
    },(error)=>{
      console.log(error)
      this.dialogRef.close({estado : 'FALLO'})
    })
  }
  editarPrograma(){
    // console.log(this.programaForm.value)
    let tipo = this.f.tipo.value
    let is1m;
    let is3m;
    let is6m;
    let is12m;
    if(this.f.mes_1.value == true || this.f.mes_1.value == 'Y'){is1m='Y'}else{is1m='N'}
    if(this.f.mes_3.value == true || this.f.mes_3.value == 'Y'){is3m='Y'}else{is3m='N'}
    if(this.f.mes_6.value == true || this.f.mes_6.value == 'Y'){is6m='Y'}else{is6m='N'}
    if(this.f.mes_12.value == true || this.f.mes_12.value == 'Y'){is12m='Y'}else{is12m='N'}
    let salida = {
      nombre: this.f.programa.value,
      tipo: tipo.codigo,
      maxSeleccion: this.f.cantidad.value || '0',
      is1m:is1m,
      is3m:is3m,
      is6m:is6m,
      is12m:is12m,
      descripcion:this.f.descripcion.value,
      programaID:this.data.programa.pro_id,
    }
    console.log(salida)
    this.programaServicio.editarPorgrama(salida).subscribe(resp=>{
      console.log(resp);
        if(resp['status'] == 'OK'){
          this.dialogRef.close({estado : 'EXITO'})
        }else{
          this.dialogRef.close({estado : 'FALLO'})
        }
    },(error)=>{
      console.log(error)
      this.dialogRef.close({estado : 'FALLO'})
    })
  }
  closeDialog(): void {
    this.dialogRef.close();
  }

  changeStatus(event){
    console.log(event.checked);
    console.log(event.source.value)
    if(this.data.programa != null || this.data.programa != undefined){
      switch (event.source.value) {
        case '1m':
          if(event.source.value == '1m' && event.checked == true){
            this.f.mes_1.setValue(event.checked)
          }else {
            this.f.mes_1.setValue(event.checked)
          }
          break;
        case '3m':
          if(event.source.value == '3m' && event.checked == true){
            this.f.mes_3.setValue(event.checked)
          }else {
            this.f.mes_3.setValue(event.checked)
          }
          break;
        case '6m':
          if(event.source.value == '6m' && event.checked == true){
            this.f.mes_6.setValue(event.checked)
          }else{
            this.f.mes_6.setValue(event.checked)
          } 
          break;
        case '12m':
          if(event.source.value == '12m' && event.checked == true){
            this.f.mes_12.setValue(event.checked)
          }else{
            this.f.mes_12.setValue(event.checked)
          }
          break;
        default:
          break;
      }
    }
    
  }

}
