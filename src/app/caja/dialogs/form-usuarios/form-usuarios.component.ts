import { elementEventFullName } from '@angular/compiler/src/view_compiler/view_compiler';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { ProgramaService } from 'src/app/shared/services/backend/programa.service';
import { AlertaService } from 'src/app/shared/services/general/alerta.service';
import { LoadingService } from 'src/app/shared/services/loading.service';

@Component({
  selector: 'app-form-usuarios',
  templateUrl: './form-usuarios.component.html',
  styleUrls: ['./form-usuarios.component.sass']
})
export class FormUsuariosComponent implements OnInit {

  formUsuario:FormGroup;

  usuario:any;
  accion:string;

  lista_usuarios:any[]=[];
  lista_usuarios_filtrados:any[]=[];

  constructor(private programaServicio:ProgramaService,
    private loadingService:LoadingService,
    private spinner:NgxSpinnerService,
    private dialogRef: MatDialogRef<FormUsuariosComponent>,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private alertaServicio:AlertaService,
    ) { 
      console.log(data)
    }

  ngOnInit(): void {
    this.crearFormulario()   
    this.usuario = this.data.nomVendedor
    this.accion = this.data.accion
    this.traerInfoUsuarios();
       
  }

  crearFormulario(){
    this.formUsuario = this.fb.group({
      usuario:['',[Validators.required]]
    })
  }
  private get f(){
    return this.formUsuario.controls;
  }

  closeDialog():void{
    this.dialogRef.close();
  }

  traerInfoUsuarios(){
    let salida={
      tipo: '0'
    }
    this.loadingService.loading$.next({opacity:0.5});
    this.spinner.show();
    this.programaServicio.listarUsuariosByCaja(JSON.stringify(salida)).subscribe((resp : any)=>{
      console.log(resp);
      this.lista_usuarios = resp;
      this.lista_usuarios_filtrados = this.lista_usuarios;
      this.lista_usuarios_filtrados = this.lista_usuarios_filtrados.filter((m) => m.usu_activo == 'Y')      
      this.lista_usuarios_filtrados.forEach(element => {
        if(element.usu_usuario == this.usuario){
          this.f.usuario.setValue(element);
        }else{
          console.log('else')
        } 
      });
      this.spinner.hide();
    },(error) => {
      console.log(error);
      this.spinner.hide();
    })
  }
  selectUsuario(event){
    console.log(event)
  }
  onSubmitClick(){
    let salida = {
      cuotaID: this.data.cuotaID,
      vendedorID: this.f.usuario.value.usu_id,
    }
    console.log(salida)
    this.programaServicio.actualizarVendedorByCaja(JSON.stringify(salida)).subscribe(resp => {
      console.log(resp);
      if(resp['status']=='OK'){     
        this.dialogRef.close({estado : 'EXITO'});       
      }else{
        this.alertaServicio.algoHaIdoMal();
        this.dialogRef.close({estado : 'FALLO'});
      }
    },(error)=>{
      console.log(error);
    })
  }

}
