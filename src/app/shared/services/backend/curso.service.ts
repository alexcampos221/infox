import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

import * as Global from '../../globals';
import { Utils } from '../../utils';
@Injectable({
  providedIn: 'root'
})
export class CursoService {

  constructor(public http : HttpClient) { }

  listaCursos(){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Curso/listar', Utils.jsonToFormDataV2(false), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

  editarCurso(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Programa/editarCurso',Utils.jsonToFormDataV2(body,false), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

  cambiarEstado(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Curso/cambiarEstado', Utils.jsonToFormDataV2(body,false), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

  guardarCurso(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Programa/agregarCurso',Utils.jsonToFormDataV2(body,false), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
}
