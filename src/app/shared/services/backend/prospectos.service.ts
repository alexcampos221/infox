import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

import * as Global from '../../globals';
import { Utils } from '../../utils';
import { utils } from 'xlsx';

@Injectable({
  providedIn: 'root'
})
export class ProspectosService {

  constructor(public http : HttpClient) { }

  listarProspectos(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Promocion/listarProspectos',Utils.jsonToFormDataV2(body,false), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  listarPromociones(){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Promocion/listar', requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
    
  }
}
