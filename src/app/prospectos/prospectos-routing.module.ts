import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProspectosComponent } from './prospectos/prospectos.component';
import { RegistroComponent } from './registro/registro.component';

const routes: Routes = [
  {
    path : 'lista',
    component : ProspectosComponent
  },
  {
    path : 'registro',
    component : RegistroComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProspectosRoutingModule { }
