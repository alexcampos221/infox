import { Component, OnInit, ElementRef, AfterViewInit, ViewChild, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormControl, FormControlName, FormGroup, Validators } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { DatePipe } from '@angular/common';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { Utils } from 'src/app/shared/utils';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { AlertaService } from 'src/app/shared/services/general/alerta.service';
import { PromocionService } from 'src/app/shared/services/backend/promocion.service';
import { PromocionFormComponent } from '../dialogs/promocion-form/promocion-form.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { LoadingService } from 'src/app/shared/services/loading.service';

@Component({
  selector: 'app-promociones',
  templateUrl: './promociones.component.html',
  styleUrls: ['./promociones.component.sass'],
  providers: [DatePipe]
})
export class PromocionesComponent implements OnInit {

  promociones_totales: any[] = [];
  promociones_filtrados: any[] = [];
  arreglo;
  dataSource_promociones_totales: MatTableDataSource<any>;

  //atributos para la tabla material
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;

  displayedColumns = [
    'proNombre',
    'programa',
    'proCode',
    'proDescripcion',
    'periodo',
    'acciones'
  ];

  promociones_procesados: any[] = [];

  filtro_control: FormControl;
  filtro$: Subscription;


  constructor(public dialog: MatDialog,
              private ruteador: Router,
              private promocionServicio: PromocionService,
              private spinner : NgxSpinnerService,
              private loading:LoadingService,
              private alertaServicio: AlertaService,) { }

  ngOnDestroy(): void {
    if (this.filtro$) {
      console.log("unsubscribe");
      this.filtro$.unsubscribe();
    }
  }

  ngOnInit(): void {

    this.dataSource_promociones_totales = new MatTableDataSource();
    this.filtro_control = new FormControl(false);
    this.traerPromociones();
    this.pendienteCambios();
  }

  private pendienteCambios() {
    this.filtro$ = this.filtro_control.valueChanges.subscribe(resp => {
      if (resp) {
        this.quitarFiltro();
      }
      else {
        this.filtrarInformacion();
      }
    });
  }

  private quitarFiltro() {
    this.promociones_filtrados = this.promociones_totales;
    this.dataSource_promociones_totales.data = this.promociones_filtrados;
  }

  private filtrarInformacion() {
    this.promociones_filtrados = [];
    this.promociones_totales.forEach(elemento => {
      if (elemento.pro_activo == 'Y') {
        this.promociones_filtrados.push(elemento)
      }
    });
    this.dataSource_promociones_totales.data = this.promociones_filtrados;
  }

  private traerPromociones() {
    this.spinner.show();
    this.loading.loading$.next({opacity:0.5})
    this.promocionServicio.listaPromociones().subscribe((resp: any) => {

      this.promociones_totales = resp;
      this.filtrarInformacion();

      console.log("promociones ", resp);
      this.spinner.hide();
    }, (error) => {
      this.spinner.hide();
      //this.eventosServicio.hideLoading();
      console.info("error promociones: ", error);
    });
  }

  ngAfterViewInit(): void {
    this.paginator._intl.itemsPerPageLabel = "Registros por página";
    this.dataSource_promociones_totales.paginator = this.paginator;
    this.dataSource_promociones_totales.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource_promociones_totales.filter = filterValue.trim().toLowerCase();
  }

  nuevaPromocion() {

    const dialogRef = this.dialog.open(PromocionFormComponent, {
      data: {
        nombre: "crear",
        programa: null,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result == undefined) {
        console.log("No se recibe nada");
      }
      else if (result.estado == 'EXITO') {
        this.alertaServicio.creacionCorrecta();
        this.traerPromociones();
      }
      else {
        console.log("ha ocurrido un error");
      }
    });
    

  }

  editarPromocion(row) {
    console.log(row);
    
    const dialogRef = this.dialog.open(PromocionFormComponent, {
      data: {
        nombre: "editar",
        promocion: row,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result == undefined) {
        console.log("No se recibe nada");
      }
      else if (result.estado == 'EXITO') {
        this.alertaServicio.actualizacionCorrecta();
        this.traerPromociones();
      }
      else {
        console.log("ha ocurrido un error");
      }
    });
    
  }

  cambiarEstado(row) {
    
    let nuevo_est = '';
    let codigo = row.pro_id;
    if (row.pro_activo == 'N') {
      nuevo_est = 'Y';
    }
    else {
      nuevo_est = 'N';
    }
    let body = {
      estadoNuevo: nuevo_est,
      promocionID: codigo,
    }

    this.promocionServicio.cambiarEstado(body).subscribe(resp => {
      if (resp['status'] == 'OK') {
        this.filtro_control.setValue(false);
        this.traerPromociones();
      }
      else {
        //error
      }
    }, (error) => {
      console.log(error);
    });
    
  }
  sortData(sort:Sort){
    const data = this.promociones_filtrados.slice();
    let arreglo_temporal :any []= [];
    if (!sort.active || sort.direction === '') {
      arreglo_temporal = data;
      this.dataSource_promociones_totales.data = arreglo_temporal;
      return;
    }

    arreglo_temporal = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'proNombre': return this.compare(a.pro_nombre||'', b.pro_nombre||'', isAsc);
        case 'programa': return this.compare(a.prg_nombre||'', b.prg_nombre||'', isAsc);
        case 'proCode': return this.compare(a.pro_code||'', b.pro_code||'', isAsc);
        case 'proDescripcion': return this.compare(a.pro_descripcion||'', b.pro_descripcion||'', isAsc);
        // case 'periodo': return this.compare(a.pro_descripcion||'', b.pro_descripcion||'', isAsc);
        default: return 0;
      }
    });
    this.dataSource_promociones_totales.data = arreglo_temporal;
  }
  private  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
  actualizarLista(){
    this.traerPromociones()
  }
  
}