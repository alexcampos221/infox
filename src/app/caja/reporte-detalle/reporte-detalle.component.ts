import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable } from 'rxjs';
import { NuevaCuota } from 'src/app/models/nueva-cuota';
import { ProgramaService } from 'src/app/shared/services/backend/programa.service';
import { AlertaService } from 'src/app/shared/services/general/alerta.service';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { AgregarCuotaComponent } from 'src/app/ventas/dialogs/agregar-cuota/agregar-cuota.component';
import { EditarCuotaComponent } from 'src/app/ventas/dialogs/editar-cuota/editar-cuota.component';
import { ProcesarCuotaComponent } from 'src/app/ventas/dialogs/procesar-cuota/procesar-cuota.component';
import Swal from 'sweetalert2';
import { FormEditCursoComponent } from '../dialogs/form-edit-curso/form-edit-curso.component';
import { FormReenviarCredencialesComponent } from '../dialogs/form-reenviar-credenciales/form-reenviar-credenciales.component';
import { MatriculaCajaComponent } from '../dialogs/matricula-caja/matricula-caja.component';

@Component({
  selector: 'app-reporte-detalle',
  templateUrl: './reporte-detalle.component.html',
  styleUrls: ['./reporte-detalle.component.sass'],
  providers:[DatePipe]
})
export class ReporteDetalleComponent implements OnInit {

  pageEvent: PageEvent;
  datasource: null;
  pageIndex:number;
  pageSize:number = 1;
  length:number;
  
  moneda: any [] = ['s/.','$'];
  existeCuotas:string;
  usuario:any;
  codigo:string;
  state$: Observable<object>;
  montoSaldo : any;
  montoTotal : any;
  montoAbonado : any;
  nueva_cuota: NuevaCuota | null;
  pagoID:string;
  estado:string='';
  isCuotaPagado:boolean = false;
  isGenCuota:boolean;
  //usuarioID:string;
  reporteDetalleForm: FormGroup
  tablaPagos: FormGroup

  isEliminarCuota : boolean = false;
  rowPago :any;
  programas_lista : any[]  = [];
  universidades_lista : any[] = [];
  cursos_lista : any[] =[];
  cuotas : any [] = [];

  isPagoCuota:boolean = false;
  isActivo:boolean = false;
  activeBtnMatricula:boolean;
  isMatriculado:boolean;
  gCuota:boolean;
  isMatricula:boolean=false;
  monedaID: any;

  arrayPagination : any [] = [];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  
  ventana:any;
  constructor(private fb: FormBuilder,
    private programaServicio: ProgramaService,
    public activatedRoute: ActivatedRoute,
    private router:Router,
    private datePipe: DatePipe,  
    private alertaServicio:AlertaService,
    private  dialog: MatDialog,
    private loadingService: LoadingService,
    private spinner: NgxSpinnerService,
    
    ) {
      
      if(JSON.parse(localStorage.getItem('pago')) != null){
        this.pagoID = JSON.parse(localStorage.getItem('pago'));
      }else{        
        this.pagoID=this.router.getCurrentNavigation().extras.state.data.pagoID;
        // this.ventana =this.router.getCurrentNavigation().extras.state.data.ventana;
      }
      if(JSON.parse(localStorage.getItem('arrayListaPendiente')) != null){
        this.arrayPagination = JSON.parse(localStorage.getItem('arrayListaPendiente'));
        this.pageIndex = JSON.parse(localStorage.getItem('indice'));
        this.length=this.arrayPagination.length;     
      }
    }

  ngOnInit(): void {        
    this.paginator._intl.getRangeLabel = (page: number, pageSize: number, length: number)=>{
      if (length == 0 || pageSize == 0) {
        return `0 de ${length}`;
      }
      const startIndex = page * pageSize;

      return `${startIndex+1} de ${length}`
    };  
    this.usuario = JSON.parse(localStorage.getItem('usu_actual'));      
    this.codigo = this.usuario.usu_id;
    this.crearFormulario();
    this.traerInfoPago();
    this.traerCuotas();
  }

  ngAfterViewInit():void{
    this.paginator._intl.getRangeLabel = (page: number, pageSize: number, length: number)=>{
      if (length == 0 || pageSize == 0) {
        return `0 de ${length}`;
      }
      const startIndex = page * pageSize;
      return `${startIndex+1} de ${length}`
    };    
  }

  private crearFormulario() {
    this.reporteDetalleForm = this.fb.group({
      
      fecha_abono:['',[Validators.required]],
      fecha_formato:['',[Validators.required]],
      nombre : ['',[Validators.required]],
      image_url : ['',[Validators.required]],  
      monto: ['',[Validators.required]],
      dni : ['',[Validators.required]],
      telefono: ['',[Validators.required]],
      email : ['',[Validators.required]],   
      monedaID: ['',[]],   
      
      activo: ['',[Validators.required]],

      matriculado :['',[Validators.required]], 
      universidad:['',[Validators.required]], 
      
      programa:['',[Validators.required]], 
      curso:['',[Validators.required]], 

      cur_lista_combo:['',[Validators.required]], 
      tiempo_contratado:['',[Validators.required]],

      pas_nombre:['',[Validators.required]],
      pag_inicio_curso:['',[Validators.required]],
      banco:['',[Validators.required]],
      bancoID:['',[Validators.required]],
      codigo_promocional:['',[Validators.required]],
      cur_usuario:['',[Validators.required]],
      cur_clave:['',[Validators.required]],
      pag_num_operacion:['',[Validators.required]],
      validado_estado:['',[Validators.required]],
      pago_cuotas:['',[Validators.required]],
      
      fecha_pago:['',[Validators.required]],
      monto_pago:['',[Validators.required]],
      
      usuario:['',[Validators.required]],
      clave:['',[Validators.required]],

    });
  }
  private get f(){
    return this.reporteDetalleForm.controls;
  }
 
  private traerInfoPago(){
    let salida={
      pagoID:this.pagoID
    };
    let body = JSON.stringify(salida);
    this.spinner.show();
    this.loadingService.loading$.next({opacity:0.5});
    this.programaServicio.traerInfoPrograma(body).subscribe((resp:any)=>{
      console.log(resp)
      this.rowPago = resp;
      if(this.rowPago != null){          
        if(this.rowPago.pag_confirmado_pago == 'N'){
          this.estado='Validando'
        }else{
          this.estado='Procesado'
        }
        this.montoSaldo = this.rowPago.pag_monto_saldo || 0;
        this.montoTotal = this.rowPago.pag_monto_total || 0;
        this.montoAbonado = this.rowPago.pag_monto_abonado || 0;
        this.f.bancoID.setValue(this.rowPago.ban_id);
        
        this.f.fecha_abono.setValue(this.rowPago.pag_fecha_abono_l);
        this.f.fecha_formato.setValue(this.rowPago.pag_fecha_abono);
        this.f.nombre.setValue(this.rowPago.pag_nombre);
        this.f.image_url.setValue(this.rowPago.pag_url);
        this.f.monto.setValue(this.rowPago.pag_monto);
        this.f.dni.setValue(this.rowPago.pag_dni);
        this.f.telefono.setValue(this.rowPago.pag_telefono);
        this.f.email.setValue(this.rowPago.pag_email);
        this.f.activo.setValue(this.rowPago.pag_activo);
        if(this.f.activo.value=='Y'){
          this.isActivo=true;
        }else{
          this.isActivo = false;
        }
        this.f.matriculado.setValue(this.rowPago.pag_matriculado);
        if(this.f.matriculado.value == 'Y'){
          this.isMatriculado = true;
        }else{
          this.isMatriculado = false;
        }
        if(this.estado == 'Validando' && (this.rowPago.validado_estado == 'Aprobado' || this.rowPago.validado_estado == 'Pendiente') && this.f.matriculado.value == 'N'){
          this.activeBtnMatricula=true;
        }else{
          this.activeBtnMatricula=false;
        }
        this.f.universidad.setValue(this.rowPago.uni_nombre || this.rowPago.uni_otros);
        //this.f.otros.setValue(this.rowPago.uni_otros);
        this.f.programa.setValue(this.rowPago.pro_nombre);
        this.f.curso.setValue(this.rowPago.cur_nombre || this.rowPago.cur_lista_combo);
        this.f.cur_lista_combo.setValue(this.rowPago.cur_lista_combo);
        this.f.tiempo_contratado.setValue(this.rowPago.pag_duracion);
        this.f.pago_cuotas.setValue(this.rowPago.pag_escuotas);        
        if(this.f.pago_cuotas.value=='Y'){
          this.isPagoCuota = true;
        }else{
          this.isPagoCuota = false;
        }
        this.f.validado_estado.setValue(this.rowPago.validado_estado);
        if((this.estado == 'Validando')  && (this.f.validado_estado.value== 'Pendiente' || this.f.validado_estado.value== 'Aprobado')){
          this.isGenCuota = true
        }else{
          this.isGenCuota = false
        }
        this.f.pas_nombre.setValue(this.rowPago.pas_nombre);
        this.f.pag_inicio_curso.setValue(this.rowPago.pag_inicio_curso);
        this.f.banco.setValue(this.rowPago.name);
        this.f.codigo_promocional.setValue(this.rowPago.pag_cod_promocion);
        this.f.cur_usuario.setValue(this.rowPago.cur_usuario);
        this.f.cur_clave.setValue(this.rowPago.cur_clave);
        if(this.rowPago.cur_clave != ''){
          this.rowPago.cur_clave='******'
        }
        this.f.pag_num_operacion.setValue(this.rowPago.pag_num_operacion);
        this.f.monedaID.setValue(this.monedaID || this.rowPago.pg_moneda);
        this.monedaID=this.rowPago.pg_moneda;
      }
      this.spinner.hide();
    },(error)=>{
      this.spinner.hide();
    })
  }
  cerrarDetalle(){
    localStorage.setItem('cerrar_detalle',JSON.stringify(true));
    this.router.navigate(['/pago_caja/reporte']);
    // console.log(this.ventana)
    // switch (this.ventana) {
    //   case 'pendientes':
    //     localStorage.setItem('cerrar_detalle',JSON.stringify(true));
    //     this.router.navigate(['/pago_caja/pagos']);
    //     break;
    //   case 'confirmados':
    //     localStorage.setItem('cerrar_detalle',JSON.stringify(true));
    //     this.router.navigate(['/pago_caja/aprobados']);
    //     break;
    //   case '':
    //     localStorage.setItem('cerrar_detalle',JSON.stringify(true));
    //     this.router.navigate(['/pago_caja/reporte']);
    //     break;
    //   case '':
    //     localStorage.setItem('cerrar_detalle',JSON.stringify(true));
    //     this.router.navigate(['/pago_caja/anulados']);
    //     break;
    //   case 'saldos':
    //     localStorage.setItem('cerrar_detalle',JSON.stringify(true));
    //     this.router.navigate(['/pago_caja/saldos']);
    //     break;
    //   default:
    //     this.router.navigate(['/caja/dashboard']);
    //     break;
    // }
  }
  private traerCuotas(){  
    let salida={
      pagoID:this.pagoID
    }
    let body = JSON.stringify(salida);  
    localStorage.setItem('pago',this.pagoID);
    this.spinner.show()
    this.loadingService.loading$.next({opacity:0.5})
    this.programaServicio.traerCuotasUsuario(body).subscribe((resp:any)=>{
      console.log(resp)
      if(resp.length == 0){
        this.gCuota=false;
        this.spinner.hide();
        console.log('no ahi cuotas')
      }else{
        this.cuotas = resp;
        // console.log(this.cuotas);        
        this.gCuota=true;
        this.spinner.hide();
      }      
    },(error)=>{
      console.log(error);
      this.spinner.hide();
    })
    //console.log(this.cuotas);
  }
 
  public getServerData(event?:PageEvent){
    this.pageIndex = event.pageIndex;
    localStorage.setItem('indice',JSON.stringify(event.pageIndex));
    this.pagoID = this.arrayPagination[event.pageIndex];    
    this.traerCuotas();
    this.traerInfoPago();
    return event;
  }

  editarCursoByCaja(){
    console.log(this.rowPago)
    const dialogRef = this.dialog.open(FormEditCursoComponent,{
      width: '600px',
      data:{
        ventana:'reporte',
        pagoID:this.rowPago.pag_id,
        usuarioID:this.codigo,
        programa_nombre: this.rowPago.pro_nombre,
        curso_nombre: this.rowPago.cur_nombre || this.rowPago.cur_lista_combo,
        usuario:this.rowPago.cur_usuario,
        clave:this.f.cur_clave.value,
        programaID: this.rowPago.curso_id,
        cursoID: this.rowPago.programa_id,
        meses:this.rowPago.pag_duracion,
        info: this.rowPago
      }
    });dialogRef.afterClosed().subscribe((result)=>{
      if( result == undefined){
        console.log("No se recibe nada");
      }
      else if(result.estado=='EXITO'){
        this.gCuota=true;        
        this.traerCuotas();
        this.traerInfoPago();
        this.alertaServicio.generarCuotaCorrecta();
      }
      else{
        console.log("ha ocurrido un error");
      }

      // if( result == undefined){
      //   console.log("No se recibe nada");
      // }
      // else if(result.estado=='EXITO'){
      //   this.isMatricula = true;
        
      //   this.alertaServicio.matriculaCorrecta();
      //   this.router.navigate(['/pago_caja/pagos']);
      // }
      // else{
      //   console.log("ha ocurrido un error");
      // }
    });   
  }
  seleccionPagoCuota(event){
    this.isPagoCuota=event.checked;
    //console.log(this.isPagoCuota);
  }
  ruta_imagen:any;
  verImageVoucher(){
    this.spinner.show();
      this.loadingService.loading$.next({message:'', opacity:0.5});
    try{      
      // this.alertaServicio.MostrarImagenVoucher(this.f.image_url.value);
      this.ruta_imagen=this.f.image_url.value
      this.spinner.hide();
    }catch(e){
      this.spinner.hide();
    }
    
  }
  hide:boolean = true;
  ocultarTxt(){
    if(this.rowPago.cur_clave != '******'){
      this.rowPago.cur_clave = '******';
      this.hide=true
    }else{
      this.hide=false
      this.rowPago.cur_clave = this.f.cur_clave.value
    }
    
  }
  aprobarCuotas(i){
    if(this.f.pago_cuotas.value == 'Y'){
      this.existeCuotas = 'Y'
    }else{
      this.existeCuotas = 'N'
    }
    let salida = {
      cuotaID : Number(this.cuotas[i].cuota_id),
      pagoID : Number(this.pagoID),
      usuarioID: Number(this.usuario.usu_id),
      esPrimero: this.existeCuotas
    }
    let body =  JSON.stringify(salida);
    // console.log(salida)
    this.programaServicio.aprobarCuotaByCaja(body).subscribe((resp)=>{
      console.log(resp);
      if(resp['status']='ok'){
        this.alertaServicio.pagoCuotaCorrecta();
        this.traerInfoPago();
        this.traerCuotas();
        this.isGenCuota=true;  
      }else{
        console.log('error');
      }
    })
  }
  aprobarPago(){
    if(this.f.pago_cuotas.value == 'Y'){
      this.existeCuotas = 'Y'
    }else{
      this.existeCuotas = 'N'
    }
    let salida = {
      cuotaID : 0,
      pagoID : Number(this.pagoID),
      usuarioID: Number(this.usuario.usu_id),
      esPrimero: this.existeCuotas
    }
    let body =  JSON.stringify(salida);
    // console.log(salida);
    this.programaServicio.aprobarCuotaByCaja(body).subscribe((resp)=>{
      if(resp['status']='ok'){
        this.traerInfoPago();
        console.log(resp)  
      }else{
        console.log('error');
      }
    })
  }


  editarPago(){
    const dialogRef = this.dialog.open(EditarCuotaComponent, {
      width: '600px',
      data: {
        nombre: "editar pago", 
        pagoID: this.pagoID,
        estado: '' || 'Validando',       
        url: this.rowPago.pag_url,
        bancoID: this.rowPago.ban_id,
        banco:this.rowPago.name,
        numOperacion: this.rowPago.pag_num_operacion,
        montoPago: this.rowPago.pag_monto,
        fechaPago: this.rowPago.pag_fecha_abono,
        cuotaID: 0,
        usuarioID: this.usuario.usu_id,
        //fVencimiento: ,
        //mCuota: this.rowPago.pag_monto,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if( result == undefined){
        console.log("No se recibe nada");
      }
      else if(result.estado=='EXITO'){
        this.gCuota=true;        
        this.alertaServicio.actualizacionCorrecta();
        this.traerCuotas();
        this.traerInfoPago();
      }
      else{
        console.log("ha ocurrido un error");
      }
    }); 
  }
  crearCuota(){    
    const dialogRef = this.dialog.open(AgregarCuotaComponent, {     
      data:{
        disableClose: true,
        codigoVendedor: this.codigo,
        pagoID: this.pagoID,
        monedaID: this.f.monedaID.value,
        montoS:this.montoSaldo,
        montoT:this.montoTotal
      }
    });
    dialogRef.afterClosed().subscribe((result) => {
      // console.log(result);
      if( result==undefined){
        console.log("No se recibe nada");
      }
      else if(result.estado=="EXITO"){
       
        this.alertaServicio.creacionCorrecta();
        this.traerCuotas();
        this.traerInfoPago();
      }
      else{
        console.log("ha ocurrido un error");
      }
  });
  }

  editCuota(index){
    //console.log(this.pagoID)
    const dialogRef = this.dialog.open(EditarCuotaComponent, {
      width: '600px',
      data: {
        nombre: "editar cuota", 
        pagoID: this.pagoID,
        estado: this.cuotas[index].cuota_estado || 'Validando',       
        url: this.cuotas[index].cuota_vaucher,
        bancoID: this.cuotas[index].ban_id,
        banco:this.cuotas[index].banco_nombre,
        numOperacion: this.cuotas[index].num_operacion,
        montoPago: this.cuotas[index].abono_monto,
        fechaPago: this.cuotas[index].fecha_abono,
        cuotaID: this.cuotas[index].cuota_id,
        usuarioID: this.codigo,
        fVencimiento: this.cuotas[index].fecha_vencimiento,
        mCuota: this.cuotas[index].cuota_monto,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if( result == undefined){
        console.log("No se recibe nada");
      }
      else if(result.estado=='EXITO'){
        this.gCuota=true;        
        this.alertaServicio.actualizacionCorrecta();
        this.traerCuotas();
        this.traerInfoPago();
      }
      else{
        console.log("ha ocurrido un error");
      }
    });         
  }

  eliminarCuota(i){
    let salida = {
      cuotaID : this.cuotas[i].cuota_id,
      pagoID : this.pagoID
    }
    let body =  JSON.stringify(salida);
    //console.log(salida);
    Swal.fire({
      title: 'Estas seguro de eliminar la cuota?',
      text: "¡No podrás revertir esto!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: '¡Sí, bórralo!'
    }).then(result => {
      if (result.value) {
        Swal.fire('¡Eliminado! ', 'La cuota ha sido eliminada.', 'success');
        this.programaServicio.eliminarCuota(body).subscribe((resp) =>{
          if(resp["status"] == "OK"){            
            this.traerCuotas();
            this.traerInfoPago();
          }else{
            this.alertaServicio.errorInterno();
            // console.log(resp["status"]);
          }     
        }) 
      }
    });
  }

  procesar(){
    // console.log(this.f)
    const dialogRef = this.dialog.open(ProcesarCuotaComponent, {
      data: {
        nombre: "generar cuota",
        url: this.f.image_url.value,
        bancoID: this.f.bancoID.value,
        numOperacion: this.f.pag_num_operacion.value,
        mPagado: this.f.monto.value,
        fPagoL: this.f.fecha_formato.value,
        vendedorID: this.codigo,
        pagoID:this.pagoID,
        monedaID: this.monedaID       
      },
    });
    dialogRef.afterClosed().subscribe((result ) => {
      if( result == undefined){
        console.log("No se recibe nada");
      }
      else if(result.estado=='EXITO'){
        this.gCuota=true;
        
        this.traerCuotas();
        this.traerInfoPago();
        this.alertaServicio.generarCuotaCorrecta();
      }
      else{
        console.log("ha ocurrido un error");
      }
    });
  }
  matricularByCaja(){
    // console.log(this.rowPago)
    const dialogRef = this.dialog.open(MatriculaCajaComponent,{
      data:{
        pagoID:this.rowPago.pag_id,
        usuario_correo:this.rowPago.pag_email,
        usuarioID:this.codigo,
        alumno_nombre:this.rowPago.pag_nombre ,
        programa_nombre: this.rowPago.pro_nombre,
        curso_nombre: this.rowPago.cur_nombre,
        tiempo_contratado:this.rowPago.pag_duracion,
        ini_cur:this.rowPago.pag_inicio_curso,
        dni: this.rowPago.pag_dni
      }
    });dialogRef.afterClosed().subscribe((result)=>{
      if( result == undefined){
        console.log("No se recibe nada");
      }
      else if(result.estado=='EXITO'){
        this.isMatricula = true;
        this.traerCuotas();
        this.traerInfoPago();
        this.alertaServicio.matriculaCorrecta();
      }
      else{
        console.log("ha ocurrido un error");
      }
    });   
  }
  enviarCredenciales(){
    const dialogRef = this.dialog.open(FormReenviarCredencialesComponent, {
      data: {
        nombre: "Reenvio",
        usuario_nombre:this.rowPago.cur_usuario,
        usuario_clave:this.f.cur_clave.value,
        usuario_correo:this.rowPago.pag_email,
        pagoID:this.pagoID,
        usuarioID:this.codigo,
        alumno_nombre:this.rowPago.pag_nombre,
        programa_nombre:this.rowPago.pro_nombre,
        alumno_dni:this.rowPago.pag_dni,
      },
    });
    dialogRef.afterClosed().subscribe((result ) => {
      console.log(result)
      if( result == undefined){
        console.log("No se recibe nada");
      }
      else if(result.estado=='EXITO'){
        this.gCuota=true;
        this.traerCuotas();
        this.traerInfoPago();
        this.alertaServicio.reenvioEmail();
      }
      else if(result.estado=='REINTENTAR'){
        console.log("error enviar email");
        this.alertaServicio.errorEnviarEmail();
      }else{
        console.log("ha ocurrido un error");
      }
    });
  }
  enviarFormulario(){}
  volver(){}

}
