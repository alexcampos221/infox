import { Component, OnInit } from '@angular/core';
import { LoadingService } from 'src/app/shared/services/loading.service';
@Component({
  selector: 'app-page-loader',
  templateUrl: './page-loader.component.html',
  styleUrls: ['./page-loader.component.sass']
})
export class PageLoaderComponent implements OnInit {

  opacity =1 ;
  message = "";

  constructor(
    public loadingService: LoadingService
  ) {
    this.loadingService.loading$.subscribe((resp)=>{
      this.message=resp['message'];
      if(resp['opacity']){
        this.opacity=resp['opacity'];
      }else{
        this.opacity=1;
      }
    })
  }

  ngOnInit() {}
}