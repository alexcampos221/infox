import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { UsuarioService } from 'src/app/shared/services/backend/usuario.service';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { EditUsuarioComponent } from '../dialogs/edit-usuario/edit-usuario.component';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.sass']
})
export class UsuariosComponent implements OnInit {

  txtNuevo:any;

  dataSource_locales_totales : MatTableDataSource<any>;
  locales_filtrados : any [] = []

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;

  displayedColumns = [
    'expandir',
    'perfil',
    'nombre',
    'nombre_completo',
    'usuario',
    'telefono',
    'email',
    'tipo_usu',
    'activo',    
    'acciones',
  ];


  constructor(private usuarioService:UsuarioService,
    private router : Router,
    private loadingService: LoadingService,
    private spinner: NgxSpinnerService,
    private  dialog: MatDialog,) { }

  ngOnInit(): void {
    this.dataSource_locales_totales = new MatTableDataSource();
    this.traerInfoUsuarios()
  }

  traerInfoUsuarios(){
    let salida={
      tipo: '0'
    }
    this.usuarioService.listar(JSON.stringify(salida)).subscribe((resp : any)=>{
      console.log(resp);
      this.dataSource_locales_totales.data = resp || [];
      this.locales_filtrados = resp || [];
      console.log(this.dataSource_locales_totales.data)
    })
  }
  detalleUsuario(row,index){
    let arreglo = [];
    let salida={
      usuID:row.usu_id
    }
    this.dataSource_locales_totales.data.forEach(elemento=>{
      arreglo.push(elemento.usu_id);
    });
    
    localStorage.setItem('indice',index);  
    localStorage.setItem('info_usu',JSON.stringify(arreglo));
    localStorage.setItem('info_usu_id',row.usu_id);  

    this.router.navigate(['maestros/detalle'] ,{ state : {data: salida } })
  }

  applyFilter(filterValue){
    let filterV=filterValue.value
    this.dataSource_locales_totales.filter = filterV.trim().toLowerCase() || '';
  }

  editUser(row){
    const dialogRef = this.dialog.open(EditUsuarioComponent,{
      
      data:{
        nombre: "Editar",
        usuario: row
      }
    });
    dialogRef.afterClosed().subscribe((result) => {
      if( result == undefined){
        console.log("No se recibe nada");
      }
      else if(result.estado=='EXITO'){
      this.traerInfoUsuarios()
      }
      else{
        console.log("ha ocurrido un error");
      }
    });   
  }

  addUser(){
    const dialogRef = this.dialog.open(EditUsuarioComponent,{
      
      data:{
        nombre: "Crear",
      }
    });
    dialogRef.afterClosed().subscribe((result) => {
      if( result == undefined){
        console.log("No se recibe nada");
      }
      else if(result.estado=='EXITO'){
      this.traerInfoUsuarios()
      }
      else{
        console.log("ha ocurrido un error");
      }
    });  
  }
  chageStateValue(data){
    if(data==='N') return 'Y';
    else if(data==='Y') return 'N';
    else throw 'error';  
  }

  ocultarRow(row){
    console.log(row);
    var body = {
      "nuevoEstado": this.chageStateValue(row.usu_activo),
      "usuarioID": 100
    }
    console.log(body)
    this.spinner.show();
    this.loadingService.loading$.next({opacity:0.5})
    this.usuarioService.status(body).subscribe((resp:any)=>{
      console.log(resp);
      this.spinner.hide();
      // //this.datos = resp||[];
      // this.dataSource_locales_totales.data = resp||[];
      // this.locales_filtrados = resp||[];
      // this.cargando = true;
      this.traerInfoUsuarios();
    },(error)=>{
      console.log(error);
      this.spinner.hide();
    });
  }
  sortData(sort: Sort){
    const data = this.locales_filtrados.slice();
    let arreglo_temporal :any []= [];
    if (!sort.active || sort.direction === '') {
      arreglo_temporal = data;
      this.dataSource_locales_totales.data = arreglo_temporal;
      return;
    }

    arreglo_temporal = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        // case 'perfil': return this.compare(a.rol_nom||'', b.rol_nom||'', isAsc);
        case 'nombre': return this.compare(a.usu_nombre||'', b.usu_nombre||'', isAsc);
        case 'nombre_completo': return this.compare(a.usu_nombre_compleo||'', b.usu_nombre_compleo||'', isAsc);
        case 'usuario': return this.compare(a.usu_usuario||'', b.usu_usuario||'', isAsc);
        case 'telefono': return this.compare(a.usu_telefono||'', b.usu_telefono||'', isAsc);
        case 'email': return this.compare(a.usu_email||'', b.usu_email||'', isAsc);
        case 'tipo_usu': return this.compare(a.usu_tipo||'', b.usu_tipo||'', isAsc);
        case 'activo': return this.compare(a.usu_activo||'', b.usu_activo||'', isAsc);
        default: return 0;
      }
    });
    this.dataSource_locales_totales.data = arreglo_temporal;
  }
  private  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
  actualizarLista(){
    this.traerInfoUsuarios()
  }
}
