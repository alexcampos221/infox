import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { MatriculaService } from 'src/app/shared/services/registros/matricula.service';
import { DateAdapter } from '@angular/material/core';
@Component({
  selector: 'app-informes',
  templateUrl: './informes.component.html',
  styleUrls: ['./informes.component.sass']
})
export class InformesComponent implements OnInit {

  codigo: any;
  formMatricula: FormGroup;

  universidades_lista: any[] = [];
  programas_cursos_lista: any[] = [];

  constructor(private rutaActiva: ActivatedRoute,
    private dateAdapter: DateAdapter<Date>,
    private fb: FormBuilder,
    private matriculaServicio: MatriculaService) {
    this.dateAdapter.setLocale('es-PE');
  }

  ngOnInit(): void {
    this.crearFormulario();
    this.traerUniversidades();
    this.listaProgramaCursos();
  }

  private crearFormulario() {
    this.formMatricula = this.fb.group({
      
      nombres: ['', [Validators.required]],
      correo: ['', [Validators.required, Validators.email]],
      telefono: ['', [Validators.required]],
      universidad : ['',Validators.required],
      otros : ['',[]],
      programa : ['',[Validators.required]]
    });
  }

  enviarFormulario() {
    //console.log(this.formMatricula.value);

    let programa = this.formMatricula.controls.programa.value.pro_id;
    let curso = this.formMatricula.controls.programa.value;
    console.log(curso);

    let body = {
      
    };


    console.log(body);

  }

  private traerUniversidades() {
    this.matriculaServicio.listaUniversidades().subscribe((resp: any) => {
      console.log(resp);
      this.universidades_lista = resp['result'];
    });
  }

  private listaProgramaCursos() {

    this.matriculaServicio.listaProgramaCursos().subscribe((resp: any) => {
      console.log(resp);
      this.programas_cursos_lista = resp['result'];
    });
  }
}
