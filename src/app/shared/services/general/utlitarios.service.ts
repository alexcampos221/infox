import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

import * as Global from '../../globals';
import { Utils } from '../../utils';

@Injectable({
  providedIn: 'root'
})
export class UtlitariosService {

  constructor(public http : HttpClient) { }

  listarPaises(){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Pais/listar', Utils.jsonToFormDataV2(false), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
 
  traerCursosConDni(body){
    const requestOptions = {
      headers: new HttpHeaders({
        //'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_base_infox+ 'buscarProgramas',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  traerCuotasPorAlumno(body){
    const requestOptions = {
      headers: new HttpHeaders({
        //'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Api/listarCuotasDePrograma',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  
  traerNombresConDNI(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.raiz_parking+ 'getDniExt', Utils.jsonToFormDataV2(body,false), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

  traerRazoSocialConRuc(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.raiz_parking+ 'getRucExt', Utils.jsonToFormDataV2(body,false), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

  enviarEmailPago(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_base_infox+ 'enviarEmailPago', Utils.jsonToFormDataV2(body,false), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  listarBancos(){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.get(Global.ruta_base_infox+ 'listaBancos', requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  guardarCuota(body){
    const requestOptions = {
      headers: new HttpHeaders({
        //'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Matricula/editarCuotaByUser', (body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  eliminarPendiente(body){
    const requestOptions = {
      headers: new HttpHeaders({
        //'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Matricula/cambiarEstado',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

  getVendedorAct(body){
    const requestOptions = {
      headers: new HttpHeaders({
        //'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_base_infox+ 'getVendedorAct',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  getListaPagosPendiente(body){
    const requestOptions = {
      headers: new HttpHeaders({
        //'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Matricula/listarPagosByVendedor',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  getListaSaldosByVendedor(body){
    const requestOptions = {
      headers: new HttpHeaders({
        //'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Matricula/listarSaldosByVendedor',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  
  getListaPagosAprobadosByVendedor(body){
    const requestOptions = {
      headers: new HttpHeaders({
        //'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Matricula/listarPagosConfirmadosByVendedor',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  // getListaPagosAprobadosByCaja(body){
  getListaPagosAprobadosByCaja(body){
    const requestOptions = {
      headers: new HttpHeaders({
        //'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Matricula/listarPagosConfirmadosByCaja',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

  getListaPagosAprobadosByVendor(body){
    const requestOptions = {
      headers: new HttpHeaders({
        //'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Matricula/listarPagosConfirmados',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }


  
  getListaPagosPendienteByCaja(body){
    const requestOptions = {
      headers: new HttpHeaders({
        //'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Matricula/listarPagosByCaja',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  getListaAnulados(body){
    const requestOptions = {
      headers: new HttpHeaders({
        //'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Matricula/listarPagosByCaja',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  matriculaByCaja(body){
    const requestOptions = {
      headers: new HttpHeaders({
        //'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Matricula/aprobarMatricula',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

  enviarWppPrueba(body){
    const requestOptions = {
      headers: new HttpHeaders({
        //'Content-Type':  'application/json'
        'Authorization': `Bearer ${Global.tokenTestWpp}`
      })
    };
    let request = this.http.post(Global.api_mensajes + 'whatsapp/messages',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  createGroupEmailPrueba(body){
    const requestOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Accept': 'application/json',
        'Authorization': `Bearer ${Global.tokenTestEmail}`
      })
    };
    let request = this.http.post(Global.api_email_test + 'api/groups',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

  enviarEmailPrueba(body){
    const requestOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Accept': 'application/json',
        'Authorization': `${Global.tokenTestEmail}`
      })
    };
    let request = this.http.post(Global.api_email_test + 'whatsapp/messages',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
}
