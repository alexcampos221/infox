import { AuthService } from './../../shared/security/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Role } from './../../shared/security/role';
import { SesionService } from 'src/app/shared/services/backend/sesion.service';
import { AlmacenamientoLocalService } from 'src/app/shared/services/general/almacenamiento-local.service';
@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss'],
})
export class SigninComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  error = '';
  hide = true;

  mensaje_error:string = '';

  error_auth : boolean = false;
  enviando : boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private sesionServicio: SesionService,
    private localServicio : AlmacenamientoLocalService,
  ) {}
  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }
  get f() {
    return this.loginForm.controls;
  }
  /*
  onSubmit() {
    this.submitted = true;
    this.error = '';
    if (this.loginForm.invalid) {
      this.error = 'Username and Password not valid !';
      return;
    } else {
      this.authService
        .login(this.f.username.value, this.f.password.value)
        .subscribe(
          (res) => {
            if (res.success) {
              const role = this.authService.getRole();

              if (role === Role.All || role === Role.Admin) {
                this.router.navigate(['/admin/dashboard/main']);
              } else if (role === Role.Teacher) {
                this.router.navigate(['/teacher/dashboard']);
              } else if (role === Role.Student) {
                this.router.navigate(['/student/dashboard']);
              } else {
                this.router.navigate(['/authentication/signin']);
              }
            } else {
              this.error = 'Invalid Login';
            }
          },
          (error) => {
            this.error = error;
            this.submitted = false;
          }
        );
    }
  }
  */

  iniciarSesion(){
    let body = {
      usuario : this.f.username.value,
      clave : this.f.password.value,
    };
    //console.log(body);
    this.error_auth = false;
    this.loginForm.controls.username.disable();
    this.loginForm.controls.password.disable();
    this.enviando = true;

    this.sesionServicio.loginBackEnd(body).subscribe(resp=>{
      console.log(resp);
      if(resp['status']=='OK'){
        if(resp['result'] !=null){
          let array = resp['result'];
          if(Array.isArray(array) && array.length){
            console.log("result ***********");
            if(resp['result'][0].usu_activo == 'Y' && resp['result'][0].usu_bloqueado=='N'){
              console.log("Hacer login");
              let user_back = resp['result'][0];
              if(resp['result'][0].usu_tipo == 200){
                console.log(resp['result'][0].usu_tipo)
                this.guardarEnStorage(user_back);              
                this.router.navigate(['/admin/dashboard']);
              }
              if(resp['result'][0].usu_tipo == 0o0){
                this.guardarEnStorage(user_back);  
                this.router.navigate(['/venta/dashboard']);
                console.log(resp['result'][0].usu_tipo)                
                console.log('vendedor')
              }
              if(resp['result'][0].usu_tipo == 100){
                this.guardarEnStorage(user_back);  
                this.router.navigate(['/caja/dashboard']);
                console.log(resp['result'][0].usu_tipo)
                console.log('caja')
              }

              if(resp['result'][0].usu_tipo == "300"){
                this.guardarEnStorage(user_back);  
                this.router.navigate(['/venta/dashboard']);
                console.log(resp['result'][0].usu_tipo)
                console.log('venta')
              }
            }
            else{
              this.mensaje_error = "Usuario no habilitado";
              this.errorAutenticacion();
            }
          }
          else{
            this.mensaje_error = "Usuario no Habilitado";
            this.errorAutenticacion();
          }
          
        }
        else{
          this.mensaje_error = "Usuario no registrado";
          this.errorAutenticacion();
        }
      }
      else{
        this.mensaje_error = "Error de Autenticación";
        this.errorAutenticacion();
      }
      
    });
  }

  private errorAutenticacion(){
    this.loginForm.controls.username.enable();
    this.loginForm.controls.password.enable();
    this.enviando =false;
    this.error_auth = true;
  }

  private guardarEnStorage(usuario){
    this.localServicio.setItem('usu_actual',usuario);
  }
}
