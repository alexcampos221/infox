import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProgramaService } from 'src/app/shared/services/backend/programa.service';
import { PromocionService } from 'src/app/shared/services/backend/promocion.service';
import { DateAdapter } from '@angular/material/core';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-promocion-form',
  templateUrl: './promocion-form.component.html',
  styleUrls: ['./promocion-form.component.sass'],
  providers : [DatePipe],
})
export class PromocionFormComponent implements OnInit {

  promocionForm: FormGroup;
  editar: boolean = false;
  nombre_modal: string;
  programas : any[] = [];

  constructor(private programaServicio: ProgramaService,
              private promocionServicio: PromocionService,
              private fb: FormBuilder,
              public dialogRef: MatDialogRef<PromocionFormComponent>,
              private dateAdapter : DateAdapter<Date>,
              private datePipe: DatePipe,
              @Inject(MAT_DIALOG_DATA) public data: any,) {
                this.dateAdapter.setLocale('es-PE');
               }

  ngOnInit(): void {
    this.nombre_modal = 'Crear Promoción';
    
    if (this.data.nombre == 'editar') {
      this.nombre_modal = 'Editar Promoción';
      this.editar = true;
      this.crearFormularioEditar();
    }
    else {
      this.crearFormulario();
    }
    this.traerProgramas();
  }

  private traerProgramas(){
    this.programaServicio.listaProgramas().subscribe((resp:any) =>{

      this.programas = resp; 
      
      console.log("programas ",resp);
      if(this.editar){
        this.promocionForm.controls.programa.setValue(this.data.promocion.prg_id);
      }
    },(error)=>{
      console.info("error programas: ",error);
    });
  }

  private crearFormulario() {
    this.promocionForm = this.fb.group({
      nombre: ['', [Validators.required]],
      desde: ['', []],
      hacia: ['', []],
      programa : ['', [Validators.required]],
      codigo_pro : ['', [Validators.required]],      
      descripcion : ['', [Validators.required]],
    });
  }

  private crearFormularioEditar() {
    let nombre = this.data.promocion.pro_nombre;
    let codigo = this.data.promocion.pro_id;
    let descripcion = this.data.promocion.pro_descripcion;
    let desde =  this.data.promocion.pro_desde;
    let hacia =  this.data.promocion.pro_hasta;
    let codigo_pro  =  this.data.promocion.pro_code;
    let desde_n = desde;
    let hacia_n = hacia;
    if(desde !=null && hacia !=null){
      console.log("wwwwwwwwww");
      desde_n = new Date(desde);
      hacia_n = new Date(hacia);
    }

    

    this.promocionForm = this.fb.group({
      nombre: [nombre, [Validators.required]],
      codigo: [codigo, [Validators.required]],
      programa : ['', [Validators.required]],
      desde: [desde_n, []],
      hacia: [hacia_n, []],
      codigo_pro : [codigo_pro, [Validators.required]],
      descripcion : [descripcion, [Validators.required]],
    });
  }

  enviarFormulario() {
    if (this.editar) {
      this.editarPromocion();
    }
    else {
      this.crearPromocion();
    }
  }

  closeDialog(): void {
    this.dialogRef.close();
  }

  private editarPromocion() {
    let body = this.crearConsulta();
    let cuerpo = body;
    cuerpo.promocionID = this.promocionForm.controls.codigo.value;
    console.log(body);
    this.promocionServicio.editarPromocion(body).subscribe(resp => {
      console.log(resp);
      if (resp['status'] == 'OK') {
        this.dialogRef.close({ estado: 'EXITO' })
      }
      else {
        this.dialogRef.close({ estado: 'FALLO' });
      }
    }, (error) => {
      console.log(error);
      this.dialogRef.close({ estado: 'FALLO' });
    });
    
  } 

  private crearPromocion() {    
    let body = this.crearConsulta();
    console.log(body);
    
    this.promocionServicio.guardarPromocion(body).subscribe(resp => {
      console.log(resp);
      if (resp['status'] == 'OK') {
        this.dialogRef.close({ estado: 'EXITO' })
      }
      else {
        this.dialogRef.close({ estado: 'FALLO' });
      }
    }, (error) => {
      console.log(error);
      this.dialogRef.close({ estado: 'FALLO' });
    });
    
  
  }

  private crearConsulta(){
    let body;
    let nombre = this.promocionForm.controls.nombre.value;
    let programa_id = this.promocionForm.controls.programa.value;
    let cod_promocion = this.promocionForm.controls.codigo_pro.value;
    let descripcion = this.promocionForm.controls.descripcion.value;
   
    let desde = this.promocionForm.controls.desde.value || '';
    let hacia = this.promocionForm.controls.hacia.value || '';

    if(desde == ''|| hacia == ''){
      body = {
        nombre : nombre,
        promoCode : cod_promocion,
        descripcion : descripcion,
        programaID : programa_id,
      }
    }
    else{
      body = {
        nombre : nombre,
        promoCode : cod_promocion,
        descripcion : descripcion,
        programaID : programa_id,
        DateFrom : this.datePipe.transform(desde,'yyyy/MM/dd'),
        DateTo : this.datePipe.transform(hacia,'yyyy/MM/dd'),
      }
    }

    return body;
  }
}