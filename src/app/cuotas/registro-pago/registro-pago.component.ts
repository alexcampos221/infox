import { Component, NgZone, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { MatriculaService } from 'src/app/shared/services/registros/matricula.service';
import { DateAdapter } from '@angular/material/core';
import { UtlitariosService } from 'src/app/shared/services/general/utlitarios.service';
import { Subscription } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { AlertaService } from 'src/app/shared/services/general/alerta.service';
import { FirebaseStorageService } from 'src/app/shared/services/storage/firebase-storage.service';
import { DatePipe } from '@angular/common';
import { ChangeDetectorRef } from '@angular/core';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-registro-pago',
  templateUrl: './registro-pago.component.html',
  styleUrls: ['./registro-pago.component.sass'],
  providers: [DatePipe],
})
export class RegistroPagoComponent implements OnInit {

  codigo: any;

  cuotaForm : FormGroup;
  documentos: any[] = [];
  cursos:any[]=[];
  todos_doc: any[] = [
    {
      nombre: 'Otros', valor: 0, tamanio :0,
    },
    {
      nombre: 'DNI', valor: 1, tamanio : 8,
    },
    {
      nombre: 'RUC', valor: 2, tamanio : 11,
    }
  ];
  color_estado:string='';
  nombre_documento : string = "Documento";

  nombre_curso : string = "Nombres";
  matricula_id : string='';
  isSelection:boolean=false;
  enviandoformulario : boolean = false;
  fecha_maxima = new Date();
  paises_lista: any[] = [];
  cuotas_pago_selected:any[] = [];
 
  lista_bancos : any []=[];

  cuotas_pago : any[] = [];
  monto_total_cuotas:number = 0;
  resultado_consulta : boolean = false;
  
  asesor_completo :any;
  asesor_nombre : string = '';

  estado_asesor : string = 'INICIO';
  
  ruta_multimedia: any;

  constructor(private rutaActiva: ActivatedRoute,
              private dateAdapter: DateAdapter<Date>,
              private fb : FormBuilder,
              private cdRef:ChangeDetectorRef,
              private utilitarios: UtlitariosService,
              private alertaServicio:AlertaService,
              private storage:FirebaseStorageService,
              private spinner:NgxSpinnerService,
              private datePipe: DatePipe,
              private loading:LoadingService,
              private zone: NgZone
              ) {
                this.dateAdapter.setLocale('es-PE');
                
               }

  ngOnInit(): void {
    this.prepararRegistro();
    this.obtenerAsesor();
  }


  ngAfterViewChecked(){
    this.cdRef.detectChanges();
  }

  private prepararRegistro(){
    this.traerPaises();
    this.traerBancos();
    this.crearFormulario();
    this.deshabilitarCampos();
    this.prepararTiposDoc([this.todos_doc[0]]);
  }
  private obtenerAsesor(){
    this.codigo = this.rutaActiva.snapshot.params.codigo;
    let salida = {
      code : this.codigo
    };
    let body = JSON.stringify(salida);
    this.spinner.show();
    this.loading.loading$.next({opacity:0.5});
    this.utilitarios.getVendedorAct(body).subscribe((resp:any)=>{
      console.log(resp);
      this.spinner.hide();
      if(resp['status']=='OK'){
        this.asesor_completo = resp;
        this.asesor_nombre = this.asesor_completo.result.usu_nombre;
        this.f.asesor.setValue(this.asesor_nombre);
       
        this.estado_asesor = 'VALIDO';
        //this.f.codigo_empleado.setValue(this.codigo);
        this.f.codigo_empleado.setValue(this.asesor_completo.result.usu_id)
      }
      else{
        //this.ruteador.navigate(['/error']);
        this.estado_asesor = 'ERROR';
      }
    },(error)=>{
      //this.alertaServicio.errorInterno();
      //this.ruteador.navigate(['/error']);
      this.spinner.hide();
      console.log(error);
      this.estado_asesor = 'ERROR';
    });
  }
  private traerPaises(){
    this.utilitarios.listarPaises().subscribe((resp: any) => {
      //console.log(resp);
      this.paises_lista = resp.filter((p)=>p.pai_activo=='Y');
    });
  }
  private traerBancos(){
    this.utilitarios.listarBancos().subscribe((resp:any)=>{
      this.lista_bancos=resp.result;     
      console.log(this.lista_bancos);
    })
  }

  private crearFormulario(){    
    this.cuotaForm = this.fb.group(
      {
        pais: ['', []],
        tipo_doc: ['', [Validators.required]],
        documento: ['', [Validators.required]],
        nombres: [this.nombre_curso, []],
        monto: ['', [Validators.required]],
        imagen: ['', [Validators.required]],
        banco_nom:['', [Validators.required]],
        bancoID:['', [Validators.required]],
        fecha: ['', [Validators.required]],
        operacion: ['', [Validators.required]],
        codigo_empleado: ['', [Validators.required]],
        curso_pago : ['',[]],
        cuota_pago : ['',[]],
        asesor : ['',[]],

        vencimiento : ['23/02/2022',[Validators.required]]
      }
    );
  }

  private get f(){
    return this.cuotaForm.controls;
  }

  private deshabilitarCampos(){
    this.f.curso_pago.disable();
    this.f.cuota_pago.disable();
    this.f.monto.disable();
    this.f.vencimiento.disable();
    this.f.imagen.disable();
    this.f.operacion.disable();
    this.f.fecha.disable();
    this.f.banco_nom.disable();
  }

  private habilitarCampos(){
    this.f.curso_pago.enable();
    this.f.cuota_pago.enable();
    this.f.monto.enable();
    this.f.vencimiento.enable();
    this.f.imagen.enable();
    this.f.operacion.enable();
    this.f.fecha.enable();
    this.f.banco_nom.enable();
  }

  public selectTipoDoc(evento) {
    let documento = evento;
    switch (documento.valor) {
      case 1:
        //setear el validator 
        console.log("Validator dni");
        this.f.documento.setValidators([]);
        this.f.documento.setValidators([Validators.required,Validators.maxLength(8), Validators.minLength(8)]);
        this.f.documento.setValue('');
        this.f.nombres.setValue('');
        break;
      case 2:
        //setear el validator 
        console.log("Validator RUC");
        this.f.documento.setValidators([]);
        this.f.documento.setValidators([Validators.required,Validators.maxLength(11), Validators.minLength(11)])
        this.f.documento.setValue('');
        this.f.nombres.setValue('');
        break;
      case 0:
        this.f.documento.setValidators([]);
        this.f.documento.setValidators(Validators.required);
        this.f.documento.setValue('');
        this.f.nombres.setValue('');
      default:
        break;
    }
  }

  private prepararTiposDoc(arreglo: any[]) {
    this.documentos = arreglo;
  }

  public buscarDocumento(cadena){ 
    switch(cadena){
      case 1:
        this.f.documento.setValidators([Validators.required,Validators.maxLength(8), Validators.minLength(8)]);
        break;
    }   
    
    console.log(cadena);
  }
  
  public selecPais(evento) {
    
    let pais = evento;
    this.f.nombres.setValue('');
    this.f.documento.setValue('');
    //this.f.universidad.setValue('');
    //this.f.cod_pais.setValue(evento.pai_codigo||'');

    //this.otra_universidad = false;

    this.nombre_documento = evento.pais_nombre_documento;
    //this.universidadesByPais(evento);

    if (pais.pai_id == "1000000") {
      this.todos_doc[0].nombre = 'Otros';
      this.prepararTiposDoc(this.todos_doc);
      this.f.tipo_doc.enable();
    }
    else {
      this.todos_doc[0].nombre = evento.pais_nombre_documento||'Documento';
      this.prepararTiposDoc([this.todos_doc[0]]);
      this.f.tipo_doc.setValue(this.todos_doc[0]);
      this.f.tipo_doc.disable();
      
      //actualizar el numero
    }
  }

  eButton(){    
    if(this.f.pais.value == ''){
      return true
    } else if(this.f.tipo_doc.value == ''){
      return true
    }else if(this.f.documento.value == '' ){
      return true
    }else{     
      return false;
    }         
  }

  public selectCurso(valor){
    this.nombre_curso = valor.pro_nombre;
    this.matricula_id = valor.mat_id;
    let salida = {      
      matriculaID:this.matricula_id,
    };
    let body = JSON.stringify(salida);
    this.utilitarios.traerCuotasPorAlumno(body).subscribe((resp:any)=>{
      this.cuotas_pago=resp; 
    })
    
  }
  
  public selectCuota(valor){    
    console.log(valor.checked);
  }
  public selecBanco(evento){
    console.log(evento.ban_nombre)
    this.f.bancoID.setValue(evento.ban_id);
    this.f.banco_nom.setValue(evento.ban_nombre)
  }
  public seleccionCuota(valor,programa:any,i){
    if(valor.checked == true){
      this.monto_total_cuotas = Number(this.cuotas_pago[i].cuota_monto) + this.monto_total_cuotas
      this.cuotas_pago_selected.push(programa);
    }else if(valor.checked == false){
      this.monto_total_cuotas = this.monto_total_cuotas - Number(this.cuotas_pago[i]['cuota_monto']);
      for(let x = 0 ; x <= this.cuotas_pago_selected.length; x++){            
        if(this.cuotas_pago_selected[x]['cuota_id'] == programa.cuota_id){    
          this.cuotas_pago_selected.splice(x,1);                        
          break;
        }
      }
    }
  }

  public buscarUsuario(){
    this.habilitarCampos();
    
    let salida = {
      documento:this.f.documento.value
    };
    let body = JSON.stringify(salida);
    this.utilitarios.traerCursosConDni(body).subscribe((resp:any)=>{
       if(resp.alumno==null && resp.programas.length==0){
          this.resultado_consulta = false;
          this.alertaServicio.usuarioProgramaNoEncontrado();
        }else if(resp.programas.length==0){
          this.resultado_consulta = false;
          this.alertaServicio.usuarioProgramaSinDeuda(resp.alumno.usu_nombre);                    
        }else{
          this.cursos=resp.programas;  
          this.resultado_consulta = true;
          //console.log(this.cursos[0])
         //this.alertaServicio.usuarioProgramaCuotas();
          this.f.curso_pago.setValue(this.cursos[0]);
          //console.log(this.f.curso_pago.value)
          this.selectCurso(this.cursos[0]);
        }
      },(error)=>{
        console.log(error);
      });
    }

  
  enviarFormulario() { 
    this.enviandoformulario = true; 
    this.spinner.show();
    //this.loading.loading$.next({opacity:0.5});
    this.storage.uploadFile(this.f.imagen.value._files,"temporales").then((publicURL:any)=>{
      this.ruta_multimedia = publicURL;
      //console.log(this.ruta_multimedia);
      this.spinner.hide();
      this.enviarCuota();

    },(error)=>{
      console.log(error);
      this.enviandoformulario = false;
      this.spinner.hide();
      this.alertaServicio.algoHaIdoMal();
    }); 
     
  }
  
  private async enviarCuota(){  
    
     if(this.cuotas_pago_selected.length >= 0){
      for(let i=0;i < this.cuotas_pago_selected.length; i++){
        // console.log(this.cuotas_pago_selected[i].cuota_id)
        // console.log(this.cuotas_pago_selected[i]['cuota_id'])
        let fecha = this.datePipe.transform(this.f.fecha.value,'yyyy-MM-dd');
        let bancoid = Number(this.f.bancoID.value);
        let numOperacion = this.f.operacion.value;
        let txtmonto = Number(this.f.monto.value);
        let cuotaid= this.cuotas_pago_selected[i].cuota_id;
        let asesorid = this.f.codigo_empleado.value;

        let salida = {
          url: this.ruta_multimedia, 
          bancoID: bancoid,
          txtNumOperacion : numOperacion,
          txtMonto : txtmonto,
          txtFechaAbono: fecha,
          cuotaID: cuotaid,    
          vendedorID: Number(asesorid)
        } 
        let body = JSON.stringify(salida);
        console.log(salida);
        this.utilitarios.guardarCuota(salida).subscribe((resp)=>{
          console.log(resp);
          if(resp['status']='ok'){
            this.alertaServicio.pagoCuotaCorrecta();
            localStorage.setItem('documento',this.f.documento.value);
            // this.buscarUsuario();
            // this.reloadPage();
            //console.log(resp);
          }else{
            console.log('ocurrio un error');
          } 
        });
      }
      
      //window.location.href='https://www.infoxeduca.com/';
    }else{
     this.alertaServicio.noSeleccionoCuotas();
    }
    
  }
  reloadPage() { // click handler or similar
    this.zone.runOutsideAngular(() => {
        location.reload();
    });
}
  public reintentar(){
    this.obtenerAsesor();
  }
}
