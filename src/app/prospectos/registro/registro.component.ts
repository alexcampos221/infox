import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtlitariosService } from 'src/app/shared/services/general/utlitarios.service';
import { DateAdapter } from '@angular/material/core';
import { DatePipe } from '@angular/common';
import { UniversidadService } from 'src/app/shared/services/backend/universidad.service';
import { ProgramaService } from 'src/app/shared/services/backend/programa.service';
import { PromocionService } from 'src/app/shared/services/backend/promocion.service';
import { BancoService } from 'src/app/shared/services/backend/banco.service';
import { MatriculaService } from 'src/app/shared/services/registros/matricula.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.sass']
})
export class RegistroComponent implements OnInit {

  registroForm : FormGroup;
  programas_cursos_lista: any[] = [];
  programas_procesados: any[] = [];
  programas_filtrados : any[] = [];
  paises_lista : any[] = [];
  universidades_lista : any[] = [];
  promociones_lista : any[]= [];
  usuarios_contactos : any[]=[];
  semestres_lista : any[]=[];

  otra_universidad : boolean = false;

  constructor(private fb : FormBuilder,
              private dateAdapter: DateAdapter<Date>,
              private universidadServicio: UniversidadService,
              private programaServicio : ProgramaService,
              private promocionServicio : PromocionService,
              private bancoServicio : BancoService,
              private utilitarios : UtlitariosService,
              private matriculaServicio: MatriculaService,
              ) {
      this.dateAdapter.setLocale('es-PE');
  }

  ngOnInit(): void {
    this.crearFormulario();
    this.traerPaises();
    this.listaProgramaCursos();
    this.traerSemestres();
  }

  private crearFormulario(){
    this.registroForm = this.fb.group({
      cliente : ['',[Validators.required]],
      dni : ['',[Validators.required]],
      nombre : ['',[Validators.required]],
      correo : ['',[Validators.required]],
      universidad : ['',[Validators.required]],//
      programa : ['',[Validators.required]], //
      promocion : ['',[Validators.required]],
      user_contact : ['',[Validators.required]],

      organizacion : ['',[Validators.required]], //
      activo : [false,[Validators.required]],
      telefono : ['',[Validators.required]], //
      otros : ['',[Validators.required]], //
      fecha : ['',[Validators.required]], //
      ifx_pais : ['',[Validators.required]], //
      semestre : ['',[Validators.required]]
    });
  }

  private traerPaises(){
    this.utilitarios.listarPaises().subscribe((resp: any) => {
      console.log(resp);
      this.paises_lista = resp.filter((p)=>p.pai_activo=='Y');
    });
  }

  private listaProgramaCursos() {

    this.matriculaServicio.listaProgramaCursos().subscribe((resp: any) => {
      console.log(resp);
      this.programas_cursos_lista = resp['result'];
      this.procesarProgramas();
    });
  }

  private procesarProgramas() {
    let arreglo_salida: any[] = [];
    //console.log(this.programas_cursos_lista); 
    this.programas_cursos_lista.forEach(elemento => {
      let tipo = elemento.pro_tipo;
      let arreglo:any[] = [];
      switch (tipo) {
        case 'PR':
          if(elemento.pro_cursos.length>0){
            let arreglo_nuevo:any[] =[];
            let curso_completo = null;
            elemento.pro_cursos.forEach(curso => {
              if(curso.cur_nombre=="PROGRAMA COMPLETO" || curso.cur_nombre=="PAQUETE COMPLETO"){
                curso_completo = curso;
              }
              else{
                let objeto = {
                  nombre : '-' +' '+curso.cur_nombre,
                  programa_id : elemento.pro_id,
                  curso_id : curso.cur_id,
                  tipo : 'CU',
                  bloqueado : false,
                  tipo_origen : elemento.pro_tipo,
                };
                arreglo_nuevo.push(objeto);
              }
            });
    
            if(curso_completo!=null){
              let objeto_completo = {
                nombre : `${elemento.pro_nombre.toUpperCase()} (Completo)`,
                programa_id : elemento.pro_id,
                curso_id : curso_completo.cur_id,
                tipo: 'PR',
                bloqueado : false,
                tipo_origen : elemento.pro_tipo,
              }
              arreglo_nuevo.unshift(objeto_completo);
            }
            else{
              let objeto_completo = {
                nombre : `${elemento.pro_nombre.toUpperCase()}`,
                programa_id : elemento.pro_id,
                curso_id : '000',
                tipo: 'PR',
                bloqueado : true,
                tipo_origen : elemento.pro_tipo,
              }
              arreglo_nuevo.unshift(objeto_completo);
            }
    
            arreglo = arreglo_nuevo;
          }
          else{
            let objeto_programa = {
              nombre : `${elemento.pro_nombre.toUpperCase()} (Completo)`,
              programa_id : elemento.pro_id,
              curso_id : '000',
              tipo: 'PR',
              bloqueado : false,
              tipo_origen : elemento.pro_tipo,
            };
            arreglo.push(objeto_programa);
          }
          break;
      
        case 'CO':
          let objeto_combo = {
            nombre : `${elemento.pro_nombre.toUpperCase()}`,
            programa_id : elemento.pro_id,
            curso_id : '000',
            tipo: 'CO',
            bloqueado : false,
            tipo_origen : elemento.pro_tipo,
            cantidad : Number(elemento.pro_qty),
            cursos : elemento.pro_cursos||[],
          }
          arreglo.push(objeto_combo);
          break;

        default:
          break;
      }

      arreglo.forEach(elemento=>{
        arreglo_salida.push(elemento);
      });

      /*
      let objeto_programa = {
        pro_id: elemento.pro_id,
        nombre: elemento.pro_nombre,
        cur_id: '',
        tipo: 'P'
      } 

      arreglo_salida.push(objeto_programa);

      elemento.pro_cursos.forEach(curso => {
        let objeto = {
          pro_id: elemento.pro_id,
          cur_id: curso.cur_id,
          nombre: curso.cur_nombre,
          tipo: 'C'
        }
        arreglo_salida.push(objeto);
      });
      */
    });


    this.programas_procesados = arreglo_salida;
    this.programas_filtrados = this.programas_procesados;
    //console.log(this.programas_procesados);
  }

  private traerSemestres(){
    this.matriculaServicio.listarSemestres().subscribe((resp: any) => {
      console.log(resp);
      this.semestres_lista = resp.filter((el) =>
      el.sem_activo =='Y');
    },(error)=>{
      console.log(error);
      
    });
  }

  private get f(){
    return this.registroForm.controls;
  }

  public selecPais(evento) {
    let pais = evento;
    /*
    this.f.nombres.setValue('');
    this.f.documento.setValue('');
    this.f.universidad.setValue('');
    this.f.cod_pais.setValue(evento.pai_codigo||'');
    */
    this.otra_universidad = false;

    //this.nombre_documento = evento.pais_nombre_documento;

    this.universidadesByPais(evento);

    /*
    if (pais.pai_id == "1000000") {
      this.todos_doc[0].nombre = 'Otros';
      this.prepararTiposDoc(this.todos_doc);
      this.f.tipo_doc.enable();
    }
    else {
      this.todos_doc[0].nombre = evento.pais_nombre_documento||'Documento';
      this.prepararTiposDoc([this.todos_doc[0]]);
      this.f.tipo_doc.setValue(this.todos_doc[0]);
      this.f.tipo_doc.disable();
    }
    */
  }

  private universidadesByPais(pais){
    let body = {
      paisID : pais.pai_id
    };
    console.log(body);
    this.universidades_lista = [];
    this.matriculaServicio.universidadListarByPais(body).subscribe((resp:any)=>{
      console.log(resp);
      this.universidades_lista = resp;
      let otros=null;
      resp.forEach(element => {
        if(element.uni_nombre=="OTROS"){
          otros = element;
        }
      });

      if(otros==null){
        this.universidades_lista.push(
          {
            uni_nombre : 'OTROS',
            uni_id : "1000005"
          }
        );
      }      
    },(error)=>{
      console.log(error);
      this.universidades_lista.push(
        {
          uni_nombre : 'OTROS',
          uni_id : "1000005"
        }
      );
    });
  }

  selectSemestre(evento){
    console.log(evento);
  }

  enviarFormulario(){
    console.log(this.registroForm.value);
  }

  volver(){

  }
}