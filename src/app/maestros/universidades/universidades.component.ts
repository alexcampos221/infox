import { Component, OnInit, ElementRef,AfterViewInit, ViewChild, OnDestroy} from '@angular/core';
import { Router, RouterLinkWithHref } from '@angular/router';
import * as moment from 'moment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormControl, FormControlName, FormGroup, Validators } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { DatePipe } from '@angular/common';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { Utils } from 'src/app/shared/utils';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatriculaService } from 'src/app/shared/services/registros/matricula.service';
import { CrearUniversidadComponent } from '../dialogs/crear-universidad/crear-universidad.component';
import { UniversidadService } from 'src/app/shared/services/backend/universidad.service';
import { Subscription } from 'rxjs';
import { AlertaService } from 'src/app/shared/services/general/alerta.service';
import { Console } from 'console';
import { NgxSpinnerService } from 'ngx-spinner';
import { LoadingService } from 'src/app/shared/services/loading.service';

@Component({
  selector: 'app-universidades',
  templateUrl: './universidades.component.html',
  styleUrls: ['./universidades.component.sass'],
  providers: [DatePipe]
})
export class UniversidadesComponent implements OnInit ,OnDestroy{

  universidades_totales : any[] = [];
  universidades_filtradas : any[] = [];
  arreglo;
  dataSource_universidades_totales : MatTableDataSource<any>;

  //atributos para la tabla material
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;

  displayedColumns = [
    'codigo',
    'universidad',
    'descripcion',
    'pais',
    'acciones',
  ];

  filtro_control : FormControl;
  filtro$ : Subscription;

  constructor(
            public dialog: MatDialog,
            private ruteador: Router,
            private universidadServicio : UniversidadService,
            private alertaServicio : AlertaService,
            private spinner : NgxSpinnerService,
            private loading:LoadingService
  ) { }
  ngOnDestroy(): void {
    if(this.filtro$){
      this.filtro$.unsubscribe();
    }
  }

  ngOnInit(): void {
    this.filtro_control = new FormControl(false);
    this.dataSource_universidades_totales = new MatTableDataSource();
    this.traerUniversidades();
    this.pendienteFiltro();
  }

  private pendienteFiltro(){
    this.filtro$ = this.filtro_control.valueChanges.subscribe(resp=>{
      console.log(resp);
      if(resp){
        this.quitarFiltro();
      } 
      else{
        this.filtrarInformacion();
      }
    });
  }

  private quitarFiltro(){
    this.universidades_filtradas = this.universidades_totales;
    this.dataSource_universidades_totales.data = this.universidades_filtradas;
  }

  private filtrarInformacion(){
    this.universidades_filtradas = [];
    this.universidades_totales.forEach(elemento=>{
      if(elemento.uni_activo == 'Y'){
        this.universidades_filtradas.push(elemento)
      }
    });
    this.dataSource_universidades_totales.data = this.universidades_filtradas;
  }

  private traerUniversidades(){
    this.spinner.show();
    this.loading.loading$.next({opacity:0.5})
    this.universidadServicio.listaUniversidades().subscribe((resp:any[]) =>{

      this.universidades_totales = resp; 
      //this.dataSource_universidades_totales.data = this.universidades_totales; 
      console.log("universidades ",resp);
      this.filtrarInformacion();    
      this.spinner.hide();
    },(error)=>{
      //this.eventosServicio.hideLoading();
      console.info("error universidades: ",error);
      this.spinner.hide();
    });
  }

  ngAfterViewInit(): void {
    this.dataSource_universidades_totales.paginator = this.paginator;
    this.dataSource_universidades_totales.sort = this.sort;
  }

  applyFilter(filterValue:string){
    this.dataSource_universidades_totales.filter = filterValue.trim().toLowerCase();
  }

  nuevaUniversidad(){
    const dialogRef = this.dialog.open(CrearUniversidadComponent, {
      data: {
        nombre: "crear",
        universidad : null,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
        if( result==undefined){
          console.log("No se recibe nada");
        }
        else if(result.estado=='EXITO'){
          this.alertaServicio.creacionCorrecta();
          this.traerUniversidades();
          console.log("Se ha creado la universidad");
        }
        else{
          console.log("ha ocurrido un error");
        }
    });
  }

  mostarModalUniversidad(row){
    if(row.uni_id != ''){
      this.abrirUniversidad(row);
    }
  }

  private abrirUniversidad(uni){
    const dialogRef = this.dialog.open(CrearUniversidadComponent, {
      data: {
        nombre: "editar",
        universidad : uni,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
        if( result==undefined){
          console.log("No se recibe nada");
        }
        else if(result.estado=='EXITO'){
          this.alertaServicio.actualizacionCorrecta();
          this.traerUniversidades();
          console.log("Se ha actualizado la universidad");
        }
        else{
          console.log("ha ocurrido un error");
        }
    });
  }

  cambiarEstado(row){
    let nuevo_est = '';
    let codigo = row.uni_id;
    if(row.uni_activo == 'N'){
      nuevo_est = 'Y';
    }
    else{
      nuevo_est = 'N';
    }
    let body = {
      estadoNuevo : nuevo_est,
      universidadID: codigo,
    }
    
    console.log(body);
    this.universidadServicio.cambiarEstado(body).subscribe(resp=>{
      console.log(resp);
      if(resp['status']=='OK'){
        this.filtro_control.setValue(false);
        this.traerUniversidades();
      }
      else{
        //error
      }
    },(error)=>{
      //error de servidor
      console.log(error);
    });

  }
  sortData(sort:Sort){
    const data = this.universidades_filtradas.slice();
    let arreglo_temporal :any []= [];
    if (!sort.active || sort.direction === '') {
      arreglo_temporal = data;
      this.dataSource_universidades_totales.data = arreglo_temporal;
      return;
    }

    arreglo_temporal = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'codigo': return this.compare(a.pro_id||'', b.pro_id||'', isAsc);
        case 'universidad': return this.compare(a.pro_nombre||'', b.pro_nombre||'', isAsc);
        default: return 0;
      }
    });
    this.dataSource_universidades_totales.data = arreglo_temporal;
  }
  private  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
  actualizarLista(){  
    this.traerUniversidades();
  }
}
