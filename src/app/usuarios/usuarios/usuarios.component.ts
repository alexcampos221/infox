import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import * as XLSX from 'xlsx';
import * as fs from 'file-saver';
import { Workbook } from 'exceljs';
import { NgxSpinnerService } from 'ngx-spinner';
import { EditUsuarioComponent } from 'src/app/maestros/dialogs/edit-usuario/edit-usuario.component';
import { ProgramaService } from 'src/app/shared/services/backend/programa.service';
import { UsuarioService } from 'src/app/shared/services/backend/usuario.service';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { AlmacenamientoLocalService } from 'src/app/shared/services/general/almacenamiento-local.service';
import { FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.sass']
})
export class UsuariosComponent implements OnInit {

  txtNuevo:any;

  dataSource_locales_totales : MatTableDataSource<any>;
  locales_filtrados : any [] = [];
  usuarios_filtrados : any[] = [];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;

  displayedColumns = [
    'expandir',
    'perfil',
    // 'nombre',
    'codigo',
    'nombre_completo',
    'usuario',
    'telefono',
    'email',
    'tipo_usu',
    'activo',    
    'acciones',
  ];


  usuario_actual : any;
  filtro_control : FormControl;
  filtro$ : Subscription;

  constructor(private usuarioService:UsuarioService,
              private router : Router,
              private loadingService: LoadingService,
              private spinner: NgxSpinnerService,
              private programaService:ProgramaService,
              private  dialog: MatDialog,
              private localServicio : AlmacenamientoLocalService,
              ) { }

  ngOnInit(): void {
    this.filtro_control = new FormControl(false);
    this.usuario_actual = this.localServicio.getItem('usu_actual');
    this.dataSource_locales_totales = new MatTableDataSource();
    this.traerInfoUsuarios();
    this.pendienteCambios();
  }

  ngAfterViewInit(): void {
    this.dataSource_locales_totales.paginator = this.paginator;
    this.dataSource_locales_totales.paginator._intl.itemsPerPageLabel = "Registros por Página";
    this.dataSource_locales_totales.sort = this.sort;
  }

  traerInfoUsuarios(){
    let salida={
      tipo: '0'
    }
    this.loadingService.loading$.next({opacity:0.5});
    this.spinner.show();
    this.usuarioService.listar(JSON.stringify(salida)).subscribe((resp : any)=>{
      console.log(resp);
      this.locales_filtrados = resp || [];
      this.filtrarInformacion();
      this.spinner.hide();
    },(error) => {
      console.log(error);
      this.spinner.hide();
    })
  }
  private pendienteCambios(){
    this.filtro$ = this.filtro_control.valueChanges.subscribe(resp=>{
      if(resp){
        this.quitarFiltro();
      }
      else{
        this.filtrarInformacion();
      }
    });
  }
  quitarFiltro(){
    this.usuarios_filtrados = this.locales_filtrados;
    this.dataSource_locales_totales.data = this.usuarios_filtrados;
  }
  filtrarInformacion(){
    this.usuarios_filtrados = [];
    this.locales_filtrados.forEach(elemento=>{
      if(elemento.usu_activo == 'Y'){
        this.usuarios_filtrados.push(elemento)
      }
    });
    this.dataSource_locales_totales.data = this.usuarios_filtrados;    
  }

  detalleUsuario(row,index){
    let arreglo = [];
    let salida={
      usuID:row.usu_id
    }
    this.dataSource_locales_totales.data.forEach(elemento=>{
      arreglo.push(elemento.usu_id);
    });
    
    localStorage.setItem('indice',index);  
    localStorage.setItem('info_usu',JSON.stringify(arreglo));
    localStorage.setItem('info_usu_id',row.usu_id);  

    this.router.navigate(['usuarios/detalle'] ,{ state : {data: salida } })
  }

  applyFilter(filterValue:string){
    this.dataSource_locales_totales.filter = filterValue.trim().toLowerCase();
  }

  editUser(row){
    const dialogRef = this.dialog.open(EditUsuarioComponent,{
      
      data:{
        nombre: "Editar",
        usuario: row
      }
    });
    dialogRef.afterClosed().subscribe((result) => {
      if( result == undefined){
        console.log("No se recibe nada");
      }
      else if(result.estado=='EXITO'){
      this.traerInfoUsuarios()
      }
      else{
        console.log("ha ocurrido un error");
      }
    });   
  }

  addUser(){
    const dialogRef = this.dialog.open(EditUsuarioComponent,{
      
      data:{
        nombre: "Crear",
      }
    });
    dialogRef.afterClosed().subscribe((result) => {
      if( result == undefined){
        console.log("No se recibe nada");
      }
      else if(result.estado=='EXITO'){
      this.traerInfoUsuarios()
      }
      else{
        console.log("ha ocurrido un error");
      }
    });  
  }
  chageStateValue(data){
    if(data==='N') return 'Y';
    else if(data==='Y') return 'N';
    else throw 'error';  
  }

  ocultarRow(row){
    console.log(row);
    var body = {
      nuevoEstado : this.chageStateValue(row.usu_activo),
      usuarioID : Number(row.usu_id),
    }
    console.log(body)
    this.spinner.show();
    this.loadingService.loading$.next({opacity:0.5})
    this.usuarioService.status(body).subscribe((resp:any)=>{
      console.log(resp);
      this.spinner.hide();
      // //this.datos = resp||[];
      // this.dataSource_locales_totales.data = resp||[];
      // this.locales_filtrados = resp||[];
      // this.cargando = true;
      this.traerInfoUsuarios();
    },(error)=>{
      console.log(error);
      this.spinner.hide();
    });
  }
  sortData(sort: Sort){
    const data = this.usuarios_filtrados.slice();
    let arreglo_temporal :any []= [];
    if (!sort.active || sort.direction === '') {
      arreglo_temporal = data;
      this.dataSource_locales_totales.data = arreglo_temporal;
      return;
    }

    arreglo_temporal = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        // case 'perfil': return this.compare(a.rol_nom||'', b.rol_nom||'', isAsc);
        // case 'nombre': return this.compare(a.usu_nombre||'', b.usu_nombre||'', isAsc);
        case 'codigo': return this.compare(a.usu_codigo||'', b.usu_codigo||'', isAsc);
        case 'nombre_completo': return this.compare(a.usu_nombre_compleo||'', b.usu_nombre_compleo||'', isAsc);
        case 'usuario': return this.compare(a.usu_usuario||'', b.usu_usuario||'', isAsc);
        case 'telefono': return this.compare(a.usu_telefono||'', b.usu_telefono||'', isAsc);
        case 'email': return this.compare(a.usu_email||'', b.usu_email||'', isAsc);
        case 'tipo_usu': return this.compare(a.usu_tipo||'', b.usu_tipo||'', isAsc);
        case 'activo': return this.compare(a.usu_activo||'', b.usu_activo||'', isAsc);
        default: return 0;
      }
    });
    this.dataSource_locales_totales.data = arreglo_temporal;
  }
  private  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
  actualizarLista(){
    this.traerInfoUsuarios()
  }

  // dataExcel : any [] = [];

  // exportData : any [] = [];
  // cantidad:any;

  // onFileChange(event: any){
  //   const target: DataTransfer = <DataTransfer>(event.target);
  //   if (target.files.length !== 1) {
  //     throw new Error('Cannot use multiple files');
  //   }
  //   const reader: FileReader = new FileReader();
  //   reader.readAsBinaryString(target.files[0]);
  //   reader.onload = (e: any) => {
  //     const binarystr: string = e.target.result;
  //     const wb: XLSX.WorkBook = XLSX.read(binarystr, { type: 'binary' });

  //     const wsname: string = wb.SheetNames[0];
  //     const ws: XLSX.WorkSheet = wb.Sheets[wsname];

  //     const data = XLSX.utils.sheet_to_json(ws);
  //     this.dataExcel = data;
  //     console.log(this.dataExcel)      
  //     for (let index = 0; index < this.dataExcel.length; index++) {
  //       this.consultarNumero(this.dataExcel[index].CARGADOS,index); 
  //     }
  //   };
  // }
  // data_f : any [] = [];
  // consultarNumero(numero,i){
  //   let body ={
  //     destinatario: numero
  //   }    
  //   this.programaService.consultarNumero(body).subscribe((resp:any)=>{
  //       this.exportData.push(resp);        
  //     if(this.exportData.length >= this.dataExcel.length){
  //       this.downloadFileExcel(this.exportData)
  //     }
       
  //   },(error)=>{
  //     console.log(error)
  //   }); 
  // }
  // downloadFileExcel(filaExcel : any){  
  //   // debugger;
  //   let data = this.exportData
 
  //   let mapHeaders=new Map([      
  //     ["Telefono","cargado"],
  //     ["Programado","programado"],
  //     ["SinWhatApp","sin_whatsaapp"],
  //     ["Enviado","enviado"],
  //   ]);

  //   let mapHeadersShow=[
  //     'Telefono',
  //     'Programado',
  //     'SinWhatApp',
  //     'Enviado',     
  //   ];

  //   let fecha = "Usuario";

  //   let dataExcel ={
  //     title: fecha,
  //     data: data,
  //     mapHeaders:mapHeaders,
  //     // data_f: data_rep,
  //     mapHeadersShow:mapHeadersShow,
  //   }

  //   console.log(dataExcel);
  //   this.crearReporteGeneral(dataExcel)
  // }

  // crearReporteGeneral(excelData) {
  //   // debugger;
  //   const title = excelData.title;

  //   let workbook = new Workbook();
  //   let worksheet = workbook.addWorksheet("Usuario");

  //   let titulo = [];    
  //   titulo[1] = title;

  //   let titulo_general = worksheet.addRow(titulo);
  //   titulo_general.alignment = { vertical: 'middle', horizontal: 'center' };
  //   titulo_general.font = {bold:true}
  //   worksheet.addRow([]);
    
  //   let titulos = excelData.mapHeadersShow;
  //   let headerRow = worksheet.addRow(titulos);    

  //   headerRow.eachCell((cell, number) => {
  //     cell.fill = {
  //       type: 'pattern',
  //       pattern: 'solid',
  //       fgColor: { argb: '4167B8' },
  //       bgColor: { argb: '' }
  //     }
  //     cell.font = {
  //       bold: true,
  //       color: { argb: 'FFFFFF' },
  //       size: 12
  //     }
  //   });

  //   excelData.data.forEach(element => { 
  //     let fila = [];
  //     for (let index = 0; index < excelData.mapHeadersShow.length; index++) {
  //       let clave = excelData.mapHeaders.get(excelData.mapHeadersShow[index]);
  //       switch (clave) {
  //         // case 400:
  //         //   fila[index+1] = '000000' ;
  //         //   break;
  //         default:
  //           fila[index+1] = element[clave];
  //           break;
  //       }
  //     }
  //     worksheet.addRow(fila);
  //   });
    
  //   worksheet.getColumn(1).width = 20;
  //   worksheet.getColumn(2).width = 20;
  //   worksheet.getColumn(3).width = 20;
  //   worksheet.getColumn(4).width = 20;
  //   worksheet.addRow([]);    
  //   workbook.xlsx.writeBuffer().then((data) => {
  //     let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
  //     fs.saveAs(blob, title + '.xlsx');
  //   })
  //   // console.log(excelData);
  // }
}
