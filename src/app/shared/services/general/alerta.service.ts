import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import Swal from 'sweetalert2';
import { resolve } from 'url';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable({
  providedIn: 'root'
})
export class AlertaService {

  constructor(
            private loadingService: LoadingService,
            private spinner: NgxSpinnerService,) { }
  errorCantidadDecimales(){
    
    Swal.fire({
      title: 'error',
      text: 'Error en la cantidad de decimales maximo 2',
      icon: 'error',
      timer: 1500
    })
  }
  creacionCorrecta(){
    Swal.fire({
      title: 'Éxito',
      text: 'Se ha creado con exito',
      icon: 'success',
      timer: 1500
    })
  }

  algoHaIdoMal() {
    Swal.fire({
      icon: 'error',
      title: 'Ago ha ido mal',
      text: 'Intentelo mas tarde',
    });
  }

  errorEnviarEmail() {
    Swal.fire({
      icon: 'error',
      title: 'No puede enviar al mismo correo varias veces',
      text: 'Intentelo de 5 minutos',
    });
  }

  sinResultados(estado) {
    Swal.fire({
      icon: 'error',
      title: `${estado} sin respuesta`,
      text: 'Por favor ingrese un nombre manualmente',
    });
  }

  eliminacionCorrecta(){
    Swal.fire({
      title: 'Éxito',
      text: 'Se ha eliminado con exito',
      icon: 'success',
      timer: 1500
    })
  }
  eliminarCuotaCorrectamente(value) {
    Swal.fire({
      title: 'Estas seguro de eliminar la cuota?',
      text: "¡No podrás revertir esto!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: '¡Sí, bórralo!'
    }).then(result => {
      if (result.value) {
          //console.log(result.value);
        Swal.fire('¡Eliminado! ', 'La cuota ha sido eliminada.', 'success');
        return value;
      }
    });
  }
  eliminarPendiente() {
    Swal.fire({
      title: 'Estas seguro de eliminar pendiente pago?',
      text: "¡No podrás revertir esto!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: '¡Sí, bórralo!'
    })
  }
  actualizacionCorrecta(){
    Swal.fire({
      title: 'Éxito',
      text: 'Actualizado con exito',
      icon: 'success',
      timer: 1500
    })
  }
  generarCuotaCorrecta(){
    Swal.fire({
      title: 'Éxito',
      text: 'Cuota generada con exito',
      icon: 'success',
      timer: 1500
    })
  }
  reenvioEmail(){
    Swal.fire({
      title: 'Éxito',
      text: 'Correo reenviado',
      icon: 'success',
      timer: 1500
    })
  }
  pagoCuotaCorrecta(){
    Swal.fire({
      title: 'Éxito',
      text: 'Pago generada con exito',
      icon: 'success',
      timer: 1500
    })
  }

  matriculaCorrecta(){
    Swal.fire({
      title: 'Éxito',
      text: 'Se ha matriculado con Éxito',
      icon: 'success',
      timer: 1500
    })
  }

  registroCorrecto(){
    Swal.fire({
      title: 'Éxito',
      text: 'Se ha registrado con Éxito',
      icon: 'success',
      timer: 1500
    })
  }

  errorInterno() {
    Swal.fire({
      icon: 'error',
      title: 'Tenemos Problemas internos',
      text: 'Intento más tarde',
    });
  }
  usuarioProgramaNoEncontrado(){
    Swal.fire({
      icon:'error',
      title:'usuario no encontrado',
    })
  }
  usuarioProgramaSinDeuda(nombre){
    Swal.fire({
      icon:'success',
      title:`Estimado(a) ${nombre}, no tienes deudas pendientes`,
    })
  }
  guardadoCorrectamente(){
    Swal.fire({title: 'Éxito',
      text: 'Se ha pagado con Éxito',
      icon: 'success',
      timer: 1500
    })
  }

  envioPruebaExistoso(){
    Swal.fire({title: 'Éxito',
      text: 'Se enviado el mensaje con Éxito',
      icon: 'success',
      timer: 1500
    })
  }

  errorMensajePrueba(){
    Swal.fire({
      title: 'Error',
      text: 'Error al envia el mensaje',
      icon: 'error',
      timer: 2000
    })
  }

  camposObligatorios(){
    Swal.fire({
      title: 'Error',
      text: 'Algunos campos estan vacios',
      icon: 'error',
      timer: 2000
    })
  }

  noSeleccionoCuotas(){
    Swal.fire({title: 'Éxito',
      text: 'no ha seleccionado ninguna cuota',
      icon: 'error',
      timer: 1500
    })
  }
  MostrarImagenVoucher(url) {
    Swal.fire({
      imageUrl: url,
      imageHeight: 'auto',
      imageWidth: 'auto',
      imageAlt: 'voucher',
      
    });
  }
  FechaEsMayor() {
    Swal.fire({
      text: 'La fecha inicial no puede ser mayor a la fecha final',
      icon: 'warning',
      timer: 1500,
      showConfirmButton:false
    });
  }
}
