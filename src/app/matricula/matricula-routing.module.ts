import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistrarMatriculaComponent } from './registrar-matricula/registrar-matricula.component';
import { InformesComponent } from './informes/informes.component';
import { PromoComponent } from './promo/promo.component';
import { ValidarComponent } from './validar/validar.component';
import { PromocionComponent } from './promocion/promocion.component';
import { Page404Component } from '../authentication/page404/page404.component';

const routes: Routes = [
  {
    path : 'pago/:codigo',
    component : RegistrarMatriculaComponent,
  },
  {
    path : 'informes',
    component : InformesComponent,
  },
  {
    path : 'promo/:codigo',
    component : PromocionComponent,
  },
  {
    path : 'validar',
    component : ValidarComponent,
  },
  {
    path: '**', 
    component: Page404Component,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MatriculaRoutingModule { }
