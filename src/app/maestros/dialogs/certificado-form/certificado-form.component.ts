import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Console } from 'console';
import { format } from 'path';
import { CertificadoService } from 'src/app/shared/services/backend/certificado.service';
import { MatriculaService } from 'src/app/shared/services/registros/matricula.service';

@Component({
  selector: 'app-certificado-form',
  templateUrl: './certificado-form.component.html',
  styleUrls: ['./certificado-form.component.sass'],
  providers:[DatePipe]
})
export class CertificadoFormComponent implements OnInit {

  nombre:any;

  editar : boolean = false;

  certificadoForm : FormGroup;

  programa:any;

  fecha_maxima:Date=new Date();
  programas : any[] = [];
  cursos : any[] = [];
  isSelect :boolean = false
  curso:any;

  constructor(private fb: FormBuilder,               
    public dialogRef: MatDialogRef<CertificadoFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private matriculaServicio: MatriculaService, 
    private datePipe:DatePipe,         
    private certificadoService: CertificadoService,
    private dateAdapter: DateAdapter<Date>,
    ) {
      console.log(data)
      this.dateAdapter.setLocale('es-PE');
     }

  ngOnInit(): void {
    this.programa =this.data.certificado
     if(this.data.nombre == 'editar'){
      this.editar = true;
      this.crearFormularioEditar();
      this.prepararRegistro();
      this.nombre = 'Editar Certificado';
      this.f.codigo.setValue(this.data.certificado.cer_codigo);   
      this.f.nombre.setValue(this.data.certificado.cer_nombre); 
      this.f.dni.setValue(this.data.certificado.cer_dni); 
      this.f.fecha.setValue(new Date(this.data.certificado.fecha_certificado_l)); 
      this.f.archivo.setValue(this.data.certificado.cer_descarga); 
    }
    else{
      this.nombre = "Crear Certificado";
      this.crearFormulario();
      this.prepararRegistro();
    }
  }
  
  private crearFormulario(){
    this.certificadoForm = this.fb.group({
      codigo: ['',[Validators.required]],
      nombre: ['',[Validators.required]],
      dni: ['',[Validators.required]],
      programa: ['',[Validators.required]],
      cursos: ['',[Validators.required]],
      fecha: ['',[Validators.required]],
      archivo: ['',[Validators.required]],
      meses_programa: ['',[Validators.required]],
    });
  }

  private crearFormularioEditar(){
    this.certificadoForm = this.fb.group({
      codigo: ['',[Validators.required]],
      nombre: ['',[Validators.required]],
      dni: ['',[Validators.required]],
      programa: ['',[Validators.required]],
      cursos: ['',[Validators.required]],
      fecha: ['',[Validators.required]],
      archivo: ['',[Validators.required]],
      meses_programa: ['',[Validators.required]],
    });
  }
  private get f(){
    return this.certificadoForm.controls;
  }

  enviarFormulario(){
    if(this.editar == true){
      this.editarCertificado();
    }else{
      this.crearCertificado();
    }
  }
  closeDialog(): void {
    this.dialogRef.close();
  }
  

  private prepararRegistro(){ 
    this.listarProgramas();
  }
  /* combo programas y cursos */
  private listarProgramas(){
    this.certificadoService.listarProgramasAll().subscribe((resp:any)=>{
      console.log(resp);
      this.programas = resp || [];
      if(this.data.nombre == 'editar'){
        this.listarCursos();
        this.programas.forEach(elemento=>{
          if(elemento.pro_id == this.data.certificado.pro_id){
            this.f.programa.setValue(elemento);
          }
        });
      }
    });
  }
  
  private listarCursos(){
    let programaID;
    if(this.curso != null || this.curso != undefined){
      programaID= this.curso.pro_id
    }else{
      programaID= this.programa.pro_id;
    }
    console.log(programaID)
    let salida ={
      programaID: programaID
    }
    this.certificadoService.listarCursos(salida).subscribe(resp=>{
      console.log(resp)
      this.cursos = resp['cursos'] || [];
      if(this.data.nombre == 'editar'){
        this.cursos.forEach(elemento=>{
          if(elemento.cur_id == this.data.certificado.cur_id){
            this.f.cursos.setValue(elemento)
            console.log(this.f.cursos.value)
          }
        })
      }
    })
  }

  selectPrograma(event){
    this.curso = event;
    console.log(this.curso.pro_id)
    this.isSelect = true
    this.listarCursos()
  }
  selectCurso(event){
    console.log(event)
    this.f.cursos.setValue(event);
  }

  crearCertificado(){
    let programa = this.f.programa.value.pro_id;
    let fecha =  this.datePipe.transform(this.f.fecha.value,'yyyy-MM-dd');
    let curso=this.f.cursos.value.cur_id;

    let salida = {
      nombreAlumno: this.f.nombre.value,
      codigoCertificado: this.f.codigo.value,
      dniAlumno:this.f.dni.value,
      programaID: programa,
      cursoID: curso,
      rutaDescarga: this.f.archivo.value,
      fechaCertificado: fecha,
    }
    console.log(salida);
    // let body = JSON.stringify(salida);
    this.certificadoService.guardarCertificados(salida).subscribe(resp=>{
      console.log(resp);
      if(resp['status']=='OK'){
        this.dialogRef.close({estado : 'EXITO'})
      }
      else{
        this.dialogRef.close({estado : 'FALLO'});
      }
    },(error)=>{
      console.log(error);
      this.dialogRef.close({estado : 'FALLO'});
    });
  }
  editarCertificado(){
    let programa = this.f.programa.value.pro_id;
    let fecha =  this.datePipe.transform(this.f.fecha.value,'yyyy-MM-dd');
    let curso=this.f.cursos.value.cur_id;
    
    let salida = {
      nombreAlumno: this.f.nombre.value,
      codigoCertificado: this.f.codigo.value,
      dniAlumno:this.f.dni.value,
      programaID: programa,
      cursoID: curso || this.data.certificado.cur_id,
      rutaDescarga: this.f.archivo.value,
      fechaCertificado: fecha,
      certificadoID:this.data.certificado.cer_id,
    }
    console.log(salida);
    // let body = JSON.stringify(salida);
    this.certificadoService.editarCertificado(salida).subscribe(resp=>{
      console.log(resp);
      if(resp['status']=='OK'){
        this.dialogRef.close({estado : 'EXITO'})
      }
      else{
        this.dialogRef.close({estado : 'FALLO'});
      }
    },(error)=>{
      console.log(error);
      this.dialogRef.close({estado : 'FALLO'});
    });
  }
}
