
export class NuevaCuota {
    numCuota: number;
    fVencimiento: string;
    mCuota: string
    banco:string;
    mPagado:string;
    fPago: string;
    nOperacion:string;
    urlVoucher:string;
    constructor(cuota){
        this.numCuota = cuota.numCuota;
        this.fVencimiento = cuota.fVencimiento;
        this.mCuota = cuota.mCuota;
        this.banco = cuota.banco;
        this.mPagado = cuota.mPagado;
        this.fPago = cuota.fPago;
        this.nOperacion = cuota.nOperacion;
        this.urlVoucher = cuota.urlVoucher;
    }
}