import { Item } from './item';
// import { FlowerCenter } from './flower-center';

export class Objeto {
  constructor(
    // private readonly flowerCenter: FlowerCenter,
    private readonly numberOfPetals: number,
    private petal: Item
  ) {}

  draw(context: CanvasRenderingContext2D) {
    this.drawPetals(context);
    // this.flowerCenter.draw(context);
  }

  private drawPetals(context: CanvasRenderingContext2D) {
    context.save();
    const rotateAngle = (2 * Math.PI) / this.numberOfPetals;
    for (let i = 0; i < this.numberOfPetals; i++) {
      context.translate(this.petal.centerPoint.x, this.petal.centerPoint.y);
      context.rotate(rotateAngle);
      context.translate(-this.petal.centerPoint.x, -this.petal.centerPoint.y);
      this.petal.draw(context);
    }
    context.restore();
  }
}
