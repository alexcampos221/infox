import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, ElementRef, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Console } from 'console';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgxSpinner } from 'ngx-spinner/lib/ngx-spinner.enum';
import { LoadingService } from 'src/app/shared/services/loading.service';

@Component({
  selector: 'app-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.sass']
})
export class CanvasComponent implements OnInit {

  poligono: any [] = [
    {
      nombre:'canvas 1',
      color:'Y',
    },
    {
      nombre:'canvas 2',
      color:'N',
    },
    {
      nombre:'canvas3',
      color:'Y',
    },
    {
      nombre:'canvas4',
      color:'Y',
    },
    {
      nombre:'canvas5',
      color:'N',
    },
    {
      nombre:'canvas6',
      color:'N',
    },
  ]
  private ctx: CanvasRenderingContext2D;
  private ctx2: CanvasRenderingContext2D;
  private ctx3: CanvasRenderingContext2D;

  arry2:any[] = []
  arry3:any[] = []

  trackByFn(index:number){
    return (index);
  }

  color :any []= [
    "#A6ACAF",
    "#2CA1DB",
  ]
  
  ancho:any;
  ancho2:any=0;
  ancho3:any=0;
  isClickCanvas : boolean = false;
  isClickSubCanvas : boolean = false;
  constructor(private el:ElementRef,
      private router : Router,
      private loadingService: LoadingService,
      private spinner: NgxSpinnerService,) {}
  
  context: CanvasRenderingContext2D;
  coordinates:any[]=[
    {
      x:0,
      y:0,
    }
  ]
  // miny:any;
  // maxy:any;
  ngOnInit(): void {
    // this.context.moveTo(this.coordinates[0].x, this.coordinates[0].y);
    
    // let minx = this.coordinates[0].x, maxx = this.coordinates[0].x, miny == this.coordinates[0].y, maxy == this.coordinates[0].y;
    // for (let index = 1; index < this.coordinates.length; index++) {
    //   if (this.coordinates[index].x < minx) minx = this.coordinates[index.x]);
    //   if (this.coordinates[index].x > maxx) maxx = this.coordinates[index.x]);
    //   if (this.coordinates[index].y < miny) miny = this.coordinates[index.y]);
    //   if (this.coordinates[index].y > maxy) maxy = this.coordinates[index.y]);
    // }
  }

  ngAfterViewInit() : void {  
    this.loadingService.loading$.next({opacity:0.5})
    this.spinner.show();
    let x = 0;
    
    switch (this.poligono.length){
      case 1:{
        x = Number(x) + 250
        this.ancho = Math.round((100 / this.poligono.length)-66);
        break;
      }
      case 2:{
        x = Number(x) + 250
        this.ancho = Math.round((100 / this.poligono.length)-17);
        break;
      }
      case 3:{
        x = Number(x) + 250
        this.ancho = Math.round((100 / this.poligono.length));
        break;
      }
      case 4:{
        x = Number(x) + 250
        this.ancho = Math.round((100 / this.poligono.length)+8);
        break;
      }
      case 5:{
        x = Number(x) + 250
        this.ancho = Math.round((100 / this.poligono.length)+13);
        break;
      }
      case 6:{
        x = Number(x) + 250
        this.ancho = Math.round((100 / this.poligono.length)+16);
        break;
      }
      case 7:{
        x = Number(x) + 250
        this.ancho = Math.round((100 / this.poligono.length)+19);
        break;
      }
      default:{
        x = Number(x) + 250
        this.ancho = Math.round((100 / this.poligono.length));
        break;
      }
    }
    setTimeout(() => {
      for (let index = 0; index < this.poligono.length; index++) {
        // console.log(x)
        if(this.poligono[index].color == 'Y'){
          this.myDrawingFunction1(index,this.poligono[index].nombre,this.color[1],x);
        }else{
          this.myDrawingFunction1(index,this.poligono[index].nombre,this.color[0],x);
        }
        this.spinner.hide();
      }  
    }, 1000);
    
    
  }

  myDrawingFunction1(index:number,nombre,color,x:number){
    // console.log(x)
    let canvas = <HTMLCanvasElement>this.el.nativeElement.querySelector('#canvas-' + index)
    this.ctx = canvas.getContext('2d');
    this.ctx.beginPath();
    this.ctx.fillStyle = color 
    this.ctx.moveTo(0,0);
    this.ctx.lineTo(x,0);
    this.ctx.lineTo(x + 50,50);
    this.ctx.lineTo(x,100);
    this.ctx.lineTo(0,100);
    this.ctx.lineTo(50,50);
    this.ctx.closePath();
    this.ctx.fill();
    this.ctx.fillStyle = '#ffffff' 
    this.ctx.font = "20px Arial";
    this.ctx.fillText(nombre, 60, 55);
  }

  selectCanvas(i){
    this.isClickCanvas = true
    this.loadingService.loading$.next({opacity:0.5});
    this.spinner.show();
    this.arry3 = [];
    let x = 0;
    switch (i){
      case 0:{
        x = Number(x) + 250
        this.arry2 = [
          {
            nombre:'subCanvas 1',
            color:'N',
            link:'N',
          },
        ];
        this.ancho2 = Math.round((100 / this.arry2.length)-66);
        break;
      }
      case 1:{
        x = Number(x) + 250
        this.arry2 = [] 
        this.ancho2 = 100;
        break;
      }
      case 2:{
        x = Number(x) + 250
        this.arry2 = [
          {
            nombre:'xxxxxxxxxx 1',
            color:'N',
            link:'N',
          },
          {
            nombre:'321321321 2',
            color:'Y',
            link:'Y',
          },
          {
            nombre:'subCanvas 3',
            color:'Y',
            link:'N',
          },
        ]
        this.ancho2 = Math.round((100 / this.arry2.length));
        break;
      }
      case 3:{
        x = Number(x) + 250
        this.arry2 = [
          {
            nombre:'subCanvas 1',
            color:'N',
            link:'N',
          },
          {
            nombre:'subCanvas 2',
            color:'Y',
            link:'N',
          },
        ]
        this.ancho2 = Math.round((100 / this.arry2.length)-17);
        break;
      }
      case 4:{
        x = Number(x) + 250;
        this.ancho2 = 100//Math.round((100 / this.poligono.length));
        this.arry2 = [] ;
        break;
      }
      case 5:{
        x = Number(x) + 250;
        this.arry2 = [] ;
        this.ancho2 = 100//Math.round((100 / this.poligono.length));
        break;
      }
      default:{
        x = Number(x) + 250;
        this.arry2 = [] ;
        this.ancho2 = 100// Math.round((100 / this.poligono.length));
        break; 
      }
    }
    console.log(this.arry2)
    console.log(this.ancho2)
    setTimeout(() => {
      for (let index = 0; index < this.arry2.length; index++) {
        if(this.arry2[index].color == 'Y'){
          this.myDrawingFunction2(index,this.arry2[index].nombre,this.color[1],x)
        }else{
          this.myDrawingFunction2(index,this.arry2[index].nombre,this.color[0],x)
        }
      }
      this.spinner.hide();
    }, 1000);
    
  };

  selectSubCanvas(index){
    console.log(index)
    this.isClickSubCanvas = true
    let x = 250;
    this.loadingService.loading$.next({opacity:0.5});
    this.spinner.show();
    switch (index){
      case 0:{
        this.arry3 = [
          {
            nombre:'Canvas HIJO 1',
            color:'N',
            link:'N',
          },
        ] 
        this.ancho3 = Math.round((100 / this.arry3.length)-66);
        break;
      }
      case 1:{
        this.arry3 = [] 
        break;
      }
      case 2:{
        this.arry3 = [
          {
            nombre:'Canvas HIJO 1',
            color:'N',
            link:'N',
          },
          {
            nombre:'Canvas HIJO 2',
            color:'Y',
            link:'Y',
          },
          {
            nombre:'Canvas HIJO 3',
            color:'Y',
            link:'N',
          },
        ]
        this.ancho3 = Math.round((100 / this.arry3.length));
        break;
      }
      case 3:{
        this.arry3 = [
          {
            nombre:'Canvas HIJO 1',
            color:'N',
            link:'N',
          },
          {
            nombre:'Canvas HIJO 2',
            color:'Y',
            link:'N',
          },
        ]
        this.ancho3 = Math.round((100 / this.arry3.length)-17);
        break;
      }
      default:{
        this.arry3 = [] 
        break;
      }
    }
    
    setTimeout(() => {
      if(this.arry2[index].link == 'Y'){
        const url = this.router.createUrlTree(['pago_caja/pagos'])
        window.open(url.toString(), '_blank');
        this.spinner.hide();
      }
      // else if(this.arry2[index].link =='N' &&  this.arry2[index].color == 'Y'){
      //   console.log('no tiene hijos y tampoco link');
      // }
      else if(this.arry2[index].color ='Y' && this.arry2[index].link == 'N'){
        for (let index = 0; index < this.arry3.length; index++) {
          if(this.arry3[index].color == 'Y'){
            this.myDrawingFunction3(index,this.arry3[index].nombre,this.color[0],x)
          }else{
            this.myDrawingFunction3(index,this.arry3[index].nombre,this.color[1],x)
          }
        }
        this.spinner.hide()
      }  
    }, 1000);
  }
  selectSubCanvas2(i){
    console.log(i)
  }

  myDrawingFunction2(index:number,nombre,color,x:number){
    let canvas = <HTMLCanvasElement>this.el.nativeElement.querySelector('#subcanvas-' + index)   
    this.ctx2 = canvas.getContext('2d');
    this.ctx2.beginPath();
    this.ctx2.fillStyle = color 
    this.ctx2.moveTo(0,0);
    this.ctx2.lineTo(x,0);
    this.ctx2.lineTo(x + 50,50);
    this.ctx2.lineTo(x,100);
    this.ctx2.lineTo(0,100);
    this.ctx2.lineTo(50,50);
    this.ctx2.closePath();
    this.ctx2.fill();
    this.ctx2.fillStyle = '#000' 
    this.ctx2.font = "20px Arial";
    this.ctx2.fillText(nombre, 55, 55);    
  }

  myDrawingFunction3(index:number,nombre,color,x){
    let canvas = <HTMLCanvasElement>this.el.nativeElement.querySelector('#hCanvas-' + index)   
    this.ctx3 = canvas.getContext('2d');
    this.ctx3.beginPath();
    this.ctx3.fillStyle = color
    this.ctx3.moveTo(0,0);
    this.ctx3.lineTo(x,0);
    this.ctx3.lineTo(x+50,50);
    this.ctx3.lineTo(x,100);
    this.ctx3.lineTo(0,100);
    this.ctx3.lineTo(50,50);
    this.ctx3.closePath();
    this.ctx3.fill();
    this.ctx3.fillStyle = '#fff' 
    this.ctx3.font = "20px Arial";
    this.ctx3.fillText(nombre, 55, 55);  
  }
}
