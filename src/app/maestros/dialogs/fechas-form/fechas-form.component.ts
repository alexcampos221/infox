import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UsuarioService } from 'src/app/shared/services/backend/usuario.service';
import { AlertaService } from 'src/app/shared/services/general/alerta.service';

@Component({
  selector: 'app-fechas-form',
  templateUrl: './fechas-form.component.html',
  styleUrls: ['./fechas-form.component.sass']
})
export class FechasFormComponent implements OnInit {

  formBusqueda : FormGroup;
  accion:string;
  
  lista_meses : any [] = [];
  lista_anios : any [] = [];

  enviando = false;

  constructor(private usuarioService:UsuarioService,
              private dialogRef: MatDialogRef<FechasFormComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private fb : FormBuilder,
              private alerta : AlertaService,
              ) { 
      console.log(data)
      this.accion = data.nombre
    }

  ngOnInit(): void {
    this.createForm();
    this.traerMeses();
    this.traerAnios();
  }

  createForm(){
    this.formBusqueda = this.fb.group({
      anios:['',[]],
      meses:['',[]],
      valor:['',[]],
    });
  }
  private get f() {
    return this.formBusqueda.controls
  }
  onSubmitClick(){
    console.log(this.formBusqueda.value)
    let salida = {
      usuarioID: this.data.usuarioID,
      periodoID: Number(this.f.anios.value),
      mesID: Number(this.f.meses.value),
      meta: this.f.valor.value
    }
    console.log(salida);
    this.usuarioService.crearMeta(JSON.stringify(salida)).subscribe(resp=>{
      console.log(resp);
      if(resp['status']='OK'){        
        this.dialogRef.close({estado : 'EXITO'})
      }else{        
        this.dialogRef.close({estado : 'FALLO'})
      }
    })
  }
  traerMeses(){
    let salida = {
      empresaID: 1000008
    }
    this.usuarioService.listarMeses(JSON.stringify(salida)).subscribe((resp:any)=>{
      console.log(resp);
      this.lista_meses = resp
    })
  }
  traerAnios(){
    // let salida = {
    //   empresaID: 1000008
    // }
    this.usuarioService.listarAnios().subscribe((resp:any)=>{
      console.log(resp);
      this.lista_anios = resp
    })
  }

  /*
  selectMeses(event){
    console.log(event)
    this.f.meses.setValue(event.mes_id)
  }
  selectAnios(event){
    this.f.anios.setValue(event.anio_id)
    console.log(event)
  }
  */
  closeDialog():void{
    this.dialogRef.close();
  }

  public enviarMeta(){
    console.log(this.formBusqueda.value);
    this.guardarMeta();
  }

  private guardarMeta(){
    let salida = {
      usuarioID: this.data.usuarioID,
      periodoID: Number(this.f.anios.value.anio_id),
      mesID: Number(this.f.meses.value.mes_id),
      meta: this.f.valor.value
    }
    console.log(salida);
    this.formBusqueda.disable();
    this.enviando = true;
    this.usuarioService.crearMeta(JSON.stringify(salida)).subscribe(resp=>{
      console.log(resp);
      if(resp['status']='OK'){
        this.alerta.registroCorrecto();
        this.dialogRef.close({estado : 'EXITO'});
      }else{
        this.alerta.algoHaIdoMal();
        this.dialogRef.close({estado : 'FALLO'})
      }
    });
  }
}
