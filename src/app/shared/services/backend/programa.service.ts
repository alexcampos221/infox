import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

import * as Global from '../../globals';
import { Utils } from '../../utils';
import { utils } from 'xlsx';

@Injectable({
  providedIn: 'root'
})
export class ProgramaService {

  constructor(public http : HttpClient) { }
  realizarBusqueda2(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Dashboard/resumenByCajaFecha2', body, requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  filtroBusqueda(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Matricula/generarReporte',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  listarSaldosByCaja(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Matricula/listarSaldosByCaja',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

  
  listaProgramas(){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Programa/listar', Utils.jsonToFormDataV2(false), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

  listaProgramasAll(){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Programa/listarTodos', Utils.jsonToFormDataV2(false), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

  editarPorgrama(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Programa/editar',Utils.jsonToFormDataV2(body,false), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  
  eliminarPrograma(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Programa/eliminar',Utils.jsonToFormDataV2(body,false), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

  status(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Programa/cambiarEstadoCurso',Utils.jsonToFormDataV2(body,false), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  cambiarEstado(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Programa/cambiarEstado', Utils.jsonToFormDataV2(body,false), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

  guardarPrograma(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Programa/guardar',Utils.jsonToFormDataV2(body,false), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  traerInfoPrograma(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Matricula/infoPago',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  traerUsuarioInfo(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Matricula/infoPago',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  traerCuotasUsuario(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Matricula/listarCuotasByPago',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  listarBancos(){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.get(Global.ruta_base_infox+ 'listaBancos', requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
 
  crearPrimeraCuota(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Matricula/convertirEnCuotas',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  listarVendedores(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Usuario/listar',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  crearNuevaCuota(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Matricula/guardarCuota',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  editarCuota(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Matricula/editarCuotaByVendedor',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  
  editarPago(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Matricula/editarPago',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  
  eliminarCuota(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Matricula/eliminarCuota',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  consultarNumero(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Api/validarNumero',Utils.jsonToFormDataV2(body,false), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  aprobarCuota(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Matricula/aprobarPago',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  actualizarVendedorByCaja(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Matricula/actualizarVendedor',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  
  listarUsuariosByCaja(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Usuario/listar',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  infoCurso(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Programa/info',Utils.jsonToFormDataV2(body,false), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  listaProgramaCursos(){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.get(Global.ruta_base_infox+ 'listaProgramaCursos', requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

  listaProgramaNotificaciones(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.get(Global.api_infox + `seguimiento?programaID=${body.programaID}`, requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

  aprobarCuotaByCaja(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Matricula/aprobarPagoByCaja',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  listarInfoCards(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Dashboard/resumenByFecha',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  listarInfoCards2(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Dashboard/resumenByCaja',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  
  // realizarBusqueda(body){
  //   const requestOptions = {
  //     headers: new HttpHeaders({
  //       // 'Content-Type':  'application/json'
  //     })
  //   };
  //   let request = this.http.post(Global.ruta_infox+ 'Dashboard/resumenByTipoFecha',(body), requestOptions).pipe(
  //     map((resp) => {
  //       return resp;
  //     })
  //   )
  //   return request;
  // }
  realizarBusqueda(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Dashboard/resumenByCajaFecha2',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  
  listarAnios(){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.apiTransvial+ 'Anio/listar', requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  realizarBusquedaAnio(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Dashboard/resumenByVendedorAnio',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }


  agregarNotificacion(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.api_infox+ 'seguimiento',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

  editarNotificacion(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.api_infox+ 'seguimiento',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  changeStatusNotificacion(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Matricula/editarPago',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
}
