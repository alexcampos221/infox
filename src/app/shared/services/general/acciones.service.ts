import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AccionesService {

  public porcentaje$: Subject<any> = new Subject();
  constructor() { }
}
