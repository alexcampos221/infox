import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ProgramaService } from 'src/app/shared/services/backend/programa.service';
import { AlertaService } from 'src/app/shared/services/general/alerta.service';
import { ExcelService } from 'src/app/shared/services/general/excel.service';
import { UtlitariosService } from 'src/app/shared/services/general/utlitarios.service';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { Utils } from 'src/app/shared/utils';
import * as Globales from 'src/app/shared/globals';
import { Console } from 'console';

@Component({
  selector: 'app-saldos',
  templateUrl: './saldos.component.html',
  styleUrls: ['./saldos.component.sass'],
  providers: [DatePipe]
})
export class SaldosComponent implements OnInit {
  Utils: Utils;

  codigo:any;
  usuario: any;

  formdata : FormGroup;
  formReporte : FormGroup;
  formTxtBusuqeda: FormGroup;
  matriculas: any[] = [];
  tipos:any[] = [];

  locales : any[] = [];
  lista_usuarios : any[] = [];
  documentos_procesados : any [] = [];
  ListaPagoPendientes_totales: any[] = [];
  lista_bancos: any[] = [];
  lista_vendedores: any[] = [];
  
  date_hoy : Date = new Date();
  s_tipos:any[]=[
    {
      nombre: 'Matricula', valor: 'MA',
    },
    {
      nombre: 'Cuota', valor: 'CU',
    },
  ]

  fechaInicial:any;
  DSlistaPagosPendientes:MatTableDataSource<any>;

  filtros:any;
  displayedColumns = [
    'infoPago',
    'fecha',  
    'banco', 
    'monto',     
    'url',
    'nombre',
    'dni',
    'telefono',             
    'programa',
    'curso',
    'tipoMatricula'
  ];

  //atributos para la tabla material
  @ViewChild(MatPaginator, { static: true }) paginatorReporte: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;


  constructor(private fb : FormBuilder,
    private loadingService: LoadingService,
    private spinner: NgxSpinnerService,
    private utilitarios: UtlitariosService,
    private datePipe: DatePipe,
    private ruteador : Router,
    private dateAdapter:DateAdapter<Date>,
    private  alertaServicio:AlertaService,
    private excel:ExcelService,
    private programaServicio : ProgramaService,
    ) {
      this.dateAdapter.setLocale('es-PE'); 
    }
    txtNuevo:string = '';
    txtFiltro:String ;
    maximo: Date = new Date();
  ngOnInit(): void {
    console.log("DFEDF");
    this.obtenerAsesor();
    this.crearFormulario();
    this.crearFormulario2();
    this.DSlistaPagosPendientes = new MatTableDataSource();
    
    this.traerDataTable();
    if(JSON.parse(localStorage.getItem('cerrar_detalle')) == true){
     
      this.txtFiltro = JSON.stringify(localStorage.getItem("txtFiltro")); 
      var reg= /"/gi
      this.txtNuevo = this.txtFiltro.replace(reg,'');
      this.applyFilter(this.txtNuevo);
    }
    this.prepararBusqueda();
    this.fechaInicial = new Date();
    this.formReporte.controls.desde.setValue(new Date(this.fechaInicial.getFullYear(),this.fechaInicial.getMonth(),1)); 
    this.formReporte.controls.hasta.setValue(new Date()); 
  }
  private prepararBusqueda(){
    this.prepararTipo(this.s_tipos);
    this.traerBancos();
    this.traerVendedores();
  }
 
  private prepararTipo(arreglo: any[]){
    this.tipos = arreglo;
  }
  private traerBancos(){
    this.programaServicio.listarBancos().subscribe((resp:any)=>{
      console.log(resp)
      this.lista_bancos = resp.result;  
    })
  }
  private traerVendedores(){
    // let usu_tipo =JSON.parse(localStorage.getItem('usu_actual')).usu_tipo;
    let salida = {
      tipo: Globales.usu_tipo
    }
    let body = JSON.stringify(salida);
    this.programaServicio.listarVendedores(body).subscribe((resp:any)=>{
      console.log(resp);
      this.lista_vendedores = resp;  
    })
  }

  private crearFormulario(){
    this.formReporte = this.fb.group({
      tipo : ['0',[]],      
      vendedor : ['0',[]],
      bancos : ['0',[]],
      desde: ['',[]],
      hasta : ['',[]],
      
      txtBusqueda: ['',[]]
    });
  }

  private crearFormulario2(){
    this.formdata = this.fb.group({
      vendedor : ['0',[]],
      desde: [Utils.primerdiaMes(),[]],
      hasta : [new Date(),[]],
     });
  }

  private get f(){
    return this.formReporte.controls;
  }

  ngAfterViewInit(): void {
    this.paginatorReporte._intl.itemsPerPageLabel = "Registros por página";
    this.DSlistaPagosPendientes.paginator = this.paginatorReporte;
    this.DSlistaPagosPendientes.sort = this.sort;
  }
  private obtenerAsesor(){
    this.usuario = JSON.parse(localStorage.getItem('usu_actual'));    
    this.codigo=this.usuario.usu_id;
  }

  traerDataTable(){
    this.realizarConsulta();

    /*
    let salida = {
      usuarioID : this.codigo
    };
    let body = JSON.stringify(salida);
    console.log(salida);
    this.spinner.show();
    this.loadingService.loading$.next({message:'', opacity:0.5});
    this.utilitarios.getListaPagosPendienteByCaja(body).subscribe((resp:any)=>{
      this.ListaPagoPendientes_totales=resp;
      this.DSlistaPagosPendientes.data = this.ListaPagoPendientes_totales;    
      console.log(this.DSlistaPagosPendientes);
      this.spinner.hide();      
    },(error)=>{
      console.log(error)
      this.spinner.hide();
    });
    */
  }
  arreglo_temporal:any[];

  sortData(sort: Sort){
    const data = this.ListaPagoPendientes_totales.slice();
    console.log(data);
    this.arreglo_temporal = [];
    if (!sort.active || sort.direction === '') {
      this.arreglo_temporal = data;
      this.DSlistaPagosPendientes.data = this.arreglo_temporal;
      return;
    }
    this.arreglo_temporal = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        
        case 'nombre': return this.compare(a.pag_nombre||'', b.pag_nombre||'', isAsc);
        case 'dni': return this.compare(a.pag_dni||'', b.pag_dni||'', isAsc);
        case 'telefono': return this.compare(a.pag_telefono||'', b.pag_telefono||'', isAsc);
        case 'programa': return this.compare(a.pro_nombre||0, b.pro_nombre||0, isAsc);
        case 'curso': return this.compare(a.cur_nombre||0, b.cur_nombre||0, isAsc);
        case 'banco': return this.compare(a.ban_nombre||'', b.ban_nombre||'', isAsc);
        case 'fecha': return this.compare(a.pag_fecha_abono || '', b.pag_fecha_abono, isAsc);
        case 'monto': return this.compare(Number(a.pag_monto) || 0, Number(b.pag_monto) || 0, isAsc);
        case 'tipoMatricula': return this.compare(a.pag_tipo || '', b.pag_tipo || '', isAsc);

        default: return 0;
      }
  });

  this.DSlistaPagosPendientes.data = this.arreglo_temporal;
  }
  selectedTipo(event){   
    this.f.tipo.setValue(event)
    console.log(event);
  }
  selectedVendedor(event){
    console.log(event);
  }
  selectedBanco(event){
    this.f.bancos.setValue(event)
    console.log(event);
  }
  EndDateChange(evento){
    console.log(evento);
  }
  realizarBusqueda(){
    this.realizarConsulta();
    // this.prepararTabla();
  }
  private realizarConsulta(){
    this.spinner.show()
    this.loadingService.loading$.next({opacity:0.5})
    // if(this.f.desde.value > this.f.hasta.value){
    //   this.alertaServicio.FechaEsMayor();
    // }else{
      
    // }
    let salida = {
      // tipo: this.f.tipo.value.valor || '0',
      vendedorID: this.codigo,
      // bancoID: Number(this.f.bancos.value.ban_id) || 0,
      desde: this.datePipe.transform(this.formdata.controls.desde.value,'yyyy-MM-dd'),
      hasta: this.datePipe.transform(this.formdata.controls.hasta.value,'yyyy-MM-dd')
    }
    let body = JSON.stringify(salida);
    console.log(salida);
    this.programaServicio.listarSaldosByCaja(body).subscribe((resp:any)=>{
      this.DSlistaPagosPendientes.data = resp;
      this.ListaPagoPendientes_totales = resp;
      console.log(this.DSlistaPagosPendientes.data);
      this.spinner.hide();
    },(error)=>{
      console.log(error);
      this.spinner.hide();
    })
    
  }

  arrayPagosID: any[] = [];
  detallePendiente(valor,index){
    if(this.DSlistaPagosPendientes.filter == ''){
      for(let index = 0; index < this.ListaPagoPendientes_totales.length; index++) {
        this.arrayPagosID.push(this.ListaPagoPendientes_totales[index].pag_id);      
      }  
    }else{
      for(let index = 0; index < this.DSlistaPagosPendientes.filteredData.length; index++) {
        this.arrayPagosID.push(this.DSlistaPagosPendientes.filteredData[index].pag_id);      
      }     
    }
    let salida={
      pagoID:valor.pag_id,
      ventana:'Saldos'
    }
    localStorage.setItem('pago',valor.pag_id);  
    localStorage.setItem('indice',index);

    localStorage.setItem('arrayListaPendiente',JSON.stringify(this.arrayPagosID));
    localStorage.setItem('txtFiltro',this.DSlistaPagosPendientes.filter);
    
    this.ruteador.navigate(['pago_caja/detalle'] ,{ state : {data: salida } });       
  }
  
  verImageVoucher(row){
    this.spinner.show();
    this.loadingService.loading$.next({message:'', opacity:0.5});
    if(row.pag_url != ''){
      this.alertaServicio.MostrarImagenVoucher(row.pag_url);  
    }else{
      this.spinner.hide()
    }
    this.spinner.hide()
  }

  downloadFileExcel(){  
    let dataToExport = this.DSlistaPagosPendientes.data;

    let desde = this.datePipe.transform(this.formReporte.controls.desde.value,'dd-MM-yyyy');
    let hasta = this.datePipe.transform(this.formReporte.controls.hasta.value,'dd-MM-yyyy');
    console.log(desde)
    console.log(hasta)    
    let mapHeaders=new Map([      
      ["Fecha","pag_fecha_cuota"],
      ["Banco","ban_nombre"],
      ["Monto","pag_monto"],
      ["Nombre","pag_nombre"],
      ["Dni","pag_dni"],
      ["Telefono","pag_telefono"],
      ["Programa","pro_nombre"],
      ["Curso","cur_nombre"],
      ["Tipo Matricula","pag_tipo"],
      ["Vendedor","usu_vendedor"],
    ]);
    let mapHeadersShow=[
      'Fecha',
      'Banco',
      'Monto',
      'Nombre',
      'Dni',
      'Telefono',
      'Programa',
      'Curso',
      'Tipo Matricula',
      'Vendedor',
    ];

    let fecha = "Saldos "+ this.datePipe.transform(new Date(),'dd-MM-yyyy');

    let informacion = {
    
      desde : desde,
      hasta: hasta,
    };

    let dataExcel ={
      title: fecha,
      data: dataToExport,
      mapHeaders:mapHeaders,
      mapHeadersShow:mapHeadersShow,
      filtro:"",
      informacion : informacion,
    }
    console.log(dataExcel)
    this.excel.crearReporteSaldosByCaja(dataExcel)
  }

  applyFilter(filterValue: string) {
    console.log(filterValue)
    this.DSlistaPagosPendientes.filter = filterValue.trim().toLowerCase() || '';
    this.ListaPagoPendientes_totales = this.DSlistaPagosPendientes.filteredData
    localStorage.removeItem('txtFiltro');  
    localStorage.removeItem('cerrar_detalle');
  }
  private compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
  actualizarLista(){
    console.log('actualizar');
    this.traerDataTable();
  }
  filtrarByFechas(){
    this.spinner.show()
    this.loadingService.loading$.next({opacity:0.5})
    if(this.formdata.controls.desde.value > this.formdata.controls.hasta.value){
      this.alertaServicio.FechaEsMayor();
      this.spinner.hide();
    }else{
      let salida = {
        vendedorID: this.codigo,
        desde: this.datePipe.transform(this.formdata.controls.desde.value,'yyyy-MM-dd'),
        hasta: this.datePipe.transform(this.formdata.controls.hasta.value,'yyyy-MM-dd')
      }
      let body = JSON.stringify(salida);
      console.log(salida);
      this.programaServicio.listarSaldosByCaja(body).subscribe((resp:any)=>{
        this.DSlistaPagosPendientes.data = resp;
        this.ListaPagoPendientes_totales = resp;
        console.log(this.DSlistaPagosPendientes.data);
        this.spinner.hide();
      },(error)=>{
        console.log(error);
        this.spinner.hide();
      })
    }
  }
}
