import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import  {PagosComponent} from '../caja/pagos/pagos.component'
import { AnuladosComponent } from './anulados/anulados.component';
import { AprobadosComponent } from './aprobados/aprobados.component';
import { PendienteDetalleComponent } from './pendiente-detalle/pendiente-detalle.component';
import { ReporteDetalleComponent } from './reporte-detalle/reporte-detalle.component';
import { ReporteGeneralComponent } from './reporte-general/reporte-general.component';
import { SaldosComponent } from './saldos/saldos.component';

const routes: Routes = [
  {
    path: 'pagos',
    component: PagosComponent,
  },
  {
    path: 'detalle',
    component: PendienteDetalleComponent,
  },  
  {
    path: 'reporte',
    component: ReporteGeneralComponent,
  },  
  {
    path: 'reporte-detalle',
    component: ReporteDetalleComponent,
  },
  {
    path: 'anulados',
    component: AnuladosComponent,
  }, 
  {
    path: 'saldos',
    component: SaldosComponent,
  }, 
  {
    path: 'aprobados',
    component: AprobadosComponent,
  }, 
   
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CajaRoutingModule { }
