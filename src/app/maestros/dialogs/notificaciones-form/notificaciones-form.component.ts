import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FirebaseStorageService } from 'src/app/shared/services/storage/firebase-storage.service';
import { AngularEditorConfig } from '@kolkov/angular-editor'
import { UsuarioService } from 'src/app/shared/services/backend/usuario.service';
import { AlertaService } from 'src/app/shared/services/general/alerta.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { ProgramaService } from 'src/app/shared/services/backend/programa.service';
import * as global from './../../../shared/globals'
import { UtlitariosService } from 'src/app/shared/services/general/utlitarios.service';
@Component({
  selector: 'app-notificaciones-form',
  templateUrl: './notificaciones-form.component.html',
  styleUrls: ['./notificaciones-form.component.sass']
})
export class NotificacionesFormComponent implements OnInit {

  nombre : string = ""
  notificacionForm:FormGroup;

  lista_tipo: any [] = [
    {
      name:"Llamadas",
      id:"LL",
    },
    {
      name:"whatsapp",
      id:"WA",
    },
    {
      name:"email",
      id:"EM",
    },
  ]

  programaID : any = ""
  editorConfig: AngularEditorConfig = {
    editable: true,
      spellcheck: true,
      height: 'auto',
      minHeight: '250px',
      maxHeight: 'auto',
      width: 'auto',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      placeholder: 'Enter text here...',
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        {class: 'arial', name: 'Arial'},
        {class: 'times-new-roman', name: 'Times New Roman'},
        {class: 'calibri', name: 'Calibri'},
        {class: 'comic-sans-ms', name: 'Comic Sans MS'}
      ],
      customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    // upload: (file: File) => { ... }
    uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      ['bold', 'italic'],
      ['fontSize']
    ]
  };
  isVendor : boolean = true
  lista_vendedores : any[]
  lista_notificaion : any;
  lista_horas : any [] = [];
  ruta_multimedia : string = "";
  isSelectT : string = "0"
  constructor(
    private fb: FormBuilder,               
    public dialogRef: MatDialogRef<NotificacionesFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,     
    private storage:FirebaseStorageService,     
    private usuarioService:UsuarioService,    
    private alertaServicio:AlertaService,
    private spinner:NgxSpinnerService,
    private loadingService: LoadingService,
    private programaServicio: ProgramaService,
    private utlitariosService : UtlitariosService
  ) { }

  ngOnInit(): void {
    console.log(this.data)
    this.programaID = this.data.programaID
    this.nombre = this.data.nombre
    this.createFormulario();
    this.generarRangoHoras()
    this.traerUsuario()
  }

  generarRangoHoras(){
    for (let index = 0; index < 24; index++) {
      if(index < 10){
        this.lista_horas.push({id: index,value: "0" + index +":00"})
      }else{
        this.lista_horas.push({id: index,value: index +":00"})
      }
    }
  }
  createFormulario(){
    this.notificacionForm = this.fb.group({
      tipo:['',[Validators.required]],
      dias:['',[Validators.required]],
      vendedor:['',[Validators.required]],
      hora:['',[]],
      mensaje:['',[]],
      recordatorio:['',[]],
      adjunto:['',[]],
      prueba:['',[]],
      // plantilla:['',[]]
    })
  }

  private get f (){
    return this.notificacionForm.controls
  }

  traerUsuario(){
    let salida={
      tipo: '0'
    }
    this.usuarioService.listarVendors(salida).subscribe((resp:any) => {
      console.log(resp)
      this.lista_vendedores = resp
    })
  }
  selectTipo(evento){
    console.log(this.f.tipo.value)
    if(this.f.tipo.value.id == 'LL'){
      this.isSelectT = "0"
      this.ruta_multimedia = ""
      this.f.adjunto.setValue(null)
    }else{
      this.isSelectT = "1"
    }
    if(this.f.tipo.value.id == 'EM'){
      this.f.adjunto.setValue(null)
      this.ruta_multimedia = ""
    }
  }

  onChangeVendor(evento){
    console.log(evento)
    this.isVendor = evento.checked
  }
  selectVendedor(evento){

  }

  subirAdjunto(){
    this.spinner.show();
    this.loadingService.loading$.next({opacity:0.5})
    this.storage.uploadFile(this.f.adjunto.value._files,"temporales").then((publicURL:any)=>{
      this.ruta_multimedia = publicURL;
      console.log(this.ruta_multimedia);
      this.spinner.hide();
      // this.guardarNotificacionByAdjunto();
      this.guardarNotificacion()
    },(error)=>{
      console.log(error);
      this.spinner.hide();
      this.alertaServicio.algoHaIdoMal();
    });
  }
  closeDialog(){
    this.dialogRef.close();
  }

  enviarPrueba(){
    console.log(this.f.adjunto.value)
    if(this.f.tipo.value.id == "WA"){
      if(this.f.adjunto.value != null){
        this.enviarWppPrueba()
      }else{
        this.alertaServicio.camposObligatorios()
      }
    }else if (this.f.tipo.value.id == "EM"){
      this.enviarPruebaEmail()
    }
  }
  enviarPruebaEmail(){
    let salida = {
      name:"test"
    }
    this.utlitariosService.createGroupEmailPrueba(salida).subscribe(resp => {
      console.log(resp)
    },(error) => {
      console.log(error)
    })
    // this.utlitariosService.enviarEmailPrueba(salida).subscribe(resp => {
    //   console.log(resp)
    // },(error) => {
    //   console.log(error)
    // })
  }
  enviarWppPrueba(){
    let salida = {
      phone: this.f.prueba.value,
      device: global.deviceWpp,
      message: this.f.mensaje.value,
      enqueue : "never"
    }
    console.log(salida)
    this.utlitariosService.enviarWppPrueba(salida).subscribe(resp => {
      console.log(resp)
      if(resp["status"] == 200){
        this.alertaServicio.envioPruebaExistoso()
      }else{
        this.alertaServicio.errorMensajePrueba()
      }
    },(error) => {
      console.log(error)
      this.alertaServicio.errorMensajePrueba()
    })
  }
  enviarFormulario(){
    // console.log(this.f.adjunto.value)
    if(this.f.adjunto.value != ""){
      console.log(this.notificacionForm.value)
      this.subirAdjunto()
    }else{
      this.guardarNotificacion()
    }
  }
  // guardarNotificacionByAdjunto(){
  //   this.spinner.show();
  //   this.loadingService.loading$.next({opacity:0.5})
  //   let salida = {

  //   }
  // }

  guardarNotificacion(){
    this.spinner.show();
    this.loadingService.loading$.next({opacity:0.5})
    let salida = {
      message: this.f.mensaje.value,
      daysNext: this.f.dias.value,
      typeNotification: this.f.tipo.value.id,
      infoExtra: this.f.recordatorio.value,
      hourAction: this.f.hora.value.id,
      ulrMultimedia: this.ruta_multimedia,
      keyMultimedia:"keyMultimedia",
      userID: this.isVendor ? 100 : this.f.vendedor.value.usu_id,
      programaID: this.programaID
    }

    console.log(salida)
    this.programaServicio.agregarNotificacion(salida).subscribe(resp => {
      console.log(resp);
      if(resp['statusCode'] == 201){
        this.dialogRef.close({estado : 'EXITO'})
        this.spinner.hide();
      }
      else{
        this.dialogRef.close({estado : 'FALLO'});
        this.spinner.hide();
      }   
    },(error) => {
      console.log(error);
      this.spinner.hide();
    })
  }
}
