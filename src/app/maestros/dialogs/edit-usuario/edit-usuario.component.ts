import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { Spinner } from 'ngx-spinner/lib/ngx-spinner.enum';
import { UsuarioService } from 'src/app/shared/services/backend/usuario.service';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { FirebaseStorageService } from 'src/app/shared/services/storage/firebase-storage.service';

@Component({
  selector: 'app-edit-usuario',
  templateUrl: './edit-usuario.component.html',
  styleUrls: ['./edit-usuario.component.sass']
})
export class EditUsuarioComponent implements OnInit {
  estado:string;

  usuario : any;

  usuarioForm:FormGroup;
  filesmultimedia: File[] = [];
  nombre_archivo :string = "";
  multimediaData : string = "";

  lista_tipo_usuario : any [] = [
    {
      'id': '000',
      'nombre':'Vendedor',      
    },
    {
      'id': '100',
      'nombre':'Caja'
    },
    {
      'id': '200', 
      'nombre':'Admin'
    },
    {
      'id': '300', 
      'nombre':'Embajador'
    },
  ];
  noImage : boolean = false;
  creando : boolean = false;
  isPassword : boolean = false;
  pwdEqual : boolean = false;
  constructor(
    private dialogRef: MatDialogRef<EditUsuarioComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb : FormBuilder,
    private storage:FirebaseStorageService,
    private usuarioService:UsuarioService,
    private loadingService: LoadingService,
    private spinner:NgxSpinnerService,
  ) { 
    console.log(data)
    this.estado = data.nombre
    this.usuario = data.usuario
  }

  ngOnInit(): void {
    this.createForm()
    if(this.usuario == null || this.usuario == undefined){
      this.noImage = false;
      this.isPassword = true;
    }else{
      this.noImage = true;
      this.editForm();
    }
  }
  private createForm(){
    this.usuarioForm = this.fb.group({
      logo : ['',[]],
      nombre : ['',[Validators.required]],
      email : ['',[Validators.required]],
      usuario : ['',[Validators.required]],
      telefono: ['',[Validators.required]],
      tipoUsu: ['',[Validators.required]],
      pwd_new: ['',[Validators.required]],
      pwd_new_re: ['',[Validators.required]],
      codigo: ['',[Validators.required]],
    });
  }
  private get f(){
    return this.usuarioForm.controls;
  }
  editForm(){
    this.f.nombre.setValue(this.usuario.usu_nombre_compleo||'');
    this.f.email.setValue(this.usuario.usu_email||'');
    this.f.usuario.setValue(this.usuario.usu_usuario||'');
    this.f.logo.setValue(this.usuario.usu_foto || '');
    this.f.codigo.setValue(this.usuario.usu_codigo || '');
    if(this.usuario.usu_tipo=='000'){
      this.f.tipoUsu.setValue(this.lista_tipo_usuario[0])
    }else if(this.usuario.usu_tipo=='100'){
      this.f.tipoUsu.setValue(this.lista_tipo_usuario[1])
    }else if(this.usuario.usu_tipo=='200'){
      this.f.tipoUsu.setValue(this.lista_tipo_usuario[2])
    }else if(this.usuario.usu_tipo=='300'){
      this.f.tipoUsu.setValue(this.lista_tipo_usuario[3])
    }
  }

  cambiarEstado(event){
    this.isPassword = event.checked    
  }
  
  public updateImage(){
    this.noImage = false;
  }
  selectTipoUsuario(event){
    console.log(event)
    this.f.tipoUsu.setValue(event)
    // this.usuario.usu_tipo = event.id
    // console.log(this.usuario.usu_tipo)
  }

  public removeImage(){
    this.noImage = true;
    this.filesmultimedia = [];
  }
  public onRemovemultimedia(event) {
    this.filesmultimedia.splice(this.filesmultimedia.indexOf(event), 1);
    console.log(this.filesmultimedia.length);
  }
  public onSelectmultimedia(event) {
    console.log("ssss")
    if(!this.filesmultimedia.length){
      this.filesmultimedia = [];
      if(event.addedFiles[0].size<6161711){
        this.filesmultimedia.push(...event.addedFiles);
        console.log("La extension es :",this.filesmultimedia[0]);    
        let archivo = this.filesmultimedia[0];
        let extencion = archivo.type.toString();
        let ext = extencion.split('/')[1];
        var n = Date.now();
        this.nombre_archivo = this.data.nombre+"0000"+n.toString()+"."+ext;
      }else{
        //this.sweetalertsService.generalWarning("El tamaño máximo de multimedia es 6MB",2000);
        console.log("Se supero el tamañao maximo");
      }
    }
    console.log(this.filesmultimedia.length);
  }

  onSubmitClick(){
    if(this.usuario == null || this.usuario==undefined){
      this.previewSave();
    }else{
      this.previewEdit();
    }
  }
  
  previewSave(){
    // console.log(this.usuarioForm.value);
    if(this.filesmultimedia.length>0){
      this.sendImageFirebase();
    }
    else{
      this.saveUser();
    }
  }
  previewEdit(){
    // console.log(this.usuarioForm.value);
    if(this.filesmultimedia.length>0){
      this.sendImageFirebase();
    }
    else{
      this.editUser();
    }
  }
  private saveUser(){
    let rol = this.f.tipoUsu.value;
    if(this.f.pwd_new.value == this.f.pwd_new_re.value){
      this.pwdEqual = false
      
      let body = {
        nombre_completo: this.f.nombre.value,
        usuario: this.f.usuario.value,
        email: this.f.email.value,
        telefono : this.f.telefono.value,
        codigo : this.f.codigo.value,
        perfil: this.multimediaData,
        rol : rol.id,
        password: this.f.pwd_new.value,
        
      };
      this.spinner.show();
      this.loadingService.loading$.next({opacity:0.5});
      console.log(body);
      this.usuarioService.crearUsuario(JSON.stringify(body)).subscribe(resp=>{
        console.log(resp);
        if(resp['status']=='OK'){
          this.dialogRef.close({estado : 'EXITO'})
          this.spinner.hide();
        }
        else{
          this.dialogRef.close({estado : 'FALLO'});
          this.spinner.hide();
        }   
      },(error)=>{
        console.log(error);
        this.spinner.hide();
      });
    }else{
      this.pwdEqual = true
    }
   
    
  }

  private editUser(){
    console.log(this.f.tipoUsu.value)
    let rol = this.f.tipoUsu.value;
    let imagen_ruta;
    if(this.noImage){
      imagen_ruta = this.f.logo.value
    }
    else{
      imagen_ruta = this.multimediaData
    }
    this.spinner.show();
    this.loadingService.loading$.next({opacity:0.5});
    if(this.isPassword == true){
      if(this.f.pwd_new.value == this.f.pwd_new_re.value){
        this.pwdEqual = false
        let body = {
          nombre_completo: this.f.nombre.value,
          usuario: this.f.usuario.value,
          email: this.f.email.value,
          telefono:this.f.telefono.value,
          rol: rol.id,
          codigo : this.f.codigo.value,
          perfil: imagen_ruta,
          usuarioID: Number(this.usuario.usu_id),
          password: this.f.pwd_new.value,
        };
        console.log(body);
        this.usuarioService.editarUsuario(JSON.stringify(body)).subscribe(resp=>{
          console.log(resp);
          if(resp['status']=='OK'){
            this.dialogRef.close({estado : 'EXITO'})
            this.spinner.hide();
          }
          else{
            this.dialogRef.close({estado : 'FALLO'});
            this.spinner.hide();
          }
        },(error)=>{
          console.log(error);
          this.spinner.hide();
        });
      }else{
        this.pwdEqual = true
      }      
    }else{
      let body = {
        nombre_completo: this.f.nombre.value,
        usuario: this.f.usuario.value,
        email: this.f.email.value,
        telefono:this.f.telefono.value,
        rol: rol.id,
        codigo : this.f.codigo.value,
        perfil: imagen_ruta,
        usuarioID: Number(this.usuario.usu_id),
      };
      console.log(body);
      this.usuarioService.editarUsuario(JSON.stringify(body)).subscribe(resp=>{
        console.log(resp);       
        if(resp['status']=='OK'){
          this.dialogRef.close({estado : 'EXITO'})
          this.spinner.hide();
        }
        else{
          this.dialogRef.close({estado : 'FALLO'});
          this.spinner.hide();
        }
      },(error)=>{
        console.log(error);
        this.spinner.hide();
      });
    }
  }
  sendImageFirebase(){
    this.storage.uploadFile(this.filesmultimedia,"temporales").then((publicURL:any)=>{ 
      console.log("aqui *******");
      this.multimediaData=publicURL;
      if(this.usuario == null || this.usuario==undefined){
        this.saveUser();
      }
      else{
        this.editUser();
      }
      
    });
  }
    

  cerrar_popup(): void{
    this.dialogRef.close();
  }
  closeDialog(): void{
    this.dialogRef.close();
  }
 
}
