import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaestrosRoutingModule } from './maestros-routing.module';
import { UniversidadesComponent } from './universidades/universidades.component';
import { ProgramasComponent } from './programas/programas.component';
import { BancosComponent } from './bancos/bancos.component';

import { MatExpansionModule } from '@angular/material/expansion';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTabsModule } from '@angular/material/tabs';

import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSelectModule } from '@angular/material/select';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSortModule } from '@angular/material/sort';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MaterialFileInputModule } from 'ngx-material-file-input';
import { MatMenuModule } from '@angular/material/menu';
import { NgxDropzoneModule } from 'ngx-dropzone';

import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatStepperModule } from '@angular/material/stepper';
import { CrearUniversidadComponent } from './dialogs/crear-universidad/crear-universidad.component';
import { CursosComponent } from './cursos/cursos.component';
import { CursoFormComponent } from './dialogs/curso-form/curso-form.component';
import { ProgramaFormComponent } from './dialogs/programa-form/programa-form.component';
import { BancoFormComponent } from './dialogs/banco-form/banco-form.component';
import { SemestresComponent } from './semestres/semestres.component';
import { SemestreFormComponent } from './dialogs/semestre-form/semestre-form.component';
import { PromocionesComponent } from './promociones/promociones.component';
import { PromocionFormComponent } from './dialogs/promocion-form/promocion-form.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { UsuarioDetailComponent } from './usuarios-detail/usuario-detail/usuario-detail.component';
import { EditUsuarioComponent } from './dialogs/edit-usuario/edit-usuario.component';
import { AniosComponent } from './anios/anios.component';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { FechasFormComponent } from './dialogs/fechas-form/fechas-form.component';
import { ProgramaDetailComponent } from './programa-detail/programa-detail.component';
import { PaisesComponent } from './paises/paises.component';
import { ProspectosComponent } from './prospectos/prospectos.component';
import { CertificadosComponent } from './certificados/certificados.component';
import { PaisFormComponent } from './dialogs/pais-form/pais-form.component';
import { CertificadoFormComponent } from './dialogs/certificado-form/certificado-form.component';
import { NotificacionesFormComponent } from './dialogs/notificaciones-form/notificaciones-form.component';
import { AngularEditorModule } from '@kolkov/angular-editor';

@NgModule({
  declarations: [UniversidadesComponent, ProgramasComponent, BancosComponent, CrearUniversidadComponent, CursosComponent, CursoFormComponent, ProgramaFormComponent, BancoFormComponent, SemestresComponent, SemestreFormComponent, PromocionesComponent, PromocionFormComponent, UsuariosComponent, UsuarioDetailComponent, EditUsuarioComponent, AniosComponent, FechasFormComponent, ProgramaDetailComponent, PaisesComponent, ProspectosComponent, CertificadosComponent, PaisFormComponent, CertificadoFormComponent, NotificacionesFormComponent],
  imports: [
    CommonModule,
    NgxDropzoneModule,
    MaestrosRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTabsModule,
    AngularEditorModule,
    MatTableModule,
    MatPaginatorModule,
    MatSnackBarModule,
    MatButtonToggleModule,
    MatDialogModule,
    MatSortModule,
    MatToolbarModule,
    MatSelectModule,
    MatDatepickerModule,
    MaterialFileInputModule,
    MatMenuModule,
    MatSlideToggleModule,
    MatStepperModule,
  ]
})
export class MaestrosModule { }
