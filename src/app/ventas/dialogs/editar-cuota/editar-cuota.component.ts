import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ProgramaService } from 'src/app/shared/services/backend/programa.service';
import { AlertaService } from 'src/app/shared/services/general/alerta.service';
import { FirebaseStorageService } from 'src/app/shared/services/storage/firebase-storage.service';
import { DateAdapter } from '@angular/material/core';
@Component({
  selector: 'app-editar-cuota',
  templateUrl: './editar-cuota.component.html',
  styleUrls: ['./editar-cuota.component.sass'],
  providers: [DatePipe],
})
export class EditarCuotaComponent implements OnInit {

  public editCuota: FormGroup;
  public editCuotaNueva: FormGroup;
  estado : any;
  ruta_multimedia: any;
  bancoID:any;
  fecha_maxima = new Date();
  lista_bancos : any []=[];
  fechaVenc = new Date();
  fechaPago:string;
  imagen_voucher:any;

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<EditarCuotaComponent>,
    private datePipe: DatePipe,
    private programaServicio: ProgramaService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private alertaServicio:AlertaService,
    private dateAdapter:DateAdapter<Date>
     ) {
       this.dateAdapter.setLocale('es-PE')
      console.log(data);
     }
  public ngOnInit(): void {
    this.crearFormulario();
    this.crearFormulario2();
    this.setDataForm();
    this.traerBancos();
    
    this.estado=this.data.estado;
    this.imagen_voucher = this.data.url;      
  }
  private setDataForm(){
    let salida ={
      ban_id: this.data.bancoID,
      ban_nombre: this.data.banco
    }
    this.editCuota.controls.banco_nom.setValue(salida);
    this.f.url.setValue(this.data.url);
    
    this.f.numOperacion.setValue(this.data.numOperacion);
    this.f.montoPagado.setValue(parseFloat(this.data.montoPago).toFixed(2));
    
    this.f.fechaPago.setValue(new Date(this.data.fechaPago));
    
    this.f.fechaVencimiento.setValue(new Date(this.data.fVencimiento));
    
    this.f.montoCuota.setValue(parseFloat(this.data.mCuota).toFixed(2));
    // console.log(this.f)
    }
  private crearFormulario(){    
    this.editCuota = this.fb.group({
      url: ['',[Validators.required]],      
      banco_nom: ['',[]],
      numOperacion: ['',[Validators.required]],  
      montoPagado: ['',[Validators.required]], 
      fechaPago  : ['',[Validators.required]],

      fechaVencimiento: ['',[Validators.required]],
      
      montoCuota: ['',[Validators.required]],   
    });
  }
  private crearFormulario2(){    
    this.editCuotaNueva = this.fb.group({    
      fechaVencimiento: ['',[Validators.required]],      
      montoCuota: ['',[Validators.required]],   
    });
  }

  private get f(){
    return this.editCuota.controls;
  }

  private traerBancos(){
    this.programaServicio.listarBancos().subscribe((resp:any)=>{
      this.lista_bancos = resp.result;  
      let salida ={
        ban_id: this.data.bancoID,
        ban_nombre: this.data.banco
      }
      //console.log(salida);
      let leerObj=null;
      for (let index = 0; index < this.lista_bancos.length; index++) {
        if(this.lista_bancos[index].ban_id == salida.ban_id){
          leerObj = this.lista_bancos[index];
          break;
        }        
      }
      if(leerObj != null){
        this.editCuota.controls['banco_nom'].setValue(leerObj);        
        this.bancoID=leerObj.ban_id; 
      }      
      console.log(this.lista_bancos);
    })
  }
  ruta_imagen:any;
  verImageVoucher(){
    // this.alertaServicio.MostrarImagenVoucher(this.imagen_voucher);
    this.ruta_imagen = this.imagen_voucher
  }
  selecBanco(event){
    this.bancoID = event.ban_id;
    console.log(event)  
  }

  onSubmitClick() {
    
    if(this.editCuota.valid){
      let salida = {
      url: this.editCuota.controls.url.value,
      bancoID: Number(this.bancoID),
      txtNumOperacion: this.editCuota.controls.numOperacion.value,
      txtMontoAbono: Number(this.editCuota.controls.montoPagado.value),
      txtFechaAbono:this.datePipe.transform(this.editCuota.controls.fechaPago.value,'yyyy-MM-dd'),
      cuotaID: this.data.cuotaID,
      usuarioID: this.data.usuarioID,
      txtMontoCuota: this.editCuota.controls.montoCuota.value,
      txtFechaCuotaVencimiento:this.datePipe.transform(this.editCuota.controls.fechaVencimiento.value,'yyyy-MM-dd'),
      pagoID: Number(this.data.cuotaID)
    }
    let body = JSON.stringify(salida); 
    console.log(salida);
    this.programaServicio.editarCuota(body).subscribe(resp=>{
      if(resp['status']=='OK'){        
        this.dialogRef.close({estado : 'EXITO'})
      }
      else{
        this.dialogRef.close({estado : 'FALLO'});
      }
    },(error)=>{
      console.log(error);
      this.dialogRef.close({estado : 'FALLO'});
    });
    }

    // if(this.editCuotaNueva.valid){
    //   let salida = {       
    //     cuotaID: this.data.cuotaID,
    //     usuarioID: this.data.usuarioID,
    //     txtMontoCuota: this.editCuota.controls.montoCuota.value,
    //     txtFechaCuotaVencimiento:this.datePipe.transform(this.editCuota.controls.fechaVencimiento.value,'yyyy-MM-dd'),
    //     pagoID: Number(this.data.cuotaID)
    //   }
    //   let body = JSON.stringify(salida); 
    //   console.log(salida);
    //   this.programaServicio.editarCuota(body).subscribe(resp=>{
    //     if(resp['status']=='OK'){        
    //       this.dialogRef.close({estado : 'EXITO'})
    //     }
    //     else{
    //       this.dialogRef.close({estado : 'FALLO'});
    //     }
    //   },(error)=>{
    //     console.log(error);
    //     this.dialogRef.close({estado : 'FALLO'});
    //   });
    // }
    
    // this.storage.uploadFile(this.editCuota.controls.url.value._files,"temporales").then((publicURL:any)=>{
    //   this.ruta_multimedia = publicURL;
    //   //console.log(this.ruta_multimedia);
    //   //this.spinner.hide();
    //   this.editarCuota();
    // },(error)=>{
    //   console.log(error);
    //   //this.enviandoformulario = false;
    //   //this.spinner.hide();
    //   this.alertaServicio.algoHaIdoMal();
    // });  
  }
  // private editarCuota(){
    
  // }
  closeDialog(): void {
    this.dialogRef.close();
  }
  cerrar_popup(): void{
    this.dialogRef.close();
  }
}
