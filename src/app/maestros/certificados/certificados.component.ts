import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { Subscription } from 'rxjs';
import { AlertaService } from 'src/app/shared/services/general/alerta.service';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { CertificadoService } from '../../shared/services/backend/certificado.service';
import { ExcelService } from 'src/app/shared/services/general/excel.service';
import { DatePipe } from '@angular/common';
import { CertificadoFormComponent } from '../dialogs/certificado-form/certificado-form.component';

@Component({
  selector: 'app-certificados',
  templateUrl: './certificados.component.html',
  styleUrls: ['./certificados.component.sass'],
  providers:[DatePipe]
})
export class CertificadosComponent implements OnInit {

  certificados_totales : any[] = [];
  certificados_filtrados : any[] = [];


  arreglo;
  dataSource_certificados_totales : MatTableDataSource<any>;

  //atributos para la tabla material
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;

  displayedColumns = [
    'codigo',
    'nombre',
    'dni',
    'programa',
    'curso',
    'fecha',
    'archivo',
    'estado',
    'acciones',
  ];

  filtro_control : FormControl;
  filtro$ : Subscription;

  usuario:any;
  constructor(public dialog: MatDialog,
              private spinner: NgxSpinnerService,
              private loading:LoadingService,
              private certificadoService: CertificadoService,
              private alertaServicio : AlertaService,
              private excel:ExcelService,
              private datePipe: DatePipe,) { 
                this.usuario = JSON.parse(localStorage.getItem('usu_actual'));  
              }

  ngOnDestroy(): void {
    if(this.filtro$){
      console.log("ubsuscribe")
      this.filtro$.unsubscribe()
    }
  }

  ngOnInit(): void {
    
    this.filtro_control = new FormControl(false);
    this.dataSource_certificados_totales = new MatTableDataSource();
    this.traerCertificados();
    this.pendienteFiltro();
  }

  ngAfterViewInit(): void {
    this.dataSource_certificados_totales.paginator = this.paginator;
    this.dataSource_certificados_totales.sort = this.sort;
  }

  private pendienteFiltro(){
    this.filtro$ = this.filtro_control.valueChanges.subscribe(resp=>{
      console.log(resp);
      if(resp){
        this.quitarFiltro();
      } 
      else{
        this.filtrarInformacion();
      }
    });
  }

  private traerCertificados(){
    this.spinner.show();
    this.loading.loading$.next({opacity:0.5});
    this.certificadoService.listarCertificados().subscribe((resp:any) =>{
      // console.log(resp)
      this.certificados_totales = resp; 
      this.filtrarInformacion();
      this.dataSource_certificados_totales.data = this.certificados_filtrados; 
      console.log("certificados ",this.certificados_filtrados);   
      this.spinner.hide(); 
    },(error)=>{
      //this.eventosServicio.hideLoading();
      this.spinner.hide();
      console.info("error bancos: ",error);
    });
  }

  applyFilter(filterValue:string){
    this.dataSource_certificados_totales.filter = filterValue.trim().toLowerCase();
    this.certificados_totales = this.dataSource_certificados_totales.filteredData
  }
  verArchivo(row){
    console.log(row)
  }
  nuevoCertificado(){
    const dialogRef = this.dialog.open(CertificadoFormComponent, {
      width:'700px',
      data: {
        nombre: "agregar",
        certificado : null
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
        if( result==undefined){
          console.log("No se recibe nada");
        }
        else if(result.estado=='EXITO'){
          this.traerCertificados();
          this.alertaServicio.creacionCorrecta();

          console.log("Se ha creado el banco");
        }
        else{
          console.log("ha ocurrido un error");
        }
    });
  }

  editarCertificado(row){
    // console.log(row);
    const dialogRef = this.dialog.open(CertificadoFormComponent, {
      width:'700px',
      data: {
        nombre: "editar",
        certificado : row,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
        if( result==undefined){
          console.log("No se recibe nada");
        }
        else if(result.estado=='EXITO'){
          this.traerCertificados();
          this.alertaServicio.actualizacionCorrecta();
          console.log("Se ha editado el banco");
        }
        else{
          console.log("ha ocurrido un error");
        }
    });
    
  }

  private filtrarInformacion(){
    this.certificados_filtrados = [];
    this.certificados_totales.forEach(elemento=>{
      if(elemento.cer_activo == 'Y'){
        this.certificados_filtrados.push(elemento)
      }
    });
    this.dataSource_certificados_totales.data = this.certificados_filtrados;
  }

  chageStateValue(data){
    if(data==='N') return 'Y';
    else if(data==='Y') return 'N';
    else throw 'error';  
  }

  public chageState(row){
    console.log(row);
    this.spinner.show();
    this.loading.loading$.next({opacity:0.5});
    let body = {
      estadoNuevo: this.chageStateValue(row.cer_activo),
      certificadoID: row.cer_id
    }
    console.log(body)
    
    this.certificadoService.status(body).subscribe((resp:any)=>{
      console.log(resp)
      this.traerCertificados();
      this.spinner.hide();
    },(error)=>{
      console.log(error);
      this.spinner.hide();
    });
  }

  private quitarFiltro(){
    this.certificados_filtrados = this.certificados_totales;
    this.dataSource_certificados_totales.data = this.certificados_filtrados;
  }
  sortData(sort:Sort){
    const data = this.certificados_filtrados.slice();
    let arreglo_temporal :any []= [];
    if (!sort.active || sort.direction === '') {
      arreglo_temporal = data;
      this.dataSource_certificados_totales.data = arreglo_temporal;
      return;
    }

    arreglo_temporal = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'codigo': return this.compare(a.ban_id||'', b.ban_id||'', isAsc);
        case 'banco': return this.compare(a.ban_nombre||'', b.ban_nombre||'', isAsc);
        default: return 0;
      }
    });
    this.dataSource_certificados_totales.data = arreglo_temporal;
  }
  private  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
  actualizarLista(){
    this.traerCertificados();
  }
  
  downloadFileExcel(){  
    let dataToExport = this.certificados_totales;
    
    let mapHeaders=new Map([      
      ["Codigo","cer_codigo"],
      ["Nombre","cer_nombre"],
      ["DNI","cer_dni"],
      ["Programa","programa_nombre"],
      ["Curso","curso_nombre"],
      ["Fecha","fecha_certificado"],
      ["Estado","cer_activo"],
    ]);
    let mapHeadersShow=[
      'Codigo',
      'Nombre',
      'DNI',
      'Programa',
      'Curso',
      'Fecha',
      'Estado',
    ];

    let fecha = "Certificados "+ this.datePipe.transform(new Date(),'dd-MM-yyyy');

    let dataExcel ={
      title: fecha,
      data: dataToExport,
      mapHeaders:mapHeaders,
      mapHeadersShow:mapHeadersShow,
      filtro:"",
      // informacion : informacion,
    }
    console.log(dataExcel)
    this.excel.crearCertificados(dataExcel)
  }
}
