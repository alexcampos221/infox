import { Component, OnInit } from '@angular/core';
import { Workbook } from 'exceljs';
import { ProgramaService } from 'src/app/shared/services/backend/programa.service';
import * as XLSX from 'xlsx';
import * as fs from 'file-saver';

@Component({
  selector: 'app-excel',
  templateUrl: './excel.component.html',
  styleUrls: ['./excel.component.sass'],
})
export class ExcelComponent implements OnInit {

  constructor(private programaService:ProgramaService) { }
  dataExcel : any [] = [];

  exportData : any [] = [];

  ngOnInit(): void {
  }

  onFileChange(event: any){
    const target: DataTransfer = <DataTransfer>(event.target);
    if (target.files.length !== 1) {
      throw new Error('Cannot use multiple files');
    }
    const reader: FileReader = new FileReader();
    reader.readAsBinaryString(target.files[0]);
    reader.onload = (e: any) => {
      const binarystr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(binarystr, { type: 'binary' });

      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      const data = XLSX.utils.sheet_to_json(ws);
      this.dataExcel = data;
      console.log(this.dataExcel)
      for (let index = 0; index < this.dataExcel.length; index++) {
        // debugger;
        this.consultarNumero(this.dataExcel[index].CARGADOS,index);  
      }
    };
  }

  consultarNumero(numero,index){
    // debugger;
    let body ={
      destinatario: numero
    }    
    this.programaService.consultarNumero(body).subscribe(resp=>{
      this.exportData.push(resp);
      console.log(this.exportData);
      this.downloadFileExcel(this.exportData[index]);
    },(error)=>{
      console.log(error)
    });    
  }

  downloadFileExcel(filaExcel : any){  
    // debugger;
    let programado;    
    if(filaExcel.exists == undefined || filaExcel.exists == null || filaExcel.exists == false){
      programado = ''
    }else{
      programado = 'si'
    }
    let sinWpp;
    if(filaExcel.isBusiness == false || filaExcel.isBusiness == undefined || filaExcel.isBusiness == null){
      sinWpp = '';
    }else{
      sinWpp = 'si';
    }
    let enviado;
    if(filaExcel.exists == undefined || filaExcel.exists == null ||  filaExcel.exists == false){ 
      enviado = ''
    }else{
      enviado = 'si'
    }

    let informacion ={
      phone: filaExcel.phone,
      programado: programado,
      sinwpp: sinWpp,
      enviado: enviado,
    }

    let mapHeaders=new Map([      
      ["Telefono","phone"],
      ["Programado","programado"],
      ["SinWhatApp","sinWpp"],
      ["Enviado","enviado"],      
    ]);

    let mapHeadersShow=[
      'Telefono',
      'Programado',
      'SinWhatApp',
      'Enviado',      
    ];

    let fecha = "Usuario Excel";

    let dataExcel ={
      title: fecha,
      data: informacion,
      mapHeaders:mapHeaders,
      mapHeadersShow:mapHeadersShow,
    }
    this.crearReporteGeneral(dataExcel)
  }

  crearReporteGeneral(excelData) {
    // debugger;
    const title = excelData.title;

    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet("Usuario");

    let titulo = [];    
    titulo[1] = title;

    let titulo_general = worksheet.addRow(titulo);
    titulo_general.alignment = { vertical: 'middle', horizontal: 'center' };
    titulo_general.font = {bold:true}
    worksheet.addRow([]);
    
    let titulos = excelData.mapHeadersShow;
    let headerRow = worksheet.addRow(titulos);    

    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: '4167B8' },
        bgColor: { argb: '' }
      }
      cell.font = {
        bold: true,
        color: { argb: 'FFFFFF' },
        size: 12
      }
    });
    // excelData.data.forEach(element => { 
      let fila = [];
      for (let index = 0; index < excelData.mapHeadersShow.length; index++) {
        let clave = excelData.mapHeaders.get(excelData.mapHeadersShow[index]);
        switch (clave) {
          default:
            fila[index+1] = excelData.data[clave];
            break;
        }
      }
      worksheet.addRow(fila);
    // })
    worksheet.getColumn(1).width = 20;
    worksheet.getColumn(2).width = 20;
    worksheet.getColumn(3).width = 20;
    worksheet.getColumn(4).width = 20;
    worksheet.addRow([]);    
    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, title + '.xlsx');
    })
    // console.log(excelData);
  }

}
