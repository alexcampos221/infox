import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ProgramaService } from 'src/app/shared/services/backend/programa.service';
import { DatePipe } from '@angular/common';
import { AlmacenamientoLocalService } from 'src/app/shared/services/general/almacenamiento-local.service';

export enum ToggleEnum {
  opcion1,
  opcion2
}

@Component({
  selector: 'app-procesar-cuota',
  templateUrl: './procesar-cuota.component.html',
  styleUrls: ['./procesar-cuota.component.sass'],
  providers: [DatePipe],
})

export class ProcesarCuotaComponent implements OnInit {

  toggleEnum = ToggleEnum;
  selectedState :any = ToggleEnum.opcion1;

  monedaID:Number;
  isGenerarCuota:boolean = false;
  isDarkSidebar = false;
  selectedValue = null;
  fechatxt:any;
  usuario_actual : any;
  constructor(  
              private dialogRef: MatDialogRef<ProcesarCuotaComponent>,
              private programaServicio: ProgramaService,  
              private datePipe: DatePipe,  
              @Inject(MAT_DIALOG_DATA) public data: any,
              private localService :AlmacenamientoLocalService,
              ) 
              
              { 
      console.log(data);
      
    }

  ngOnInit(): void {
    this.usuario_actual = this.localService.getItem('usu_actual');
  } 
        
  onChange($event) {
    this.isGenerarCuota=true;
    // 100 dolares
    // 308 soles
    if($event.value == 1){
      this.selectedState = $event.value;
      this.monedaID = 308 
    }else if($event.value == 2){
      this.selectedState = $event.value;
      this.monedaID = 100
    }else{
      this.selectedState = $event.value;
      console.log("no selecciono ningun valor");
    }
    console.log(this.monedaID);    
  }
 
  closeDialog(){
    this.dialogRef.close();
  }

  seleccionaMoneda(value){
    console.log(value);
  }


  generarCuota(){
    let fecha = this.datePipe.transform(this.data.fPagoL,'yyyy-MM-dd');
    if(this.data.estado == 'Aprobado'){
      localStorage.setItem('aprobado','Y');
    }

    let caja_envio = "";
    if(this.usuario_actual.usu_tipo=="100"){
      caja_envio = "Y"
    }
    else{
      caja_envio = "N"
    }

    let salida={
      url:this.data.url,
      bancoID: Number(this.data.bancoID),
      txtNumOperacion: this.data.numOperacion,
      txtMontoAbono: Number(this.data.mPagado),
      txtFechaAbono: fecha,
      vendedorID: this.data.vendedorID,
      pagoID: Number(this.data.pagoID),
      monedaID: Number(this.monedaID), //|| Number(this.data.monedaID)
      isCaja:caja_envio,
      aprobado : 'Y',
    }
    let body = JSON.stringify(salida);
    console.log(salida)
    
    this.programaServicio.crearPrimeraCuota(body).subscribe((resp)=>{
      console.log(resp);
      this.dialogRef.close({estado : 'EXITO'})
    });
  }
}
