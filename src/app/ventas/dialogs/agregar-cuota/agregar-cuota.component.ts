import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DateAdapter } from '@angular/material/core';
import { ProgramaService } from 'src/app/shared/services/backend/programa.service';
import { AlertaService } from 'src/app/shared/services/general/alerta.service';

@Component({
  selector: 'app-agregar-cuota',
  templateUrl: './agregar-cuota.component.html',
  styleUrls: ['./agregar-cuota.component.sass'],
  providers: [DatePipe],
})
export class AgregarCuotaComponent implements OnInit {
  public addCuota: FormGroup;
  
  montoSaldo:string;
  montoTotal:string;
  monedaID:any;
  enviandoformulario : boolean = false;
  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<AgregarCuotaComponent>,
    private datePipe: DatePipe,
    private alertaServicio:AlertaService,
    private programaServicio: ProgramaService,
    private dateAdapter:DateAdapter<Date>,
    @Inject(MAT_DIALOG_DATA) public data: any,
     ) {
      this.dateAdapter.setLocale('es-PE')
       console.log(data);
       console.log(this.data.montoS)
       this.montoTotal = this.data.montoT;
       this.montoSaldo = this.data.montoS || 0;
       this.monedaID = this.data.monedaID

     }
  public ngOnInit(): void {
    this.crearFormulario();
    this.addCuota.controls.vendedorID.setValue(this.data.codigoVendedor);
    this.addCuota.controls.pagoID.setValue(this.data.pagoID);
    this.addCuota.controls.monedaID.setValue(this.data.monedaID);
  }

  private crearFormulario(){ 
    this.addCuota = this.fb.group({
      montoCuota: ['',[Validators.required,
        //Validators.pattern('[0-9]{1,2}[.][0-9]{1,3}[.][0-9]{1,3}[,]\d*')
      ]], 
      fechaVencimiento: ['',[Validators.required]],
      pagoID: ['',[Validators.required]],
      vendedorID : ['',[Validators.required]],
      monedaID: ['',[Validators.required]]
    });
  }
  closeDialog(): void {
    this.dialogRef.close();
  }
  cerrar_popup(): void{
    this.dialogRef.close();
  }
  filterNumero(event){
    console.log(event);
    // if(event != Validators.pattern('[0-9]{1,2}[.][0-9]{1,3}[.][0-9]{1,3}[,]\d*')){
    //   this.alertaServicio.errorCantidadDecimales();
    // }
    // ^[0-9]+([,][0-9]+)?$
    //var valor = event.value;
    //event.value = (valor.indexOf(".") >= 0) ? (valor.substr(0, valor.indexOf(".")) + valor.substr(valor.indexOf("."), 3)) : valor;
  }
  onSubmitClick() {
    let txtMonto= parseFloat(this.addCuota.controls.montoCuota.value)
    let salida = {
      txtFechaVencimiento: this.datePipe.transform(this.addCuota.controls.fechaVencimiento.value,'yyyy-MM-dd'),
      txtMonto: txtMonto,
      pagoID: this.addCuota.controls.pagoID.value,
      vendedorID: this.addCuota.controls.vendedorID.value,
      monedaID:Number(this.addCuota.controls.monedaID.value)
    }
    console.log(salida)
    let body = JSON.stringify(salida);    
    this.programaServicio.crearNuevaCuota(body).subscribe(resp=>{
      console.log(resp);
      if(resp['status']=='OK'){
        this.dialogRef.close({estado : 'EXITO'})
      }
      else{
        this.dialogRef.close({estado : 'FALLO'});
      }
    },(error)=>{
      console.log(error);
      this.dialogRef.close({estado : 'FALLO'});
    });
    
    
  }
}
