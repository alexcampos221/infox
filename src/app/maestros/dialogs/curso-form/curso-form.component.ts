import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatriculaService } from 'src/app/shared/services/registros/matricula.service';
import { ProgramaService } from 'src/app/shared/services/backend/programa.service';
import { CursoService } from 'src/app/shared/services/backend/curso.service';

@Component({
  selector: 'app-curso-form',
  templateUrl: './curso-form.component.html',
  styleUrls: ['./curso-form.component.sass']
})
export class CursoFormComponent implements OnInit {

  programas_totales : any[] = [];

  cursoForm : FormGroup;

  public nombre : string = '';
  editar : boolean = false;

  constructor(//private servicios:MatriculaService,
              private fb: FormBuilder,               
              public dialogRef: MatDialogRef<CursoFormComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,              
              private programaServicio: ProgramaService,
              private cursosServicio: CursoService,
              ) { 
                console.log(data)
              }

  ngOnInit(): void {
    this.nombre = "Crear Curso";
    if(this.data.nombre == 'editar'){
      // console.log(this.data.curso);
      this.editar = true;
     
      this.crearFormularioEditar();
      this.nombre = 'Editar Curso';
      this.f.curso.setValue(this.data.curso.cur_nombre);   
      this.f.descripcion.setValue(this.data.curso.cur_descripcion);   
      
    }
    else{
      this.crearFormulario();
      // this.traerProgramas();
    }
    this.traerProgramas();
  }

  private traerProgramas(){
    this.programaServicio.listaProgramas().subscribe((resp:any) =>{      
      this.programas_totales = resp.filter(elemento=>{
        return elemento.pro_activo == 'Y';
      });
      if(this.data.nombre == 'editar'){
        let programa = {
          pro_id: this.data.curso.pro_id,
          pro_nombre:this.data.curso.programa_nombre
        }
        for (let index = 0; index < this.programas_totales.length; index++) {
          if(programa.pro_id == this.programas_totales[index].pro_id){
            this.cursoForm.controls.programa.setValue(this.programas_totales[index]);
            break;
          }        
        }
      }
    },(error)=>{
      //this.eventosServicio.hideLoading();
      console.info("error Usuarios: ",error);
    });
  }

  private crearFormulario(){
    this.cursoForm = this.fb.group({
       curso: ['',[Validators.required]],
       descripcion: ['',[Validators.required]],
    });
  }

  private crearFormularioEditar(){
    this.cursoForm = this.fb.group({
      curso: ['',[Validators.required]],
      descripcion: ['',[Validators.required]],
    });
  }
  private get f (){
    return this.cursoForm.controls;
  }
  closeDialog(): void {
    this.dialogRef.close();
  }

  enviarFormulario(){
    if(this.editar){
      this.editarCurso();
    }
    else{
      this.crearCurso();
    }
  }

  private editarCurso(){
    
    let nombre = this.cursoForm.controls.curso.value;
    let descripcion = this.cursoForm.controls.descripcion.value;
    
    let body = {
      nombre : nombre,
      cursoID : this.data.curso.cur_id,
      descripcion: descripcion,
    }
    console.log(body)
    
    this.cursosServicio.editarCurso(body).subscribe(resp=>{
      if(resp['status']=='OK'){
        this.dialogRef.close({estado : 'EXITO'})
      }
      else{
        this.dialogRef.close({estado : 'FALLO'});
      }
    },(error)=>{
      console.log(error);
      this.dialogRef.close({estado : 'FALLO'});
    });
    
  }

  private crearCurso(){
    let nombre = this.cursoForm.controls.curso.value;
    let descripcion = this.cursoForm.controls.descripcion.value;
    let body = {
      nombre : nombre,
      descripcion:descripcion,
      programaID : this.data.programaID,
    }
    console.log(body);
    this.cursosServicio.guardarCurso(body).subscribe(resp=>{
      console.log(resp);
      if(resp['status']=='OK'){
        this.dialogRef.close({estado : 'EXITO'})
      }
      else{
        this.dialogRef.close({estado : 'FALLO'});
      }
    },(error)=>{
      console.log(error);
      this.dialogRef.close({estado : 'FALLO'});
    });
    
  }
  
}
