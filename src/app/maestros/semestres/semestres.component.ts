import { Component, OnInit, ElementRef,AfterViewInit, ViewChild, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormControl, FormControlName, FormGroup, Validators } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { DatePipe } from '@angular/common';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { Utils } from 'src/app/shared/utils';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { AlertaService } from 'src/app/shared/services/general/alerta.service';
import { SemestreService } from 'src/app/shared/services/backend/semestre.service';
import { SemestreFormComponent } from '../dialogs/semestre-form/semestre-form.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { LoadingService } from 'src/app/shared/services/loading.service';

@Component({
  selector: 'app-semestres',
  templateUrl: './semestres.component.html',
  styleUrls: ['./semestres.component.sass'],
  providers: [DatePipe]
})
export class SemestresComponent implements OnInit , OnDestroy {


  semestres_totales : any[] = [];
  semestres_filtrados : any[] = [];
  arreglo;
  dataSource_semestres_totales : MatTableDataSource<any>;

  //atributos para la tabla material
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;

  displayedColumns = [
    'codigo',
    'semestre',
    'acciones',
  ];

  semestres_procesados : any [] = [];

  filtro_control : FormControl;
  filtro$ : Subscription;


  constructor(
              public dialog: MatDialog,
              private ruteador: Router,
              private semestreServicio : SemestreService,
              private alertaServicio:AlertaService,
              private spinner : NgxSpinnerService,
              private loading:LoadingService) { }

  ngOnDestroy(): void {
    if(this.filtro$){
      console.log("unsubscribe");
      this.filtro$.unsubscribe();
    }
  }

  ngOnInit(): void {
  
    this.dataSource_semestres_totales = new MatTableDataSource();
    this.filtro_control = new FormControl(false);
    this.traerSemestres();
    this.pendienteCambios();
  }

  private pendienteCambios(){
    this.filtro$ = this.filtro_control.valueChanges.subscribe(resp=>{
      if(resp){
        this.quitarFiltro();
      }
      else{
        this.filtrarInformacion();
      }
    });
  }

  private quitarFiltro(){
    this.semestres_filtrados = this.semestres_totales;
    this.dataSource_semestres_totales.data = this.semestres_filtrados;
  }

  private filtrarInformacion(){
    this.semestres_filtrados = [];
    this.semestres_totales.forEach(elemento=>{
      if(elemento.sem_activo == 'Y'){
        this.semestres_filtrados.push(elemento)
      }
    });
    this.dataSource_semestres_totales.data = this.semestres_filtrados;
  }

  private traerSemestres(){
    this.spinner.show();
    this.loading.loading$.next({opacity:0.5})
    this.semestreServicio.listaSemestres().subscribe((resp:any) =>{

      this.semestres_totales = resp; 
      this.filtrarInformacion();
      
      console.log("semestres ",resp);
      this.spinner.hide();
    },(error)=>{
      //this.eventosServicio.hideLoading();
      this.spinner.hide();
      console.info("error semestres: ",error);
    });
  }

  ngAfterViewInit(): void {
    this.paginator._intl.itemsPerPageLabel = "Registros por página";
    this.dataSource_semestres_totales.paginator = this.paginator;
    this.dataSource_semestres_totales.sort = this.sort;
  }

  applyFilter(filterValue:string){
    this.dataSource_semestres_totales.filter = filterValue.trim().toLowerCase();
  }

  nuevoSemestre(){
    
    const dialogRef = this.dialog.open(SemestreFormComponent, {
      data: {
        nombre: "crear",
        programa :null,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
        if( result==undefined){
          console.log("No se recibe nada");
        }
        else if(result.estado=='EXITO'){
          this.alertaServicio.creacionCorrecta();
          this.traerSemestres();
        }
        else{
          console.log("ha ocurrido un error");
        }
    });
    
  }

  editarSemestre(row){
    console.log(row);
    
    const dialogRef = this.dialog.open(SemestreFormComponent, {
      data: {
        nombre: "editar",
        programa :row,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
        if( result==undefined){
          console.log("No se recibe nada");
        }
        else if(result.estado=='EXITO'){
          this.alertaServicio.actualizacionCorrecta();
          this.traerSemestres();
        }
        else{
          console.log("ha ocurrido un error");
        }
    });
    
  }

  cambiarEstado(row){
    
    let nuevo_est = '';
    let codigo = row.sem_id;
    if(row.sem_activo == 'N'){
      nuevo_est = 'Y';
    }
    else{
      nuevo_est = 'N';
    }
    let body = {
      estadoNuevo : nuevo_est,
      semestreID: codigo,
    }

    this.semestreServicio.cambiarEstado(body).subscribe(resp=>{
      if(resp['status']=='OK'){
        this.filtro_control.setValue(false);
        this.traerSemestres();
      }
      else{
        //error
      }
    },(error)=>{
      console.log(error);
    });
  }

  sortData(sort:Sort){
    const data = this.semestres_filtrados.slice();
    let arreglo_temporal :any []= [];
    if (!sort.active || sort.direction === '') {
      arreglo_temporal = data;
      this.dataSource_semestres_totales.data = arreglo_temporal;
      return;
    }

    arreglo_temporal = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'codigo': return this.compare(a.sem_id||'', b.sem_id||'', isAsc);
        case 'semestre': return this.compare(a.sem_nombre||'', b.sem_nombre||'', isAsc);
        default: return 0;
      }
    });
    this.dataSource_semestres_totales.data = arreglo_temporal;
  }
  private  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
  actualizarLista(){
    this.traerSemestres()
  }
}
