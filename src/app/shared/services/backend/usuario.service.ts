import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

import * as Global from '../../globals';
import { Utils } from '../../utils';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(public http : HttpClient) { }

  listar(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Usuario/listar',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  listarVendors(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Usuario/listarVendedores',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  infoUsuario(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Usuario/info',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  crearUsuario(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Usuario/guardar',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  crearMeta(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Usuario/agregarMeta',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  editarUsuario(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Usuario/editar',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  
  rolesUsuario(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Usuario/listar',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  status(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Usuario/status',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  cambiarPassword(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Curso/editar',Utils.jsonToFormDataV2(body,false), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  listarMeses(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.apiTransvial+ 'Mes/listar',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  listarAnios(){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.apiTransvial+ 'Anio/listar', requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  listarValores(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.apiTransvial+ 'Anio/listar',(body), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

}
