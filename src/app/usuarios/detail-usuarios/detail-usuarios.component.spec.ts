import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailUsuariosComponent } from './detail-usuarios.component';

describe('DetailUsuariosComponent', () => {
  let component: DetailUsuariosComponent;
  let fixture: ComponentFixture<DetailUsuariosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailUsuariosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailUsuariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
