import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { EChartOption } from 'echarts';
import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexXAxis,
  ApexDataLabels,
  ApexTooltip,
  ApexYAxis,
  ApexPlotOptions,
  ApexStroke,
  ApexLegend,
  ApexFill,
} from 'ng-apexcharts';
import { ProgramaService } from 'src/app/shared/services/backend/programa.service';
import { ExcelService } from 'src/app/shared/services/general/excel.service';
export type areaChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  yaxis: ApexYAxis;
  stroke: ApexStroke;
  tooltip: ApexTooltip;
  dataLabels: ApexDataLabels;
  legend: ApexLegend;
  colors: string[];
};

export type barChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  dataLabels: ApexDataLabels;
  plotOptions: ApexPlotOptions;
  yaxis: ApexYAxis;
  xaxis: ApexXAxis;
  fill: ApexFill;
  colors: string[];
};

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  providers:[DatePipe]
})
export class MainComponent implements OnInit {
  public areaChartOptions: Partial<areaChartOptions>;

  public vendedor_mes : EChartOption;
  public embajador_mes : EChartOption;

  formSearch : FormGroup;
  formAnio : FormGroup;
  
  date_hoy = new Date();

  estado:string;
  porcentaje :number;

  constructor(private programaService:ProgramaService,
    private excel :ExcelService,
    private datePipe: DatePipe,
    private dateAdapter: DateAdapter<Date>,
    private fb:FormBuilder) {
      this.dateAdapter.setLocale('es-PE');
    }

  public chartGauge1: any;
  line_bar_chart: EChartOption
  // public chartBar1 : EChartOption;
  // public chartBar2 : EChartOption;
  // public chartBar3 : EChartOption;
  

  public info_cards : any [] = [];
  public info_chart_mes : any [] = [];
  public info_chart_anio : any [] = [];
  public infoCard : any;
  anios_lista:any[]=[]

  paleta_colores = [    
    '#D35400',
    '#A6ACAF',
    '#3498DB',
    '#16A085',
    '#F4D03F',
    '#7B241C',
    '#884EA0',
    '#76D7C4',
    '#E5E8E8',
  ];
  color_actual = 0;
  //char de meses embajador
  emp_embajador_mes : any[] = [];
  empleados_mes: any[] = [];
  ngOnInit() {    
    this.traerInfoCharts();   
    this.createForm();
    this.createForm2();
    this.traerAnios();
    this.f.desde.setValue( new Date(this.date_hoy.getFullYear(), this.date_hoy.getMonth(), 1));
    this.f.hasta.setValue(new Date());
  }
  createForm(){
    this.formSearch = this.fb.group({
      desde:['',[]],
      hasta:['',[]],     
    })
  }
  createForm2(){
    this.formAnio = this.fb.group({      
      anios:['',[]],
    })
  }

  private get f (){
    return this.formSearch.controls;
  }
  traerAnios(){    
    this.programaService.listarAnios().subscribe((resp:any[])=>{
      console.log(resp);
      this.anios_lista = resp;
      for (let index = 0; index < this.anios_lista.length; index++) {
        if(this.anios_lista[index].anio_nombre == (new Date().getFullYear()).toString()){
          this.formAnio.controls.anios.setValue(this.anios_lista[index]);
          console.log(this.formAnio.controls.anios.value)
          break;
        }  else{
          console.log('else')
        }
      }
      },(error)=>{
        console.log(error)
      })
  }
  selectAnio(event){
    this.formAnio.controls.anios.setValue(event);
  }
  realizarBusqueda(){
    let vendedorID = JSON.parse(localStorage.getItem('usu_actual')).usu_id;
    let salida = {
      desde: this.datePipe.transform(this.f.desde.value,'yyyy-MM-dd'),
      hasta: this.datePipe.transform(this.f.hasta.value,'yyyy-MM-dd'),
      // vendedorID: Number(vendedorID),
    }
    console.log(salida)
    this.programaService.realizarBusqueda2(JSON.stringify(salida)).subscribe((resp:any)=>{
      console.log(resp)
      this.info_chart_mes = resp || []; 
      console.log(this.info_chart_mes);
      this.empleados_mes = resp['serie_mes_v']||[];
      this.emp_embajador_mes = resp['serie_mes_e']||[];
      this.actualizarGraficoVendedor();
      this.actualizarGraficoEmbajador();
    })
    // let vendedorID = JSON.parse(localStorage.getItem('usu_actual')).usu_id;
    // let salida = {
    //   desde: this.datePipe.transform(this.f.desde.value,'yyyy-MM-dd'),
    //   hasta: this.datePipe.transform(this.f.hasta.value,'yyyy-MM-dd'),
    //   vendedorID: Number(vendedorID),
    // }
    // console.log(salida)
    // this.programaService.realizarBusqueda(JSON.stringify(salida)).subscribe((resp:any)=>{
    //   console.log(resp)
    //   this.info_chart_mes = resp || []; 
    //   this.empleados_mes = resp['serie_mes_v'] || [];
    //   this.emp_embajador_mes = resp['serie_mes_e'] || []
    //   console.log(this.info_chart_mes);  
    //   this.chart1();
    //   this.actualizarGraficoEmbajador();
    //   this.actualizarGraficoVendedor();
    // })
  }
  busquedaAnio(){
    let vendedorID = JSON.parse(localStorage.getItem('usu_actual')).usu_id;
    let anio = this.formAnio.controls.anios.value
    let salida = {
      anioID: anio.anio_id,
      vendedorID: Number(vendedorID),
    }
    console.log(salida)
    this.programaService.realizarBusquedaAnio(JSON.stringify(salida)).subscribe((resp:any)=>{
      console.log(resp)
      this.info_chart_anio = resp || []; 
      console.log(this.info_chart_anio);
      this.chart5();
      
    })
  }

  EndDateChange(evento){
    console.log(evento);
  }
  public traerInfoCharts(){    
    let vendedor = JSON.parse(localStorage.getItem('usu_actual'));
    let desde = new Date(this.date_hoy.getFullYear(), this.date_hoy.getMonth(), 1)
    let salida = {
      vendedorID: Number(vendedor.usu_id),
      desde: this.datePipe.transform(desde,'yyyy-MM-dd'),
      hasta: this.datePipe.transform(this.date_hoy,'yyyy-MM-dd'),
      tipo: vendedor.usu_tipo
    }
    console.log(salida);
    let body=JSON.stringify(salida);
    this.programaService.listarInfoCards2(body).subscribe((resp:any[]) =>{
      console.log(resp)
      this.info_cards = resp['info'] || [];
      this.info_chart_mes = resp['serie_mes'] || [];
      this.info_chart_anio = resp['serie_anio'] || [];
      this.empleados_mes = resp['serie_mes_v'] || [];
      this.emp_embajador_mes = resp['serie_mes_e'] || []
      this.infoCard= this.info_cards;
      this.porcentaje = Math.round((this.infoCard.venta_mes /this.infoCard.meta_mes )*100)||0 ;
      // console.log(this.porcentaje)
      if(this.porcentaje > 66){
        this.estado='success'
      }else if(this.porcentaje > 33){
        this.estado='warning'
      }else{
        this.estado='danger'
      }
      
      // this.chart2();
      this.chart3();
      this.chart1();
      this.chart5();
      this.actualizarGraficoEmbajador();
      this.actualizarGraficoVendedor();
      // this.chart4();
    },(error)=>{
      console.log(error);
    });
      

  }
  // private chart5(){
  //   let data_data: any [] = [];
  //   let data_mes: any [] = [];
  //   let data_promedio: any [] = [];
  //   this.info_chart_anio.forEach(elemento=>{
  //     let meses = elemento.MES
  //     let proVenta = elemento.meta
  //     let data = elemento.total_ventas;
  //     data_data.push(data);
  //     data_mes.push(meses);
  //     data_promedio.push(proVenta)
  //   });
  //   this.line_bar_chart = {
  //     grid: {
  //       containLabel: true,
  //       top: '6',
  //       right: '0',
  //       bottom: '17',
  //       left: '25'
  //     },
  //     xAxis: {
  //       data: data_mes,
  //       axisLine: {
  //         lineStyle: {
  //           color: '#eaeaea'
  //         }
  //       },
  //       axisLabel: {
  //         fontSize: 10,
  //         color: '#9aa0ac'
  //       }
  //     },
  //     tooltip: {
  //       show: true,
  //       showContent: true,
  //       alwaysShowContent: false,
  //       triggerOn: 'mousemove',
  //       trigger: 'axis'
  //     },
  //     yAxis: {
  //       splitLine: {
  //         lineStyle: {
  //           color: '#eaeaea'
  //         }
  //       },
  //       axisLine: {
  //         lineStyle: {
  //           color: '#eaeaea'
  //         }
  //       },
  //       axisLabel: {
  //         fontSize: 10,
  //         color: '#9aa0ac'
  //       }
  //     },
  //     series: [
  //       {
  //         name: 'Ventas',
  //         type: 'bar',
  //         data: data_data
  //       },
  //       {
  //         name: 'Metas',
  //         type: 'line',
  //         smooth: true,
  //         lineStyle: {
  //           width: 3,
  //           shadowColor: 'rgba(0,0,0,0.4)',
  //           shadowBlur: 10,
  //           shadowOffsetY: 10
  //         },
  //         data: data_promedio,
  //         symbolSize: 10
  //       },
        
  //     ],
  //     color: ['#9f78ff', '#3FA7DC', '#F6A025']
  //   };
  // }
  private chart1() {
    let data_data: any [] = [];
    let data_dia: any [] = [];
    let data_promedio: any [] = [];
    this.info_chart_mes.forEach(elemento=>{
      let dia = elemento.dia_l
      let promVenta = elemento.meta_dia
      let data = elemento.venta_dia;
      data_data.push(data);
      data_dia.push(dia);
      data_promedio.push(promVenta)
    });
    
    this.areaChartOptions = {
      series: [
        {
          name: 'Venta',
          data: data_data,
        },
        {
          name: 'Meta',
          data: data_promedio,
        },
      ],
      chart: {
        height: 350,
        type: 'area',
        toolbar: {
          show: false,
        },
        foreColor: '#9aa0ac',
      },
      colors: ['#9F8DF1', '#E79A3B'],
      dataLabels: {
        enabled: false,
      },
      stroke: {
        curve: 'smooth',
      },
      xaxis: {
        type: 'category',
        categories: data_dia
      },
      legend: {
        show: true,
        position: 'top',
        horizontalAlign: 'center',
        offsetX: 0,
        offsetY: 0,
      },

      tooltip: {
        x: {
          format: 'dd/MM/yy HH:mm',
        },
      },
    };

  }

  private chart3( ){
    let valor = this.porcentaje
    let nuevaMeta = {
      series: [
        {
          type: 'gauge',
          startAngle: 180,
          endAngle: 0,
          min: 0,
          max: 100,
          splitNumber: 8,
          axisLine: {
            lineStyle: {
              width: 15,
              color: [
                [0.333, '#FF6E76'],
                [0.666, '#FDDD60'],
                [1, '#7CFFB2']
              ]
            }
          },
          // pointer: {
          //   icon: 'path://M12.8,0.7l12,40.1H0.7L12.8,0.7z',
          //   length: '15%',
          //   width: 20,
          //   offsetCenter: [0, '-75%'],
          //   itemStyle: {
          //     color: 'auto'
          //   }
          // },
          axisTick: {
            length: 0,
            lineStyle: {
              color: 'auto',
              width: 0
            }
          },
          splitLine: {
            length: 0,
            lineStyle: {
              color: 'auto',
              width: 0
            }
          },
          axisLabel: {
            color: '#464646',
            fontSize: 20,
            distance: -60,
            formatter: function (value) {
              if (value === 100) {
                return '';
              } else if (value == 50) {
                return '';
              } else if (value === 0) {
                return '';
              } 
              return '';
            }
          },
          title: {
            offsetCenter: [0, '35%'],
            fontSize: 10
          },
          detail: {
            fontSize: 18,
            offsetCenter: [0, '65%'],
            valueAnimation: true,
            formatter: function (value) {
              return Math.round(value)  + '%';
            },
            color: 'auto'
          },
          data: [
            {
              value: valor,
              name: 'Meta'
            }
          ]
        }
      ]
    };   

    this.chartGauge1 = nuevaMeta;
  }



  private chart5(){
    console.log(this.info_chart_anio)
    let data_data: any [] = [];
    let data_mes: any [] = [];
    let data_promedio: any [] = [];
    this.info_chart_anio.forEach(elemento=>{
      let meses = elemento.MES
      let proVenta = elemento.meta
      let data = elemento.total_ventas;
      data_data.push(data);
      data_mes.push(meses);
      data_promedio.push(proVenta)
    });
    this.line_bar_chart = {
      grid: {
        containLabel: true,
        top: '5%',
        right: '0',
        bottom: '17',
        left: '25',
      },
      legend:{
        show: true,
        position: 'bottom',
        horizontalAlign: 'center',
        offsetX: 0,
        offsetY: 0,
      },
      xAxis: {
        data: data_mes,
        axisLine: {
          lineStyle: {
            color: '#eaeaea'
          }
        },
        axisLabel: {
          fontSize: 10,
          color: '#9aa0ac'
        }
      },
      tooltip: {
        show: true,
        showContent: true,
        alwaysShowContent: false,
        triggerOn: 'mousemove',
        trigger: 'axis'
      },
      yAxis: {
        splitLine: {
          lineStyle: {
            color: '#eaeaea'
          }
        },
        axisLine: {
          lineStyle: {
            color: '#eaeaea'
          }
        },
        axisLabel: {
          fontSize: 10,
          color: '#9aa0ac'
        }
      },
      series: [
        {
          name: 'Ventas',
          type: 'bar',
          data: data_data
        },
        {
          name: 'Metas',
          type: 'line',
          smooth: true,
          lineStyle: {
            width: 3,
            shadowColor: 'rgba(0,0,0,0.4)',
            shadowBlur: 10,
            shadowOffsetY: 10
          },
          data: data_promedio,
          symbolSize: 10
        },
      ],
      color: ['#9f78ff', '#3FA7DC', '#F6A025']
    };
  }

  private actualizarGraficoEmbajador(){
    let data_x: any [] = [];
    let data_data: any [] = [];
    let data_promedio: any [] = [];
    this.emp_embajador_mes.forEach(elemento=>{
      let data = {
        value : elemento.vendedor_dia,       
        itemStyle: {
          color : this.conseguirColor(),
        }
      };
      data_data.push(data);
      data_x.push(elemento.vendedor_l);
      data_promedio.push(elemento.meta_dia)
    });
    // this.altura = data_x.length * 20 + 200;
    let option = {
      // height: this.altura -100,
      title: {
        // text: 'World Population'
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow'
        }
      },
      legend: {},
      grid: {
        containLabel: true,
        left:'0',
      },
      xAxis: {
        type: 'value',
        boundaryGap: [0, 0.01]
      },
      yAxis: {
        type: 'category',
        data: data_x,
        // min: 1,
        // max: data_x.length,
      },
      series: [
        {
          name: 'Ventas',
          type: 'bar',
          data: data_data,
          color:'#4074BF',
        },
        {
          name: 'Meta',
          type: 'line',
          smooth: true,
          
          color:'#FC0000',
          lineStyle: {
            width: 3,
            shadowColor: 'rgba(0,0,0,0.4)',
            shadowBlur: 10,
            shadowOffsetY: 10
          },
          data: data_promedio,
          symbolSize: 10
        },
      ]
    };
    this.embajador_mes = option;
  }
  
  altura:any;
  private actualizarGraficoVendedor(){
    let data_x: any [] = [];
    let data_data: any [] = [];
    let data_promedio: any [] = [];
    this.empleados_mes.forEach(elemento=>{
      let data = {
        value : elemento.vendedor_dia,       
        itemStyle: {
          color : this.conseguirColor(),
        }
      };
      data_data.push(data);
      data_x.push(elemento.vendedor_l);
      data_promedio.push(elemento.meta_dia)
    });
    this.altura = data_x.length * 20 + 200;
    let option = {
      height: this.altura -100,
      title: {
        // text: 'World Population'
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow'
        }
      },
      legend: {},
      grid: {
        containLabel: true,
        left:'10',
      },
      xAxis: {
        type: 'value',
        boundaryGap: [0, 0.01]
      },
      yAxis: {
        type: 'category',
        data: data_x,
        min: 1,
        max: data_x.length,
      },
      series: [
        {
          name: 'Ventas',
          type: 'bar',
          data: data_data,
          color:'#4074BF',
        },
        {
          name: 'Meta',
          type: 'line',
          smooth: true,
          color:'#FC0000',
          lineStyle: {
            width: 3,
            shadowColor: 'rgba(0,0,0,0.4)',
            shadowBlur: 10,
            shadowOffsetY: 10
          },
          data: data_promedio,
          symbolSize: 10
        },
      ]
    };
    this.vendedor_mes = option;
  }

  private conseguirColor(){
    if(this.color_actual > this.paleta_colores.length-1){
      this.color_actual = 0;
    }
    this.color_actual++;
    return this.paleta_colores[this.color_actual-1];
  }

  downloadExcel_anio(){
    let dataToExport = this.info_chart_anio;

    // let desde = this.datePipe.transform(this.formSearch.controls.desde.value,'dd-MM-yyyy');
    // let hasta = this.datePipe.transform(this.formSearch.controls.hasta.value,'dd-MM-yyyy');
    
    let mapHeaders=new Map([      
      ["Mes","MES"],
      ["Meta","meta"],
      ["Total de ventas","total_ventas"],
    ]);
    let mapHeadersShow=[
      'Mes',
      'Meta',
      'Total de ventas',
    ];

    let fecha = "Reporte Ventas Anuales "+ this.datePipe.transform(new Date(),'dd-MM-yyyy');

    let informacion = {      
      // desde : desde,
      // hasta: hasta,
    };

    let dataExcel ={
      title: fecha,
      data: dataToExport,
      mapHeaders:mapHeaders,
      mapHeadersShow:mapHeadersShow,
      filtro:"",
      informacion : informacion,
    }
    this.excel.crearReporteVentasAnuales(dataExcel)
  }

  downloadExcel_e_v(){
    let arreglo_salida: any[] = [];
    let arreglo_empleados : any []=[];
    this.empleados_mes.forEach(elemento =>{
      let obj ={
        meta_dia:elemento.meta_dia,
        usu_id:elemento.usu_id,
        vendedor_dia:elemento.vendedor_dia,
        vendedor_l:elemento.vendedor_l,
        vendedor_nombre:elemento.vendedor_nombre,
        tipo:'Vendedor'
      }
      arreglo_empleados.push(obj)
    })
    this.emp_embajador_mes.forEach(elemento =>{
      let obj ={
        meta_dia:elemento.meta_dia,
        usu_id:elemento.usu_id,
        vendedor_dia:elemento.vendedor_dia,
        vendedor_l:elemento.vendedor_l,
        vendedor_nombre:elemento.vendedor_nombre,
        tipo:'Embajador'
      }
      arreglo_empleados.push(obj)
    })
    arreglo_salida=arreglo_empleados
    console.log(arreglo_salida);
    this.downloadExcel(arreglo_salida)
  }

  downloadExcel(array){
    let dataToExport = array;

    let desde = this.datePipe.transform(this.formSearch.controls.desde.value,'dd-MM-yyyy');
    let hasta = this.datePipe.transform(this.formSearch.controls.hasta.value,'dd-MM-yyyy');
    
    let mapHeaders=new Map([      
      ["Vendedor","vendedor_l"],
      ["Meta","meta_dia"],
      ["Venta","vendedor_dia"],
      ["Tipo","tipo"],
    ]);
    let mapHeadersShow=[
      'Vendedor',
      'Meta',
      'Venta',
      'Tipo',
    ];

    let fecha = "Reporte Ventas "+ this.datePipe.transform(new Date(),'dd-MM-yyyy');

    let informacion = {
      
      desde : desde,
      hasta: hasta,
    };

    let dataExcel ={
      title: fecha,
      data: dataToExport,
      // data2: dataToExport2,
      mapHeaders:mapHeaders,
      mapHeadersShow:mapHeadersShow,
      filtro:"",
      informacion : informacion,
    }
    // console.log(dataExcel)
    this.excel.crearReporteVendedorEmbajador(dataExcel)
  }
}
