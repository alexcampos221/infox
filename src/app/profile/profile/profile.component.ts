import { Component, OnInit } from '@angular/core';
import { Observable, Subscriber } from 'rxjs';
import { finalize } from 'rxjs/operators'
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators, FormGroupDirective } from '@angular/forms';
import { AlmacenamientoLocalService } from 'src/app/shared/services/general/almacenamiento-local.service';
import { UsuarioService } from 'src/app/shared/services/backend/usuario.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.sass']
})
export class ProfileComponent implements OnInit {

  perfilForm : FormGroup;
  passwordForm : FormGroup;
  data_user : any;


  formPersonales : FormGroup;
  url_perfil = "";
  editar_personal: boolean = false;

  constructor(private fb:FormBuilder,
              private localServicio: AlmacenamientoLocalService,
              private usuarioServicio : UsuarioService) { }

  ngOnInit(): void {
    this.traerInfoUser();
    this.crearFormulario();
    this.crearFormularioPassword();
    this.crearFormularioPersonal();
  }

  private crearFormulario(){
    console.log(this.data_user);
    this.perfilForm = this.fb.group({
      nombre : [this.data_user.usu_nombre||'',[]],
      email : [this.data_user.usu_email||'',[]],
      paquete : [this.data_user.usu_paquete||'',[]],
      dni : [this.data_user.usu_dni,[]],
    });
  }

  private crearFormularioPersonal(){
    this.formPersonales = this.fb.group({
      telefono : [this.data_user.usu_telefono,[]],
      nombres : [this.data_user.usu_nombre,[]],
      correo : [this.data_user.usu_email,[]],
      dni : [this.data_user.usu_email,[]],
    });
  }


  private traerInfoUser(){
    this.data_user = this.localServicio.getItem('usu_actual');
  }

  private crearFormularioPassword(){
    this.passwordForm = this.fb.group({
      usuario : ['',[Validators.required]],
      actual : ['',[Validators.required]],
      nuevo : ['',[Validators.required]],
      confirmacion : ['',Validators.required],
    });
  }

  enviarFormulario(){
    console.log(this.passwordForm.value);
    let actual = this.passwordForm.controls.actual.value;
    let nuevo  = this.passwordForm.controls.nuevo.value;
    let confirmacion = this.passwordForm.controls.confirmacion.value;

    if(this.comparaCadenas(nuevo,confirmacion)){
      console.log("Error de confirmacion");
    }
    else{
      if(this.comparaCadenas(actual,nuevo)){
        console.log("La contraseña nueva no puede ser igual a la anterior");
      }
      else{
        console.log("Actualizar Passwords");
        this.actualizarPassword();
      }
    }
  }

  private comparaCadenas(cad1:string,cad2:string){
    let salida : boolean = false;
    if(cad1 == cad2){
      salida = true;
    }
    return salida;
  }

  private actualizarPassword(){
    let actual = this.passwordForm.controls.actual.value;
    let nuevo  = this.passwordForm.controls.nuevo.value;
    let confirmacion = this.passwordForm.controls.confirmacion.value;

    let body = {
      atual : actual,
      nuevo : nuevo,
      confirmacion : confirmacion,
    };

    this.usuarioServicio.cambiarPassword(body).subscribe(resp=>{
      if(resp['status']=='OK'){
        //reiniciar el formulario}
        this.passwordForm.controls.actual.reset();
        this.passwordForm.controls.nuevo.reset();
        this.passwordForm.controls.confirmacion.reset();      
        this.passwordForm.reset();
        this.crearFormularioPassword();
      }
      else{
        //error
      }
    },(error)=>{
      console.log(error);
      //error
    });
    
  }

  editarPersonal(){
    this.editar_personal = !this.editar_personal;
  }

  cambiarInfo(){
    
  }

}
