import { Page404Component } from './authentication/page404/page404.component';
import { AuthLayoutComponent } from './layout/app-layout/auth-layout/auth-layout.component';
import { MainLayoutComponent } from './layout/app-layout/main-layout/main-layout.component';
import { AuthGuard } from './shared/security/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Role } from './shared/security/role';
import { SigninComponent } from './authentication/signin/signin.component';

const routes: Routes = [

  {
    path : 'login',
    component : SigninComponent,
  },

  {
    path: '',
    component: MainLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: '/authentication/signin', pathMatch: 'full' },
      {
        path: 'admin',
        canActivate: [AuthGuard],
        data: {
          role: Role.Admin,
        },
        loadChildren: () =>
          import('./admin/admin.module').then((m) => m.AdminModule),
      },

      {
        path : 'maestros',
        loadChildren : () =>
            import('./maestros/maestros.module').then((m)=> m.MaestrosModule)
      },
      {
        path : 'reporte',
        loadChildren : () =>
            import('./reporte/reporte.module').then((m)=> m.ReporteModule)
      },
      {
        path : 'usuarios',
        loadChildren : () =>
            import('./usuarios/usuarios.module').then((m)=> m.UsuariosModule)
      },
      

      {
        path : 'pago_caja',
        loadChildren : () =>
            import('./caja/caja.module').then((m)=> m.CajaModule)
      },

      {
        path : 'pagos',
        loadChildren :() =>
            import('./pagos/pagos.module').then((m) => m.PagosModule)
      },
      {
        path : 'ventas',
        loadChildren :() =>
            import('./ventas/ventas.module').then((m) => m.VentasModule)
      },

      {
        path : 'prospectos',
        loadChildren :() =>
            import('./prospectos/prospectos.module').then((m) => m.ProspectosModule)
      },

      {
        path :'perfil',
        loadChildren : () =>
          import('./profile/profile.module').then((m)=>m.ProfileModule)
      },

      {
        path: 'venta',
        canActivate: [AuthGuard],
        data: {
          role: Role.Teacher,
        },
        loadChildren: () =>
          import('./teacher/teacher.module').then((m) => m.TeacherModule),
      },
      {
        path: 'caja',
        canActivate: [AuthGuard],
        data: {
          role: Role.Student,
        },
        loadChildren: () =>
          import('./student/student.module').then((m) => m.StudentModule),
      },

      // Extra components
      {
        path: 'extra-pages',
        loadChildren: () =>
          import('./extra-pages/extra-pages.module').then(
            (m) => m.ExtraPagesModule
          ),
      },
      {
        path: 'multilevel',
        loadChildren: () =>
          import('./multilevel/multilevel.module').then(
            (m) => m.MultilevelModule
          ),
      },
    ],
  },
  {
    path: 'authentication',
    component: AuthLayoutComponent,
    loadChildren: () =>
      import('./authentication/authentication.module').then(
        (m) => m.AuthenticationModule
      ),
  },

  {
    path : 'utils/registro',
    loadChildren: () => import('./matricula/matricula.module').then((m)=> m.MatriculaModule)
  },
  // {
  //   path : 'canvas/grafico',
  //   loadChildren: () => import('./canvas/canvas.module').then((m)=> m.CanvasModule)
  // },
  
  {
    path : 'utils/certificados',
    loadChildren: () => import('./certificados/certificados.module').then((m)=> m.CertificadosModule)
  },
  {
    path : 'utils/excel',
    loadChildren: () => import('./excel/excel.module').then((m)=> m.ExcelModule)
  },

  {
    path : 'utils/cuotas',
    loadChildren: () => import('./cuotas/cuotas.module').then((m)=> m.CuotasModule)
  },
  { path: '**', component: Page404Component },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
