import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Subscription } from 'rxjs';
import { AlertaService } from 'src/app/shared/services/general/alerta.service';
import { UtlitariosService } from 'src/app/shared/services/general/utlitarios.service';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { Utils } from 'src/app/shared/utils';

@Component({
  selector: 'app-saldos',
  templateUrl: './saldos.component.html',
  styleUrls: ['./saldos.component.sass'],
  providers:[DatePipe]
})
export class SaldosComponent implements OnInit {

  codigo:any;
  usuario: any;
  ListaPagoPendientes_totales: any[] = [];
  promociones_filtrados: any[] = [];

  DSlistaPagosPendientes: MatTableDataSource<any>;

  //atributos para la tabla material
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  //@ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;

  
  displayedColumns = [
    'infoPago',
    'fecha',  
    //'banco', 
    'monto',     
    //'url',
    'nombre',
    'dni',
    'telefono',
    'programa',
    'curso',
    'tipo',
    'estado',
  ];

  promociones_procesados: any[] = [];

  filtro_control: FormControl;
  filtro$: Subscription;

  formdata : FormGroup;
  maximo:Date = new Date();

  isEstado:boolean;
  constructor(private fb : FormBuilder,
    public dialog: MatDialog,
    private alertaServicio : AlertaService,
    private ruteador : Router,
    private utilitarios: UtlitariosService,
    private rutaActiva: ActivatedRoute,
    private loadingService: LoadingService,
    private spinner: NgxSpinnerService,
    private datePipe : DatePipe,
    private dateAdapter:DateAdapter<Date>,
    ) {
      this.dateAdapter.setLocale('es-PE'); 
     }

  private crearFormulario(){
    this.formdata = this.fb.group({
      id:['',[Validators.required]],
      fecha:['',[Validators.required]],
      nombre:['',[Validators.required],],
      banco:['',[Validators.required],],
      url:['',[Validators.required]],
      monto:['',[Validators.required]],
      dni: ['',[Validators.required]],
      telefono: ['',[Validators.required]], 
      estado: ['',[Validators.required]], 
      desde: [Utils.primerdiaMes(),[Validators.required]],
      hasta: [new Date(),[Validators.required]],

    });
  }
  txtNuevo:string = '';
  txtFiltro:String ;
  
  ngOnInit(): void {
    this.obtenerAsesor();
    this.crearFormulario();
    this.DSlistaPagosPendientes = new MatTableDataSource();
    
    this.traerListaSaldos();
    if(JSON.parse(localStorage.getItem('cerrar_detalle')) == true){
     
      this.txtFiltro = JSON.stringify(localStorage.getItem("txtFiltro")); 
      var reg= /"/gi
      this.txtNuevo = this.txtFiltro.replace(reg,'');
      this.applyFilter(this.txtNuevo);  
    }
    
  }
  private obtenerAsesor(){
    this.usuario = JSON.parse(localStorage.getItem('usu_actual'));    
    this.codigo=this.usuario.usu_id;
    // let salida = {
    //   code : this.codigo
    // };
    // let body = JSON.stringify(salida);
    
    // this.utilitarios.getVendedorAct(body).subscribe((resp:any)=>{
    //   console.log(resp);
    // });
  }
  traerListaSaldos(){    
    let salida = {
      usuarioID : this.codigo,
      desde: this.datePipe.transform(this.formdata.controls.desde.value,'yyyy-MM-dd'),
      hasta: this.datePipe.transform(this.formdata.controls.hasta.value,'yyyy-MM-dd')
    };
    let body = JSON.stringify(salida);
    console.log(salida);
    this.spinner.show();
    this.loadingService.loading$.next({message:'', opacity:0.5});
    this.utilitarios.getListaSaldosByVendedor(body).subscribe((resp:any)=>{
      console.log(resp);
      this.ListaPagoPendientes_totales = resp;
      this.formdata.controls.estado.setValue(resp.validado_estado);
      if(this.formdata.controls.estado.value=="Pendiente"){
        this.isEstado=true;
      }
     this.DSlistaPagosPendientes.data = this.ListaPagoPendientes_totales;    
     
     this.spinner.hide();
    },(error)=>{
      this.spinner.hide();
    });
  }
  applyFilter(filterValue: string){
    this.DSlistaPagosPendientes.filter = filterValue.trim().toLowerCase() || '';
  }
  ngAfterViewInit(): void {
    this.paginator._intl.itemsPerPageLabel = "Registros por página";
    this.DSlistaPagosPendientes.paginator = this.paginator;
    //this.DSlistaPagosPendientes.sort = this.sort;
    this.DSlistaPagosPendientes.sort = this.sort;
  }
  nuevoPago(){}
  ruta_imagen:any
  verImageVoucher(row){
    this.ruta_imagen = row.pag_url
  //   this.spinner.show();
  //   this.loadingService.loading$.next({message:'', opacity:0.5});
  // try{  
    // this.alertaServicio.MostrarImagenVoucher(row.pag_url);

  //   this.spinner.hide();
  //   }catch(e){
  //     this.spinner.hide();
  //   }
  }
  
  arrayPagosID: any[] = [];
  detallePendiente(valor,index){
    if(this.DSlistaPagosPendientes.filter == ''){
      for(let index = 0; index < this.ListaPagoPendientes_totales.length; index++) {
        this.arrayPagosID.push(this.ListaPagoPendientes_totales[index].pag_id);      
      }  
    }else{
      for(let index = 0; index < this.DSlistaPagosPendientes.filteredData.length; index++) {
        this.arrayPagosID.push(this.DSlistaPagosPendientes.filteredData[index].pag_id);      
      }     
    }
    
    let salida={
      pagoID:valor.pag_id,
      ventana: 'saldos',
    }
    localStorage.setItem('pago',valor.pag_id);  
    
    localStorage.setItem('txtFiltro',this.DSlistaPagosPendientes.filter);

    localStorage.setItem('arrayListaPendiente',JSON.stringify(this.arrayPagosID));
    this.ruteador.navigate(['ventas/detalle'] ,{ state : {data: salida } });      
  }

arreglo_temporal:any[];

sortData(sort: Sort){
  const data = this.ListaPagoPendientes_totales.slice();
  console.log(data);
  this.arreglo_temporal = [];
  if (!sort.active || sort.direction === '') {
    this.arreglo_temporal = data;
    this.DSlistaPagosPendientes.data = this.arreglo_temporal;
    return;
  }

  this.arreglo_temporal = data.sort((a, b) => {
    console.log(this.ListaPagoPendientes_totales)
    const isAsc = sort.direction === 'asc';
    switch (sort.active) {
      //case 'infoPago': return this.compare(a.par_ticket_no||'', b.par_ticket_no||'', isAsc);
      case 'nombre': return this.compare(a.pag_nombre||'', b.pag_nombre||'', isAsc);
      case 'dni': return this.compare(a.pag_dni||'', b.pag_dni||'', isAsc);
      case 'telefono': return this.compare(a.pag_telefono||'', b.pag_telefono||'', isAsc);
      case 'programa': return this.compare(a.pro_nombre||0, b.pro_nombre||0, isAsc);
      case 'curso': return this.compare(a.cur_nombre||0, b.cur_nombre||0, isAsc);
      case 'banco': return this.compare(a.ban_nombre||'', b.ban_nombre||'', isAsc);
      case 'fecha': return this.compare(a.pag_fecha_abono||'', b.pag_fecha_abono||'', isAsc);
      case 'monto': return this.compare(Number(a.pag_monto)||0, Number(b.pag_monto)||0, isAsc);

      //case 'url': return this.compare(a.par_nombre_cliente||'', b.par_nombre_cliente||'', isAsc);

      default: return 0;
    }
  });

  this.DSlistaPagosPendientes.data = this.arreglo_temporal;
}


private  compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
actualizarLista(){
  this.traerListaSaldos();
}
filtrarByFechas(){
  let salida = {
    usuarioID : this.codigo,
    desde: this.datePipe.transform(this.formdata.controls.desde.value,'yyyy-MM-dd'),
    hasta: this.datePipe.transform(this.formdata.controls.hasta.value,'yyyy-MM-dd')
  };
  this.spinner.show();
  this.loadingService.loading$.next({message:'', opacity:0.5});
  console.log(salida)
  if(this.formdata.controls.desde.value > this.formdata.controls.hasta.value){
    this.alertaServicio.FechaEsMayor();
    this.spinner.hide();
  }else{
    this.utilitarios.getListaSaldosByVendedor(JSON.stringify(salida)).subscribe((resp:any)=>{
      console.log(resp);
      this.ListaPagoPendientes_totales = resp;
      this.formdata.controls.estado.setValue(resp.validado_estado);
      if(this.formdata.controls.estado.value=="Pendiente"){
        this.isEstado=true;
      }
     this.DSlistaPagosPendientes.data = this.ListaPagoPendientes_totales;    
     this.spinner.hide();
    },(error)=>{
      console.log(error)
      this.spinner.hide();
    });
  }
  
}
EndDateChange(event){
  console.log(event)
}
}
