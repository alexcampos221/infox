import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtlitariosService } from 'src/app/shared/services/general/utlitarios.service';
import { DateAdapter } from '@angular/material/core';
import { DatePipe } from '@angular/common';
import { UniversidadService } from 'src/app/shared/services/backend/universidad.service';
import { ProgramaService } from 'src/app/shared/services/backend/programa.service';
import { PromocionService } from 'src/app/shared/services/backend/promocion.service';
import { BancoService } from 'src/app/shared/services/backend/banco.service';
import { MatriculaService } from 'src/app/shared/services/registros/matricula.service';

@Component({
  selector: 'app-documento',
  templateUrl: './documento.component.html',
  styleUrls: ['./documento.component.sass']
})
export class DocumentoComponent implements OnInit {

  documentoForm : FormGroup;

  bancos_lista : any[] = [];

  constructor(private fb : FormBuilder,
              ) { }

  ngOnInit(): void {
    this.crearFormulario();
  }

  private crearFormulario(){
    this.documentoForm = this.fb.group({
      tipo_doc : ['',[Validators.required]],
      estado : ['',[Validators.required]],
      numero : ['',[Validators.required]],
      tercero : ['',[Validators.required]],
      descripcion : ['',[Validators.required]],
      fecha_orden : ['',[Validators.required]],
      total : ['',[Validators.required]],
      fecha_facturacion : ['',[Validators.required]],
      usuario : ['',[Validators.required]],
      moneda : ['',[Validators.required]],
      fecha_contable : ['',[Validators.required]],
      direccion_tercero : ['',[Validators.required]],
    });    
  }

  enviarFormulario(){
    console.log(this.documentoForm.value);
  }

  volver(){
    console.log("Volver");
  }

}
