import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

import * as Global from '../../globals';
import { Utils } from '../../utils';

@Injectable({
  providedIn: 'root'
})
export class SesionService {

  constructor(public http: HttpClient) { }

  loginBackEnd(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.goberna_utils_url+ 'getLogin',Utils.jsonToFormDataV2(body,false), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
}
