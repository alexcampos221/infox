import { Injectable } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import * as uuid from 'uuid';
import { map } from 'rxjs/operators';
import { AccionesService } from '../general/acciones.service';

@Injectable({
  providedIn: 'root'
})
export class FirebaseStorageService {

  constructor(
              private storage:AngularFireStorage,
              private servicioAcciones: AccionesService
  ) { }

  public uploadFileCloudStorage(path, nombreArchivo, datos){
    return this.storage.upload(path+'/'+nombreArchivo, datos);
  }
  public referenciaCloudStorage(nombreArchivo){
    return this.storage.ref(nombreArchivo);
  }
  public uploadFile(file,path){
    //this.eventsService.showLoading();
    return new Promise((resolve)=>{
      let hashName = uuid();
      let fileContainer = new FormData();
      fileContainer.delete("file");
      fileContainer.append("file", file[0], file[0].name);
      let referencia = this.referenciaCloudStorage(path+'/'+fileContainer.get('file')['name']);
      let request = this.uploadFileCloudStorage(path,fileContainer.get('file')['name'], fileContainer.get('file'));
      request.percentageChanges().subscribe((porcentaje)=>{
        //this.eventsService.porcentaje$.next(Math.round(porcentaje)+'%');
        this.servicioAcciones.porcentaje$.next(Math.round(porcentaje)+'%');
        console.info("PORCENTAJE ARCHIVOS: ",Math.round(porcentaje));
        //return Math.round(porcentaje);
      },(err)=>{
        //this.sweetalertsService.generalError("Error cargando archivo.",);
      },
      ()=>{
        referencia.getDownloadURL().subscribe((publicUrl)=>{
          //this.eventsService.hideLoading();
          resolve(publicUrl);
        })
      })
    })
  }

  public eliminarArchivoAudio(nombre){
    return this.storage.ref("temporales/"+nombre).delete();
  }

}
