import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PendienteDetalleFormComponent } from './pendiente-detalle-form.component';

describe('PendienteDetalleFormComponent', () => {
  let component: PendienteDetalleFormComponent;
  let fixture: ComponentFixture<PendienteDetalleFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PendienteDetalleFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PendienteDetalleFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
