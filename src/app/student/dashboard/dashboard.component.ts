import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { areaChartOptions } from 'src/app/admin/dashboard/main/main.component';
import { EChartOption } from 'echarts';
import { DashboardService } from '../../shared/services/backend/dashboard.service'
import { ExcelService } from 'src/app/shared/services/general/excel.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass'],
  providers: [DatePipe]
})
export class DashboardComponent implements OnInit {
  
  public areaChartOptions: Partial<areaChartOptions>;

  public vendedor_mes : EChartOption;
  public embajador_mes : EChartOption;

  formSearch : FormGroup;
  formAnio : FormGroup;
  
  date_hoy = new Date();

  estado:string;
  porcentaje :number;

  //char de meses empleados
  empleados_mes : any[] = [];
  
  paleta_colores = [    
    '#138D75',
    '#0035FF',
    '#F8FF00',
    '#00F5FF',
    '#9400FF',
    '#FF9200',
    '#656561',
    '#FF1700',
  ];
  //char de meses embajador
  emp_embajador_mes : any[] = [];

  constructor(private dashboardService:DashboardService,
    private datePipe: DatePipe,
    private dateAdapter: DateAdapter<Date>,
    private fb:FormBuilder,
    private excel :ExcelService,) {
      this.dateAdapter.setLocale('es-PE');
    }

  public chartGauge1: any;
  line_bar_chart: EChartOption
  

  public info_cards : any [] = [];
  public info_chart_mes : any [] = [];
  public info_chart_anio : any [] = [];
  public infoCard : any;
  anios_lista:any[]=[];
  color_actual = 0;
  

  ngOnInit() {    
    this.traerInfoCharts();   
    this.createForm();
    this.createForm2();
    this.traerAnios();
    this.f.desde.setValue( new Date(this.date_hoy.getFullYear(), this.date_hoy.getMonth(), 1));
    this.f.hasta.setValue(new Date());
    
  }
  createForm(){
    this.formSearch = this.fb.group({
      desde:['',[]],
      hasta:['',[]],     
    })
  }
  createForm2(){
    this.formAnio = this.fb.group({      
      anios:['',[]],
    })
  }

  private get f (){
    return this.formSearch.controls;
  }
  traerAnios(){    
    this.dashboardService.listarAnios().subscribe((resp:any[])=>{
      console.log(resp);
      this.anios_lista = resp;
      for (let index = 0; index < this.anios_lista.length; index++) {
        if(this.anios_lista[index].anio_nombre == (new Date().getFullYear()).toString()){
          this.formAnio.controls.anios.setValue(this.anios_lista[index]);
          console.log(this.formAnio.controls.anios.value)
          break;
        }  else{
          console.log('else')
        }
      }
      },(error)=>{
        console.log(error)
      })
  }
  selectAnio(event){
    this.formAnio.controls.anios.setValue(event);
  }
  // realizarBusqueda(){
  //   let vendedorID = JSON.parse(localStorage.getItem('usu_actual')).usu_id;
  //   let salida = {
  //     desde: this.datePipe.transform(this.f.desde.value,'yyyy-MM-dd'),
  //     hasta: this.datePipe.transform(this.f.hasta.value,'yyyy-MM-dd'),
  //     // vendedorID: Number(vendedorID),
  //   }
  //   console.log(salida)
  //   this.dashboardService.realizarBusqueda(JSON.stringify(salida)).subscribe((resp:any)=>{
  //     console.log(resp)
  //     this.info_chart_mes = resp || []; 
  //     console.log(this.info_chart_mes);
  //     this.empleados_mes = resp||[];
  //     this.actualizarGraficoVendedor();
  //     this.actualizarGraficoEmbajador();
  //   })
  // }
  realizarBusqueda2(){
    let vendedorID = JSON.parse(localStorage.getItem('usu_actual')).usu_id;
    let salida = {
      desde: this.datePipe.transform(this.f.desde.value,'yyyy-MM-dd'),
      hasta: this.datePipe.transform(this.f.hasta.value,'yyyy-MM-dd'),
      // vendedorID: Number(vendedorID),
    }
    console.log(salida)
    this.dashboardService.realizarBusqueda2(JSON.stringify(salida)).subscribe((resp:any)=>{
      console.log(resp)
      this.info_chart_mes = resp || []; 
      console.log(this.info_chart_mes);
      this.empleados_mes = resp['serie_mes_v']||[];
      this.emp_embajador_mes = resp['serie_mes_e']||[];
      this.actualizarGraficoVendedor();
      this.actualizarGraficoEmbajador();
    })
  }
  downloadExcel_e_v(){
    let arreglo_salida: any[] = [];
    let arreglo_empleados : any []=[];
    this.empleados_mes.forEach(elemento =>{
      let obj ={
        meta_dia:elemento.meta_dia,
        usu_id:elemento.usu_id,
        vendedor_dia:elemento.vendedor_dia,
        vendedor_l:elemento.vendedor_l,
        vendedor_nombre:elemento.vendedor_nombre,
        tipo:'Vendedor'
      }
      arreglo_empleados.push(obj)
    })
    this.emp_embajador_mes.forEach(elemento =>{
      let obj ={
        meta_dia:elemento.meta_dia,
        usu_id:elemento.usu_id,
        vendedor_dia:elemento.vendedor_dia,
        vendedor_l:elemento.vendedor_l,
        vendedor_nombre:elemento.vendedor_nombre,
        tipo:'Embajador'
      }
      arreglo_empleados.push(obj)
    })
    arreglo_salida=arreglo_empleados
    console.log(arreglo_salida);
    this.downloadExcel(arreglo_salida)
  }
  downloadExcel(array){
    let dataToExport = array;
    // let dataToExport2 = this.emp_embajador_mes;

    let desde = this.datePipe.transform(this.formSearch.controls.desde.value,'dd-MM-yyyy');
    let hasta = this.datePipe.transform(this.formSearch.controls.hasta.value,'dd-MM-yyyy');
    
    let mapHeaders=new Map([      
      ["Vendedor","vendedor_l"],
      ["Meta","meta_dia"],
      ["Venta","vendedor_dia"],
      ["Tipo","tipo"],
    ]);
    let mapHeadersShow=[
      'Vendedor',
      'Meta',
      'Venta',
      'Tipo',
    ];

    let fecha = "Reporte Ventas"+ this.datePipe.transform(new Date(),'dd-MM-yyyy');

    let informacion = {
      
      desde : desde,
      hasta: hasta,
    };

    let dataExcel ={
      title: fecha,
      data: dataToExport,
      // data2: dataToExport2,
      mapHeaders:mapHeaders,
      mapHeadersShow:mapHeadersShow,
      filtro:"",
      informacion : informacion,
    }
    // console.log(dataExcel)
    this.excel.crearReporteVendedorEmbajador(dataExcel)
  }
  downloadExcel_anio(){
    let dataToExport = this.info_chart_anio;

    // let desde = this.datePipe.transform(this.formSearch.controls.desde.value,'dd-MM-yyyy');
    // let hasta = this.datePipe.transform(this.formSearch.controls.hasta.value,'dd-MM-yyyy');
    
    let mapHeaders=new Map([      
      ["Mes","MES"],
      ["Meta","meta"],
      ["Total de ventas","total_ventas"],
    ]);
    let mapHeadersShow=[
      'Mes',
      'Meta',
      'Total de ventas',
    ];

    let fecha = "Reporte Ventas Anuales "+ this.datePipe.transform(new Date(),'dd-MM-yyyy');

    let informacion = {      
      // desde : desde,
      // hasta: hasta,
    };

    let dataExcel ={
      title: fecha,
      data: dataToExport,
      mapHeaders:mapHeaders,
      mapHeadersShow:mapHeadersShow,
      filtro:"",
      informacion : informacion,
    }
    this.excel.crearReporteVentasAnuales(dataExcel)
  }
  busquedaAnio(){
    let vendedorID = JSON.parse(localStorage.getItem('usu_actual')).usu_id;
    let anio = this.formAnio.controls.anios.value
    let salida = {
      anioID: anio.anio_id,
      vendedorID: Number(vendedorID),
    }
    console.log(salida)
    this.dashboardService.realizarBusquedaAnio(JSON.stringify(salida)).subscribe((resp:any)=>{
      console.log(resp)
      this.info_chart_anio = resp || []; 
      console.log(this.info_chart_anio);
      this.chart5();
    })
  }

  EndDateChange(evento){
    console.log(evento);
  }
  public traerInfoCharts(){    
    // let vendedorID = JSON.parse(localStorage.getItem('usu_actual')).usu_id;
    let desde = new Date(this.date_hoy.getFullYear(), this.date_hoy.getMonth(), 1)
    let salida = {
      // vendedorID: vendedorID,
      desde: this.datePipe.transform(desde,'yyyy-MM-dd'),
      hasta: this.datePipe.transform(this.date_hoy,'yyyy-MM-dd'),
    }
    console.log(salida);
    let body=JSON.stringify(salida);
    this.dashboardService.dashboardCajaInfo(body).subscribe((resp:any[]) =>{
      console.log(resp)
      this.info_cards = resp['info'] || [];
      this.info_chart_mes = resp['serie_mes'] || [];
      this.info_chart_anio = resp['serie_anio'] || [];
      this.empleados_mes = resp['serie_mes_v'] || [];
      this.emp_embajador_mes = resp['serie_mes_e'] || [];
      this.infoCard= this.info_cards;
      this.porcentaje = Math.round((this.infoCard.venta_mes /this.infoCard.meta_mes )*100)||0 ;
      // console.log(this.porcentaje)
      if(this.porcentaje > 66){
        this.estado='success'
      }else if(this.porcentaje > 33){
        this.estado='warning'
      }else{
        this.estado='danger'
      }
      this.chart5();
      this.actualizarGraficoVendedor();
      this.actualizarGraficoEmbajador();
    },(error)=>{
      console.log(error);
    });
      

  }
  private chart5(){
    let data_data: any [] = [];
    let data_mes: any [] = [];
    let data_promedio: any [] = [];
    this.info_chart_anio.forEach(elemento=>{
      let meses = elemento.MES
      let proVenta = elemento.meta
      let data = elemento.total_ventas;
      data_data.push(data);
      data_mes.push(meses);
      data_promedio.push(proVenta)
    });
    this.line_bar_chart = {
      grid: {
        containLabel: true,
        top: '5%',
        right: '0',
        bottom: '17',
        left: '25',
      },
      legend:{
        show: true,
        position: 'bottom',
        horizontalAlign: 'center',
        offsetX: 0,
        offsetY: 0,
      },
      xAxis: {
        data: data_mes,
        axisLine: {
          lineStyle: {
            color: '#eaeaea'
          }
        },
        axisLabel: {
          fontSize: 10,
          color: '#9aa0ac'
        }
      },
      tooltip: {
        show: true,
        showContent: true,
        alwaysShowContent: false,
        triggerOn: 'mousemove',
        trigger: 'axis'
      },
      yAxis: {
        splitLine: {
          lineStyle: {
            color: '#eaeaea'
          }
        },
        axisLine: {
          lineStyle: {
            color: '#eaeaea'
          }
        },
        axisLabel: {
          fontSize: 10,
          color: '#9aa0ac'
        }
      },
      series: [
        {
          name: 'Ventas',
          type: 'bar',
          data: data_data
        },
        {
          name: 'Metas',
          type: 'line',
          smooth: true,
          lineStyle: {
            width: 3,
            shadowColor: 'rgba(0,0,0,0.4)',
            shadowBlur: 10,
            shadowOffsetY: 10
          },
          data: data_promedio,
          symbolSize: 10
        },
      ],
      color: ['#9f78ff', '#3FA7DC', '#F6A025']
    };
  }
  altura:any;
  
  private actualizarGraficoVendedor(){
    // let data_y=['ven1','ven2','ven3','ven15','ven15','ven23','ven32','ven11','ven11','ven21','ven31','ven2','ven3','ven15','ven2','ven3','ven15','ven15','ven23','ven32','ven11']

    let data_x: any [] = [];
    let data_data: any [] = [];
    let data_promedio: any [] = [];
    this.empleados_mes.forEach(elemento=>{
      let data = {
        value : elemento.vendedor_dia,       
        itemStyle: {
          color : this.conseguirColor(),
        }
      };
      data_data.push(data);
      data_x.push(elemento.vendedor_l);
      data_promedio.push(elemento.meta_dia)
    });
    this.altura = data_x.length * 20 + 200;
    let option = {
      height: this.altura -100,
      title: {
        // text: 'World Population'
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow'
        }
      },
      legend: {},
      grid: {
        containLabel: true,
        left:'0',
      },
      xAxis: {
        type: 'value',
        boundaryGap: [0, 0.01]
      },
      yAxis: {
        type: 'category',
        // splitNumber: 1,
        data: data_x,
        min: 1,
        max: data_x.length,
      },
      series: [
        {
          name: 'Ventas',
          type: 'bar',
          data: data_data
        },
        {
          name: 'Meta',
          type: 'line',
          smooth: true,
          lineStyle: {
            width: 3,
            shadowColor: 'rgba(0,0,0,0.4)',
            shadowBlur: 10,
            shadowOffsetY: 10
          },
          data: data_promedio,
          symbolSize: 10
        },
      ]
    };
    this.vendedor_mes = option;
  }

  private actualizarGraficoEmbajador(){
    let data_x: any [] = [];
    let data_data: any [] = [];
    let data_promedio: any [] = [];
    this.emp_embajador_mes.forEach(elemento=>{
      let data = {
        value : elemento.vendedor_dia,       
        itemStyle: {
          color : this.conseguirColor(),
        }
      };
      data_data.push(data);
      data_x.push(elemento.vendedor_l);
      data_promedio.push(elemento.meta_dia)
    });
    // this.altura = data_x.length * 20 + 200;
    let option = {
      height: this.altura -100,
      title: {
        // text: 'World Population'
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow'
        }
      },
      legend: {},
      grid: {
        containLabel: true,
        left:'0',
      },
      xAxis: {
        type: 'value',
        boundaryGap: [0, 0.01]
      },
      yAxis: {
        type: 'category',
        data: data_x,
        // min: 1,
        // max: data_x.length,
      },
      series: [
        {
          name: 'Ventas',
          type: 'bar',
          data: data_data
        },
        {
          name: 'Meta',
          type: 'line',
          smooth: true,
          lineStyle: {
            width: 3,
            shadowColor: 'rgba(0,0,0,0.4)',
            shadowBlur: 10,
            shadowOffsetY: 10
          },
          data: data_promedio,
          symbolSize: 10
        },
      ]
    };
    this.embajador_mes = option;
  }

  private conseguirColor(){
    if(this.color_actual > this.paleta_colores.length-1){
      this.color_actual = 0;
    }
    this.color_actual++;
    return this.paleta_colores[this.color_actual-1];
  }

}
