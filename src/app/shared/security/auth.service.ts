import { Role } from './role';
import { User } from './user';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  isLogin = false;
  roleAs: string;

  users: User[] = [
    {
      id: 1,
      img: 'assets/images/user/admin.jpg',
      username: 'admin@school.org',
      password: 'admin@123',
      firstName: 'Sarah',
      lastName: 'Smith',
      role: Role.Admin,
    },
    {
      id: 2,
      img: 'assets/images/user/teacher.jpg',
      username: 'teacher@school.org',
      password: 'teacher@123',
      firstName: 'Ashton',
      lastName: 'Cox',
      role: Role.Teacher,
    },
    {
      id: 3,
      img: 'assets/images/user/student.jpg',
      username: 'student@school.org',
      password: 'student@123',
      firstName: 'Ashton',
      lastName: 'Cox',
      role: Role.Student,
    },
  ];

  constructor() {}

  login(uname: string, pwd: string) {
    const user = this.users.find(
      (x) => x.username === uname && x.password === pwd
    );

    if (user) {
      this.roleAs = user.role;
      localStorage.setItem('STATE', 'true');
      localStorage.setItem('ROLE', user.role);
      localStorage.setItem('USERIMG', user.img);
      localStorage.setItem('FULLNAME', user.firstName + ' ' + user.lastName);
      this.isLogin = true;
    } else {
      this.roleAs = '';
      this.isLogin = false;
      localStorage.setItem('STATE', 'false');
    }
    return of({ success: this.isLogin, role: this.roleAs });
  }

  logout() {
    this.isLogin = false;
    this.roleAs = '';
    localStorage.setItem('STATE', 'false');
    localStorage.setItem('ROLE', '');
    localStorage.setItem('FULLNAME', '');
    localStorage.setItem('USERIMG', '');
    return of({ success: this.isLogin, role: '' });
  }

  isLoggedIn() {
    const loggedIn = localStorage.getItem('STATE');
    if (loggedIn === 'true') {
      this.isLogin = true;
    } else {
      this.isLogin = false;
    }
    return this.isLogin;
  }

  getRole() {
    return JSON.parse(localStorage.getItem('usu_actual')).usu_tipo;
  }
  getUserFullName() {
    return localStorage.getItem('FULLNAME');
  }
  getUserImg() {
    return localStorage.getItem('USERIMG');
  }
}
