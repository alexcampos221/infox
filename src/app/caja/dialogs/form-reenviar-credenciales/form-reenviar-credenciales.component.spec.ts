import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormReenviarCredencialesComponent } from './form-reenviar-credenciales.component';

describe('FormReenviarCredencialesComponent', () => {
  let component: FormReenviarCredencialesComponent;
  let fixture: ComponentFixture<FormReenviarCredencialesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormReenviarCredencialesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormReenviarCredencialesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
