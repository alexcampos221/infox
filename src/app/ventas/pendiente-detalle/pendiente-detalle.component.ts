import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ProgramaService } from 'src/app/shared/services/backend/programa.service';
import { AlertaService } from 'src/app/shared/services/general/alerta.service';
import { ProcesarCuotaComponent } from '../dialogs/procesar-cuota/procesar-cuota.component';
import { NuevaCuota } from '../../models/nueva-cuota';
import { AgregarCuotaComponent } from '../dialogs/agregar-cuota/agregar-cuota.component'
import { EditarCuotaComponent } from '../dialogs/editar-cuota/editar-cuota.component'
import Swal from 'sweetalert2';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { DatePipe } from '@angular/common';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormEditCursoComponent } from 'src/app/caja/dialogs/form-edit-curso/form-edit-curso.component';

@Component({
  selector: 'app-pendiente-detalle',
  templateUrl: './pendiente-detalle.component.html',
  styleUrls: ['./pendiente-detalle.component.sass'],
  providers:[DatePipe]
})
export class PendienteDetalleComponent implements OnInit {

  //paginator
  pageEvent: PageEvent;
  datasource: null;
  pageIndex:number;
  pageSize:number = 1;
  length:number;

  ventana:any;
  
  moneda: any [] = ['s/.','$'];
  usuario:any;
  codigo:string;
  state$: Observable<object>;
  montoSaldo : any;
  montoTotal : any;
  nueva_cuota: NuevaCuota | null;
  pagoID:string;
  estado:string='Validando';
  isCuotaPagado:boolean = false;
  isGenCuota:boolean = false;
  //usuarioID:string;
  pagoPendienteForm: FormGroup
  tablaPagos: FormGroup

  isEliminarCuota : boolean = false;
  rowPago :any;
  programas_lista : any[]  = [];
  universidades_lista : any[] = [];
  cursos_lista : any[] =[];
  cuotas : any [] = [];

  isPagoCuota:boolean = false;
  isActivo:boolean = false;
  isMatriculado:boolean = false;
  gCuota:boolean;
  monedaID: any;

  arrayPagination : any [] = [];

  isEditMatricula:boolean = false;

  
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  
  constructor(private fb: FormBuilder,
    private programaServicio: ProgramaService,
    public activatedRoute: ActivatedRoute,
    private router:Router,
    private datePipe: DatePipe,  
    private alertaServicio:AlertaService,
    private  dialog: MatDialog,
    private loadingService: LoadingService,
    private spinner: NgxSpinnerService,
    
    ) {
      
      if(JSON.parse(localStorage.getItem('pago')) != null){
        this.pagoID = JSON.parse(localStorage.getItem('pago'));
        this.ventana = this.router.getCurrentNavigation().extras.state.data.ventana;
      }else{        
        this.pagoID = this.router.getCurrentNavigation().extras.state.data.pagoID;
        this.ventana = this.router.getCurrentNavigation().extras.state.data.ventana;
      }
      if(JSON.parse(localStorage.getItem('arrayListaPendiente')) != null){
        this.arrayPagination = JSON.parse(localStorage.getItem('arrayListaPendiente'));
        this.pageIndex = JSON.parse(localStorage.getItem('indice'));;
        this.length=this.arrayPagination.length;     
      }
      console.log(this.ventana)
    }

    

  ngOnInit(): void {    
    
    this.paginator._intl.getRangeLabel = (page: number, pageSize: number, length: number)=>{
      if (length == 0 || pageSize == 0) {
        return `0 de ${length}`;
      }
      const startIndex = page * pageSize;

      return `${startIndex+1} de ${length}`
    };  
    this.usuario = JSON.parse(localStorage.getItem('usu_actual'));      
    //console.log(JSON.parse(localStorage.getItem('pago')));
    this.codigo = this.usuario.usu_id;
    this.crearFormulario();
    this.traerInfoPago();
    this.traerCuotas();    
  }
  ngAfterViewInit():void{
    this.paginator._intl.getRangeLabel = (page: number, pageSize: number, length: number)=>{
      if (length == 0 || pageSize == 0) {
        return `0 de ${length}`;
      }
      const startIndex = page * pageSize;

      return `${startIndex+1} de ${length}`
    };    
  }
  cerrarDetalle(){
    
    localStorage.setItem('cerrar_detalle',JSON.stringify(true));
    console.log(this.pageIndex)
    if(this.ventana=='aprobados'){
      this.router.navigate(['/ventas/confirmados']);
    }else if(this.ventana == 'saldos'){
      this.router.navigate(['/ventas/saldos']);
    }
    else{
      this.router.navigate(['/ventas/ventas']);
    }
    
  }
  private crearFormulario() {
    this.pagoPendienteForm = this.fb.group({
      //id:['',[Validators.required]],
      fecha_abono:['',[Validators.required]],
      fecha_formato:['',[Validators.required]],
      nombre : ['',[Validators.required]],
      image_url : ['',[Validators.required]],  
      monto: ['',[Validators.required]],
      dni : ['',[Validators.required]],
      telefono: ['',[Validators.required]],
      email : ['',[Validators.required]],   
      monedaID: ['',[]],   
      
      activo: ['',[Validators.required]],

      matriculado :['',[Validators.required]], 
      universidad:['',[Validators.required]], 
      //otros:['',[Validators.required]], 

      programa:['',[Validators.required]], 
      curso:['',[Validators.required]], 

      cur_lista_combo:['',[Validators.required]], 
      tiempo_contratado:['',[Validators.required]],

      pas_nombre:['',[Validators.required]],
      pag_inicio_curso:['',[Validators.required]],
      banco:['',[Validators.required]],
      bancoID:['',[Validators.required]],
      codigo_promocional:['',[Validators.required]],
      cur_usuario:['',[Validators.required]],
      cur_clave:['',[Validators.required]],
      pag_num_operacion:['',[Validators.required]],
      validado_estado:['',[Validators.required]],
      pago_cuotas:['',[Validators.required]],
      
      fecha_pago:['',[Validators.required]],
      monto_pago:['',[Validators.required]],
      
      usuario:['',[Validators.required]],
      clave:['',[Validators.required]],

    });
  }
  private get f(){
    return this.pagoPendienteForm.controls;
  }
  universidad:string
  private traerInfoPago(){
    let salida={
      pagoID:this.pagoID
    };
    let body = JSON.stringify(salida);
    console.log(salida);
    this.programaServicio.traerInfoPrograma(body).subscribe((resp:any)=>{
      this.rowPago = resp;
      console.log(resp)
      console.log(this.rowPago);
      if(this.rowPago!=null){    
        this.montoSaldo = this.rowPago.pag_monto_total- this.rowPago.pag_monto_abonado || 0;
        this.montoTotal = this.rowPago.pag_monto_total || 0;
        this.f.bancoID.setValue(this.rowPago.ban_id);
        
        this.f.fecha_abono.setValue(this.rowPago.pag_fecha_abono_l);
        this.f.fecha_formato.setValue(this.rowPago.pag_fecha_abono);
        this.f.nombre.setValue(this.rowPago.pag_nombre);
        this.f.image_url.setValue(this.rowPago.pag_url);
        this.f.monto.setValue(this.rowPago.pag_monto);
        this.f.dni.setValue(this.rowPago.pag_dni);
        this.f.telefono.setValue(this.rowPago.pag_telefono);
        this.f.email.setValue(this.rowPago.pag_email);
        this.f.activo.setValue(this.rowPago.pag_activo);
        if(this.f.activo.value=='Y'){
          this.isActivo=true;
        }else{
          this.isActivo = false;
        }
        this.f.matriculado.setValue(this.rowPago.pag_matriculado);
        if(this.f.matriculado.value == 'Y'){
          this.isMatriculado=true;
          this.isEditMatricula = false;
        }else{
          this.isMatriculado = false;
          this.isEditMatricula = true
        }
        if(this.rowPago.uni_nombre == 'OTROS'){
          this.f.universidad.setValue(this.rowPago.uni_otros);
          this.universidad = this.rowPago.uni_otros
        }else{
          this.f.universidad.setValue(this.rowPago.uni_nombre);
          this.universidad = this.rowPago.uni_nombre
        }
        
        //this.f.otros.setValue(this.rowPago.uni_otros);
        this.f.programa.setValue(this.rowPago.pro_nombre);
        // if(this.rowPago.cur_nombre != null){
        //   this.f.curso.setValue(this.rowPago.cur_nombre);
        // }else{
        //   this.f.curso.setValue(this.rowPago.cur_lista_combo);
        // }
        
        this.f.cur_lista_combo.setValue(this.rowPago.cur_lista_combo);
        this.f.tiempo_contratado.setValue(this.rowPago.pag_duracion);
        this.f.pago_cuotas.setValue(this.rowPago.pag_escuotas);        
        if(this.f.pago_cuotas.value=='Y'){
          this.isPagoCuota = true;
        }else{
          this.isPagoCuota = false;
        }
        this.f.validado_estado.setValue(this.rowPago.validado_estado);
        // if(this.f.validado_estado.value=="Aprobado"){
        //   this.isEditMatricula = true
        // }
        // else if(this.f.validado_estado.value=="Pendiente"){
        //   this.isEditMatricula = false
        // }
        this.f.pas_nombre.setValue(this.rowPago.pas_nombre);
        this.f.pag_inicio_curso.setValue(this.rowPago.pag_inicio_curso);
        this.f.banco.setValue(this.rowPago.name);
        this.f.codigo_promocional.setValue(this.rowPago.pag_cod_promocion);
        this.f.cur_usuario.setValue(this.rowPago.cur_usuario);
        this.f.cur_clave.setValue(this.rowPago.cur_clave);
        this.f.pag_num_operacion.setValue(this.rowPago.pag_num_operacion);
        this.f.monedaID.setValue(this.monedaID || this.rowPago.pg_moneda);
        this.monedaID=this.rowPago.pg_moneda;
      }
     
    })
  }
  private traerCuotas(){  
    let salida={
      pagoID:this.pagoID
    }
    let body = JSON.stringify(salida);  
    localStorage.setItem('pago',this.pagoID);
    this.programaServicio.traerCuotasUsuario(body).subscribe((resp:any)=>{
      if(resp.length == 0){
        this.gCuota=false;
        console.log('no ahi cuotas')
      }else{
        this.cuotas = resp;
        console.log(this.cuotas);        
        this.gCuota=true;
      }      
    })
    //console.log(this.cuotas);
  }

 
  public getServerData(event?:PageEvent){
    this.pageIndex = event.pageIndex;
    localStorage.setItem('indice',JSON.stringify(event.pageIndex));
    this.pagoID = this.arrayPagination[event.pageIndex];    
    this.traerCuotas();
    this.traerInfoPago();
    return event;
  }


  seleccionPagoCuota(event){
    this.isPagoCuota=event.checked;
    //console.log(this.isPagoCuota);
  }
  ruta_imagen:any;
  verImageVoucher(cuota){
    console.log(cuota)
    this.spinner.show();
      this.loadingService.loading$.next({message:'', opacity:0.5});
    try{      
      // this.alertaServicio.MostrarImagenVoucher(this.f.image_url.value);
      if(this.gCuota == false){
        console.log('true')
        this.ruta_imagen = cuota.pag_url
      }else{
        console.log('false')
        this.ruta_imagen = cuota.cuota_vaucher
      }
      
      this.spinner.hide();
    }catch(e){
      this.spinner.hide();
    }
    
  }
  aprobarCuotas(i){
    let salida = {
      cuotaID : Number(this.cuotas[i].cuota_id),
      pagoID : Number(this.pagoID),
      usuarioID: Number(this.usuario.usu_id)
    }
    let body =  JSON.stringify(salida);
    console.log(salida)
    this.programaServicio.aprobarCuota(body).subscribe((resp)=>{
      console.log(resp);
      if(resp['status']='ok'){
        this.alertaServicio.pagoCuotaCorrecta();
        this.traerInfoPago();
        this.traerCuotas();
        // this.isGenCuota = true;
        this.isEditMatricula = true;
        this.isCuotaPagado = true;
        // console.log(resp)  
      }else{
        console.log('error');
      }
    })
  }
  aprobarPago(){
    let salida = {
      cuotaID : 0,
      pagoID : Number(this.pagoID),
      usuarioID: Number(this.usuario.usu_id)
    }
    let body =  JSON.stringify(salida);
    console.log(salida);
    this.programaServicio.aprobarCuota(body).subscribe((resp)=>{
      if(resp['status']='ok'){
        this.traerInfoPago();
        // this.isGenCuota = true;
        this.isCuotaPagado = true;
        this.isEditMatricula = true;
        console.log(resp)  
      }else{
        console.log('error');
      }
    })
  }
  crearCuota(){    
    const dialogRef = this.dialog.open(AgregarCuotaComponent, {     
      data:{
        disableClose: true,
        codigoVendedor: this.codigo,
        pagoID: this.pagoID,
        monedaID: this.f.monedaID.value,
        montoS:this.montoSaldo,
        montoT:this.montoTotal
      }
    });
    dialogRef.afterClosed().subscribe((result) => {
      console.log(result);
      if( result==undefined){
        console.log("No se recibe nada");
      }
      else if(result.estado=="EXITO"){
       
        this.alertaServicio.creacionCorrecta();
        this.traerCuotas();
        this.traerInfoPago();
      }
      else{
        console.log("ha ocurrido un error");
      }
  });
  }

  editCuota(index){
    //console.log(this.pagoID)
    const dialogRef = this.dialog.open(EditarCuotaComponent, {
      width: '600px',
      data: {
        nombre: "editar cuota", 
        pagoID: this.pagoID,
        estado: this.cuotas[index].cuota_estado || 'Validando',       
        url: this.cuotas[index].cuota_vaucher,
        bancoID: this.cuotas[index].ban_id,
        banco:this.cuotas[index].banco_nombre,
        numOperacion: this.cuotas[index].num_operacion,
        montoPago: this.cuotas[index].abono_monto,
        fechaPago: this.cuotas[index].fecha_abono,
        cuotaID: this.cuotas[index].cuota_id,
        usuarioID: this.codigo,
        fVencimiento: this.cuotas[index].fecha_vencimiento,
        mCuota: this.cuotas[index].cuota_monto,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if( result == undefined){
        console.log("No se recibe nada");
      }
      else if(result.estado=='EXITO'){
        this.gCuota=true;        
        this.alertaServicio.actualizacionCorrecta();
        this.traerCuotas();
        this.traerInfoPago();
      }
      else{
        console.log("ha ocurrido un error");
      }
    });         
  }

  eliminarCuota(i){
    let salida = {
      cuotaID : this.cuotas[i].cuota_id,
      pagoID : this.pagoID
    }
    let body =  JSON.stringify(salida);
    //console.log(salida);
    Swal.fire({
      title: 'Estas seguro de eliminar la cuota?',
      text: "¡No podrás revertir esto!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: '¡Sí, bórralo!'
    }).then(result => {
      if (result.value) {
        Swal.fire('¡Eliminado! ', 'La cuota ha sido eliminada.', 'success');
        this.programaServicio.eliminarCuota(body).subscribe((resp) =>{
          if(resp["status"] == "OK"){            
            this.traerCuotas();
            this.traerInfoPago();
          }else{
            this.alertaServicio.errorInterno();
            console.log(resp["status"]);
          }     
        }) 
      }
    });
  }

  
  editarCursoByVendedor(){
    console.log(this.f)
    const dialogRef = this.dialog.open(FormEditCursoComponent, {
      data: {
        pagoID:this.rowPago.pag_id,
        usuarioID:this.codigo,
        programa_nombre: this.rowPago.pro_nombre,
        curso_nombre: this.rowPago.cur_nombre || this.rowPago.cur_lista_combo,
        info : this.rowPago,
        tipo : 'vendedor'
      },
    });
    dialogRef.afterClosed().subscribe((result ) => {
      if( result == undefined){
        console.log("No se recibe nada");
      }
      else if(result.estado=='EXITO'){
        this.gCuota=true;        
        this.traerCuotas();
        this.traerInfoPago();
        this.alertaServicio.generarCuotaCorrecta();
      }
      else{
        console.log("ha ocurrido un error");
      }
    });
  }
  procesar(){
    console.log(this.f)
    const dialogRef = this.dialog.open(ProcesarCuotaComponent, {
      data: {
        nombre: "generar cuota",
        url: this.f.image_url.value,
        bancoID: this.f.bancoID.value,
        numOperacion: this.f.pag_num_operacion.value,
        mPagado: this.f.monto.value,
        fPagoL: this.f.fecha_formato.value,
        vendedorID: this.codigo,
        pagoID:this.pagoID,
        monedaID: this.monedaID       
      },
    });
    dialogRef.afterClosed().subscribe((result ) => {
      if( result == undefined){
        console.log("No se recibe nada");
      }
      else if(result.estado=='EXITO'){
        this.gCuota=true;
        
        this.traerCuotas();
        this.traerInfoPago();
        this.alertaServicio.generarCuotaCorrecta();
      }
      else{
        console.log("ha ocurrido un error");
      }
    });
  }
  enviarFormulario(){}
  volver(){}
}
