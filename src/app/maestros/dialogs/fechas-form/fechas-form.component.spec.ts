import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FechasFormComponent } from './fechas-form.component';

describe('FechasFormComponent', () => {
  let component: FechasFormComponent;
  let fixture: ComponentFixture<FechasFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FechasFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FechasFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
