import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-pendiente-detalle-form',
  templateUrl: './pendiente-detalle-form.component.html',
  styleUrls: ['./pendiente-detalle-form.component.sass']
})
export class PendienteDetalleFormComponent implements OnInit {

  detalleForm : FormGroup;
  rowDetalle: any [] = [];
  constructor(
            public dialogRef: MatDialogRef<PendienteDetalleFormComponent>,
            private fb : FormBuilder,
            @Inject(MAT_DIALOG_DATA) public data: any,
            ) {               
              this.rowDetalle.push(data.infoPago);
            }

  ngOnInit(): void {
    this.crearFormulario();
    if(this.rowDetalle != null){
      this.f.nombre.setValue(this.rowDetalle[0].pag_nombre);
      this.f.dni.setValue(this.rowDetalle[0].pag_dni);
      this.f.fecha_abono.setValue(this.rowDetalle[0].pag_fecha_abono);
      this.f.id.setValue(this.rowDetalle[0].pag_id);
      this.f.monto.setValue(this.rowDetalle[0].pag_monto);
      this.f.telefono.setValue(this.rowDetalle[0].pag_telefono);
      this.f.url.setValue(this.rowDetalle[0].pag_url);
    }
  }

  private crearFormulario(){
    this.detalleForm = this.fb.group(
      {
        nombre: ['', [Validators.required]],
        dni: ['', [Validators.required]],
        fecha_abono: ['', [Validators.required]],
        id: ['', []],
        monto: ['', [Validators.required]],
        telefono: ['', [Validators.required]],
        url:['', [Validators.required]],
      }
    );
  }
  private get f(){
    return this.detalleForm.controls;
  }

  enviarFormulario(){}

  closeDialog(): void {
    this.dialogRef.close();
  }

}
