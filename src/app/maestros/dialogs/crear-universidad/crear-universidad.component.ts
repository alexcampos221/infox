import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { UniversidadService } from 'src/app/shared/services/backend/universidad.service';

@Component({
  selector: 'app-crear-universidad',
  templateUrl: './crear-universidad.component.html',
  styleUrls: ['./crear-universidad.component.sass']
})
export class CrearUniversidadComponent implements OnInit {

  public addCusForm: FormGroup;
  public nombre : string = '';
  editar : boolean = false;

  lista_paises : any []
  constructor(private fb: FormBuilder, 
              public dialogRef: MatDialogRef<CrearUniversidadComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private universidadServicio:UniversidadService) { }

  ngOnInit(): void {
    this.listarPaises()
    this.nombre = 'Crear Universidad';
    //this.crearFormulario();
    if(this.data.nombre == 'editar'){
      console.log(this.data.universidad);
      this.editar = true;
      this.crearFormularioEditar();
      this.nombre = 'Editar Universidad';
    }
    else{
      this.crearFormulario();
    }
  }

  private crearFormulario(){    
    this.addCusForm = this.fb.group({
      nombre: ['',[Validators.required, Validators.pattern('[a-zA-Z]+([a-zA-Z ]+)*')]],
      descripcion:['', [Validators.required]],
      pais: ['', [Validators.required]]
    });
  }
  private crearFormularioEditar(){
    let codigo = this.data.universidad.uni_id;
    let nombre =  this.data.universidad.uni_nombre
    let descripcion =  this.data.universidad.uni_descripcion
    this.addCusForm = this.fb.group({
      codigo : [codigo,[Validators.required]],
      nombre : [nombre,[Validators.required, Validators.pattern('[a-zA-Z]+([a-zA-Z ]+)*')]],
      descripcion:[descripcion, [Validators.required]],
      pais: ['', [Validators.required]]
    });
  }
  listarPaises(){
    this.universidadServicio.listarPaises().subscribe((resp:any) => {
      console.log(resp)
      this.lista_paises = resp;
      if(this.data.nombre == 'editar'){
        this.lista_paises.forEach(ele => {
          if(ele.pai_id == this.data.universidad.pais_id){
            this.addCusForm.controls.pais.setValue(ele)
          }
        })
      }
    })
  }

  selectPais(evento){
    console.log(evento)
  }
  closeDialog(): void {
    this.dialogRef.close();
  }

  enviarFormulario(){
    console.log(this.addCusForm.value);
    if(this.editar){
      this.editarUniversidad()
    }
    else{
      this.crearUniversidad();
    }
  }

  private editarUniversidad(){
    let nombre = this.addCusForm.controls.nombre.value;
    let codigo = this.addCusForm.controls.codigo.value;
    let body = {
      nombre : nombre,
      universidadID : codigo,
      descripcion: this.addCusForm.controls.descripcion.value,
      pais_id: this.addCusForm.controls.pais.value.pai_id,
    }

    this.universidadServicio.editarUniversidad(body).subscribe(resp=>{
      if(resp['status']=='OK'){
        this.dialogRef.close({estado : 'EXITO'})
      }
      else{
        this.dialogRef.close({estado : 'FALLO'});
      }
    },(error)=>{
      console.log(error);
      this.dialogRef.close({estado : 'FALLO'});
    });
  }

  private crearUniversidad(){
    let nombre = this.addCusForm.controls.nombre.value;
    let body = {
      nombre : nombre,
      descripcion: this.addCusForm.controls.descripcion.value,
      pais_id: this.addCusForm.controls.pais.value.pai_id,
    }

    this.universidadServicio.guardarUniversidad(body).subscribe(resp=>{
      console.log(resp);
      if(resp['status']=='OK'){
        this.dialogRef.close({estado : 'EXITO'})
      }
      else{
        this.dialogRef.close({estado : 'FALLO'});
      }
    },(error)=>{
      console.log(error);
      this.dialogRef.close({estado : 'FALLO'});
    });
  }
  
}