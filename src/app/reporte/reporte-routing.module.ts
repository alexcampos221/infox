import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReporteGeneralComponent } from './reporte-general/reporte-general.component';

const routes: Routes = [
  {
    path : '',
    component : ReporteGeneralComponent,
  }, 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReporteRoutingModule { }
