import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { DateAdapter } from '@angular/material/core';
import { DatePipe } from '@angular/common';
import { UniversidadService } from 'src/app/shared/services/backend/universidad.service';
import { ProgramaService } from 'src/app/shared/services/backend/programa.service';
import { PromocionService } from 'src/app/shared/services/backend/promocion.service';
import { BancoService } from 'src/app/shared/services/backend/banco.service';

@Component({
  selector: 'app-pago-form',
  templateUrl: './pago-form.component.html',
  styleUrls: ['./pago-form.component.sass'],
  providers : [DatePipe],
})
export class PagoFormComponent implements OnInit {


  pagoForm: FormGroup;
  editar: boolean = false;
  nombre_modal: string;

  universidades_lista : any[] = [];
  bancos_lista : any[] = [];
  programas_lista : any[]  = [];
  semestres_lista : any[] = [];
  cursos_lista : any[] = [
    {
      cur_id :'100',
      cur_nombre : 'Curso prueba 01',
    },
    {
      cur_id :'101',
      cur_nombre : 'Curso prueba 02',
    },
    {
      cur_id :'102',
      cur_nombre : 'Curso prueba 03',
    }
  ];

  constructor(private programaServicio: ProgramaService,
              private promocionServicio: PromocionService,
              private fb: FormBuilder,
              public dialogRef: MatDialogRef<PagoFormComponent>,
              private dateAdapter : DateAdapter<Date>,
              private datePipe: DatePipe,
              private universidadServicio : UniversidadService,
              private bancoServicio : BancoService,
              @Inject(MAT_DIALOG_DATA) public data: any,) {
                this.dateAdapter.setLocale('es-PE');
              }

  ngOnInit(): void {
    this.crearFormulario();
    this.traerBancos();
    this.traerUniversidades();
    this.traerProgramas();
  }

  private crearFormulario() {
    this.pagoForm = this.fb.group({
      nombre : ['',[Validators.required]],
      dni : ['',[Validators.required]],
      correo : ['',[Validators.required]],
      universidad : ['',[Validators.required]],
      programa : ['',[Validators.required]],
      representante : ['',[Validators.required]],
      vendedor : ['',[Validators.required]],
      usuario : ['',[Validators.required]],
      telefono : ['',[Validators.required]],
      estado : ['',[Validators.required]],
      otros : ['',[Validators.required]],
      curso : ['',[Validators.required]],
      codigo_promocional : ['',[Validators.required]],
      clave : ['',[Validators.required]],
      banco : ['',[Validators.required]],
      monto : ['',[Validators.required]],
      modo : ['',[Validators.required]],
      numero : ['',[Validators.required]],
      fecha : ['',[Validators.required]],
      image_url : ['',[Validators.required]],
    });
  }

  private traerUniversidades(){
    this.universidadServicio.listaUniversidades().subscribe((resp:any[]) =>{
      this.universidades_lista = resp;  
      console.log("universidades ",resp);
    },(error)=>{
      console.info("error universidades: ",error);
    });
  }

  private traerProgramas(){

    this.programaServicio.listaProgramas().subscribe((resp:any) =>{

      this.programas_lista = resp; 
      console.log("programas ",resp);
    },(error)=>{
      console.info("error programas: ",error);
    });
  }

 private traerBancos(){
    this.bancoServicio.listaBancos().subscribe((resp:any)=>{
      this.bancos_lista = resp;
      console.log("bancos",resp);
    },(error)=>{
      console.log("error bancos: ",error);
    });
 }

  closeDialog(): void {
    this.dialogRef.close();
  }

  enviarFormulario() {
    console.log(this.pagoForm.value);
  }

}
