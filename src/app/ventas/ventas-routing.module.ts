import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AprobadosComponent } from './aprobados/aprobados.component';
import { EquipoComponent } from './equipo/equipo.component';
import { PendienteDetalleComponent } from './pendiente-detalle/pendiente-detalle.component';
import { SaldosComponent } from './saldos/saldos.component';
import { VentasComponent } from './ventas/ventas.component';
import { DetalleEquipoComponent } from './../ventas/equipo/detalle-equipo/detalle-equipo.component'

const routes: Routes = [
  {
    path : 'ventas',
    component : VentasComponent,
  },
  {
    path : 'detalle',
    component : PendienteDetalleComponent,
  },
  {
    path : 'confirmados',
    component : AprobadosComponent,
  },
  {
    path : 'saldos',
    component : SaldosComponent,
  },
  {
    path : 'equipo',
    component : EquipoComponent,
  },
  {
    path:'detalle-equipo',
    component : DetalleEquipoComponent
  }
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VentasRoutingModule { }
