import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

import * as Global from '../../globals';
import { Utils } from '../../utils';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(public http : HttpClient) { }

  dashboardCajaInfo(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Dashboard/resumenByCaja', body, requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  
  realizarBusqueda(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Dashboard/resumenByCajaFecha', body, requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  realizarBusqueda2(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Dashboard/resumenByCajaFecha2', body, requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  realizarBusquedaAnio(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Dashboard/resumenByCajaAnio', body, requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
  listarAnios(){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.apiTransvial+ 'Anio/listar', requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }
}
