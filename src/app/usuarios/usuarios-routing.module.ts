import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { DetailUsuariosComponent } from './detail-usuarios/detail-usuarios.component'
const routes: Routes = [
  {
    path : '',
    component : UsuariosComponent,
  }, 
  {
    path : 'detalle',
    component : DetailUsuariosComponent,
  }, 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuariosRoutingModule { }
