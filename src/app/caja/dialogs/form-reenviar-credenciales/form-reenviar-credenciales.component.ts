import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgxSpinner } from 'ngx-spinner/lib/ngx-spinner.enum';
import { AlertaService } from 'src/app/shared/services/general/alerta.service';
import { UtlitariosService } from 'src/app/shared/services/general/utlitarios.service';
import { LoadingService } from 'src/app/shared/services/loading.service';

@Component({
  selector: 'app-form-reenviar-credenciales',
  templateUrl: './form-reenviar-credenciales.component.html',
  styleUrls: ['./form-reenviar-credenciales.component.sass']
})
export class FormReenviarCredencialesComponent implements OnInit {

  credencialesForm:FormGroup;
  usuario:any;
  constructor(private dialogRef: MatDialogRef<FormReenviarCredencialesComponent>,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private utlitariosService:UtlitariosService,
    private router:Router,
    private alertaServicio:AlertaService,
    private loading:LoadingService,
    private spinner:NgxSpinnerService
    ) { 
      console.log(data)
      this.usuario = data;
    }

  ngOnInit(): void {
    this.crearFormulario();
    this.f.usuario_correo.setValue(this.data.usuario_correo);
    this.f.programa_nombre.setValue(this.data.programa_nombre);
    this.f.curso_nombre.setValue(this.data.curso_nombre);
    this.f.usu_nombre.setValue(this.data.usu_nombre);
    this.f.usu_clave.setValue(this.data.usu_clave);
    this.f.alumno_nombre.setValue(this.data.alumno_nombre);
    this.f.usu_nombre.setValue(this.data.usu_nombre);
    this.f.usu_nombre.setValue(this.data.usu_nombre);
  }
  private crearFormulario(){    
    this.credencialesForm = this.fb.group({
      usu_nombre: ['',[Validators.required]],
      usu_clave: ['',[Validators.required]],
      usuario_correo: ['',[Validators.required]],
      alumno_nombre: ['',[Validators.required]],
      programa_nombre: ['',[Validators.required]],
      curso_nombre: ['',[Validators.required]],
    });
  }
  private get f(){
    return this.credencialesForm.controls;
  }

  reenviarCredenciales(){
    this.spinner.show();
    this.loading.loading$.next({message:'', opacity:0.5});
    let salida = {
      usuario_nombre: this.data.usuario_nombre,
      usuario_clave: this.data.usuario_clave,
      usuario_correo: this.f.usuario_correo.value,
      pagoID: this.data.pagoID,
      usuarioID:Number(this.data.usuarioID),
      alumno_nombre: this.data.alumno_nombre,
      programa_nombre: this.data.programa_nombre,
      alumno_dni: this.data.alumno_dni
    }
    console.log(salida);
    this.utlitariosService.matriculaByCaja(JSON.stringify(salida)).subscribe(resp=>{
      console.log(resp)
      if(resp['status']=='OK'){        
        if(resp['enviado']=='OK'){
          this.dialogRef.close({estado : 'EXITO'})
        }else{
          this.dialogRef.close({estado : 'REINTENTAR'});  
        }         
      }else{
        this.alertaServicio.errorEnviarEmail();
        this.dialogRef.close({estado : 'FALLO'});
      }
      this.spinner.hide();
    },(error)=>{
      console.log(error);
      this.alertaServicio.algoHaIdoMal();
      this.dialogRef.close({estado : 'FALLO'});
      this.spinner.hide();
    })
  }
  closeDialog():void{
    this.dialogRef.close();
  }
}
