export const factor500_envios = 'https://factor500.simplex-erp.com/Utils/Envios/';
export const inboundUtil = 'https://factor500.simplex-erp.com/Utils/Inbound/';
export const inboundBase = 'https://factor500.simplex-erp.com/Inbound/';
export const factor_extras = 'https://factor500.simplex-erp.com/F500/';

export const envios_secas = 'https://factor500.simplex-erp.com/Envios/';
export const raiz_parking = 'https://aparking.simplex-erp.com/Utils/Parking/';

//https://inbound1.simplex-erp.com/Parking/

//ruta de parkeo

export const parking = 'https://inbound1.simplex-erp.com/Parking/';
export const utils_parking = 'https://inbound1.simplex-erp.com/Utils/Parking/'; //va a cambiar

export const ruta_base_infox = 'https://factor500.simplex-erp.com/Infox/Api/';
export const goberna_utils_url = 'https://factor500.simplex-erp.com/Utils/Masivos/';

export const ruta_infox = 'https://factor500.simplex-erp.com/Infox/';
export const api_mensajes = "https://apis.simplex-erp.com/ecommerce/api-menbiz/v4/"
export const ruta_base_info = 'https://apis.simplex-erp.com/ecommerce';
export const infox_version = '/infox/v1/';
export const api_infox = ruta_base_info + infox_version;
export const apiTransvial ='https://api.transvial.simplex-erp.com/Transvial/'

export const meses = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
export const dias = ['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sábado',];

export const deviceWpp = "62b9b38adf4b5d482d7b7a34"
export const tokenTestWpp = "9ae68be44ceb5bbe4b371de59e507e451689abe669888680d38587940e2eb83e32806aefe4bcb24b"
export const tokenTestEmail = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI0IiwianRpIjoiMTMwZGY3NDJhMWU0MTVmMTkwYTg0NjdlNWRjM2MzYzE1ODEwYWU0ZmM3ZWE1YTcwNjU5NWUzMWQzNmJlNzA5OTYyNTYyOThlZjBiNGJiZTciLCJpYXQiOjE2Njg4MzI1NzQuMjM5NDU2LCJuYmYiOjE2Njg4MzI1NzQuMjM5NDU4LCJleHAiOjQ4MjQ1MDYxNzQuMjMzNjk0LCJzdWIiOiIxNDcyNCIsInNjb3BlcyI6W119.gOAa0AWPrdo1fiGT9LcXhb49rsuv9F5RTeJS6wV9DOEFUlolO7oPrXewbtFtu7nOK52ivLCxmoTHMqsem_gzkxCI8oDqj3eKyVEtv0YEG7PbnEXmUHq279MYbrsfqbno7M9LXE3avKEOb5LEqU7meSs_orPcHyfzBN3le2m6E9KH3ENvkEOqrwMNCIYB8Yau8qo6ObYDds0fw91DpQJgmKO8uwWkrUTJFfxlCjhKiEesv38vncBzO-Q4tA7NDf9LupcJi6yeSTCW_2eJclfGbnHAtGg6hR32mZw87gME4cKCFXrHIy43GaYk9dI0REr_Wh-_oQ3QZ6OCm-IsDr4PqmizJgEsleLhz3aWe1JhMRjtzXNI_ScerB-lURqRQVq1xkC3b8qSCW2wZTQyq1XNl6TSZDRcH7tmWTIp8l9D5w5uHjR6e_kdj00JR2kOm4dyyYJy0_YiBY7HmH3BdILbUs6ItPMMnMqpM5IWND0CAT4j8_ukFaLEfRahvV_4B64i4FYRlrCs7WIpm-0tFEIpgB4Dg4CTJAVQ7zXdqAPVjOMCq74iVHm6VU6wXYkpwgADQGaSjVPNDdvBUYPwLcxx-EZtHd9aoVvpThk2pbemzuxcwtDEDs1TtpkRXBJ6Itng1eMj3hn392TLgWVsW3kmwT3GD7nWR2gQci4vY66Z7uk"

export const api_email_test = "https://connect.mailerlite.com/"
export const extensiones = new Map([
    ["xlsx","EXCEL"],
    ["xls","EXCEL"],
    ["csv","EXCEL"],
    ["docx","WORD"],
    ["doc","WORD"],
    ["pptx","POWER"],
    ["ppt","POWER"],
    ["pdf","PDF"],
    ["zip","COMP"],
    ["rar","COMP"],
]);
export const imagenes_extensiones = new Map([
    ["EXCEL","/assets/images/icons/excel.svg"],
    ["WORD","/assets/images/icons/file-doc.svg"],
    ["POWER","/assets/images/icons/file-type-ppt.svg"],
    ["PDF","/assets/images/icons/file-type-pdf.svg"],
    ["COMP","/assets/images/icons/file-zip.svg"],
    ["DEFECTO","/assets/images/icons/file-type-text.svg"],
]);

export const roles = new Map([
    ["200","Master"],
    ["000","Vendor"],
    ["100","Caja"],
    ["300","Embajador"],
]);
export const usu_tipo = '000'