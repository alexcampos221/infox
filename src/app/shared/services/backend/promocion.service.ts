import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

import * as Global from '../../globals';
import { Utils } from '../../utils';

@Injectable({
  providedIn: 'root'
})
export class PromocionService {

  constructor(public http : HttpClient) { }

  listaPromociones(){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Promocion/listar', Utils.jsonToFormDataV2(false), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

  editarPromocion(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Promocion/editar',Utils.jsonToFormDataV2(body,false), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

  cambiarEstado(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Promocion/cambiarEstado', Utils.jsonToFormDataV2(body,false), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

  guardarPromocion(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json'
      })
    };
    let request = this.http.post(Global.ruta_infox+ 'Promocion/guardar',Utils.jsonToFormDataV2(body,false), requestOptions).pipe(
      map((resp) => {
        return resp;
      })
    )
    return request;
  }

}
