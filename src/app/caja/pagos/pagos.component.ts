import { Component, OnInit, ElementRef,  ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup,Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { AlertaService } from 'src/app/shared/services/general/alerta.service';
import { UtlitariosService } from 'src/app/shared/services/general/utlitarios.service';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { DateAdapter } from '@angular/material/core';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-pagos',
  templateUrl: './pagos.component.html',
  styleUrls: ['./pagos.component.sass']
})
export class PagosComponent implements OnInit {

  codigo:any;
  usuario: any;
  ListaPagoPendientes_totales: any[] = [];
  promociones_filtrados: any[] = [];

  DSlistaPagosPendientes: MatTableDataSource<any>;

  //atributos para la tabla material
  @ViewChild(MatPaginator, { static: true }) paginatorCaja: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;


  displayedColumns = [
    'infoPago',
    'fecha',
    'banco',
    'monto',
    'url',
    'nombre',
    'dni',
    'telefono',
    'programa',
    'curso',
    'tipoMatricula',
    'acciones'
  ];

  promociones_procesados: any[] = [];

  filtro_control: FormControl;
  filtro$: Subscription;

  formdata : FormGroup;

  constructor(private fb : FormBuilder,
    public dialog: MatDialog,
    private alertaServicio : AlertaService,
    private ruteador : Router,
    private utilitarios: UtlitariosService,
    private rutaActiva: ActivatedRoute,
    private loadingService: LoadingService,
    private spinner: NgxSpinnerService,
    private dateAdapter: DateAdapter<Date>,
    ) {
      this.dateAdapter.setLocale('es-PE');
     }

  private crearFormulario(){
    this.formdata = this.fb.group({
      id:['',[Validators.required]],
      fecha:['',[Validators.required]],
      nombre:['',[Validators.required],],
      banco:['',[Validators.required],],
      url:['',[Validators.required]],
      monto:['',[Validators.required]],
      dni: ['',[Validators.required]],
      telefono: ['',[Validators.required]],
    });
  }
  txtFiltro:string;
  txtNuevo:string;
  ngOnInit(): void {
    this.obtenerAsesor();
    this.crearFormulario();
    this.DSlistaPagosPendientes = new MatTableDataSource();
    this.traerListaPagoPendientes();
    if(JSON.parse(localStorage.getItem('cerrar_detalle')) == true){

      this.txtFiltro = JSON.stringify(localStorage.getItem("txtFiltro"));
      var reg= /"/gi
      this.txtNuevo = this.txtFiltro.replace(reg,'');
      this.applyFilter(this.txtNuevo);

    }else{
      console.log('else')
    }
  }
  private obtenerAsesor(){
    this.usuario = JSON.parse(localStorage.getItem('usu_actual'));
    this.codigo=this.usuario.usu_id;
    // let salida = {
    //   code : this.codigo
    // };
    // let body = JSON.stringify(salida);

    // this.utilitarios.getVendedorAct(body).subscribe((resp:any)=>{
    //   console.log(resp);
    // });
  }
  traerListaPagoPendientes(){
    let salida = {
      usuarioID : this.codigo,
      estado: 'Y'
    };
    let body = JSON.stringify(salida);
    this.loadingService.loading$.next({message:'', opacity:0.5});
    this.spinner.show();
    this.utilitarios.getListaPagosPendienteByCaja(body).subscribe((resp:any)=>{
      this.ListaPagoPendientes_totales=resp;
      this.DSlistaPagosPendientes.data = this.ListaPagoPendientes_totales;
      console.log(this.DSlistaPagosPendientes.data);
      this.spinner.hide();
    },(error)=>{
      this.spinner.hide();
    });
  }
  applyFilter(filterValue: string){
    this.DSlistaPagosPendientes.filter = filterValue.trim().toLowerCase();
  }

  ngAfterViewInit(): void {
    this.paginatorCaja._intl.itemsPerPageLabel = "Registros por página";
    this.DSlistaPagosPendientes.paginator = this.paginatorCaja;
    this.DSlistaPagosPendientes.sort = this.sort;
  }
  ruta_imagen:any;
  nuevoPago(){}
  verImageVoucher(row){
    console.log(row)
    this.spinner.show();
    this.loadingService.loading$.next({message:'', opacity:0.5});
    if(row.pag_url != ''){
      this.ruta_imagen=row.pag_url;
      // this.alertaServicio.MostrarImagenVoucher(row.pag_url);
    }else{
      this.spinner.hide()
    }
    this.spinner.hide()

  }
  eliminarPendiente(row){
    console.log(row)
    let recordID;
    let isCuota;
    if(row.pag_tipo == "CUOTA"){
      recordID = row.cuota_id
      isCuota = 'Y'
    }else{
      recordID = row.pag_id
      isCuota = 'N'
    }
    let body ={
      usuarioID: Number(this.codigo),
      recordID: Number(recordID),
      isCuota: isCuota,
      estadoNuevo: 'N',
    }
    console.log(body)

    Swal.fire({
      title: `Estas seguro de enviar ah anulados ${row.pag_nombre}?`,
      text: `Monto de pago s/. ${row.pag_monto}`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: '¡Sí!'
    }).then(result  => {
      if(result.value){
        Swal.fire('¡Eliminado! ', 'el pago pendiente ha sido enviado a anulados.', 'success');
          this.loadingService.loading$.next({opacity:0.5});
          this.spinner.show()
          this.utilitarios.eliminarPendiente(JSON.stringify(body)).subscribe(resp => {
          console.log(resp)
          if(resp['status'] == 'OK'){
            this.traerListaPagoPendientes();
          }else{
            this.alertaServicio.errorInterno();
          }
          this.spinner.hide()
        },(error) => {
          console.log(error)
          this.spinner.hide()
        });
      }
    });
  }
  arrayPagosID: any[] = [];
  detallePendiente(valor,index){
    // console.log(valor)
    if(this.DSlistaPagosPendientes.filter == ''){
      for(let index = 0; index < this.ListaPagoPendientes_totales.length; index++) {
        this.arrayPagosID.push(this.ListaPagoPendientes_totales[index].pag_id);
      }
    }else{
      for(let index = 0; index < this.DSlistaPagosPendientes.filteredData.length; index++) {
        this.arrayPagosID.push(this.DSlistaPagosPendientes.filteredData[index].pag_id);
      }
    }

    let salida = {
      pagoID:valor.pag_id,
      ventana: 'Pendientes'
    }
    localStorage.setItem('pago',valor.pag_id);
    localStorage.setItem('indice',index);
    localStorage.setItem('txtFiltro',this.DSlistaPagosPendientes.filter);

    localStorage.setItem('arrayListaPendiente',JSON.stringify(this.arrayPagosID));
    this.ruteador.navigate(['pago_caja/detalle'] ,{ state : {data: salida } });
  }

arreglo_temporal:any[];

sortData(sort: Sort){
  const data = this.ListaPagoPendientes_totales.slice();
  // console.log(data);
  this.arreglo_temporal = [];
  if (!sort.active || sort.direction === '') {
    this.arreglo_temporal = data;
    this.DSlistaPagosPendientes.data = this.arreglo_temporal;
    return;
  }

  this.arreglo_temporal = data.sort((a, b) => {
    const isAsc = sort.direction === 'asc';
    switch (sort.active) {
      case 'nombre': return this.compare(a.pag_nombre||'', b.pag_nombre||'', isAsc);
      case 'dni': return this.compare(a.pag_dni||'', b.pag_dni||'', isAsc);
      case 'telefono': return this.compare(a.pag_telefono||'', b.pag_telefono||'', isAsc);
      case 'programa': return this.compare(a.pro_nombre||0, b.pro_nombre||0, isAsc);
      case 'curso': return this.compare(a.cur_nombre||0, b.cur_nombre||0, isAsc);
      case 'banco': return this.compare(a.ban_nombre||'', b.ban_nombre||'', isAsc);
      case 'fecha': return this.compare(a.pag_fecha_abono_l||'',b.pag_fecha_abono_l||'', isAsc);
      case 'monto': return this.compare(Number(a.pag_monto)||0, Number(b.pag_monto)||0, isAsc);
      case 'tipoMatricula': return this.compare(Number(a.pag_tipo)||0, Number(b.pag_tipo)||0, isAsc);

      default: return 0;
    }
  });
  this.DSlistaPagosPendientes.data = this.arreglo_temporal;
}


private  compare(a: number | string , b: number | string , isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
actualizarLista(){
  console.log('actualizar');
  this.traerListaPagoPendientes();
}

}
